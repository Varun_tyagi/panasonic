import { StyleSheet } from 'react-native';
import Colors from '../helpers/Colors';

const styles = StyleSheet.create({
  lightTitle: {
    fontSize: 15,
    fontWeight: '700',
    marginLeft: 10,
    color: Colors.white,
  },
  textField: {
    fontSize: 15,
    fontWeight: 'normal',
  },
  fieldTitle: {
    fontSize: 16,
    fontWeight: '700',
    textAlign:'center',
    color: 'white',
  },
  fieldTitles: {
    fontSize: 16,
    fontWeight: '700',
    textAlign:'center',
    color: '#1976D2',
  },
  error: {
    fontSize: 14,
    color: Colors.red,
  },
});

export default styles;
