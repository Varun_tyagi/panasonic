import {AsyncStorage} from 'react-native';
import * as constants from './constants';

export const checkAuth = async ()=>{
  // alert('testing')
    var access='';
    var access_token = await AsyncStorage.getItem('access_token');
    var token_time = await AsyncStorage.getItem('token_time');
    if(access_token && parseInt(token_time) > (new Date().getTime())){
      access = access_token;
    }else{
       access = await getAuth();
    }
    
    return access;
}

export const getAuth = ()=>{
  var access = ''
   access= fetch(constants.base_url_auth+'token/authorize', {
          method: "POST",
          headers: {'Content-Type':'application/json'},
          body:JSON.stringify({
            "grant_type":"client_credentials",
            "client_id":"testclient",
            "client_secret":"testpass"
          })
        }).then(res => res.json())
        .then(async(result) => {
          if(Object.keys(result).length){
            access = result.access_token;
            await AsyncStorage.setItem('token_time',(new Date().getTime()+parseInt((parseInt(result.expires_in)-300)*1000))+'');
            await AsyncStorage.setItem('access_token',result.access_token);
            return access;
          }else{
            alert(result.message);
            // access = '';
            return ''; 
          }
          
        }).catch(error => ()=>{
        if(error.indexOf('Network')!=-1){
          alert('Please check your internet connection.');
        }else{
          alert('Error : '+error);
        }
      }); 
      return access;
}


export const getDate = (dt, sep=' ', showTime=false) => {
  // alert(dt);
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
  let time = showTime?dt.split(sep)[1]:'';
  dt = dt.split(sep)[0].split('-');
  return dt[2] + ' ' + months[parseInt(dt[1][0] == 0 ? dt[1][1] : dt[1])-1] + ' ' + dt[0]+' '+time;
}

