/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
/* eslint-disable import/no-unresolved */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
// eslint-disable-next-line import/first
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
// eslint-disable-next-line import/order
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';


class SideMenu extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
  }

  componentDidUpdate() {
    if (this.props.user === null) {
      this.props.navigation.navigate('Auth');
    }
    return null;
  }


  render() {
    return (
        // eslint-disable-next-line global-require
        <ImageBackground source={require('../../assets/ic_home/pinkBG.png')} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >
      <View style={{ flex: 1, backgroundColor: 'transparent' }} />
        </ImageBackground>
    );
  }
}

SideMenu.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

SideMenu.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
