/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/sort-comp */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './styles';
// import CustomListview from './CustomListview';
import CustomRow from './CustomRow';

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);
    this.getData =
     [{
       key: 1,
       title: 'Albert Einstein',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor \n  incididunt ut labore,\n Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore, Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://vivirtupasion.com/wp-content/uploads/2016/05/DANI_PERFILzoomCircle.png',
     },
     {
       key: 2,
       title: 'Isaac newton',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,\n sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png',
     },
     {
       key: 1,
       title: 'Albert Einstein',
       description: 'Lorem ipsum dolor sit amet,\n consectetur adipiscing elit,\n sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://vivirtupasion.com/wp-content/uploads/2016/05/DANI_PERFILzoomCircle.png',
     },
     {
       key: 2,
       title: 'Isaac newton',
       description: 'Lorem ipsum dolor sit amet, consectetur \n adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png',
     },
     {
       key: 1,
       title: 'Albert Einstein',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://vivirtupasion.com/wp-content/uploads/2016/05/DANI_PERFILzoomCircle.png',
     },
     {
       key: 2,
       title: 'Isaac newton',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png',
     },
     {
       key: 1,
       title: 'Albert Einstein',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://vivirtupasion.com/wp-content/uploads/2016/05/DANI_PERFILzoomCircle.png',
     },
     {
       key: 2,
       title: 'Isaac newton',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png',
     },
     {
       key: 1,
       title: 'Albert Einstein',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://vivirtupasion.com/wp-content/uploads/2016/05/DANI_PERFILzoomCircle.png',
     },
     {
       key: 2,
       title: 'Isaac newton',
       description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
       image_url: 'http://3.bp.blogspot.com/-jd5x3rFRLJc/VngrSWSHcjI/AAAAAAAAGJ4/ORPqZNDpQoY/s1600/Profile%2Bcircle.png',
     },
     ];
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  componentDidUpdate() {
    if (this.props.user === null) {
      this.props.navigation.navigate('Auth');
    }
    return null;
  }

  // eslint-disable-next-line class-methods-use-this
  onPress(index, dataDict) {
    Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: Colors.groupTableViewBgColor }} >
        <Header
          containerStyle={{ backgroundColor: 'purple', height: '10%' }}
          backgroundColor={Colors.primary}

          leftComponent={
            {
              icon: 'arrow-back',
              color: '#fff',
              type: 'material',
              size: 30,
            onPress: () => { this.props.navigation.navigate('Main'); },

          }}
          leftContainerStyle={{ marginTop: -20 }}
          rightContainerStyle={{ marginTop: -20 }}
          centerContainerStyle={{ marginTop: -20 }}
          centerComponent={{ text: 'Product Listing', style: { color: '#fff', fontSize: 20 } }}
         // rightComponent={{ icon: 'notifications', color: '#fff' }}
        />


        <ScrollView>
        {
                this.getData.map((prod, ind) => (
                  <View style={styles.RowHeightcontainer} key={ind}>
              <Image source={{ uri: prod.image_url }} style={styles.photo} />
              <View style={styles.container_text}>
                  <Text style={styles.title}>
                      {prod.title}
                  </Text>
                  <Text style={styles.description}>
                      {prod.description}
                  </Text>
                  <Button
                    backgroundColor={Colors.gray}
                    onPress={() => {
                        this.onPress(ind, prod);
                        }}
                    title="Press"
                    color={Colors.red}
                    accessibilityLabel="Tap on Me"

                  />

              </View>

                  </View>

                ))
                }
        </ScrollView>


      </View>
    );
  }
}

ProductList.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ProductList.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
