
import { StyleSheet , Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';


let widthFull = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 40,
  },
  containers: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.primary,
    padding: 40,
  },

  tblcontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 10,
    height: 300,
    borderBottomColor: 'red',
    borderBottomWidth: 5,
    // height:'10%',
  },

  container_text: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 12,
    justifyContent: 'center',
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  photo: {
    height: 100,
    // width: widthFull ,
    width: 100,

  },
  RowHeightcontainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 10,
    height: 'auto',
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 5,
    borderLeftWidth: 5,
    borderLeftColor: '#EFEFF4',
    borderRightColor: '#EFEFF4',
    borderRightWidth: 5,
    flexDirection: 'row',
  },
});

export default styles;

