import React from 'react';
import {ActivityIndicator, StyleSheet, View, Text} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { SafeAreaView } from 'react-navigation';

export default class Loader extends React.Component{
    render(){
        return(
          // <View style={{flex:1, justifyContent:'center', alignItems:'center' }}>
             <View style={{...styles.loaderStyle,}}>
              <ActivityIndicator size={40} color="purple" />
              {/* <Text style={styles.loaderText}>Loading...</Text> */}
            </View> 
            
        );
    }
}

const styles = StyleSheet.create({
    loaderStyle:{
        flex:1,
        // backgroundColor:'rgba(128,0,128,0.3)',
        position: 'absolute',
        justifyContent:'center',
        alignItems:'center',
        
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        zIndex:20,
      },
      loaderText:{
        color:'purple',
        fontSize:25,
        fontWeight:'bold'
      }
})