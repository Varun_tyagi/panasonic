
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
// import styles from './Styles';
import TNC from '../TNC';
import Swiper from 'react-native-swiper';
import AmcExtended from '../AmcExtended/AmcExtended';


class AccessoriesData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      BannerdataSource:[],
     };
  //   this.onPress = this.onPress.bind(this);

  // // eslint-disable-next-line react/sort-comp
  // this.amcCategoryArray = [{
  //   catrgoryURl:'',
  //   categoryNAme:'Home Appliances',
  // },{
  //   catrgoryURl:'',
  //   categoryNAme:'Air Conditioners',
  // },{
  //   catrgoryURl:'',
  //   categoryNAme:'Home Entertainment',
  // },{
  //   catrgoryURl:'',
  //   categoryNAme:'Mobile Phones',
  // },{
  //   catrgoryURl:'',
  //   categoryNAme:'Beauty Care',
  // },];

  // this.amcProductListArray = [{
  //   sectionName:'DIRECT COOL',
  //   productList:[{
  //     yearPrice:1025,
  //       yearDesc:'',
  //       year:1,
  //   },{
  //     yearPrice:1641,
  //       yearDesc:'',
  //       year:2,

  //   },{
  //     yearPrice:2667,
  //       yearDesc:'',
  //       year:3,

  //      }
  //   ],
  // },
  // {
  //   sectionName:'500 LITRE & ABOVE',
  //   productList:[{
  //     yearPrice:3590,
  //       yearDesc:'',
  //       year:1,

  //   },{
  //     yearPrice:5745,
  //       yearDesc:'', 
  //                year:2,

  //   },{
  //     yearPrice:9234,
  //       yearDesc:'',
  //       year:3,

  //      }
  //   ],
  // },
  // {
  //   sectionName:'BELOW 500 LITRE',
  //   productList:[{
  //       yearPrice:3077,
  //     yearDesc:'',
  //     yearImage:'oneYear',
  //     year:1,

  //   },{
  //     yearPrice:5129,
  //     yearDesc:'',
  //     yearImage:'twoYear',
  //     year:2,

  //   },{
  //     yearPrice:7900,
  //     yearDesc:'',
  //     yearImage:'threeYear',
  //     year:3,

  //      }
  //   ],
  // },
  // ];
    
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };


  // onPress(index, dataDict) {
  //   Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
  // }
  // onPressTnc(index,prodCatDict){
  //   this.props.navigation.navigate('TNC', { user: prodCatDict, index:index });

  // }
  // onPressCell(index,prodCatDict){
  //   this.props.navigation.navigate('AmcExtended', { user: prodCatDict, index:index });

  // }

  // componentDidMount(){
  //   // progressOn();
  //   fetch("http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/getaccessories?customer_id=1",
  //   {headers: { "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==", 'Content-Type':'application/json'}})
    
  //   .then(response => response.json())
  //   .then((responseJson)=> {
  //     // BannerdataSource=responseJson.accessories
  //     // alert(BannerdataSource)
  //     // alert(JSON.stringify(responseJson.accessories.product_group))
  //     // progressOff();
  //     //alert(JSON.stringify(responseJson.accessories.product_group))
  //     this.setState({
  //       loading: false,
  //       BannerdataSourcess: responseJson.accessories,
        
  //      })
       
  //     // this.setState({
  //     //  loading: false,
  //     //  BannerdataSource: responseJson.accessories,
  //     //  amcCategoryArray:responseJson.accessories
       
      
  //     // //  MyApplianceData: responseJson.customer_products,
  //     // //  MyAccessoriesData:responseJson.accessories,
  //     // })
  //   })
  //   .catch(error=>console.log(error)) //to catch the errors if any
  //   }

  componentDidMount(){
    // progressOn();
    fetch("http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/getaccessories?customer_id=1",
    {headers: { "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==", 'Content-Type':'application/json'}})
    
    .then(response => response.json())
    .then((responseJson)=> {
      // progressOff();
      // alert(JSON.stringify(responseJson))
      this.setState({
       loading: false,
       BannerdataSource: responseJson.accessories,
      //  MyApplianceData: responseJson.customer_products,
      //  MyAccessoriesData:responseJson.accessories,
      })
    })
    .catch(error=>console.log(error)) //to catch the errors if any
    }

  render() {
    return (
      <ImageBackground source={require('../../assets/ic_home/amcExtendBg.png')} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >


    <View style={{ flex: 1, backgroundColor: 'transparent' }} >
      <Header
        containerStyle={{ backgroundColor: 'transparent', height: '10%' }}
        backgroundColor={Colors.primary}
        leftContainerStyle={{ marginTop: -20 }}
        rightContainerStyle={{ marginTop: -20 }}
        centerContainerStyle={{ marginTop: -20 }}
        centerComponent={{ text: 'Product Accessories', style: { color: '#fff', fontSize: 20 } }}
        leftComponent={
            {
              icon: 'arrow-back',
              color: '#FFFF',
              type: 'material',
              size: 30,
            onPress: () => { this.props.navigation.goBack() },

          }}

      />

      <ScrollView  bounces={false} showsHorizontalScrollIndicator={false} style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, paddingRight: 15,height:110,marginTop:30,}}horizontal={true}> 
     
       <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 0, paddingRight: 0,height:110,}}  >
           {
            
             this.state.BannerdataSource.map((item, i) => (
            
                <View style={{  flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection:'column', width:120,borderRightWidth:10, borderRightColor:'transparent', height:140, }}>
                 {/* Alert.alert(this.state.BannerdataSource) */}
                <Image source={require('../../assets/ic_home/ac.png')} style={{width:70, height:70, resizeMode:'contain'}}  />
                <Text  numberOfLines={2} style={{colors:'#ffffff',alignContent:'center',padding:10,height:70}}>
                    {item.product_group}</Text>
                </View>
              ))
              }

        </View>
      </ScrollView>


      {/* const numColumns = 3;
const size = Dimensions.get('window').width/numColumns;
const styles = StyleSheet.create({
  itemContainer: {
    width: size,
    height: size,
  },
  item: {
    flex: 1,
    margin: 3,
    backgroundColor: 'lightblue',
  }
}); */}


  

           {/* TNC END HERE */}
   </View>

   
      </ImageBackground>
  );
  }
}

AccessoriesData.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

AccessoriesData.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AccessoriesData);
