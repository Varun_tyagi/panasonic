/* eslint-disable react/sort-comp */
/* eslint-disable no-underscore-dangle */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
/* eslint-disable import/no-unresolved */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  AsyncStorage,
// eslint-disable-next-line import/first
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import * as constants from '../../helpers/constants';
import styles from './Styles';
import Swiper from 'react-native-swiper';
import ApplianceDetail from '../ApplianceDetail';
import LinearGradient from 'react-native-linear-gradient';
import * as authFun from '../../helpers/auth';
import Loader from '../common/Loader';
import NavigationService from '../navigation/NavigationService';
import {createNavigator,createNavigationContainer,StackRouter,addNavigationHelpers, StackActions,} from 'react-navigation';


// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
const widthFull = Dimensions.get('window').width;
const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};

class MyAppliance extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state={
      loading:true,
      token:'',
      customerID:'',
      customerProducts:[],
      customerProductsAmcExpired:[]
    }
    this.capitalize = this.capitalize.bind(this);
    this.getDate = this.getDate.bind(this);
    this.productAdded = this.productAdded.bind(this);
   }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  productAdded = async ()=>{
    const result = await AsyncStorage.getItem('productAdded');
    if(result==1){
      await AsyncStorage.removeItem('productAdded');
      return true;
    }else{
    return false;
    }
  }
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
      },()=>{
        this.getData();
      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }


 getData = async ()=>{
    // https://www.ecarewiz.com/ewarrantyapi/getApplianceData?customer_id=1
    await this.setToken();
    this.setState({loading:true});
        var location = 'getApplianceData?customer_id='+this.state.customerID
        var url = constants.base_url+location;
        // var query = id!=0?name=='state'?'&country_id='+'101':name=='city'?'&state_id='+id:'&city_id='+id:'';
        url=url+"&access_token="+this.state.token;
        fetch(url,{headers: {'Content-Type':'application/json'}})
            .then(response => response.json())
            .then((responseJson)=> {
                if(responseJson.status){
                    this.setState({loading:false, customerProducts:responseJson.customer_products.filter((item)=>parseInt(item.amc_days)>30),
                    customerProductsAmcExpired:responseJson.customer_products.filter((item)=>parseInt(item.amc_days)<=30)
                  });
                }
                this.setState({loading:false})
            
            }).catch((error) =>{
              NetInfo.fetch().then(async (state)=>{
                if(!state.isConnected){
                  alert('Please check your internet connection.');
                }else{
                  alert(error);
                }
              });
              this.setState({loading:false})
            })  
  }
  
  async componentDidMount(){
      this.loadData();
    this.props.navigation.addListener('willFocus', async ()=>{
          this.loadData();
    });     
   }

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    this.forceUpdate();
  };
  
  capitalize=(txt)=>{
    if(txt!=null)
    {
      return txt.split(' ').map((word)=>word.split('')[0].toUpperCase()+word.split('').splice(1,).join('')).join(' ');
    }else{
      return '';
    }
    
  }
  getDate = (dt)=>{
    const months =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')
    
    return dt[2]+' '+months[dt[1][0]==0?dt[1][1]:dt[1]]+' '+dt[0];
  }

  render() {
    // console.warn(this.state.customerProducts[0])
    return (
      <View style={{flex:1, backgroundColor:"#e6e8e9",justifyContent:'flex-start'}} >
       <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
       {this.state.loading?<Loader/>:null}
       
       <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#e6e8e9'}}>
         <View style={{paddingTop:30,marginTop:StatusBar.currentHeight, flex:1,justifyContent:'flex-start', alignItems:'center',paddingBottom:15,  minHeight:this.state.customerProductsAmcExpired.length?365:120}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:this.state.customerProductsAmcExpired.length?-570:-680,
            height:800, 
            width:800,
            borderRadius:400,
            position:'absolute'
          }}
            >
          </LinearGradient>
             <View style={{flexDirection:'row', justifyContent:'flex-start',
              alignItems:'center', padding:5, paddingTop:0, marginTop:15}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1 }} >
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'white',fontFamily:'RubikMedium', fontSize:20, textAlign:'center', marginLeft:-30}}>MY APPLIANCES</Text>
            </View>
              { this.state.customerProductsAmcExpired.length?<Text style={{width:'100%',marginRight:15,  fontFamily:"RubikBold",alignSelf:'flex-start', fontSize:16,color:'white', marginHorizontal:10, marginTop:15}}>NEEDS YOUR ATTENTION</Text>:null}
            
              <View style={{ alignSelf:'stretch',flex:1,height:this.state.customerProductsAmcExpired.length?265:0 }}>
              {
                 this.state.customerProductsAmcExpired.length?
              <Swiper  showsButtons={false} loop={true} 
              dotStyle={{borderColor:'#a17df4', borderWidth:1,height:10, width:10,borderRadius:5, marginBottom:-70 }}
              activeDotStyle={{borderColor:'#a17df4',borderWidth:1,height:10,borderRadius:5, width:10,marginBottom:-70 }} 
              dotColor="transparent" activeDotColor="#9a79f2"
              containerStyle={{flex:1,justifyContent:'flex-start', alignItems:'stretch',height:'100%', alignSelf:'stretch', marginHorizontal:10 }}
              >
                    {
                     
                      this.state.customerProductsAmcExpired.map((customerProduct, ind)=>
                    <LinearGradient key={ind} colors={['#ff619e','#ff9fa6','#ff9fa6','#ff9fa6']} locations={[0.1,0.5,0.8,1]} 
                    angleCenter={{x:0,y:0}}  useAngle={true} angle={225} 
                    style={{flex:1, backgroundColor:'purple',  alignSelf:'stretch',borderRadius:12,elevation:8,margin:5,padding:10,justifyContent:'center',alignItems:'stretch'}}
                    >
                    <View style={{ flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start'}}>
                    <View >
                    <TouchableOpacity   onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})} style={{height:140, width:120,  borderRadius:10,backgroundColor:'#fff', borderWidth:2, borderColor:'#f0f0f0', }}>
                          <Image source={{uri:customerProduct.image}} style={{resizeMode:'cover', height:'100%', width:'100%'}}/>
                      </TouchableOpacity>
                      <TouchableOpacity   onPress={()=>this.props.navigation.navigate('AddAppliance', {update:customerProduct.id})} 
                      style={{flexDirection:'row', marginTop:10, alignSelf:'center', alignItems:'center', justifyContent:'space-between',}}>
                        <Image source={require('../../assets/ic_home/edit_light.png')} style={{height:10, width:10, resizeMode:'cover'}} /><Text style={{fontFamily:'RubikRegular', fontSize:12, color:'white', marginLeft:5}}>Edit Product</Text>
                      </TouchableOpacity>
                   </View>
                    <View style={{flex:1, marginLeft:15,}}>
                          <TouchableOpacity   onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})}>
                           {customerProduct.status!=3? <Text style={{color:'black', fontFamily:'RubikBold', fontSize:17}}>{customerProduct.amc_days?`${customerProduct.amc_days} Days Left`:`EWC Expired`}</Text>:null}
                            <Text style={{color:'white', fontFamily:'RubikBold', fontSize:15, marginTop:10,}}>{this.capitalize(customerProduct.product_name)}</Text>
                            <Text style={{color:'#fefefe', fontFamily:'RubikMedium', fontSize:16, marginTop:5}}>{this.capitalize(customerProduct.sub_product_name)}</Text>
                            <Text style={{color:'#f9e4e8', fontFamily:'RubikRegular', fontSize:15}}>Model No : {customerProduct.model_name}</Text>
                            <Text style={{color:'#f9e4e8', fontFamily:'RubikRegular', fontSize:15}}>Brand : {customerProduct.brand_name}</Text>
                            <Text style={{color:'#f9e4e8', fontFamily:'RubikRegular', fontSize:15}}>DOP : {authFun.getDate(customerProduct.purchase_date)}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, color:customerProduct.status==3?'red':'green'}}><Text style={{color:'#4f4f4f'}}>Status : </Text>{(customerProduct.status==3?'Pending':'Approved')}</Text>
                          </TouchableOpacity>
                          <TouchableOpacity   style={{flexDirection:'row',  flex:1, alignItems:'center',marginTop:5}} onPress={()=>NavigationService.navigate('AccessoriesNavigation', {product_group_id:customerProduct.product_group_id})}>
                            <Image source={require('../../assets/ic_home/basket_light.png')} style={{resizeMode:'contain', height:12, width:12}} />
                            <Text style={{color:'white', fontFamily:'RubikBold', fontSize:12, marginLeft:5}}>Related Accessories</Text>
                          </TouchableOpacity>
                        </View>
                    </View> 
                    <View style={{ flexDirection:'row', justifyContent:'center', alignItems:'center',marginTop:10}}>
                      <TouchableOpacity   onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:customerProduct})}}   
                      style={{ flex:1, flexDirection:'row', alignItems:'center',padding:8,justifyContent:'center',borderRadius:5, borderColor:'white', borderWidth:1, marginRight:10}}>
                          <Image  source={require('../../assets/ic_home/support_light.png')} style={{height:20,width:20,resizeMode:'contain'}} />
                          <Text style={{paddingLeft:10, color:'white',fontFamily:'RubikBold', fontSize:11}}>Request Service</Text>
                      </TouchableOpacity>
                      <TouchableOpacity   onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"ENQUIRY"})}
                 style={{flex:1, flexDirection:'row', alignItems:'center',justifyContent:'center', borderRadius:5, borderColor:'white', borderWidth:1, padding:8}}>
                          <Image  source={require('../../assets/enquirenow.png')}  style={{height:20,width:20, resizeMode:'contain', tintColor:'white'}} />
                          <Text style={{paddingLeft:10, color:'white',fontFamily:'RubikBold', fontSize:11}}>Enquire Now</Text>
                      </TouchableOpacity>
                    </View> 
                      </LinearGradient>)}
            </Swiper>
            :null
          }
              </View>
              
           
         </View>
          {/*products and filter button area  */}
        <View style={{flex:1, flexDirection:'row', justifyContent:'space-between',alignItems:'center', paddingHorizontal:15, 
        marginTop:this.state.customerProductsAmcExpired.length?0:20}}>
            <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#5960e5', flex:1}}>
              {this.state.customerProducts.length} {this.state.customerProducts.length==1 || this.state.customerProducts.length==0?'PRODUCT':'PRODUCTS'}
            </Text>
        </View>
          {/*products and filter button area  end*/}
          {
                   this.state.customerProducts.length?  
                this.state.customerProducts.map((customerProduct, ind)=>
              <View key={ind} style={{backgroundColor:'white',alignSelf:'stretch',borderRadius:5,elevation:10,margin:15,padding:10,justifyContent:'flex-start',}}>
               <View style={{  flexDirection:'row', justifyContent:'flex-start', alignItems:'flex-start'}}>
                 <View >
                 <TouchableOpacity  onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})} style={{height:140, width:120,  borderRadius:10,backgroundColor:'#fff', borderWidth:2, borderColor:'#f0f0f0',padding:5 }}>
                        <Image source={{uri:customerProduct.image}} style={{resizeMode:'cover', height:'100%', width:'100%'}}/>
                    </TouchableOpacity>
                    <TouchableOpacity   onPress={()=>this.props.navigation.navigate('AddAppliance', {update:customerProduct.id})} 
                      style={{flexDirection:'row', marginTop:10, alignSelf:'center', alignItems:'center', justifyContent:'space-between',}}>
                        <Image source={require('../../assets/ic_home/edit_light.png')} style={{height:10, width:10, resizeMode:'cover', tintColor:'black'}} /><Text style={{fontFamily:'RubikRegular', fontSize:12, color:'black', marginLeft:5}}>Edit Product</Text>
                      </TouchableOpacity>
                 </View>
                 
                  <View style={{height:'100%',flex:1, marginLeft:15}}>
                    <TouchableOpacity  onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})}>
                      <Text style={{color:'#3875c9', fontFamily:'RubikBold', fontSize:15, marginTop:10,}}>{customerProduct.product_name.toUpperCase()}</Text>
                      <Text style={{color:'#4f4f4f', fontFamily:'RubikMedium', fontSize:16, marginTop:5}}>{this.capitalize(customerProduct.sub_product_name)}</Text>
                      <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:15}}>Model No : {customerProduct.model_name}</Text>
                      <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:15}}>Brand : {customerProduct.brand_name}</Text>
                      <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:15}}>DOP : {authFun.getDate(customerProduct.purchase_date)}</Text>
                      <Text style={{fontFamily:'RubikRegular', fontSize:15, color:customerProduct.status==3?'red':'green'}}><Text style={{color:'#4f4f4f'}}>Status : </Text>{(customerProduct.status==3?'Pending':'Approved')}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{flexDirection:'row',  flex:1, alignItems:'center',paddingTop:10}} onPress={()=>NavigationService.navigate('AccessoriesNavigation', {product_group_id:customerProduct.product_group_id})}>
                      <Image source={require('../../assets/ic_home/basket_dark.png')} style={{resizeMode:'contain', height:12, width:12}} />
                      <Text style={{color:'black', fontFamily:'RubikBold', fontSize:12, marginLeft:5}}>Related Accessories</Text>
                    </TouchableOpacity>
                  </View>
              </View> 
              <View style={{ flexDirection:'row', justifyContent:'center', alignItems:'center',marginTop:10}}>
              <TouchableOpacity disabled={customerProduct.status==3} onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:customerProduct})}}  style={{ flex:1, flexDirection:'row', alignItems:'center',padding:8,justifyContent:'center',borderRadius:5, borderColor:'#c0c0c2', borderWidth:2, marginRight:10}}>
                    <Image  source={require('../../assets/ic_home/support_dark.png')} style={{height:20,width:20,resizeMode:'contain'}} />
                    <Text style={{paddingLeft:10, color:'#2c2c2e',fontFamily:'RubikBold',  fontSize:11}}>Request Service</Text>
                </TouchableOpacity>
                <TouchableOpacity disabled={customerProduct.status==3} onPress={()=>this.props.navigation.navigate('AMCNavigation')}
                 style={{flex:1, flexDirection:'row', alignItems:'center',justifyContent:'center', borderRadius:5, borderColor:'#c0c0c2', borderWidth:2, padding:8}}>
                    <Image  source={require('../../assets/ic_home/cart_dark.png')} style={{height:20,width:20, resizeMode:'contain'}} />
                    <Text style={{paddingLeft:10, color:'#2c2c2e',fontFamily:'RubikBold', fontSize:11}}>Buy AMC</Text>
                </TouchableOpacity>
              </View> 
                </View>)
              :
              <Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20, alignSelf:'center'}}>No data found.</Text>}

        </ScrollView>
        <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('AddAppliance');
              // this.setState({ showAddAppModal: true })
          }}
            style={{ height: 60, width: 60, borderRadius: 35, position: 'absolute', alignItems: 'center', justifyContent: 'center', bottom: 20, right: 20 }}>
            {/* <View style={{height:70, width:70, borderRadius:35, elevation:15,backgroundColor:'red'}}> */}
            <Image source={require('../../assets/ic_home/plus.png')} style={{ height: 80, width: 80 }} />
            {/* </View> */}
          </TouchableOpacity>
      </View>
    );
  }
}

MyAppliance.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

MyAppliance.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyAppliance);
