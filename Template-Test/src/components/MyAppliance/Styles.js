

// eslint-disable-next-line no-unused-vars
import { StyleSheet, Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';


const widthFull = Dimensions.get('window').width;
// let heightFull = Dimensions.get('window').height;

const styles = StyleSheet.create({

  scrollerStyle: {
    height: 200,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 40,
  },
  containers: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.primary,
    padding: 40,
  },

  tblcontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 10,
    height: 300,
    borderBottomColor: 'red',
    borderBottomWidth: 5,
    // height:'10%',
  },

  container_text: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 12,
    justifyContent: 'center',
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  photo: {
    height: 100,
    // width: widthFull ,
    width: 100,

  },
  photoLogo: {
    height: 30,
    // width: widthFull ,
    width: 50,

  },
  RowHeightcontainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: 'white',
    padding: 10,
    height: 180,
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 10,
    borderLeftWidth: 25,
    borderLeftColor: '#EFEFF4',
    borderRightColor: '#EFEFF4',
    borderRightWidth: 25,
    flexDirection: 'row',
  },
  ColumHeightcontainer: {
    flex: 1,
   // justifyContent: 'flex-start',
   // alignItems: 'center',
    color: 'blue',
    backgroundColor: 'transparent',
    padding: 0,
    height: 120,
    width: widthFull - 30,
    borderBottomColor: 'transparent',
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: 'transparent',
    borderRightColor: '#EFEFF4',
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderTopColor: 'transparent',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
});

export default styles;

