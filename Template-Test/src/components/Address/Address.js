import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  AsyncStorage,
  TextInput,
  ImageBackground,
  ActivityIndicator,
  KeyboardAvoidingView,
  PixelRatio,
  Picker,
  Alert,
  createFormData,
  Keyboard,
  Icon,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, Avatar } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextStyles from '../../helpers/TextStyles';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './styles';
import DatePicker from 'react-native-datepicker';
import Loader from '../common/Loader';
import LinearGradient from 'react-native-linear-gradient';

class Profile extends Component {
  static navigationOptions = {
    // title: strings.profile,
    header: null,

  };

  constructor(props) {
    super(props);
    this.state = {
      customerID: '',
      customerToken: '',
      fName: '',
      email: '',
      gender: '',
      mNum: '',
      aNum: '',
      occupation: '',
      birthday: '',
      anniversary: '',
      verifyANum:false,
      dataLoading: true,
      inputOnFocusStyle: { borderColor: '#5D61E6' },
      showDatePicker: false,
      avatarImageSource: 'https://reactnativeexample.com/content/images/2018/06/20180607093721.jpg',
      photo: undefined,
      filePath: '',
      showGenderPicker: false,
      profile_image:'',
      otp:['','','',''],
      showOtp:false,
      verifyOtp:false,
      oldAnum:'',
      aNumVer:false,
      maximumDate: new Date(),
      minimumDate: new Date(1920, 0, 1),
    };

    this.loadData = this.loadData.bind(this);
  }

  onFocus() {
    this.setState({
      borderColor: '#5D61E6',
      borderWidth: 1,
    });
  }

  endFocus() {
    this.setState({
      borderColor: 'transparent',
      borderWidth: 0,
    });
  }

  openPhotoPicker() {

  }

  componentDidUpdate() {

    // if (this.props.user === null) {
    //   this.props.navigation.navigate('Auth');
    // }
    // return null;
  }

  async componentDidMount() {
    this.loadData();
  }

  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      // alert('usere data : '+userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        customerToken: data.customer_token ? data.customer_token : '',
        fName: data.name ? data.name : this.state.fName,
        email: data.email ? data.email : this.state.email,
        gender: data.gender ? data.gender : this.state.gender,
        mNum: data.mobile ? data.mobile : this.state.mNum,
        aNum: data.alternate_mobile_no ? data.alternate_mobile_no : this.state.aNum,
        occupation: data.occupation ? data.occupation : this.state.occupation,
        birthday: data.dob.indexOf('0000') == -1 ? data.dob : this.state.birthday,
        anniversary: data.anniversary.indexOf('0000') == -1 ? data.anniversary : this.state.anniversary,
        dataLoading: false,
        profile_image:data.profile_image?data.profile_image:null,
      },()=>this.setState({oldAnum:this.state.aNum}));
    } else {
      this.setState({ dataLoading: false });
    }
  }

  sendOtp=()=>{
    this.setState({dataLoading:true});
    fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/send_otp', {
          method: "POST",
          headers: { "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==",
          'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.aNum})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:true,},()=>{
              this.setState({dataLoading:false, verifyANum:false})
            });
          }else{
            
            alert(result.message);
            this.setState({dataLoading:false, verifyANum:false})
          }
          
        },
        (error) => { alert(error);
          this.setState({dataLoading:false, verifyANum:false})
        }
      );
  }
  verifyOtp=()=>{
    this.setState({dataLoading:true});
    fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/verifySmsOtp', {
          method: "POST",
          headers: { "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==",
          'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.aNum,'otp':parseInt(this.state.otp.join(""))})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:false,aNumVer:true},()=>{
              this.setState({dataLoading:false})
            });
          }else{
            
            alert(result.message);
            this.setState({dataLoading:false})
          }
          
        },
        (error) => { alert(error);
          this.setState({dataLoading:false})
        }
      );
  }
  handleUploadPhoto = async () => {
    const data = new FormData();
    data.append('customer_id', 2);
    data.append('image', { uri: `file://${this.state.photo.path}`, type: 'image/*', name: this.state.photo.fileName });
    // console.log('\n\n\n\n\n\n  form data \n\n\n\n\n\n', data);
    fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/upload_customerimg', {
      method: 'POST',
      headers: {
        Authorization: 'Basic cGFuYXNvbmljOk5ldXJvQDEyMw==',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    })
      .then(response => response.json())
      .then((response) => {
        if (response.status) {
          console.log('\n\n\n\n\n\n upload succes \n\n\n\n\n\n', response);
          //Alert.alert(`Upload success!${JSON.stringify(response)}`);
        } else {
          console.log('\n\n\n\n\n\n upload succes \n\n\n\n\n\n', response);
          Alert.alert(`Upload failed error!${JSON.stringify(response)}`);
        }
      })
      .catch((error) => {
        console.log('\n\n\n\nupload error \n\n\n\n\n', error);
        NetInfo.fetch().then(async (state)=>{
          if(!state.isConnected){
            alert('Please check your internet connection.');
          }else{
            Alert.alert(`Upload failed.`);
          }
        })
        
        
      });
  };


  logout = () => this.props.logout();

  isFormValidated = () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.fName.trim() === '') {
      Alert.alert('Please enter name');
      return false;
    } else if (this.state.email.trim() === '') {
      Alert.alert('Please enter email');
      return false;
    } else if (reg.test(this.state.email) === false) {
      Alert.alert('Please enter valid email');
      return false;
    } else if (this.state.gender.trim() === '') {
      Alert.alert('Please select gender');
      return false;
    } else if (this.state.mNum.trim() === '') {
      Alert.alert('Please enter mobile number');
      return false;
    } else if (this.state.aNum.trim() === '') {
      Alert.alert('Please enter alternate mobile number');
      return false;
    } else if (this.state.aNum.length < 7) {
      Alert.alert('Please enter valid alternate number');
      return false;
    } else if (this.state.occupation.trim() === '') {
      Alert.alert('Please enter occupation');
      return false;
    } else if (this.state.birthday.trim() === '') {
      Alert.alert('Please select your birthday');
      return false;
    } else if (this.state.anniversary.trim() === '') {
      Alert.alert('Please select your anniversary');
      return false;
    }
    return true;
  };

  updateData= () => {
    if (this.isFormValidated() === true)
{
    this.setState({ dataLoading: true });

    fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/update_profile', {
      method: 'POST',
      headers: {
        Authorization: 'Basic cGFuYXNvbmljOk5ldXJvQDEyMw==',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        customer_id: this.state.customerID,
        name: this.state.fName,
        email: this.state.email,
        alternate_mobile_no: this.state.aNumVer?this.state.aNum:this.state.oldAnum,
        gender: this.state.gender,
        dob: this.state.birthday,
        anniversary: this.state.anniversary,
        occupation: this.state.occupation,
      }),
    }).then(res => res.json())
      .then(
        (result) => {
          if (result.status) {
            this.setState({ dataLoading: false });
            AsyncStorage.setItem('userData', JSON.stringify(result.data), () => {
              alert('Data Updated Successfully.');
              this.loadData();
            });
          } else {
            alert(result.message);
            this.setState({ dataLoading: false });
          }
        },
        (error) => { alert(error); },
      );
    }
  }

  getDob(date) {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${day < 10 ? `0${day}` : day}/${month < 10 ? `0${month}` : month}/${year}`;
  }

  // Image Picker
  chooseFile = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [
      //   { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      // ],
      mediaType: 'photo',
      noData: true,
      storageOptions: {
        skipBackup: true,
        // path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      // this.state.avatarImageSource = response;
      console.log(`\n\n\n\n\nImage Response\n\n\n\n${response.uri}---${response.origURL}---${response.path}---${response.Image}---\n\n\n\n\n\n`);
      // alert(response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = response;
        this.setState({ photo: source }, () => {
          console.log(`\n\n\n\n\n\n\n\n\nhello source${JSON.stringify(source)}\n\n\n\n\n\n\n`);
          this.handleUploadPhoto();
        });
        this.setState({
          filePath: source,

        });
      }
    });
  };

  // Image Picker end
  changeTextNameField(inputText, field) {
    const specialCharReg = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const numReg = /^[0-9]*$/;
    const nameReg=/^[a-zA-Z\s ]+$/;
    const numEntry = /[0-9]*/;
    if (field === 'fName' && nameReg.test(inputText)) {
      this.setState({ fName: inputText });
    }
    if(field==='anum' && numReg.test(inputText)){
      this.setState({ aNum: inputText },()=>{
        if(this.state.aNum.length==10){
          this.setState({verifyANum:true})
        }else{
          this.setState({verifyANum:false, showOtp:false, otp:['','','','']})
        }
      });
    }
  }
  _renderGenderPicker() {
    if (this.state.showGenderPicker) {
        return (
          <View
            style={{
                  backgroundColor: 'white',
                  borderRadius: 20,
                  textAlign: 'left',
                  fontSize: 20,
                  shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,
                  marginBottom: 15,
                  }}
                >

          <Picker
            selectedValue={this.state.gender}
            onValueChange={(itemValue, itemIndex) =>
            this.setState({ gender: itemValue, showGenderPicker: false })
            }
          >
            <Picker.Item label="Male" value="Male" />
            <Picker.Item label="Female" value="Female" />
          </Picker>
        </View>
        );
    } 
        return null; 
    
}
otpChanged = (value,id,callBack=undefined) => {
  var temp = this.state.otp;
  const testExp = /^[0-9]+$/;
  if(testExp.test(value) || value==''){
    temp[id]=value
    this.setState({ otp: temp },()=>{
      if(this.state.otp[0] && this.state.otp[1] && this.state.otp[2] && this.state.otp[3]){
        this.setState({verifyOtp:true});
      }else{
        this.setState({verifyOtp:false});
      }
      if(callBack){
        callBack();
    }
  });
  }
}

  render() {
    return (        
        <View style={{ flex: 1 ,backgroundColor:'#e6e8e9' }}>
          {this.state.dataLoading?<Loader/>:null}
        <ScrollView>
        <View style={{height:210,}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:-340,
            height:500, 
            width:500,
            borderRadius:230,
            position:'absolute'
          }}
            >
          </LinearGradient>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10}}>
            
              <TouchableOpacity>
                <Image source={require('../../assets/ic_home/align-left.png')} style={{height:20,width:20,}}/>
              </TouchableOpacity>
              <Text style={{fontSize:15,color:'#e9eaff',fontFamily:'RubikMedium'}}>PERSONAL INFORMATION</Text>     
              <TouchableOpacity>
                <Image  style={{height:20,width:20,}}/>
              </TouchableOpacity>
            </View>

               {/* Header of profile page */}
          <View style={{ backgroundColor: 'transparent',height:150 , marginTop:70 }}>
    
            <Avatar
            containerStyle={{ alignSelf: 'center' }}
            size="large"
            rounded
            activeOpacity={0.8}
            source={{uri:this.state.photo?`file://${this.state.photo.path}`:this.state.profile_image?this.state.profile_image:'https://img.icons8.com/clouds/100/000000/user.png'}}
            showEditButton
            onEditPress={this.chooseFile.bind(this)}
           />
          </View>
          
         </View>

            {/* Containing Input */}
            <KeyboardAvoidingView style={{ ...styles.inputContainer }}>
              {/* <KeyboardAvoidingView> */}
              <TextInput
                style={{ ...styles.inputBox, borderColor: this.state.borderColor, borderWidth: this.state.borderWidth }}
                maxLength={50}
                contextMenuHidden
                placeholder="Full Name"
                onChangeText={value => this.changeTextNameField(value, 'fName')}
                value={this.state.fName}
                ref={input => this.fName = input}
              />
              <TextInput
                style={styles.inputBox}
                keyboardType="Email ID*"
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Email ID*"
                onChangeText={value => this.setState({ email: value })}
                value={this.state.email}
                ref={input => this.email = input}
              />

                <View
                  style={{
                        backgroundColor: 'white',
                        borderRadius: 30,
                        textAlign: 'left',
                        fontSize: 15,
                        shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,
                        color: '#707171',
                        marginBottom: 15,
                      }}
                  >

                <Picker
                style={{color:'#707171', textAlign:'left', fontSize:15, padding:10,paddingHorizontal: 10,}}
                  selectedValue={this.state.gender}
                  onValueChange={(itemValue, itemIndex) =>
                  this.setState({ gender: itemValue, showGenderPicker: false })
                  }
                 >
                  <Picker.Item label="Gender*" value="one" />
                  <Picker.Item label="Male" value="Male" />
                  <Picker.Item label="Female" value="Female" />
                </Picker>
              </View>

              {/* {this._renderGenderPicker()} */}

              <TextInput
                style={styles.inputBox}
                maxLength={10}
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Mobile Number*"
                keyboardType="phone-pad"
                //  onEndEditing={()=>this.alterNum.focus()}
                onChangeText={value => this.setState({ mNum: value })}
                value={this.state.mNum}
                ref={input => this.mobile = input}
                editable={false}
              />
              <View style={{ paddingBottom:5,backgroundColor:this.state.showOtp?'rgba(0,0,0,0.1)':'transparent', borderRadius:20}}>
              <TextInput
                style={styles.inputBox}
                maxLength={10}
                secureTextEntry={false}
                keyboardType="phone-pad"
                contextMenuHidden
                placeholder="Alternate Number"
                // onEndEditing={this.sendOtp}
                // onChangeText={value => this.setState({ aNum: value })}
                onChangeText={value => this.changeTextNameField(value, 'anum')}
                value={this.state.aNum}
                ref={input => this.alterNum = input}
                
              />
              {this.state.aNumVer?<Text style={TextStyles.fieldTitleNote}>Number Verified Successfully.</Text>:null}
              {this.state.verifyANum?<TouchableOpacity
              onPress={this.sendOtp}
              style={styles.getOtp}
              ><Text style={{fontSize:16, color:'purple'}}>Verify Number</Text></TouchableOpacity>:null}
              {this.state.verifyANum?<Text style={TextStyles.fieldTitleNote}>Note: Only verified number will get saved.</Text>:null}
              {this.state.showOtp?
            <View style={{flex:1, flexDirection:'row', justifyContent:'center',alignItems:'center',marginBottom:10, marginTop:10}}>
              
              <TextInput

              style={{backgroundColor:'white',marginEnd:15, textAlign:'center', fontSize:15,width:40,height:40,}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',0)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,0,()=>this.otp2.focus())}
              value={this.state.otp[0]}
              ref={(input)=>this.otp1=input}
              />
              <TextInput
              style={{backgroundColor:'white',marginEnd:15, width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',1)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,1,()=>this.otp3.focus())}
              value={this.state.otp[1]}
              ref={(input)=>this.otp2=input}
              />
              <TextInput
              style={{backgroundColor:'white',marginEnd:15,width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',2)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,2,()=>this.otp4.focus())}
              value={this.state.otp[2]}
              ref={(input)=>this.otp3=input}
              />
              <TextInput
              style={{backgroundColor:'white', width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',3)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,3) }
              value={this.state.otp[3]}
              ref={(input)=>this.otp4=input}
              />
             
              </View>:null
              }
              {this.state.showOtp?
              <View style={{flex:1, flexDirection:"column", justifyContent:'center'}} >
              {this.state.verifyOtp?
                <TouchableOpacity
              onPress={this.verifyOtp}
              style={styles.updateButton}
              >
              <Image
                source={require('../../assets/ic_home/next.png')}
                style={{ resizeMode: 'contain', height: 70 }}
              />
            </TouchableOpacity>
              :null}
              <Text style={TextStyles.fieldTitle}>
                Didn't receive the verification code?
              </Text>
              <TouchableOpacity onPress={this.sendOtp}>
                <Text style={{textAlign:'center', color:'purple', fontSize:15}}>Resend verification code.</Text>
              </TouchableOpacity>
              </View>
              :null}
              </View>
            
              <TextInput
                style={styles.inputBox}
                maxLength={50}
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Occauption*"
          // onEndEditing={()=>this.birthday.focus()}
                onChangeText={value => this.setState({ occupation: value })}
                value={this.state.occupation}
                ref={input => this.occupation = input}
              />
              <DateTimePicker
                style={{ ...styles.inputBox }}
                isVisible={this.state.showDatePicker}
                onConfirm={(date) => {
                this.state.showDatePicker == 'dob' ? this.setState({ birthday: this.getDob(date), showDatePicker: false })
                            : this.setState({ anniversary: this.getDob(date), showDatePicker: false });
                }}
                onCancel={() => { this.setState({ showDatePicker: false }); }}
                mode="date"
                minimumDate={this.state.minimumDate}
                maximumDate={this.state.maximumDate}
              />
              {/* <View style={{ ...styles.inputBox, }} >
              <Text style={{position:'relative',marginTop:-20, alignSelf:'flex-end'}} >Birthday</Text> */}
             <View style={styles.inputBoxDate}>
             <DatePicker
             style={{width:'100%'}}
              date={this.state.birthday}
              mode="date"
              placeholder="Birthday* "
              format="DD/MM/YYYY"
              minDate="01/01/1800"
              maxDate={this.getDob(new Date())}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              iconSource={require('../../assets/ic_home/cal.png')} 
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 5,
                  top: 10,
                  height:20,
                  width:20,
                  marginLeft: 0
                },
                dateInput:{
                  borderWidth:0
                },
                placeholderText:{
                  fontSize:15,
                  color:'#707171',
                  padding: 10,
                  alignSelf:'flex-start'
                },
                dateText:{
                  fontSize:15,
                  padding: 10,
                  alignSelf:'flex-start'
                }
              }}
              onDateChange={(date) => {this.setState({birthday: date})}}
            />
             </View>
              
             <View style={styles.inputBoxDate}>
             <DatePicker
             style={{width:'100%',}}
              date={this.state.anniversary}
              mode="date"
              placeholder="Anniversary*"
              format="DD/MM/YYYY"
              minDate="01/01/1800"
              maxDate={this.getDob(new Date())}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"     
              iconSource={require('../../assets/ic_home/cal.png')}                                                                                                                                                                                                                                   
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 5,
                  top: 10,
                  height:20,
                  width:20,
                  marginLeft: 0
                },
                dateInput:{
                  borderWidth:0
                },
                placeholderText:{
                  fontSize:15,
                  padding: 10,
                  color:'#707171',
                  alignSelf:'flex-start'
                },
                dateText:{
                  fontSize:15,
                  padding: 10,
                  alignSelf:'flex-start'
                }
              }}
              onDateChange={(date) => {this.setState({anniversary: date})}}
            />
             </View>
            
            </KeyboardAvoidingView>

            {/* update button code */}
            <TouchableOpacity
              onPress={this.updateData}
              style={styles.updateButton}
            >
              <Image
                source={require('../../assets/ic_home/next.png')}
                style={{ resizeMode: 'contain', height: 70, marginBottom:100 }}
              />
            </TouchableOpacity>

           
          </ScrollView>
        </View>
      // </ImageBackground>
    );
  }
}

Profile.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

Profile.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

