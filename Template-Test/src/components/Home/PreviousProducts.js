import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {View, Text, ImageBackground,ScrollView, TouchableOpacity, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import styles from './styles';
import Swiper from 'react-native-swiper';
import { Image, Button } from 'react-native-elements';
// import {   } from 'react-native-gesture-handler';
import Loader from '../common/Loader';

export default class PreviousProducts extends React.Component{
    constructor(props){
        super(props);
        this.state={
            loading:true,
            data:[],
            dataLen:0,
            prod:0,
            confirmDel:false,
            confirmAll:false,

        }
    }

   componentDidMount(){
       this.setState({data:this.props.data?this.props.data:[], dataLen:this.props.data?this.props.data.length:0});
   }

   
    render(){
        console.warn(this.state.data);
        return(
                <LinearGradient style={{...styles.ppContainer}} colors={["#5960e5", "#b785f8","#b785f8"]}  
                useAngle={true} 
                angle={45} 
                angleCenter={{x:0, y:0}}
                >
                {  
                
                !this.state.confirmDel?
                <ScrollView>
                    <View style={{marginHorizontal:15}}>
                    <Text style={{...styles.headerText}} >SELECT YOUR PRODUCT</Text>
                    {/* <Text style={{...styles.subHeaderText}}>Lorem ipsum dolar sit amet, consectetur adipiscing elit sed do eiusmod</Text> */}
                    {this.props.data.length?
                    <Swiper  
                    dotStyle={{borderColor:'white', borderWidth:1,height:8, width:8,marginBottom:-100  }}
                    activeDotStyle={{borderColor:'white', borderWidth:1,height:8, width:8,marginBottom:-100 }} 
                    dotColor="transparent" activeDotColor="white"
                    bounces={false}
                    onMomentumScrollEnd={(e,state,context)=>this.setState({prod:state.index})}
                    // scrollEnabled={false}
                    // onIndexChanged={(index)=>{
                    //     this.setState({prod:index})}
                    // }
                    containerStyle={{height:340, }}
                    ref={(swp)=>this.prodS=swp}
                    >
                        {
                            this.props.data.map((product, index)=>{
                                return(
                                    <View style={{...styles.productView}} key={index}>
                                        <ImageBackground source={{uri:product.image}}
                                            resizeMode="stretch"
                                            style={{height:90, width:'100%', justifyContent:'center', alignItems:'flex-start',borderRadius:8 }}
                                        >
                                        {/* <View style={{padding:15,paddingLeft:20,paddingRight:20,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4,borderRadius:20, justifyContent:'center',height:40, alignItems:'center',
                                        backgroundColor:'#30d9f9',
                                        marginLeft:20}}>
                                            <Text style={{textTransform:'uppercase', fontFamily:'RubikBold'}}>
                                                Purchased{'\n'}2 years ago
                                            </Text>
                                        </View> */}
                                        
                                        </ImageBackground>
                                        <Text style={{color:'#3875c9', fontFamily:'RubikBold',fontSize:12.5,marginTop:15}}>{product.product_name?product.product_name.toUpperCase():''}</Text>
                                        <Text style={{color:'black', fontFamily:'RubikBold',fontSize:17.5, marginTop:9.5}}>{product.sub_product_name?product.sub_product_name.toUpperCase():''}</Text>
                                        <Text style={{...styles.regularKey}}>Model Name: <Text>{product.model_name}</Text>  </Text>
                                        <Text style={{...styles.regularKey}}>Serial No.: <Text>{product.serial_no}</Text> </Text>  
                                            
                                        <Text style={{...styles.regularKey}}>Date of Purchase: <Text style={{...styles.boldAnswer}}>{product.purchase_date.split(' ')[0]}</Text> </Text> 
                                        
                                        <Text style={{...styles.regularKey}}>Brand: <Text style={{...styles.boldAnswer}}>Panasonic</Text></Text>  
                                            
                                        
                                        <TouchableOpacity>
                                            <Text style={{color:'#2c20ea', fontFamily:'RubikMedium', textDecorationLine:'underline',fontSize:14,marginTop:15}}>Add More Information</Text>
                                        </TouchableOpacity>
                                        
                                    </View>
                                    
                                );
                            })
                        }
                        
                     </Swiper>:null
                   }
                    <View style={{...styles.footerBtn}}>
                        <View style={{flex:0.2, justifyContent:'center',alignItems:'center',}}>
                            <Image source={require('../../assets/ic_home/trashcan.png')} style={{resizeMode:'contain', height:35}} />
                            <Text style={{position:'absolute', backgroundColor:'#111dff',right:-10,fontFamily:'RubikMedium',color:'white', width:25, borderRadius:8, textAlign:'center'}}>{this.props.delData.length}</Text>
                        </View>
                        
                        <View style={{flex:1,height:60, justifyContent:'center', alignItems:'center'}}>
                          
                            <ImageBackground source={require('../../assets/ic_home/eclipse.png')}
                            resizeMode="stretch"
                            style={{height:60, width:140,flexDirection:'row', justifyContent:'space-between', paddingHorizontal:0}}
                            imageStyle={{height:70, width:'100%'}}
                            >
                             <TouchableOpacity style={{ justifyContent:'center', height:60, width:60}}
                              onPress={()=>{
                                // alert('on delete : '+this.state.prod);
                                  this.setState({confirmDel:true})
                            }}
                             >
                                <Image source={require('../../assets/ic_home/delete.png')} style={{resizeMode:'contain',alignSelf:'center', height:90}} />
                              </TouchableOpacity>
                              
                              <TouchableOpacity style={{  justifyContent:'center',height:60, width:60,marginRight:6}}
                              onPress={()=>
                                {

                                    this.props.addApp(this.state.prod);   
                                    this.setState({prod:0})
                                }
                                }
                              >
                                <Image source={require('../../assets/ic_home/add.png')} style={{resizeMode:'contain',alignSelf:'center', height:90, }} />
                              </TouchableOpacity>
                            </ImageBackground>
                            
                        </View>
                       
                        <View style={{flex:0.2, justifyContent:'center',alignItems:'center',}}>
                            <Image source={require('../../assets/ic_home/box.png')} style={{resizeMode:'contain', height:35}} />
                            <Text style={{position:'absolute', backgroundColor:'#111dff',left:-15,fontFamily:'RubikMedium'
                            ,color:'white', width:25, borderRadius:8, textAlign:'center'}}>{this.props.addData?this.props.addData.length:'0'}</Text>
                        </View>
                    </View>
                    </View>
                </ScrollView>
                :
                <View style={{flex:1, justifyContent:'center', alignItems:'center', width:'100%'}}>

                    <View style={{alignSelf:'center', justifyContent:'center', height:180, width:180, marginBottom:30}}>
                        <Image source={require('../../assets/ic_home/modal_back.png')} 
                        style={{alignSelf:'center',position:'absolute', height:170, width:170, }}/>
                        <View style={{alignSelf:'center',height:65, width:50, marginTop:45}}>
                            <Image source={require('../../assets/ic_home/line_trash.png')} style={{alignSelf:'center', height:'100%', width:'100%',marginRight:10}}/>
                            
                        </View>
                    </View>
                    <Text style={{fontFamily:'RubikBold', fontSize:25, textAlign:'center', color:'white', marginBottom:10}}>ARE YOU SURE,{"\nYOU WANT TO DELETE?"}</Text>
                    <Text style={{fontFamily:'RubikMedium', fontSize:18, textAlign:'center', color:'white', marginBottom:10}}> {this.props.data.length?this.props.data[this.state.prod].sub_product_name+' '+this.props.data[this.state.prod].product_name:''}</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, textAlign:'center', color:'white',}}>Serial Number:  {this.props.data.length?this.props.data[this.state.prod].serial_no:''}</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, textAlign:'center', color:'white', marginBottom:35}}>Model Number: {this.props.data.length?this.props.data[this.state.prod].model_id:''} </Text>
                    <TouchableOpacity style={{backgroundColor:'blue', width:'80%', borderRadius:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7, marginBottom:16}}
                    onPress={()=>{
                       this.props.delApp(this.state.prod);

                        this.setState({confirmDel:false, prod:0})
                    }}
                    >
                        <LinearGradient style={{ height:60,borderRadius:10, justifyContent:'center', alignItems:'center'}} 
                        colors={["#ff619e", "#ff9fa6"]} 
                        ><Text style={{fontFamily:'RubikBold', fontSize:17,color:'white'}}>DELETE PRODUCT</Text></LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity style={{backgroundColor:'transparent', width:'80%',borderColor:'white', 
                    borderWidth:2, height:60, borderRadius:10,justifyContent:'center', alignItems:'center'}}
                    onPress={()=>{
                        this.setState({confirmDel:false})
                        }
                    }>
                        <Text style={{fontFamily:'RubikBold', fontSize:17,color:'white'}}>CANCEL</Text>
                    </TouchableOpacity>
                </View>
            }
                </LinearGradient>
            
            
        );
       
    }
}

