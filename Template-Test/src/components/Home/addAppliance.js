import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {
    View, Text, AsyncStorage, ScrollView,Dimensions,
    TouchableOpacity, PermissionsAndroid, StatusBar, FlatList,
    KeyboardAvoidingView, TextInput, BackHandler, Modal, Keyboard, Platform
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import styles from './styles';
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import { Image, Avatar } from 'react-native-elements';
// import {   } from 'react-native-gesture-handler';
import Loader from '../common/Loader';
import * as constants from '../../helpers/constants';
import * as authFun from '../../helpers/auth';
import QRCodeScanner from 'react-native-qrcode-scanner';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import ModalDropdown from 'react-native-modal-dropdown';
import axios from 'axios';
import Permissions from 'react-native-permissions';



export default class AddAppliance extends React.Component {
    static navigationOptions = {
        //headerVisible: false,
        header: null,
      };
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            data: [],
            prod: 0,
            confirmDel: false,
            confirmAll: false,
            prod2Del: '',
            screen2: false,
            screen3: false,
            screent: false,
            scan: false,
            manually: true,
            birthday: '',
            selected: [{ label: 'brand', val: 0 }, { label: 'group', val: 0 }, { label: 'subgroup', val: 0 }],
            brandSelected:'',
            updateAllowed:true,
            customer_product_id:'',
            approved:false,
            groupSelected:'',
            subgroupSelected:'',
            showFilterModel: false,
            filterValues: [],
            showOptions:false,
            choice:'',
            invalid_serial_no:0,
            selectedAdd: [{ label: 'country', val: 0 }, { label: 'state', val: 0 }, { label: 'city', val: 0 }, { label: 'area', val: 0 },],
            SelectedAdressID:'',
            Myaddresses:[],
            oldAddress:false,
            panasonicBrands:['panasonic', 'sanyo'],
            countrySelected:'',
            stateSelected:'',
            citySelected:'',
            areaSelected:'',
            modelNum: '',
            models: [],
            serialNum: '',
            invoiceDate: '',
            warrantyDate: '',
            warrantyPeriod: '',
            IDupdated: true,
            WXupdated: true,
            fileList: [],
            dealerName: '',
            dealerAdd: '',
            invoiceNum: '',
            landmark: '',
            address: '',
            pincode: '',
            invoice_photo: '',
            product_photo: '',
            warranty_photo: '',
            other_photo: '',
            invalid_serial: -1,
            country: [],
            state: [],
            city: [],
            area: [],
            brand: [],
            group: [],
            subgroup: [],
            loading: false,
            editInfo:'',
            file_max_size:'',
            token: '',
            radioButtons: [{ label: 'HOME', selected: true }, { label: 'OFFICE', selected: false }, { label: 'OTHER', selected: false }],
            applianceLocation: [{ label: 'KITCHEN', image: require('../../assets/ic_home/kitchen.png'), selected: true },
            { label: 'DRAWING ROOM', image: require('../../assets/ic_home/waiting_room.png'), selected: false },
            { label: 'BATHROOM', image: require('../../assets/ic_home/bathroom.png'), selected: false },
            { label: 'BEDROOM', image: require('../../assets/ic_home/bedroom.png'), selected: false },
            { label: 'STUDYROOM', image: require('../../assets/ic_home/studyroom.png'), selected: false }],
            height: 100,
            scanner: false,
            customerID:'',
            pApplianceData: '',
            pDealerData: '',
            pDocumentData: '',
            mappedFirstScreen:false,
            mappedSecondScreen:false,
            mappedThirdScreen:false,
        }
        this.selectRadioBtn = this.selectRadioBtn.bind(this)
        this.selectLocation = this.selectLocation.bind(this)
        this.setValue = this.setValue.bind(this);
        this.getBrands = this.getBrands.bind(this);
        this.getCategory = this.getCategory.bind(this);
        this.getAppliance = this.getAppliance.bind(this);
        this.getArea = this.getArea.bind(this);
        this.selectCheckBtn = this.selectCheckBtn.bind(this);
        this.pickDocuments = this.pickDocuments.bind(this);
        this.handleUploadFiles = this.handleUploadFiles.bind(this);
        this.checkValidity = this.checkValidity.bind(this);
        this.uploadCustomerProduct = this.uploadCustomerProduct.bind(this);
        this.requestStoragePermission = this.requestStoragePermission.bind(this);
        this.updateSize = this.updateSize.bind(this);
        this.changeFormat = this.changeFormat.bind(this);
        this.validateSerialNumber = this.validateSerialNumber.bind(this);
        this.handleBackPress = this.handleBackPress.bind(this);
        this.unsetValue = this.unsetValue.bind(this);
    }
    

    onSuccess = (e) => {
        var data = e.data;
        typesOf2D = ['DATA_MATRIX', 'QR_CODE']
        if(typesOf2D.indexOf(e.type)!=-1){
            if(data.indexOf('_')!=-1){
                data = data.split('_');
                if (data.length > 1) {
                    this.setState({ modelNum: data[0].replace(/%/g,''), serialNum: data.length > 2 ? data[2].replace(/%/g,'') : '' })
                } else {
                    alert('Code could not be recognized');
                }
            }
        }else{
            if(this.state.scanner=='serial')
            {
                this.setState({serialNum:data.replace(/%/g,'')});
            }else if(this.state.scanner == 'model'){
                this.setState({modelNum:data.replace(/%/g,'')})
            }
        }
        this.setState({ scanner: false });
    }

    loadData = async (updateData) => {
        const file_max_size = await AsyncStorage.getItem('file_max_size');
        const userData = await AsyncStorage.getItem('userData');
        if (userData) {
          const data = JSON.parse(userData);
          this.setState({
            customerID: data.customer_id ? data.customer_id : '',
            loading: false,file_max_size:Platform.OS=='ios'?file_max_size:file_max_size*1024
          },async  () => {
            await this.getDataofAddress();
           await  this.getBrands();
            // this.getArea();
            if(updateData){
                this.setState({editInfo: updateData},()=>{
                    this.getBrands();
                    this.getDataToBeUpdated();
                });
                
            }
          });
        } else {
          this.setState({ loading: false });
        }
      }


      
    getDataToBeUpdated = async () => {
        let updateData = this.state.editInfo;
        await this.setToken();
        this.setState({ loading: true })
        let url = constants.base_url + 'editProductOfCustomerAppliance';
        url = url + "?access_token=" + this.state.token;
        url = url + "&customer_id=" + this.state.customerID;
        url = url + "&customer_product_id=" + updateData;
        axios.get(url).then((response) => {
            
            this.setState({pApplianceData:response.data.data.appliance,pDealerData:response.data.data.dealer, pDocumentData:response.data.data.document,ActivityIndicator:false},()=>{
                console.warn(this.state.pApplianceData,'\n',this.state.pDealerData,'\n',this.state.pDocumentData);
                this.mapFirstScreenData();
            })
            }).catch((error) => {
                if(error.message.indexOf("Network")!=-1)
                {
                  alert('Please check your internet connection.');
                }else{
                  alert(error.message);
                }            
            this.setState({ActivityIndicator: false});
        });
      }

      mapFirstScreenData(){
        
        this.getCategory(this.state.pApplianceData.customer_product_id);
        this.getAppliance(this.state.pApplianceData.product_group_id);
        this.getModels(this.state.pApplianceData.sub_product_group_id);
        let temp = this.state.selected;
        let filtered_brands = this.state.brand.filter((item)=>item.brand_name.toLowerCase() == this.state.pApplianceData.brand_name.toLowerCase());
        temp[0].val = filtered_brands.length?filtered_brands[0].brand_id:'0';
        temp[1].val = this.state.pApplianceData.product_group_id;
        temp[2].val = this.state.pApplianceData.sub_product_group_id;
        this.setState({
            mappedFirstScreen:true,
            modelNum:this.state.pApplianceData.model_name,
            serialNum: this.state.pApplianceData.serial_no,
            brandSelected: this.state.pApplianceData.brand_name,
            groupSelected: this.state.pApplianceData.product_name,
            subgroupSelected: this.state.pApplianceData.sub_product_name,
            customer_product_id: this.state.pApplianceData.customer_product_id,
            approved: this.state.pApplianceData.status==1?true:false,
            updateAllowed: this.state.pApplianceData.status==1?false:true,
        });        

      }

      getDisplayFormatDate(dt){
        dt = new Date(dt);
        let day = parseInt(dt.getDate());
        let month = parseInt(dt.getMonth())+1;
        let year = dt.getFullYear();
        return ((day<10?('0'+day):day) + '/' + (month<10?('0'+month):month) + '/' + year);
      }

      mapSecondScreenData(){
        this.setState({
            mappedSecondScreen:true,
            dealerName: this.state.pDealerData.dealer_name, 
            dealerAdd: this.state.pDealerData.dealer_address, 
            invoiceNum: this.state.pDealerData.invoice_number, 
            invoiceDate: this.state.pDealerData.invoice_date?this.getDisplayFormatDate(this.state.pDealerData.invoice_date.split(' ')[0]):'', 
            warrantyDate:this.state.pDealerData.warranty_expire_date.indexOf('0000')==-1? this.getDisplayFormatDate(this.state.pDealerData.warranty_expire_date.split(' ')[0]):'0000-00-00 00:00:00'});
      }

      mapThirdScreenData(){
        let addressTypeId = this.state.radioButtons.map((item)=>item.label).indexOf(this.state.pDocumentData.address_type);
        let locationId = this.state.applianceLocation.map((item)=>item.label).indexOf(this.state.pDocumentData.product_location);
        this.selectRadioBtn(addressTypeId);
        this.selectLocation(locationId);
        this.setState({
            mappedThirdScreen:true,
            pincode: this.state.pDocumentData.pincode,
            landmark: this.state.pDocumentData.near_by,
            address: this.state.pDocumentData.address,
            // SelectedAdressID: this.state.pDocumentData.customer_address_id,
        },()=>{
            this.pincodeChanged(1);
        });
        this.mapImagesThirdScreen();
      }

      mapImagesThirdScreen(){
        let invoice = this.state.pDocumentData.invoice_copy;
        let product = this.state.pDocumentData.product_sticker;
        let warranty = this.state.pDocumentData.warranty_card;
        let other = this.state.pDocumentData.other_document;
        this.setState({
            invoice_photo: invoice?{name: invoice.split('/')[invoice.split('/').length-1], path: invoice, label:'Invoice'}:'',
            product_photo: product?{name: product.split('/')[product.split('/').length-1], path: product, label:'Product'}:'',
            warranty_photo: warranty?{name: warranty.split('/')[warranty.split('/').length-1], path: warranty, label:'Warranty'}:'',
            other_photo: other?{name: other.split('/')[other.split('/').length-1], path: other, label:'Other'}:''
        });
        
      }
    handleBackPress(){
        if(this.state.screen3){
            this.setState({screen3:false,});
        }else if(this.state.screen2){
          this.setState({screen2:false,});
        }else if(this.state.scanner){
            this.setState({scanner:false})
        }else if(this.state.screent){
            this.props.navigation.goBack(null);
        }else {
            this.props.navigation.goBack(null);
        }
        return true;
      }
      
     checkAndRequestPermissionIOS = () => {
        Permissions.check('camera').then(response => {
            //response is an object mapping type to permission
            if(response.camera!='authorized'){
                this.requestPermission();
            }

          });
      };

      requestPermission = () => {
        Permissions.request('camera').then(response => {
          // Returns once the user has chosen to 'allow' or to 'not allow' access
          // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
          this.setState({photoPermission: response});
        });
      };
      
    async componentDidMount() {
        if(Platform.OS == 'ios'){
            this.checkAndRequestPermissionIOS();
        }
        
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    //    console.warn(this.props.navigation.state)
            this.loadData(this.props.navigation.state.params?this.props.navigation.state.params.update:'');
    }

    setToken = async () => {
        var token = await authFun.checkAuth();
        this.setState({ token: token })
        return token;
    }

    getBrands = async () => {
        // console.warn('brands called')
        await this.setToken();
        this.setState({ loading: true })
        var url = constants.base_url + 'getBrand';
        url = url + "?access_token=" + this.state.token;
        fetch(url, { headers: { 'Content-Type': 'application/json' } })
            .then(response => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    this.setState({ brand: responseJson.data.length ? responseJson.data : [] })
                }
                this.setState({ loading: false })

            })
            .catch(error => {
                alert(error);
                this.setState({ loading: false })
            })
    }

    getCategory = (id) => {
        this.setState({ loading: true })
        var url = constants.base_url + 'getProductGroup';
        url = url + "?access_token=" + this.state.token + '&brand_id=' + id;

        fetch(url, { headers: { 'Content-Type': 'application/json' } })

            .then(response => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    this.setState({ group: responseJson.data.length ? responseJson.data : [] })
                }
                this.setState({ loading: false })

            })
            .catch(error => {
                alert(error);
                this.setState({ loading: false })
            })
    }

    getAppliance = (id) => {
        this.setState({ loading: true });
        var url = constants.base_url + 'getSubProductGroup';
        url = url + "?access_token=" + this.state.token + '&product_group_id=' + id;

        fetch(url, { headers: { 'Content-Type': 'application/json' } })

            .then(response => response.json())
            .then((responseJson) => {
                if (responseJson) {
                    this.setState({ subgroup: responseJson.data.length ? responseJson.data : [] });
                }
                this.setState({ loading: false })

            })
            .catch(error => {
                alert(error);
                this.setState({ loading: false })
            })
    }

    getModels = (id) => {
        var url = constants.base_url + 'getModels';
        url = url + "?access_token=" + this.state.token + '&sub_product_group_id=' + id;
        fetch(url, { headers: { 'Content-Type': 'application/json' } })
            .then(response => response.json())
            .then((responseJson) => {
                if (responseJson.status) {
                    if (responseJson.data.length) {
                        // alert(JSON.stringify(responseJson.data))
                        this.setState({
                            models: responseJson.data,
                            // modelNum:responseJson.data[0].model_name
                        })
                    } else if(this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())!=-1) {
                        alert('No models present for the selected Appliance. ');
                    }
                }
                this.setState({ loading: false });

            })
            .catch(error => {
                alert('Something went wrong.');
                this.setState({ loading: false })
            })
    }

    validateSerialNumber = () => {
        let invalid_serial = '';
      if(this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())!=-1) {
        this.setState({ loading: true });
        const data = new FormData();
        data.append('model_id', this.state.models.filter((item) => item.model_name == this.state.modelNum)[0].model_id + '');
        data.append('serial_no', this.state.serialNum + '');
        console.warn(data);
        var url = constants.base_url + 'validateSerialNo';
        url = url + "?access_token=" + this.state.token;
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: data,
        })
            .then(response => response.json())
            .then((responseJson) => {
                console.warn(responseJson)
                if (responseJson.status) {
                    invalid_serial = 0;
                    this.setState({ invalid_serial: 0, invalid_serial_no: 0 });
                    this.setState({ screen2: true},()=>{
                        if(this.state.editInfo && !this.state.mappedSecondScreen){
                            this.mapSecondScreenData();
                        }
                    });
                } else {
                    invalid_serial = 1;
                    this.setState({ invalid_serial: 1,invalid_serial_no: 1})
                }
                this.setState({ loading: false })

            })
            .catch(error => {
                alert(error);
                this.setState({ loading: false })
            });
            return invalid_serial;
        }else{
            invalid_serial = 0;
            this.setState({ screen2: true, invalid_serial},()=>{
                if(this.state.editInfo && !this.state.mappedSecondScreen){
                    this.mapSecondScreenData();
                }
            });
            return true;
        }
        
        
    }
    
    selectRadioBtn = (index) => {
        var temp = this.state.radioButtons
        temp.forEach((btn, ind) => {
            if (ind == index) {
                btn.selected = true
            } else {
                btn.selected = false
            }
        });
        this.setState({ radioButtons: temp })
    }

    selectCheckBtn = (index) => {
        var temp = this.state.fileList
        temp.forEach((btn, ind) => {
            if (ind == index) {
                btn.selected = !btn.selected;
            }
        });
        this.setState({ fileList: temp })
    }

    unsetValue(screen, field){
        let temp = screen == 1 ? this.state.selected : this.state.selectedAdd;
       
        if(field == 'group'){
            this.setState({subgroupSelected:'' ,groupSelected:''});
        }else  if(field == 'subgroup'){
            this.setState({ subgroupSelected:''});
        }
        if(field == 'country'){
            this.setState({countrySelected:'', stateSelected:'', citySelected:'', areaSelected:'', pincode:'', });
        }else if(field == 'state'){
            this.setState({stateSelected:'', citySelected:'', areaSelected:'', pincode:'', });
        }else if(field == 'city'){
            this.setState({citySelected:'', areaSelected:'', pincode:'', });
        }else if(field == 'area'){
            this.setState({areaSelected:'', pincode:'', });
        }

        temp = temp.map((item)=>{
            if(item.label == field){
                item.val = '';
            }
            return item;
        });
        if(screen==1){
            this.setState({selected:temp,})
        }else{
            this.setState({selectedAdd:temp})
        }
    }

    setValue =  (screen, field, val, pinChange=false) => {
        var temp = screen == 1 ? this.state.selected : this.state.selectedAdd;
        temp = temp.map((item) => {
            if (item.label == field) {
                item.val = val;
                // if(field == 'country' && pinChange){
                //     let countries = this.state.country.map((element)=>element.country_id);
                //     let selectedCountry = countries.indexOf(val);
                //     this.countrySelector.select(selectedCountry);
                // }
                if(field == 'state' && pinChange && this.stateSelector){
                    let states = this.state.state.map((element)=>element.state_id);
                    let selectedstate = states.indexOf(val);
                    this.stateSelector.select(selectedstate);
                }
                if(field == 'city' && pinChange && this.citySelector){
                    let cities = this.state.city.map((element)=>element.city_id);
                    let selectedcity = cities.indexOf(val);
                    this.citySelector.select(selectedcity);
                }
                if(field == 'area' && pinChange && this.areaSelector){
                    let areas = this.state.area.map((element)=>element.area_id);
                    let selectedarea = areas.indexOf(val);
                    this.areaSelector.select(selectedarea);
                }
            }
            return item;
        });
        if (screen == 1)
            this.setState({ selected: temp, invalid_serial:-1  });
        else
            this.setState({ selectedAdd: temp });

    }
    
    pickDocuments = async (doc) => {
        try {
            this.setState({ loading: true })
            const results = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            if(parseInt(results.size) > parseInt(this.state.file_max_size)){
                this.setState({ loading:false},()=>{
                    alert('Maximum file size allowed is 2MB');
                });
            }else{
                if(Platform.OS=='ios'){
                    let correctpath =  results.uri;
                    if (doc == 'invoice') {
                        this.setState({ invoice_photo: { name: results.name, path: correctpath, label: 'Invoice' } })
                    } else if (doc == 'product') {
                        this.setState({ product_photo: { name: results.name, path: correctpath, label: 'Product' } })
                    } else if (doc == 'warranty') {
                        this.setState({ warranty_photo: { name: results.name, path: correctpath, label: 'Warranty' } })
                    } else {
                        this.setState({ other_photo: { name: results.name, path: correctpath, label: 'Other' } })
                    }
                }else{
                    RNFetchBlob.fs
                    .stat(results.uri) // Relative path obtained from document picker
                    .then(stats => {
                        var str1 = "file://";
                        var str2 = stats.path;
                        var correctpath =  str1.concat(str2);
                        if (doc == 'invoice') {
                            this.setState({ invoice_photo: { name: results.name, path: correctpath, label: 'Invoice' } })
                        } else if (doc == 'product') {
                            this.setState({ product_photo: { name: results.name, path: correctpath, label: 'Product' } })
                        } else if (doc == 'warranty') {
                            this.setState({ warranty_photo: { name: results.name, path: correctpath, label: 'Warranty' } })
                        } else {
                            this.setState({ other_photo: { name: results.name, path: correctpath, label: 'Other' } })
                        }
                    
                        this.setState({ loading: false, showOptions:false });

                    })
                    .catch(err => {
                        
                        this.setState({ loading: false, showOptions:false });
                        console.warn(err);
                    });
                }
                
            }
            
           
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                console.warn(err)
                this.setState({ loading: false })
            } else {
                console.warn(err)
                this.setState({ loading: false })
                throw err;
            }
            this.setState({showOptions:false});
        }
        
    }
    updateSize = () => {
        this.setState({ height: 100 })
    }

    

     // Image Picker
  chooseFile = (doc) => {
    const options = {
      title: 'Select Image',mediaType: 'photo',noData: true,storageOptions: {skipBackup: true,}};
    
      ImagePicker.showImagePicker({title: 'Select Image',quality:0.8,maxHeight:400,maxWidth:400,mediaType: 'photo',noData: true,
      storageOptions: {skipBackup: true,}},
     (response) => {
      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = response;
        console.warn(response);
        if(parseInt(response.size) > parseInt(this.state.file_max_size)){
            alert('Maximum file size allowed is 2MB');
            this.setState({loading:false});
            console.log('====================testing=====================2=====');
         }else{
            
            var str1 = "file://";
            var str2 = source.path;
            var correctpath = Platform.OS=='ios'?source.uri:str1.concat(str2);
            var split_correctpath = correctpath.split('/');
            var fileName = Platform.OS=='ios'?split_correctpath[split_correctpath.length-1]:source.fileName;
            console.log(source,'====================testing=====================', correctpath);
            if (doc == 'invoice') {
                this.setState({ invoice_photo: { name: fileName, path: correctpath, label: 'Invoice' } })
            } else if (doc == 'product') {
                this.setState({ product_photo: { name: fileName, path: correctpath, label: 'Product' } })
            } else if (doc == 'warranty') {
                this.setState({ warranty_photo: { name: fileName, path: correctpath, label: 'Warranty' } })
            } else {
                this.setState({ other_photo: { name: fileName, path: correctpath, label: 'Other' } })
            }
        }
        this.setState({ loading: false, showOptions:false });
        // this.handleUploadPhoto(source);
      }
    });
  }

//   document picker 
    requestStoragePermission = async (doc) => {
        console.warn('camera called');
        try {
            if(Platform.OS == "android"){
                console.warn("Androd Running");

                var perm = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE);
            if (!perm) {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    {
                        title: 'Permission to access photos and documents',
                        message:
                            'This App needs access to your storage ' +
                            'so you can upload pictures and documents.',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this.pickDocuments(doc)
                    console.warn('You can use the storage');
                } else {
                    console.warn('Storage permission denied');
                }
            } else {
                this.pickDocuments(doc);
            }
            }else{
                console.warn("ios Running");
                this.pickDocuments(doc);
            }
            

        } catch (err) {
            console.warn(err);
        }
       
    }


// function to upload images directly if required not required now.
    handleUploadFiles = async () => {
        const data = new FormData();
        data.append('image', { uri: `${this.state.photo.path}`, type: this.state.photo.name.indexOf('pdf') != -1 ? 'application/pdf' : 'image/*', name: this.state.photo.name });
        var url = constants.base_url + "uploadCustomerProductImage?access_token=" + this.state.token;
        // alert('url : '+url)
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            body: data,
        })
            .then(response => response.json())
            .then((response) => {
                //   alert("image data response : "+JSON.stringify(response))
                if (response.status) {
                    var temp = this.state.fileList;
                    temp.push({ label: this.state.photo.name, selected: true, path: this.state.photo.path, data: response.data })
                    this.setState({ fileList: temp, loading: false });
                }
            })
            .catch((error) => {
                NetInfo.fetch().then(async (state)=>{
                    if(!state.isConnected){
                        alert('Please check your internet connection.');
                      }else{
                        alert('Upload failed.');
                      }
                    console.log('\n\n\n\nupload error \n\n\n\n\n', error);
                    alert(`Upload failed!`);
                  })
                
            });
    };

    getDob(date) {
        date = new Date(date);
        const year = date.getFullYear();
        const month = date.getMonth() + 1;
        const day = date.getDate();
        return `${day < 10 ? `0${day}` : day}/${month < 10 ? `0${month}` : month}/${year}`;
    }
    selectLocation = (index) => {
        var temp = this.state.applianceLocation
        temp.forEach((btn, ind) => {
            if (ind == index) {
                btn.selected = true
            } else {
                btn.selected = false
            }
        });
        this.setState({ applianceLocation: temp })
    }

    cleanAddress(){
        this.setState({ selectedAdd:[{label:'country', val:0},{label:'state', val:0},{label:'city', val:0},{label:'area', val:0}, ],stateSelected:'', citySelected:'', areaSelected:'',pincode:'' });
        this.getArea();
        setTimeout(()=>{
            if(this.stateSelector){
                this.stateSelector.select(-1);
                this.citySelector.select(-1);
                this.areaSelector.select(-1);
            }
            
        },100);
        

      } 
    
  pincodeChanged =async (edit=0)=>{
    let pincode = this.state.pincode;
    await this.setToken();
    this.setState({loading:true})
    if(pincode!='' && pincode.length==6){
      let url = constants.base_url + 'pincodeBasedData';
      url = url + "?access_token=" + this.state.token + '&pincode=' + pincode;
      fetch(url,
        { headers: { 'Content-Type': 'application/json' } })
        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.status) {
            // this.setValue(2, 'country', responseJson.data.country_id, true);
            this.getArea('state', responseJson.data.country_id, responseJson.data.state_id, true);
            this.getArea('city',responseJson.data.state_id, responseJson.data.city_id);
            this.getArea('area', responseJson.data.city_id, responseJson.data.area_id, responseJson.data.area, edit);
            this.setState({loading:false,areaSelected:edit?responseJson.data.area_name:'', stateSelected: responseJson.data.state_name, citySelected: responseJson.data.city_name,  countrySelected: responseJson.data.country_name});
            if(!edit){
                this.areaSelector.select(-1);
            }
          } else {
              alert('Unable to locate '+this.state.pincode)
            this.setState({ loading: false });
            this.cleanAddress();
          }
  
        })
        .catch(error => { console.log(error); this.setState({ loading: false }) })
    }else{
      alert('Enter a valid postal code.');
      this.setState({loading:false})
    }
  }

    getArea = async (name = 'country', id = 0, pinChange = false, data = [], edit=0) => {
        // console.warn(this.state.selectedAdd)
        await this.setToken();
        this.setState({ loading: true });
        var location = id != 0 ? name == 'state' ? 'getState' : name == 'city' ? 'getCity' : 'getArea' : 'getCountry';
        var url = constants.base_url + location;
        var query = id != 0 ? name == 'state' ? '&country_id=' + id : name == 'city' ? '&state_id=' + id : '&city_id=' + id : '';
        url = url + "?access_token=" + this.state.token + query;

        fetch(url, { headers: { 'Content-Type': 'application/json' } })

            .then(response => response.json())
            .then((responseJson) => {

                if (responseJson.status) {
                    if (name == 'country') {
                        let temp = this.state.selectedAdd;
                        let country_id = responseJson.data.filter((item)=>item.country_name.toLowerCase() == 'india')[0].country_id;
                        temp[0].val = country_id;
                        this.setState({ country: responseJson.data.length ? responseJson.data : [], selectedAdd:temp  }, () => {
                            // this.setValue(2, 'country', country_id+'', true);
                            this.getArea('state', country_id);
                        });
                    }
                    else if (name == 'state')
                        this.setState({ state: responseJson.data.length ? responseJson.data : [] }, ()=>{
                            if(pinChange){
                                this.setValue(2, 'state', pinChange, true);
                            }
                        });
                    else if (name == 'city')
                        this.setState({ city: responseJson.data.length ? responseJson.data : [] }, ()=>{
                            if(pinChange){
                                this.setValue(2, 'city', pinChange, true);
                            }
                        });
                    else
                    this.setState({ area: data.length?data:responseJson.data.length ? responseJson.data : [] }, ()=>{
                            if(edit){
                                this.setValue(2, 'area', pinChange, true);
                            }
                        });
                }
                this.setState({ loading: false })

            })
            .catch(error => {
                alert(error);
                this.setState({ loading: false })
            })
    }
    changeTextField(inputText, field) {
        const specialCharReg = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        const numReg = /^[0-9]*$/;
        const serialNumReg = /^[0-9A-Za-z-]*$/;
        const nameReg = /^[a-zA-Z\s ]*$/;
        const addReg = /^[a-zA-Z0-9\s-/, ]*$/;
        const invoiceNumReg = /^[a-zA-Z0-9\-#/]*$/;
        const numEntry = /[0-9]*/;
        const pincode = /^[0-9]*$/
        if (field == 'serialNum' && serialNumReg.test(inputText)) {
            this.setState({ 'serialNum': inputText, invalid_serial:-1 });
        }
        else if (field == 'dealerName' && nameReg.test(inputText)) {
            this.setState({ 'dealerName': inputText })
        }
        else if (field == 'dealerAdd' && addReg.test(inputText)) {
            this.setState({ 'dealerAdd': inputText })
        } else if (field == 'invoiceNum' && invoiceNumReg.test(inputText)) {
            this.setState({ 'invoiceNum': inputText })
        } else if (field == 'landmark' && addReg.test(inputText)) {
            this.setState({ landmark: inputText })
        } else if (field == 'pincode' && pincode.test(inputText)) {
            this.setState({ pincode: inputText })
        } else if (field == 'address' && addReg.test(inputText)) {
            this.setState({ address: inputText })
        }
    }

  checkValidity(screen) {
        if (screen == 1) {
            temp = this.state.selected;
            if (temp[0].val == 0) {
                alert('Please select ' + temp[0].label)
                return false;
            } else if (temp[1].val == 0) {
                alert('Please select category')
                return false;
            } else if (temp[2].val == 0) {
                alert('Please select appliance')
                return false;
            } else if (this.state.modelNum == '') {
                alert('Please enter a model number');
                return false;
            } else if (!this.state.models.filter((item) => item.model_name == this.state.modelNum).length &&  this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())!=-1) {
                alert('Please enter a valid model number');
                return false;
            }
            else if (this.state.serialNum == '') {
                alert('Please enter a valid serial number')
                return false;
            }
            else if (this.state.invalid_serial == -1) {
                this.validateSerialNumber();
            }else{
                this.setState({ screen2: true},()=>{
                    if(this.state.editInfo && !this.state.mappedSecondScreen){
                        this.mapSecondScreenData();
                    }
                });
            }
        }
        else if (screen == 2) {
            if (this.state.dealerName == '') {
                alert('Please enter the dealer name');
                return false;
            } else if (this.state.dealerAdd == '') {
                alert('Please enter the dealer\'s address')
                return false;
            } else if (this.state.invoiceNum == '') {
                alert('Please enter the valid invoice number')
                return false;
            } else if (!this.state.IDupdated) {
                alert('Please select the invoice date')
                return false;
            } else if (!this.state.invoiceDate) {
                alert('Please select the invoice date.');
                return false;
            } else if (!this.state.warrantyDate &&  this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())==-1 ) {
                alert('Please select the warranty expiry date');
                return false;
            } 
        } else if (screen == 3) {
            let temp = this.state.selectedAdd;
            let message = this.state.Myaddresses.length?' or choose your address':'';
            if(!this.state.SelectedAdressID){
                if (temp[0].val == 0) {
                    alert('Please select country'+message);
                    return false;
                } else if (temp[1].val == 0) {
                    alert('Please select state'+message);
                    return false;
                } else if (temp[2].val == 0) {
                    alert('Please select city'+message);
                    return false;
                } else if (temp[3].val == 0) {
                    alert('Please select area'+message);
                    return false;
                } else if (this.state.address == '') {
                    alert('Please enter your address'+message);
                    return false;
                } else if (this.state.pincode == '') {
                    alert("Please enter a valid pincode"+message);
                    return false;
                } else if(this.state.invoice_photo == ''){
                    alert('Please upload an invoice copy.');
                    return false;
                }
            }
        }
        return true;
    }


    uploadCustomerProduct = async () => {
        if (this.checkValidity(3)) {
            var data = new FormData();
            data.append('customer_id', this.state.customerID);
            this.state.selected.forEach((item) => {
                if (item.label == 'brand')
                    data.append('brand_id', item.val + '')
                else if (item.label == 'group')
                    data.append('product_group_id', item.val + '')
                else
                    data.append('sub_product_group_id', item.val + '')
            });
            data.append('model_name', this.state.modelNum.toUpperCase());
            data.append('serial_no', this.state.serialNum.toUpperCase());
            data.append('dealer_name', this.state.dealerName);
            data.append('dealer_address', this.state.dealerAdd);
            data.append('invoice_number', this.state.invoiceNum.toUpperCase());
            data.append('invalid_serial_no', this.state.invalid_serial_no);
            data.append('invoice_date', this.changeFormat(this.state.invoiceDate));
            data.append('warranty_expire_date', this.state.warrantyDate?this.changeFormat(this.state.warrantyDate):'0000-00-00 00:00:00');
            data.append('product_location', this.state.applianceLocation.filter((item) => item.selected)[0].label);
            data.append('customer_address_id', this.state.SelectedAdressID);
            data.append('address_type', this.state.SelectedAdressID?'':this.state.radioButtons.filter((item) => item.selected)[0].label);
            // // alert(JSON.stringify(this.state.selectedAdd))
            this.state.selectedAdd.forEach((item) => {
                if (item.label == 'country')
                    data.append('country_name',this.state.SelectedAdressID?'': this.state.country.filter((name) => name.country_id == item.val)[0].country_name);
                if (item.label == 'state')
                    data.append('state_name', this.state.SelectedAdressID?'':this.state.state.filter((name) => name.state_id == item.val)[0].state_name);
                if (item.label == 'city')
                    data.append('city_name', this.state.SelectedAdressID?'':this.state.city.filter((name) => name.city_id == item.val)[0].city_name);
                else if (item.label == 'area')
                    data.append('area_name', this.state.SelectedAdressID?'':this.state.area.filter((name) => name.area_id == item.val)[0].area_name);
            });
            data.append('address', this.state.address);
            data.append('pincode', this.state.pincode + '');
            data.append('near_by', this.state.landmark);
            // data.append('files', JSON.stringify(this.state.fileList.length?this.state.fileList.map((item)=>item.data):[]) );
            data.append('invoice_copy', this.state.invoice_photo && this.state.invoice_photo.path.indexOf('http')==-1 ? { uri: `${this.state.invoice_photo.path}`, type: this.state.invoice_photo.name.indexOf('pdf') != -1 ? 'application/pdf' : 'image/*', name: this.state.invoice_photo.name } : '');
            data.append('product_sticker', this.state.product_photo && this.state.product_photo.path.indexOf('http')==-1 ? { uri: `${this.state.product_photo.path}`, type: this.state.product_photo.name.indexOf('pdf') != -1 ? 'application/pdf' : 'image/*', name: this.state.product_photo.name } : '');
            data.append('warranty_card', this.state.warranty_photo && this.state.warranty_photo.path.indexOf('http')==-1 ? { uri: `${this.state.warranty_photo.path}`, type: this.state.warranty_photo.name.indexOf('pdf') != -1 ? 'application/pdf' : 'image/*', name: this.state.warranty_photo.name } : '');
            data.append('other_document', this.state.other_photo && this.state.other_photo.path.indexOf('http')==-1 ? { uri: `${this.state.other_photo.path}`, type: this.state.other_photo.name.indexOf('pdf') != -1 ? 'application/pdf' : 'image/*', name: this.state.other_photo.name } : '');
            // this.setState({loading:true});
            if(this.state.editInfo){
                data.append('customer_product_id', this.state.editInfo);
            }
            this.setState({loading:true});            
            await this.setToken();
            let url = constants.base_url + (this.state.editInfo?'editProductOfCustomer':'addProductOfCustomer');
            url = url + "?access_token=" + this.state.token;
            fetch(url, { method: 'POST', headers: { 'Content-Type': 'multipart/form-data' }, body: data })
                .then((response) => {
                    return response.json()
                })
                .then(async (responseJson) => {
                    if (responseJson.status) {
                        await AsyncStorage.setItem('productAddedHome', '' + 1)
                        await AsyncStorage.setItem('productAdded', '' + 1, () => {

                            this.setState({ loading: false, screent:true });
                            
                        });

                    }else{
                        alert(responseJson.message)
                        this.setState({loading:false})
                    }
                })
                .catch(error => {
                    console.warn('error: ',error);
                    this.setState({ loading: false })
                });
        }
        
    }
    changeFormat = (dt) => {
        var arr = dt.split('/');
        return arr[2] + '-' + arr[1] + '-' + arr[0] + ' 00:00:00';
    }

    setSelected(screen, brands){
        setTimeout(()=>{
            this.brandSelector.select(brands.indexOf(this.state.selected[0].val));
        }, 100);

    }
    addApp = (ind) => {
        var tempP = this.state.productsList;
        var tempA = this.state.addList;
        var addedProd = tempP.splice(ind, 1)[0];
        tempA.push(addedProd);
        this.setState({ addList: [] }, () => {
          this.setState({ addList: tempA, productsList: tempP, }, () => this.setState({ confirmAll: tempA.length + this.state.delList.length == this.state.productsListLength }));
        })
    
      }
      genFrame(style, length) {
        style.height = length > 7 ? 200 : - 1;
        style.left = Dimensions.get("screen").width*0.10;
        style.width = Dimensions.get('screen').width*0.80;
        // style.left = Dimensions.get("screen")*0.10;
        
        return style;
        }
        async getDataofAddress() {
            await this.setToken();
            let accessToken = this.state.token;
            let customer_id = this.state.customerID;
            axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerAddresses?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
              this.setState({Myaddresses:response.data.data,ActivityIndicator:false})
            }).catch((error) => {
              NetInfo.fetch().then(async (state)=>{
                if(!state.isConnected){
                  alert('Please check your internet connection.');
                }
              })
              
              this.setState({ActivityIndicator: false});
            });
          }

    render() {
        // console.warn(this.state.pDocumentData)
        let brands = this.state.brand.map((element)=>element.brand_name);
        let groups = this.state.group.map((element)=>element.product_name);
        let sub_groups = this.state.subgroup.map((element)=>element.sub_product_name);
        let states = this.state.state.map((element)=>element.state_name);
        let cities = this.state.city.map((element)=>element.city_name);
        let countries = this.state.country.map((element)=>element.country_name);
        let areas = this.state.area.map((element)=>element.area_name);
       
        if (this.state.scanner ) {
            return (
                <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={() => {this.setState({ scanner: false })}}
                        style={{ alignSelf: 'flex-start', position: 'absolute', top: StatusBar.currentHeight, zIndex: 1 ,paddingTop: Platform.OS == 'ios'? 30:0}}>
                        <Image source={require('../../assets/ic_home/back.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }} />
                    </TouchableOpacity>
                    <QRCodeScanner
                        reactivate={true}
                        // containerStyle={{zIndex:-1}}
                        ref={(scanner) => this.scanner = scanner}
                        onRead={this.onSuccess}
                        showMarker={true}
                        cameraStyle={{ height: '100%'  }}
                        customMarker={
                            // <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                                <View style={{ backgroundColor: 'transparent', height: 180, width: 180, justifyContent: 'space-between' }}>
                                {/* top */}
                                <View style={{
                                    width: 15, backgroundColor: 'transparent', height: 15,
                                    borderTopColor: 'white', borderTopWidth: 2,
                                    borderLeftColor: 'white', borderLeftWidth: 2, position: 'absolute', top: 0, left: 0
                                }} />
                                <View style={{
                                    width: 15, backgroundColor: 'transparent', height: 15,
                                    borderTopColor: 'white', borderTopWidth: 2,
                                    borderRightColor: 'white', borderRightWidth: 2, position: 'absolute', right: 0, top: 0
                                }} />
                                {/* bottom */}
                                <View style={{
                                    width: 15, backgroundColor: 'transparent', height: 15,
                                    borderBottomColor: 'white', borderBottomWidth: 2,
                                    borderLeftColor: 'white', borderLeftWidth: 2, position: 'absolute', bottom: 0, left: 0
                                }} />
                                <View style={{
                                    width: 15, backgroundColor: 'transparent', height: 15,
                                    borderBottomColor: 'white', borderBottomWidth: 2,
                                    borderRightColor: 'white', borderRightWidth: 2, position: 'absolute', bottom: 0, right: 0
                                }} />
                            </View>
                            // </View>    
                        }

                    />

                </View>


            )
        } else
            return (
                <View style={{ flex: 1, backgroundColor: "#e9eaff" }} >
                    {this.state.loading ? <Loader /> : null}
                    {
                        this.state.screen2 ? this.state.screen3 ? this.state.screent ?

                            <View style={{backgroundColor:'red', flex:1}}>
                                <LinearGradient style={{ flex:1,justifyContent:'flex-start',alignItems:'center',color:'white',padding:10, height:'100%' }} colors={["#5960e5", "#b785f8", "#b785f8"]} useAngle={true} angle={45} angleCenter={{ x: 0, y: 0 }}>
                                <Image source={require('../../assets/ic_home/finished_add_prod.png')} style={{ height: 180, width: 180, marginBottom: 50, resizeMode: 'contain', marginVertical: 100 }} />
                                <Text style={{ fontFamily: 'RubikMedium', fontSize: 18, color: 'white',textAlign:'center', marginBottom:30 }}>
                                Thank you for {this.state.editInfo?'udpating':'adding'} your product, your data is under validation.
                                    {/* YOUR APPLIANCE {this.state.editInfo?'UPDATED':'ADDED'} */}
                                    </Text>
                                {/* <Text style={{ fontFamily: 'RubikBold', fontSize: 30, color: 'white', marginBottom: 30 }}>SUCCESSFULLY</Text> */}
                                <TouchableOpacity style={{ backgroundColor: 'blue', width: '80%', borderRadius: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 7, marginBottom: 16 }}
                                    onPress={() => {
                                        this.props.navigation.navigate('My Appliance');
                                    }
                                    }
                                >
                                    <LinearGradient style={{ height: 60, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
                                        colors={["#ff619e", "#ff9fa6"]}
                                    ><Text style={{ fontFamily: 'RubikBold', fontSize: 17, color: 'white' }}>VIEW PRODUCT</Text></LinearGradient>
                                </TouchableOpacity>
                            </LinearGradient>
                            </View> :

                           <ScrollView style={{flex:1, paddingTop: Platform.OS == 'ios'? 30:0}}>
                                <View>
                                <View style={{
                                    height: 90, backgroundColor: 'transparent', width: '100%',
                                    marginTop: StatusBar.currentHeight, alignSelf: 'center', justifyContent: 'flex-start'
                                }}>
                                    <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                                        angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                                            backgroundColor: 'purple',
                                            alignSelf: 'center',
                                            top: -1030,
                                            height: 1100,
                                            width: 1100,
                                            borderRadius: 550,
                                            position: 'absolute'
                                        }}
                                    >
                                    </LinearGradient>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5, paddingTop: 0, }}>
                                        <TouchableOpacity onPress={() => this.setState({ screen3: false, })} style={{ zIndex: 1 }}>
                                            <Image source={require('../../assets/ic_home/back.png')} style={{ height: 30, width: 30, }} />
                                        </TouchableOpacity>
                                        <Text style={{ flex: 1, color: '#e9eaff', fontFamily: 'RubikMedium', fontSize: 20, textAlign: 'center', marginLeft: -30 }}> DOCUMENT DETAILS</Text>
                                    </View>
                                </View>

                                {/* Main Content Screen 1 */}
                                <KeyboardAvoidingView style={{ ...styles.inputContainerScreen3 }}>
                                <View style={{ alignSelf:'stretch', flexDirection: 'row', alignItems:'center',  margin:15, }}>
                                {this.state.Myaddresses.length?<TouchableOpacity onPress={() =>{this.setState({oldAddress:true,});this.cleanAddress();}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:this.state.oldAddress?'#c4c3c1':'transparent'}}>
                                    <Text  style={{color: this.state.oldAddress?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                                        {"Choose Your Address"}
                                    </Text>
                                </TouchableOpacity>:null}
                                <TouchableOpacity onPress={() =>{this.setState({oldAddress:false,});this.cleanAddress();}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:(!this.state.oldAddress || !this.state.Myaddresses.length)?'#c4c3c1':'transparent'}}>
                                    <Text  style={{color:(!this.state.oldAddress || !this.state.Myaddresses.length)?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                                        {"Add New Address"}
                                    </Text>
                                </TouchableOpacity>
                                </View> 
                               { this.state.oldAddress && this.state.Myaddresses.length?
                                    <FlatList
                                    data = {this.state.Myaddresses}
                                    extraData={this.state}
                                    keyExtractor={(index,item)=>index.toString()}
                                    renderItem = {({item,index}) =>{
                                    return(
                                        <TouchableOpacity key={index} activeOpacity={0.7} onPress={()=>this.setState({SelectedAdressID:item.customer_address_id == this.state.SelectedAdressID?'':item.customer_address_id,},()=>this.setState({oldAddress:this.state.SelectedAdressID?false:true}))} 
                                        style={{borderRadius:5, flexDirection:'row',justifyContent:'flex-start',alignItems:'center',marginHorizontal:15,marginVertical:10 }}>
                                        <View style={{flex:1, backgroundColor:'white', padding:15, borderRadius:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                                            <Text style={{fontFamily:'RubikMedium', fontSize:15,color:'black'}}>{item.address_type?item.address_type.toUpperCase():''}</Text>
                                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.state_name}</Text>
                                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.city_name}, {item.pincode}</Text>
                                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.address}, {item.area_name} {item.near_by != ""? ", "+item.near_by:null}</Text>
                                        </View>
                                        <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center', marginHorizontal:15}}>
                                            <View style={{backgroundColor:this.state.SelectedAdressID == item.customer_address_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                                        </View>
                                        </TouchableOpacity>
                                        
                                    )}
                                }
                                />
                               :
                                    <View style={{ marginHorizontal: 20 }}>  
                                    { this.state.SelectedAdressID && !this.state.oldAddress?
                                    <View style={{flex:1, backgroundColor:'white', padding:15, borderRadius:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                                        <TouchableOpacity onPress={()=>this.setState({SelectedAdressID:''})} style={{top:15, right:15, position:'absolute', height:20, width:20, borderRadius:10, borderWidth:1.5, borderColor:'#c4c3c1', justifyContent:'center', alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, backgroundColor:'white', zIndex:1}}>
                                            <Text style={{ fontFamily:'RubikMedium', fontSize:12, }}>X</Text>
                                        </TouchableOpacity>
                                        <Text style={{fontFamily:'RubikMedium', fontSize:15,color:'black'}}>{this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].address_type?this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].address_type.toUpperCase():''}</Text>
                                        <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].state_name}</Text>
                                        <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].city_name}, {this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].pincode}</Text>
                                        <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].address}, {this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].area_name} {this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].near_by != ""? ", "+this.state.Myaddresses.filter((item)=>item.customer_address_id == this.state.SelectedAdressID)[0].near_by:null}</Text>
                                    </View>:
                                    <View>
                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                                            <View style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,  }}>
                                                <TextInput
                                                    autoCorrect={false}
                                                    style={{ backgroundColor:'white', 
                                                    color:'#707171', 
                                                    borderRadius:30, 
                                                    textAlign:'left', 
                                                    fontFamily:'RubikRegular',
                                                    fontSize:15,
                                                    padding:10, 
                                                    paddingHorizontal: 10, flex:1 }}
                                                    secureTextEntry={false}
                                                    contextMenuHidden
                                                    keyboardType='number-pad'
                                                    maxLength={6}
                                                    placeholder="Pincode*"
                                                    onEndEditing={()=>this.pincodeChanged()}
                                                    onChangeText={value => this.changeTextField(value, 'pincode')}
                                                    value={this.state.pincode}
                                                    ref={input => this.model = input}
                                                />
                                                </View>
                                            
                                            <TouchableOpacity activeOpacity={1} onPress={()=>this.stateSelector.show()} 
                                            style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, padding:5, marginLeft:10  }}>
                                                <ModalDropdown
                                                    style={{backgroundColor:'white', flex:1, borderRadius:25}}
                                                    ref={ (ref) => this.stateSelector = ref}
                                                    defaultValue={this.state.stateSelected?this.state.stateSelected:'State*'}
                                                    onSelect={(rowData,index) =>{ 
                                                        let state_id = this.state.state.filter((element)=>element.state_name==states[rowData])[0].state_id;
                                                        if (state_id != '0') {
                                                            this.setState({stateSelected:states[rowData]});
                                                            this.setValue(2, 'state', state_id);
                                                            this.getArea('city', state_id);
                                                            this.citySelector.select(-1);
                                                            this.unsetValue(3, 'city');
                                                            this.areaSelector.select(-1);
                                                            this.unsetValue(3, 'area');
                                                            this.setState({pincode:''});
                                                        }
                                                    }}
                                                    options={states}
                                                    dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}
                                                    textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                                    adjustFrame={style => this.genFrame(style, states.length)}
                                                    dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:Platform.OS=='ios'?0:-15    }}
                                                    dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                                />
                                                <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                            </TouchableOpacity>
                                        </View> 
                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                                             
                                            <TouchableOpacity activeOpacity={1} onPress={()=>this.citySelector.show()} 
                                            style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, padding:5  }}>
                                                <ModalDropdown
                                                    style={{backgroundColor:'white', flex:1, borderRadius:25}}
                                                    ref={ (ref) => this.citySelector = ref}
                                                    defaultValue={this.state.citySelected?this.state.citySelected:'City*'}
                                                    onSelect={(rowData,index) =>{ 
                                                        let city_id = this.state.city.filter((element)=>element.city_name==cities[rowData])[0].city_id;
                                                        if (city_id != '0') {
                                                            this.setState({citySelected:cities[rowData]});
                                                            this.setValue(2, 'city', city_id);
                                                            this.getArea('area', city_id);
                                                            this.areaSelector.select(-1);
                                                            this.unsetValue(3, 'area');
                                                            this.setState({pincode:''});
                                                        }
                                                    }}
                                                    options={cities}
                                                    dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                                    textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                                    adjustFrame={style => this.genFrame(style, cities.length)}
                                                    dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:Platform.OS=='ios'?0:-15    }}
                                                    dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                                />
                                                <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                            </TouchableOpacity>
                                            
                                            <TouchableOpacity activeOpacity={1} onPress={()=>this.areaSelector.show()} 
                                            style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, padding:5, marginLeft:10  }}>
                                                <ModalDropdown
                                                    style={{backgroundColor:'white', flex:1, borderRadius:25}}
                                                    ref={ (ref) => this.areaSelector = ref}
                                                    defaultValue={this.state.areaSelected?this.state.areaSelected:'Area*'}
                                                    onSelect={(rowData,index) =>{ 
                                                        let area_id = this.state.area.filter((element)=>element.area_name==areas[rowData])[0].area_id;
                                                        if (area_id != '0') {
                                                            this.setValue(2, 'area', area_id);
                                                            this.state.area.forEach((item) => {
                                                                if (area_id == item.area_id) {
                                                                    this.setState({areaSelected:areas[rowData]});
                                                                    if(item.postal_code){
                                                                        this.setState({ pincode: item.postal_code })
                                                                      }
                                                                }
                                                            })
                                                        }
                                                    }}
                                                    options={areas}
                                                    dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                                    textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                                    adjustFrame={style => this.genFrame(style, areas.length)}
                                                    dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,marginTop:Platform.OS=='ios'?0:-15   }}
                                                    dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                                />
                                                <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                            </TouchableOpacity>
                                        </View>
                                       
                                        <TextInput
                                            autoCorrect={false}
                                            style={styles.documentInputBox}
                                            secureTextEntry={false}
                                            contextMenuHidden
                                            placeholder="Landmark"
                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value => this.changeTextField(value, 'landmark')}
                                            value={this.state.landmark}
                                            ref={input => this.landmark = input}
                                        />
                                        <View style={{
                                            flex: 1, flexDirection: 'row', justifyContent: 'flex-start',
                                            alignItems: 'center', marginBottom: 15
                                        }}>
                                            <View style={{ flex: 1, flexDirection: 'row', width: '100%', justifyContent: 'flex-start', alignItems: 'center' }}>

                                                {
                                                    this.state.radioButtons.map((btn, ind) => {
                                                        return (
                                                            <TouchableOpacity key={ind} onPress={() => this.selectRadioBtn(ind)} style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                                                <View style={{ ...styles.radioBtnOuter }}>
                                                                    {btn.selected ? <View style={{ ...styles.radioBtnInner }} /> : null}
                                                                </View>
                                                                <Text style={{ fontFamily: btn.selected ? 'RubikBold' : 'RubikRegular', fontSize: 14 }}>{btn.label}</Text>
                                                            </TouchableOpacity>
                                                        );
                                                    })

                                                }
                                            </View>
                                        </View>
                                        <TextInput
                                            autoCorrect={false}
                                            onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                                            style={{ ...styles.addInputBox, marginBottom: 15, height: this.state.height }}
                                            secureTextEntry={false}
                                            multiline={true}
                                            numberOfLines={4}
                                            placeholder="Address*"
                                            contextMenuHidden
                                            textContentType="streetAddressLine1"
                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value => this.changeTextField(value, 'address')}
                                            value={this.state.address}
                                            ref={input => this.address = input}
                                        />
                                        </View>}
                                    </View>
                                    }
                                    
                                    <View style={{  marginBottom: 15, }}>
                                        <Text style={{ fontFamily: 'RubikMedium', fontSize: 15, margin: 15,marginBottom:0  }}>Choose Product Location</Text>
                                        <ScrollView horizontal contentContainterStyle={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                                            {
                                                this.state.applianceLocation.map((location, ind) => {
                                                    return (
                                                        <TouchableOpacity key={ind} onPress={() => this.selectLocation(ind)} style={{
                                                            backgroundColor: location.selected ? '#b785f8' : 'white', height: 80, width: 80, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10, 
                                                            justifyContent: 'center', alignItems: 'center', padding: 10, marginVertical:10, marginHorizontal:7.5
                                                        }}>
                                                            <Image source={location.image} style={{ height: 30, width: 30, marginBottom: 5, resizeMode: 'contain' }} />
                                                            <Text style={{ textAlign: 'center', fontFamily: 'RubikMedium', fontSize: 10 }}>{location.label}</Text>
                                                        </TouchableOpacity>
                                                    );
                                                })

                                            }

                                        </ScrollView>
                                    </View>
                                    <View style={{ marginHorizontal: 15 }}>
                                        <View style={{ flex: 1, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', padding: 10, paddingTop: 0 }}>

                                            <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', padding: 10, }}>
                                                {
                                                    this.state.invoice_photo ?
                                                    <View style={{ paddingVertical: 5, borderBottomColor:'#c2c4c4', borderBottomWidth:1}}>
                                                        <Text style={{fontFamily:'RubikRegular', fontSize:12}}>Invoice</Text>
                                                        <TouchableOpacity disabled={ this.state.invoice_photo.path.indexOf('http')!=-1?true:false} onPress={() => this.setState({ invoice_photo: '' })} style={{flexDirection: 'row',alignSelf: 'center', justifyContent: 'space-between', marginTop: 5,alignItems:'center'}}>
                                                            <Image source={this.state.invoice_photo.name.indexOf('pdf') != -1 ? require('../../assets/ic_home/pdf.png') : { uri: this.state.invoice_photo.path }}
                                                                style={{ height: 25, width: 25, marginRight: 10, resizeMode: 'contain' }} />
                                                            <Text style={{
                                                                flex: 1, fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'left',
                                                                textAlignVertical: 'center', alignSelf: 'center'
                                                            }}>{this.state.invoice_photo.name}</Text>
                                                            <View style={{ ...styles.checkBox }}>
                                                                <Image source={require('../../assets/ic_home/check_box.png')}
                                                                    style={{ ...styles.checkBoxImage, resizeMode: 'contain' }} />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>  : null
                                                }
                                                {
                                                    this.state.product_photo ?
                                                    <View style={{ paddingVertical: 5, borderBottomColor:'#c2c4c4', borderBottomWidth:1}}>
                                                        <Text style={{fontFamily:'RubikRegular', fontSize:12}}>Product</Text>
                                                        <TouchableOpacity disabled={this.state.product_photo.path.indexOf('http')!=-1?true:false} onPress={() => this.setState({ product_photo: '' })} style={{flexDirection: 'row',alignSelf: 'center', justifyContent: 'space-between', marginTop: 5,alignItems:'center'}}>
                                                            <Image source={this.state.product_photo.name.indexOf('pdf') != -1 ? require('../../assets/ic_home/pdf.png') : { uri: this.state.product_photo.path }}
                                                                style={{ height: 25, width: 25, marginRight: 10, resizeMode: 'contain' }} />
                                                            <Text style={{flex: 1, fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'left',textAlignVertical: 'center', alignSelf: 'center'}}>{this.state.product_photo.name}</Text>
                                                            <View style={{ ...styles.checkBox }}>
                                                                <Image source={require('../../assets/ic_home/check_box.png')}style={{ ...styles.checkBoxImage, resizeMode: 'contain' }} />
                                                            </View>
                                                        </TouchableOpacity></View> : null
                                                }
                                                {
                                                    this.state.warranty_photo ?
                                                    <View style={{ paddingVertical: 5, borderBottomColor:'#c2c4c4', borderBottomWidth:1}}>
                                                        <Text style={{fontFamily:'RubikRegular', fontSize:12}}>Warranty</Text>
                                                        <TouchableOpacity disabled={this.state.warranty_photo.path.indexOf('http')!=-1?true:false} onPress={() => this.setState({ warranty_photo: '' })} style={{flexDirection: 'row',alignSelf: 'center', justifyContent: 'space-between', marginTop: 5,alignItems:'center'}}>
                                                            <Image source={this.state.warranty_photo.name.indexOf('pdf') != -1 ? require('../../assets/ic_home/pdf.png') : { uri: this.state.warranty_photo.path }}
                                                                style={{ height: 25, width: 25, marginRight: 10, resizeMode: 'contain' }} />
                                                            <Text style={{
                                                                flex: 1, fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'left',
                                                                textAlignVertical: 'center', alignSelf: 'center'
                                                            }}>{this.state.warranty_photo.name}</Text>
                                                            <View style={{ ...styles.checkBox }}>
                                                                <Image source={require('../../assets/ic_home/check_box.png')}
                                                                    style={{ ...styles.checkBoxImage, resizeMode: 'contain' }} />
                                                            </View>
                                                        </TouchableOpacity> 
                                                         </View>
                                                        : null
                                                }
                                                {
                                                    this.state.other_photo ?
                                                    <View style={{ paddingVertical: 5, borderBottomColor:'#c2c4c4', borderBottomWidth:1}}>
                                                        <Text style={{fontFamily:'RubikRegular', fontSize:12}}>Other</Text>
                                                        <TouchableOpacity disabled={this.state.other_photo.path.indexOf('http')!=-1?true:false} onPress={() => this.setState({ other_photo: '' })} style={{flexDirection: 'row',alignSelf: 'center', justifyContent: 'space-between', marginTop: 5,alignItems:'center'}}>
                                                            <Image source={this.state.other_photo.name.indexOf('pdf') != -1 ? require('../../assets/ic_home/pdf.png') : { uri: this.state.other_photo.path }}
                                                                style={{ height: 25, width: 25, marginRight: 10, resizeMode: 'contain' }} />
                                                            <Text style={{
                                                                flex: 1, fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'left',
                                                                textAlignVertical: 'center', alignSelf: 'center'
                                                            }}>{this.state.other_photo.name}</Text>
                                                            <View style={{ ...styles.checkBox }}>
                                                                <Image source={require('../../assets/ic_home/check_box.png')}
                                                                    style={{ ...styles.checkBoxImage, resizeMode: 'contain' }} />
                                                            </View>
                                                        </TouchableOpacity>
                                                         </View>: null
                                                }
                                            </View>

                                            <View style={{ flexDirection: 'row' }}>
                                                <TouchableOpacity
                                                    // onPress={() => this.requestStoragePermission('invoice')}
                                                    onPress = {()=> this.setState({showOptions:true,choice:'Invoice'})}
                                                    style={{backgroundColor: "#e6e8e9", borderStyle: 'dashed', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, borderRadius: 5,borderWidth: 0.5, flex: 1, justifyContent: 'flex-start', alignItems: 'center', padding: 10,alignItems:'center'}}>
                                                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', marginBottom: 5 }}>
                                                        <Image source={require('../../assets/ic_home/pdf.png')} style={{ height: 30, width: 30, resizeMode: 'center' }} />
                                                        <Image source={require('../../assets/ic_home/image.png')} style={{ height: 30, width: 30, marginLeft: 10, resizeMode: 'center' }} />
                                                    </View>
                                                    <Text style={{ fontFamily: 'RubikMedium', fontSize: 10, color: '#747474', textAlign: 'center' }}>INVOICE</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress = {()=> this.setState({showOptions:true,choice:'product'})}
                                                    style={{
                                                        backgroundColor: "#e6e8e9", borderStyle: 'dashed', marginLeft: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, borderRadius: 5,
                                                        borderWidth: 0.5, flex: 1, justifyContent: 'flex-start', alignItems: 'center', padding: 10
                                                    }}>
                                                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', marginBottom: 5 }}>
                                                        <Image source={require('../../assets/ic_home/pdf.png')} style={{ height: 30, width: 30, resizeMode: 'center' }} />
                                                        <Image source={require('../../assets/ic_home/image.png')} style={{ height: 30, width: 30, marginLeft: 10, resizeMode: 'center' }} />
                                                    </View>
                                                    <Text style={{ fontFamily: 'RubikMedium', fontSize: 10, color: '#747474', textAlign: 'center' }}>PRODUCT IMAGE</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress = {()=> this.setState({showOptions:true,choice:'warranty'})}
                                                    style={{
                                                        backgroundColor: "#e6e8e9", borderStyle: 'dashed', marginLeft: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, borderRadius: 5,
                                                        borderWidth: 0.5, flex: 1, justifyContent: 'flex-start', alignItems: 'center', padding: 10
                                                    }}>
                                                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', marginBottom: 5 }}>
                                                        <Image source={require('../../assets/ic_home/pdf.png')} style={{ height: 30, width: 30, resizeMode: 'center' }} />
                                                        <Image source={require('../../assets/ic_home/image.png')} style={{ height: 30, width: 30, marginLeft: 10, resizeMode: 'center' }} />
                                                    </View>
                                                    <Text style={{ fontFamily: 'RubikMedium', fontSize: 10, color: '#747474', textAlign: 'center' }}>WARRANTY CARD</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity
                                                    onPress = {()=> this.setState({showOptions:true,choice:'other'})}
                                                    style={{
                                                        backgroundColor: "#e6e8e9", borderStyle: 'dashed', marginLeft: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, borderRadius: 5,
                                                        borderWidth: 0.5, flex: 1, justifyContent: 'flex-start', alignItems: 'center', padding: 10
                                                    }}>
                                                    <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', marginBottom: 5 }}>
                                                        <Image source={require('../../assets/ic_home/pdf.png')} style={{ height: 30, width: 30, resizeMode: 'center' }} />
                                                        <Image source={require('../../assets/ic_home/image.png')} style={{ height: 30, width: 30, marginLeft: 10, resizeMode: 'center' }} />
                                                    </View>
                                                    <Text style={{ fontFamily: 'RubikMedium', fontSize: 10, color: '#747474', textAlign: 'center' }}>OTHER {this.state.fileList.length > 0 ? 'MORE' : ''} FILES</Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>
                                    </View>

                                </KeyboardAvoidingView>
                                <TouchableOpacity
                                    onPress={() => { this.uploadCustomerProduct(); }}
                                    style={{...styles.updateButton, marginBottom:Platform.OS=='ios'?30:0}}
                                >
                                    <Image
                                        source={require('../../assets/ic_home/next.png')}
                                        style={{ resizeMode: 'contain', height: 70, }}
                                    />
                                </TouchableOpacity>
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={this.state.showOptions}
                                    onRequestClose={() => {this.setState({choice:''})}}>
                                    <TouchableOpacity onPress = {()=> this.setState({showOptions:false})} style={{flex:1, backgroundColor:'rgba(0,0,0,0.5)', justifyContent:'flex-end', alignItems:'stretch'}}>
                                        <View style={{ height:100, flexDirection:'row', backgroundColor:'white', justifyContent:'space-around', alignItems:'center', }} >
                                            <TouchableOpacity onPress = {() => this.chooseFile(this.state.choice.toLowerCase())} style={{ margin:10, justifyContent:'center', alignItems:'center'}}>
                                                <Image source={require('../../assets/ic_home/camera.png')} style={{height:50, width:50, resizeMode:'center'}}/>
                                                <Text>Camera</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress = {() => this.requestStoragePermission(this.state.choice.toLowerCase())} style={{ margin:10, justifyContent:'center', alignItems:'center'}}>
                                                <Image source={require('../../assets/ic_home/folder.png')} style={{height:50, width:50, resizeMode:'center'}}/>
                                                <Text>Document</Text>
                                            </TouchableOpacity>
                                            
                                        </View>
                                    </TouchableOpacity>
                                    
                                </Modal>
                            </View>
                            </ScrollView> :
                            <ScrollView style={{flex:1, paddingTop: Platform.OS == 'ios'? 30:0}}>
                                <View >
                                {/* Page header start screen2*/}
                                <View style={{ height: 200, width: '100%', overflow: 'visible', marginTop: StatusBar.currentHeight, alignSelf: 'center', justifyContent: 'flex-start' }}>
                                    <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                                        angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45}
                                        style={{
                                            backgroundColor: 'purple',
                                            alignSelf: 'center',
                                            top: -920,
                                            height: 1100,
                                            width: 1100,
                                            borderRadius: 550,
                                            position: 'absolute'
                                        }}
                                    >
                                    </LinearGradient>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5, paddingTop: 0, }}>
                                        <TouchableOpacity onPress={() => this.setState({ screen2: false,})} style={{ zIndex: 1 }}>
                                            <Image source={require('../../assets/ic_home/back.png')} style={{ height: 30, width: 30, }} />
                                        </TouchableOpacity>
                                        <Text style={{ flex: 1, color: '#e9eaff', fontFamily: 'RubikMedium', fontSize: 20, textAlign: 'center', marginLeft: -30 }}> INVOICE DETAILS</Text>
                                    </View>
                                    <View style={{ alignSelf: 'center', flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5, paddingTop: 10, }}>
                                        {/* <TouchableOpacity onPress={() => this.setState({ scan: true, scanner: true, manually: false })}
                                            style={{ ...styles.invoiceBtn, backgroundColor: this.state.scan ? '#4325f4' : 'transparent' }}>
                                            <Image source={this.state.scan ? require('../../assets/ic_home/sbarcode_scan.png') : require('../../assets/ic_home/barcode_scan.png')}
                                                style={{ height: 40, width: 40, marginBottom: 5, resizeMode: 'contain' }} />
                                            <Text style={{ fontFamily: 'RubikMedium', color: this.state.scan ? '#5dffff' : 'white', fontSize: 11, textAlign: 'center', }}>SCAN {'\nAND AUTOFILL'}</Text>
                                        </TouchableOpacity> */}
                                        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ scan: false, manually: true })}
                                            style={{ ...styles.invoiceBtn, backgroundColor: this.state.manually ? '#4325f4' : 'transparent' }}>
                                            <Image source={this.state.manually ? require('../../assets/ic_home/sbarcode_scan.png') : require('../../assets/ic_home/barcode_scan.png')}
                                                style={{ height: 40, width: 40, marginBottom: 5, resizeMode: 'contain' }} />
                                            <Text style={{ fontFamily: 'RubikMedium', color: this.state.manually ? '#5dffff' : 'white', fontSize: 11, textAlign: 'center',flexWrap:'wrap' }}>ENTER {'\n MANUALLY'}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* End of Page header screen2*/}
                                {/* Input area start */}
                                <KeyboardAvoidingView style={{ ...styles.inputContainer }}>
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ marginBottom: 5, fontFamily: 'RubikRegular', fontSize: 15 }}>
                                            Dealer Name*
                            </Text>
                                        <TextInput
                                            autoCorrect={false}
                                            style={ {...styles.inputBox, height:40}}
                                            secureTextEntry={false}
                                            contextMenuHidden
                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value => this.changeTextField(value, 'dealerName')}
                                            value={this.state.dealerName}
                                            ref={input => this.model = input}
                                        />
                                    </View>
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ marginBottom: 5, fontFamily: 'RubikRegular', fontSize: 15 }}>
                                            Dealer Address*
                            </Text>
                                        <TextInput
                                            autoCorrect={false}
                                            onContentSizeChange={(e) => this.updateSize(e.nativeEvent.contentSize.height)}
                                            style={{ ...styles.addInputBox, height: this.state.height }}
                                            secureTextEntry={false}
                                            multiline={true}
                                            numberOfLines={4}
                                            contextMenuHidden
                                            textContentType="streetAddressLine1"
                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value => this.changeTextField(value, 'dealerAdd')}
                                            value={this.state.dealerAdd}
                                            ref={input => this.model = input}
                                        />
                                    </View>
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ marginBottom: 5, fontFamily: 'RubikRegular', fontSize: 15 }}>
                                            Invoice Number*
                            </Text>
                                        <TextInput
                                    
                                            autoCorrect={false}
                                            style={{height:40,...styles.inputBox}}
                                            secureTextEntry={false}
                                            contextMenuHidden
                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value => this.changeTextField(value, 'invoiceNum')}
                                            value={this.state.invoiceNum}
                                            ref={input => this.model = input}
                                        />
                                    </View>
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ marginBottom: 5, fontFamily: 'RubikRegular', fontSize: 15 }}>Invoice Date*</Text>
                                        <View style={styles.inputBoxDate}>
                                            <DatePicker
                                                style={{ width: '100%' }}
                                                date={this.state.invoiceDate}
                                                mode="date"
                                                format="DD/MM/YYYY"
                                                placeholder=" "
                                                maxDate={new Date()}
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                iconSource={require('../../assets/ic_home/cal.png')}
                                                customStyles={{
                                                    dateIcon: {
                                                        position: 'absolute',
                                                        right: 5,
                                                        top: 10,
                                                        height: 15,
                                                        width: 15,
                                                        marginLeft: 0
                                                    },
                                                    dateInput: {
                                                        borderWidth: 0
                                                    },
                                                    placeholderText: {
                                                        fontSize: 15,
                                                        color: '#707171',
                                                        padding: 10,
                                                        alignSelf: 'flex-start'
                                                    },
                                                    dateText: {
                                                        fontSize: 15,
                                                        padding: 10,
                                                        alignSelf: 'flex-start'
                                                    }
                                                }}
                                                onDateChange={(date) => {
                                                    // let filteredValues = this.state.models.filter((val) => val.model_name == this.state.modelNum)
                                                    // let warranty_period = this.state.models.length ? filteredValues.length && parseInt(filteredValues[0].warranty_period.split(' ')[0]) : 12;
                                                    // let invoiceDate = date.split('/');
                                                    // let warrantyDate =   invoiceDate[1] + '/' + invoiceDate[0] + '/' + (parseInt(invoiceDate[2]) + parseInt(warranty_period / 12)) 
                                                    // warrantyDate = new Date(warrantyDate);
                                                    // warrantyDate = warrantyDate.setTime(warrantyDate.getTime()-86400000);
                                                    // let dt = new Date(warrantyDate);
                                                    // let day = parseInt(dt.getDate());
                                                    // let month = parseInt(dt.getMonth())+1;
                                                    // let year = dt.getFullYear();
                                                    // warrantyDate = (day<10?('0'+day):day) + '/' + (month<10?('0'+month):month) + '/' + year;
                                                   
                                                    this.setState({
                                                        invoiceDate: date, IDupdated: true,
                                                        // warrantyDate: warrantyDate
                                                    });
                                                }
                                                }
                                            />
                                        </View>
                                    </View>
                                { this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())==-1?
                                    <View style={{ marginBottom: 10 }}>
                                        <Text style={{ marginBottom: 5, fontFamily: 'RubikRegular', fontSize: 15 }}>
                                            Warranty Expiry Date{ this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())==-1?'*':''}
                            </Text>
                                        <View style={styles.inputBoxDate}>
                                            <DatePicker
                                                style={{ width: '100%' }}
                                                date={this.state.warrantyDate}
                                                mode="date"
                                                format="DD/MM/YYYY"
                                                placeholder=" "
                                                confirmBtnText="Confirm"
                                                cancelBtnText="Cancel"
                                                iconSource={require('../../assets/ic_home/cal.png')}
                                                customStyles={{
                                                    dateIcon: {
                                                        position: 'absolute',
                                                        right: 5,
                                                        top: 10,
                                                        height: 15,
                                                        width: 15,
                                                        marginLeft: 0
                                                    },
                                                    dateInput: {
                                                        borderWidth: 0
                                                    },
                                                    placeholderText: {
                                                        fontSize: 15,
                                                        color: '#707171',
                                                        padding: 10,
                                                        alignSelf: 'flex-start'
                                                    },
                                                    dateText: {
                                                        fontSize: 15,
                                                        padding: 10,
                                                        alignSelf: 'flex-start'
                                                    }
                                                }}
                                                onDateChange={(date) => { this.setState({ warrantyDate: date, WXupdated: true }) }}
                                            />
                                        </View>
                                    </View>:null
                               }
                                </KeyboardAvoidingView>
                                {/* Input area end */}
                                <TouchableOpacity
                                    onPress={() => {
                                        if (this.checkValidity(2)) {
                                            this.setState({ screen3: true, }, async () => {
                                                setTimeout(()=>{
                                                    // if(!this.state.editInfo)
                                                        this.getArea();
                                                    if(this.state.editInfo && !this.state.mappedThirdScreen){
                                                        this.mapThirdScreenData();
                                                    }
                                                }, 100);
                                                
                                            })
                                        }

                                    }
                                    }
                                    style={styles.updateButton}
                                >
                                    <Image
                                        source={require('../../assets/ic_home/next.png')}
                                        style={{ resizeMode: 'contain', height: 70, }}
                                    />
                                </TouchableOpacity>
                            </View></ScrollView> :
                            <ScrollView style={{flex:1, paddingTop: Platform.OS == 'ios'? 30:0}}>
                            <View>
                                <View style={{
                                    height: 90, backgroundColor: 'transparent', width: '100%',
                                    marginTop: StatusBar.currentHeight, alignSelf: 'center', justifyContent: 'flex-start'
                                }}>
                                    <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                                        angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                                            backgroundColor: 'purple',
                                            alignSelf: 'center',
                                            top: -1030,
                                            height: 1100,
                                            width: 1100,
                                            borderRadius: 550,
                                            position: 'absolute'
                                        }}
                                    >
                                    </LinearGradient>
                                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5, paddingTop: 0, }}>
                                        <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{ zIndex: 1 }}>
                                            <Image source={require('../../assets/ic_home/back.png')} style={{ height: 30, width: 30, }} />
                                        </TouchableOpacity>
                                        <Text style={{ flex: 1, color: '#e9eaff', fontFamily: 'RubikMedium', fontSize: 20, textAlign: 'center', marginLeft: -30 }}> {this.state.editInfo?'UPDATE':'ADD'} APPLIANCE</Text>
                                    </View>
                                </View>

                                {/* Main Content Screen 1 */}
                                <KeyboardAvoidingView style={{ ...styles.inputContainer }}>

                                <TouchableOpacity disabled = {!this.state.updateAllowed} activeOpacity={1} onPress={()=>this.brandSelector.show()} 
                                    style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, padding:5  }}>
                                    <ModalDropdown
                                        disabled = {!this.state.updateAllowed}
                                        style={{backgroundColor:'white', flex:1, borderRadius:25}}
                                        ref={ (ref) => this.brandSelector = ref}
                                        defaultValue={this.state.brandSelected?this.state.brandSelected:'Select Brand'}
                                        onSelect={(rowData,index) =>{ 
                                            let brand_id = this.state.brand.filter((element)=>element.brand_name==brands[rowData])[0].brand_id;
                                                if (brand_id != '0') {
                                                    this.setState({ modelNum: '', serialNum: '', filterValues: [], brandSelected:brands[rowData] });
                                                    this.setValue(1, 'brand', brand_id);
                                                    this.groupSelector.select(-1);
                                                    this.unsetValue(1, 'group');
                                                    this.subGroupSelector.select(-1);
                                                    this.unsetValue(1, 'subgroup');
                                                    this.getCategory(brand_id);
                                                }
                                            }}
                                        options={brands}
                                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                        textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                        adjustFrame={style => this.genFrame(style, brands.length)}
                                        dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:Platform.OS=='ios'?0:-15 }}
                                        dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                        />
                                    <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                </TouchableOpacity>
                                <TouchableOpacity disabled = {!this.state.updateAllowed} activeOpacity={1} onPress={()=>this.groupSelector.show()} 
                                    style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,padding:5  }}>
                                    <ModalDropdown
                                    disabled = {!this.state.updateAllowed}
                                    style={{backgroundColor:'white', flex:1, borderRadius:25}}
                                        ref={ (ref) => this.groupSelector = ref}
                                        defaultValue={this.state.groupSelected?this.state.groupSelected:'Select Category'}
                                        onSelect={(rowData,index) =>{ 
                                            let group_id = this.state.group.filter((element)=>element.product_name==groups[rowData])[0].product_group_id;
                                                this.setState({ modelNum: '', serialNum: '', filterValues: [], groupSelected:groups[rowData] })
                                                if (group_id != '0') {
                                                    this.setValue(1, 'group', group_id);
                                                    this.subGroupSelector.select(-1);
                                                    this.unsetValue(1, 'subgroup');
                                                    this.getAppliance(group_id);
                                                }
                                            }}
                                        options={groups}
                                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                        textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                        adjustFrame={style => this.genFrame(style, groups.length)}
                                        dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:Platform.OS=='ios'?0:-15  }}
                                        dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                        />
                                    <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                </TouchableOpacity>
                                <TouchableOpacity disabled = {!this.state.updateAllowed} activeOpacity={1} onPress={()=>this.subGroupSelector.show()} 
                                    style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,padding:5  }}>
                                    <ModalDropdown
                                    disabled = {!this.state.updateAllowed}
                                    style={{backgroundColor:'white', flex:1, borderRadius:25,}}
                                        ref={ (ref) => this.subGroupSelector = ref}
                                        defaultValue={this.state.subgroupSelected?this.state.subgroupSelected:'Select Appliance'}
                                        onSelect={(rowData,index) =>{ 
                                            let sub_group_id = this.state.subgroup.filter((element)=>element.sub_product_name==sub_groups[rowData])[0].sub_product_group_id;
                                                if (sub_group_id != '0') {
                                                    this.setState({ modelNum: '', serialNum: '', filterValues: [], subgroupSelected:sub_groups[rowData] })
                                                    this.setValue(1, 'subgroup', sub_group_id);
                                                    this.getModels(sub_group_id);
                                                }
                                            }}
                                        options={sub_groups}
                                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                        textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                        adjustFrame={style => this.genFrame(style, sub_groups.length)}
                                        dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:Platform.OS=='ios'?0:-15   }}
                                        dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                        />
                                    <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                                </TouchableOpacity>
                                    <View style={{ ...styles.greyView }}>

                                        <TouchableOpacity disabled = {!this.state.updateAllowed} onPress={() => this.setState({ scanner: 'model' })} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 10, }}>
                                            <Image source={require('../../assets/ic_home/qr_scan.png')} style={{ height: 20, width: 20, marginRight: 10, }} />
                                            <Text style={{ fontFamily: 'RubikMedium', textAlignVertical: 'center', fontSize: 14, textAlign: 'center' }}>Scan Model Number</Text>
                                        </TouchableOpacity>

                                        <Text style={{ fontFamily: 'RubikMedium', fontSize: 14, marginBottom: 10, textAlign: 'center' }}>Or</Text>
                                       <View style={{flex:1, flexDirection:'row', alignItems:'center', backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7, borderRadius:30}}>
                                        <TextInput
                                                editable = {this.state.updateAllowed}
                                                autoCorrect={false}
                                                style={{  height:40, flex:1,color:'#707171', borderRadius:30,  fontFamily:'RubikRegular',fontSize:15,paddingHorizontal:10, textAlign: 'center' }}
                                                secureTextEntry={false}
                                                contextMenuHidden={true}
                                                placeholder="Enter Manually"
                                                //  onEndEditing={()=>this.alterNum.focus()}
                                                onChangeText={value => {
                                                    this.setState({ modelNum: value }, () => {
                                                        // this.state.brand[this.state.selected[0].val].toLowerCase()=='E-warranty' && 
                                                        if (this.state.models.length && this.state.modelNum != '') {
                                                            this.setState({ filterValues: this.state.models.filter((item) => item.model_name.toLowerCase().indexOf(this.state.modelNum.toLowerCase()) != -1) },()=>{
                                                            })
                                                        }
                                                    })
                                                }
                                                }
                                                value={this.state.modelNum}
                                                ref={input => this.model = input}
                                            />
                                           { ((this.state.filterValues.length && this.state.filterValues.length == 1 && this.state.filterValues.filter((val) => val == this.state.modelNum).length == 0)
                                                || this.state.filterValues.length && (this.state.filterValues.length != 1)) && this.state.modelNum && this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())!=-1?
                                                <TouchableOpacity onPress={()=>Keyboard.dismiss()} style={{height:'100%',width:20, justifyContent:'center', alignItems:'center', marginRight:5}}>
                                                    <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'center',}}/>
                                                </TouchableOpacity>
                                                
                                            :null}
                                       </View>
                                       
                                        {
                                            ((this.state.filterValues.length && this.state.filterValues.length == 1 && this.state.filterValues.filter((val) => val == this.state.modelNum).length == 0)
                                                || this.state.filterValues.length && (this.state.filterValues.length != 1)) && this.state.modelNum && this.state.panasonicBrands.indexOf(this.state.brandSelected.toLowerCase())!=-1?
                                                <View style={{ flex:1, backgroundColor: 'white',  alignItems: 'stretch', marginTop: 10, borderRadius: 10, padding: 5, height:'auto', maxHeight:200,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5  }} >
                                                    <ScrollView showsVerticalScrollIndicator={true} nestedScrollEnabled={true}>
                                                    {
                                                        this.state.filterValues.map((item, index) =>
                                                            <TouchableOpacity key={index} onPress={() => {
                                                                this.setState({
                                                                    modelNum: item.model_name, warrantyPeriod: item.warranty_period,
                                                                    filterValues: [item.model_name]
                                                                })
                                                            }} style={{ flex:1, padding:7.5, borderBottomColor:'#c2c4c4', borderBottomWidth:1}}>
                                                                <Text style={{ fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'center', }} >
                                                                    {item.model_name}
                                                                </Text></TouchableOpacity>)
                                                    }
                                                    </ScrollView>

                                                </View> : null
                                        }
                                    </View>
                                    <View style={{ ...styles.greyView }}>
                                        <TouchableOpacity disabled = {!this.state.updateAllowed} onPress={() => { this.setState({ scanner: 'serial' }) }} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginBottom: 10, }}>
                                            <Image source={require('../../assets/ic_home/qr_scan.png')} style={{ height: 20, width: 20, marginRight: 10 }} />
                                            <Text style={{ fontFamily: 'RubikMedium', textAlignVertical: 'center', fontSize: 14, textAlign: 'center' }}>Scan Serial Number</Text>
                                        </TouchableOpacity>

                                        <Text style={{ fontFamily: 'RubikMedium', fontSize: 14, marginBottom: 10, textAlign: 'center' }}>Or</Text>
                                        <TextInput
                                            editable = {this.state.updateAllowed}
                                            textContentType="none"
                                            text
                                            style={{ ...styles.inputBox, textAlign: 'center',height:40, }}
                                            secureTextEntry={false}
                                            contextMenuHidden
                                            maxLength={20}
                                            placeholder="Enter Manually"

                                            //  onEndEditing={()=>this.alterNum.focus()}
                                            onChangeText={value =>{ this.changeTextField(value, 'serialNum');}}
                                            value={this.state.serialNum}
                                            ref={input => this.serial = input}
                                        />
                                        {this.state.invalid_serial == 1 ?
                                            <Text style={{ fontFamily: 'RubikRegular', textAlign: 'center', fontSize: 12, marginTop: 5, color:'red', textDecorationLine:'underline' }}>Given Serial Number doesn't exist. Change the serial number or continue.</Text>
                                            : null}
                                    </View>

                                </KeyboardAvoidingView>
                                <TouchableOpacity
                                    onPress={() => {this.checkValidity(1);}}
                                    style={styles.updateButton}
                                >
                                    <Image
                                        source={require('../../assets/ic_home/next.png')}
                                        style={{ resizeMode: 'contain', height: 70, }}
                                    />
                                </TouchableOpacity>
                            </View>
                            </ScrollView>
                   }

                </View>


            );

    }
}

