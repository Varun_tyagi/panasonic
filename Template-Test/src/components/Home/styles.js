import { StyleSheet } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
    marginRight:10,
    marginLeft:10
  },
  linearGradient: {
    flex: 1,
    borderRadius: 5,
    marginRight:10,
    padding:10,
    paddingTop:0,
    margin:10,
    justifyContent:'center',
    alignItems:'center'
    // height:160
  },
  linearGradientSelected: {
    flex: 1,
    borderRadius: 5,
    marginRight:10,
    borderWidth:10,
    padding:10,
    paddingTop:0,
    margin:10,
    justifyContent:'center',
    alignItems:'center'
    // height:160
  },
  ppContainer:{
    flex:1,
    justifyContent:'flex-start',
    alignItems:'center',
    color:'white',
    padding:10
  },
  headerText:{
    fontFamily:'RubikBold',
    fontSize:20,
    marginTop:50,
    color:'white',
    textAlign:'center',
    marginHorizontal:10
  },
  subHeaderText:{
    fontFamily:'RubikRegular',
    fontSize:16,
    textAlign:'center',
    color:'white',
    marginTop:10,
    marginHorizontal:5
    
  },
  productView:{
    flex:1,
    justifyContent:'flex-start',
    backgroundColor:'#ebebeb',
    marginTop:22,
    padding:23,
    borderRadius:5,
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:8,
    marginHorizontal:10

  },
  boldAnswer:{
    fontFamily:'RubikBold',
    fontSize:15
  },
  regularKey:{
    fontFamily:'RubikRegular',
    fontSize:15
  },
  footerBtn:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    height:70,
    marginTop:60,
    marginHorizontal:30
  },
  inputContainer:{ 
    flex:1, 
    justifyContent:'flex-start', 
    alignItems:'stretch',
    marginTop:10, 
    backgroundColor: 'transparent',
    marginHorizontal:20,
    
  },
  inputContainerScreen3:{ 
    flex:1, 
    justifyContent:'flex-start', 
    alignItems:'stretch',
    marginTop:10, 
    backgroundColor: 'transparent',
  },
  updateButton:{
    flex:0.2,
    justifyContent:'center', 
    alignItems:'center',
    alignSelf:'center',
    width:70 
  },
  inputBox:{  
    backgroundColor:'white', 
    color:'#707171', 
    borderRadius:30,  
    fontFamily:'RubikRegular',
    fontSize:15,
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7, 
    paddingHorizontal:10, 
  },
  documentInputBox:{  
    backgroundColor:'white', 
    color:'#707171', 
    borderRadius:30, 
    textAlign:'left', 
    fontFamily:'RubikRegular',
    fontSize:15,
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7, 
    padding:10, 
    marginBottom:15,
    paddingHorizontal: 10,
  },
  addInputBox:{  
    backgroundColor:'white', 
    color:'#707171', 
    borderRadius:10, 
    textAlign:'left',
    textAlignVertical:'top', 
    fontFamily:'RubikRegular',
    fontSize:15,
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7, 
    padding:10, 
    // height:100,
    // marginBottom:15,
    paddingHorizontal: 10,
  },
  greyView:{
    backgroundColor: '#d9dbdc',
    borderRadius: 15,
    textAlign: 'left',
    padding:20,
    fontSize: 15,
    color: '#707171',
    marginBottom: 15,

  },
  invoiceBtn:{
    justifyContent:'center',
    // marginLeft:25,
    borderRadius:15,
    padding:10, 
    alignItems:'center',
    height:110, 
    width:120
  },
  inputBoxDate:{  
    backgroundColor:'white', 
    color:'#707171', 
    borderRadius:30, 
    textAlign:'left',
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7,  
    padding:5, 
    marginBottom:15, 
    borderWidth:0, 
    flex:1, 
    alignItems:'stretch',
    flexDirection:'column', 
    justifyContent:'center'},
  radioBtnOuter:{
    height:20, 
    width:20, 
    borderRadius:10,
    justifyContent:'center', 
    alignItems:'center', 
    marginRight:5,
    backgroundColor:'white', 
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:10},
  radioBtnInner:{
    height:10, 
    width:10, 
    borderRadius:5, 
    backgroundColor:'#5960e5'
  },
  checkBox:{
    height:20, 
    width:20, 
    borderRadius:10,
    justifyContent:'center', 
    alignItems:'center', 
    alignSelf:'center',
    backgroundColor:'#b785f8', 
    shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:6,
    overflow:'hidden'
  },
    checkBoxImage:{
      height:25, 
      width:25, 
      borderRadius:12.5,
      justifyContent:'center', 
      alignSelf:'center', 
      marginLeft:5,
      marginTop:5,
      resizeMode:'center'
    },
});


export default styles;
