import React, { Component } from 'react';
import {
  View,
  Text,
  StatusBar, Platform,
  Image, ScrollView, ImageBackground, TouchableOpacity, TouchableHighlight, FlatList, AsyncStorage, ActivityIndicator,
  Dimensions,Modal
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, Button, Card, Avatar } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import strings from '../../localization';
import getUser from '../../selectors/UserSelectors';
import Swiper from 'react-native-swiper';
import NavigationService from '../navigation/NavigationService';
import ProductScreen from './PreviousProducts';
import * as constants from '../../helpers/constants';
import LinearGradient from 'react-native-linear-gradient';
import AddAppliance from './addAppliance';
import * as authFun from '../../helpers/auth';
import Loader from '../common/Loader';
import { drawer } from '../navigation/AppNavigator';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Geocode from 'react-geocode';
import axios from 'react-native-axios';


const data = [
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something"
  },
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something two"
  },
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something three"
  },
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something four"
  },
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something five"
  },
  {
    imageUrl: "http://via.placeholder.com/160x160",
    title: "something six"
  }
];

// const softkeys = NativeModules.SoftKeyDetectorModule.height;
class Home extends Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   data: data,
    // };

    this.state = {
      loading: true,
      addRemPrevProd: false,
      BannerdataSource: [],
      MyApplianceData: [],
      MyAccessoriesData: [],
      customerID: '',
      mNum: '',
      showFinalList: false,
      animating: false,
      productsList: '',
      delList: [],
      addList: [],
      showAppModal: false,
      showAppliance: false,
      profile_image:'',
      customerProducts: [],
      showAddView: false,
      showAddAppModal: false,
      productsListLength:0,
      token: '',
      name:'',
      activeSlide: 0,
      CartItemCount:0,
    };
    this.loadData = this.loadData.bind(this);
    this.fetchHomeData = this.fetchHomeData.bind(this);
    this.delApp = this.delApp.bind(this);
    this.sendFinalList = this.sendFinalList.bind(this);
    this.closeAddAppModal = this.closeAddAppModal.bind(this);
    this.setToken = this.setToken.bind(this);
    this.productAdded = this.productAdded.bind(this);
  }

  saveData() {
    let name = "Michal";
    AsyncStorage.setItem('user', name);
  }

  productAdded = async () => {
    const result = await AsyncStorage.getItem('productAddedHome');
    if (result == 1) {
      await AsyncStorage.removeItem('productAddedHome');
      return true;
    } else {
      return false;
    }
  }

  displayData = async () => {
    try {
      let name = "Michal";
      AsyncStorage.setItem('user', name);
      let user = await AsyncStorage.getItem('user');
      // alert(user);  
    }
    catch (error) {
      alert(error)
    }
  }


  static navigationOptions = {
    //headerVisible: false,
    header: null,
  };

  getMessage = () => {
    const { user } = this.props;
    return `${strings.homeMessage} ${user && user.name}`;
  };

  onBellAction = () => {

  };

  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      global.profile_image = data.profile_image;
      this.setState({
        name: data.name ? data.name.split(' ')[0] : '',
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
        profile_image:data.profile_image?data.profile_image:null,
        loading: false,
      }, () => {
        this.getCartItemCount();
        this.fetchHomeData();
      });
    } else {
      this.setState({ loading: false });
    }
  }

  modifyList = (data, opr) => {
    tempA = this.state.productsList;
    tempD = this.state.delList;
    if (opr == 'add') {
      tempA.push(id);
    } else {
      tempD.push(id);
      this.setState({ del: tem })
    }
  }

   fetchAppliancesData = async () => {
    await this.setToken();
    var url = constants.base_url + 'getUserAppliance';
    url = url + "?access_token=" + this.state.token + '&mobile=' + this.state.mNum;
    // fetch("http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/getUserAppliance?mobile="+this.state.mNum,
    fetch(url,
      { headers: { 'Content-Type': 'application/json' } })

      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.status) {
          this.setState({
            productsList: responseJson.data,
            showAddView: true,
            loading: false
          },()=>this.setState({productsListLength:responseJson.data.length,}));
        } else {
          this.setState({ loading: false });
        }

      })
      .catch((error) =>{
        NetInfo.fetch().then(async (state)=>{
          if(!state.isConnected){
            alert('Please check your internet connection.');
          }else{
            alert(error);
          }
        });
        this.setState({loading:false})
      }) 
  }


  delApp = (ind) => {
    var tempP = this.state.productsList;
    var tempD = this.state.delList;
    var tempA = this.state.addList;
    var deletedProd = tempP.splice(ind, 1)[0];
    if (tempA.indexOf(deletedProd) != -1)
      tempA.splice(tempA.indexOf(deletedProd), 1);
    tempD.push(deletedProd);
    this.setState({ delList: [], addList: [] }, () => {
      this.setState({ productsList: tempP, delList: tempD, addList: tempA, }, () => this.setState({ confirmAll: tempA.length + this.state.delList.length == this.state.productsListLength }));
    });
    // return tempA.length + tempD.length == this.state.productsListLength;

  }

  addApp = (ind) => {
    var tempP = this.state.productsList;
    var tempA = this.state.addList;
    var addedProd = tempP.splice(ind, 1)[0];
    tempA.push(addedProd);
    this.setState({ addList: [] }, () => {
      this.setState({ addList: tempA, productsList: tempP, }, () => this.setState({ confirmAll: tempA.length + this.state.delList.length == this.state.productsListLength }));
    })

  }
  sendFinalList = async () => {
    await this.setToken();
    this.setState({loading:true});
    var fData = new FormData();
    if (this.state.addList) {
      var add = this.state.addList.map((prod) => {
        return (prod.id);
      });
    } else {
      var add = [];
    }

    if (this.state.delList.length) {
      var del = this.state.delList.map((prod) => {
        return (prod.id);
      });
    } else {
      var del = [];
    }

    fData.append('accepted_product', JSON.stringify(add));
    fData.append('rejected_product', JSON.stringify(del));
    // alert(JSON.stringify(fData));
    var url = constants.base_url + 'customerFinalProduct';
    url = url + "?access_token=" + this.state.token;
    // fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/customerFinalProduct', 
    fetch(url, {
      method: "POST",
      headers: { "Content-Type": "multipart/form-data" },
      body: fData,
    }).then(res => res.json())
      .then((result) => {
        if (result.status) {
          this.fetchHomeData();
          this.setState({ showAppModal: false, confirmAll:false, loading:false });
        } else {
          alert(result.message);
          this.setState({ loading: false })
        }

      },
        (error) => {
          alert(JSON.stringify(error));
          this.setState({ loading: false, verifyANum: false })
        }
      );
  }

  async fetchHomeData() {
    await this.setToken();
    this.setState({ loading: true }, () => {
      var url = constants.base_url + 'gethomeData';
      url = url + "?access_token=" + this.state.token + '&customer_id=' + this.state.customerID;
      fetch(url, { headers: { 'Content-Type': 'application/json' } })

        .then(response => response.json())
        .then(async (responseJson) => {
          if (responseJson.status) {
            await AsyncStorage.setItem('file_max_size', responseJson.file_max_size);
            this.setState({
              BannerdataSource: responseJson.banners,
              MyApplianceData: responseJson.customer_products,
              MyAccessoriesData: responseJson.accessories,
              customerProducts: responseJson.customer_products,
              showAppliance: true
            }, () => {
              global.myApplianceCount = this.state.customerProducts.length;
              if (!this.state.customerProducts.length)
                this.fetchAppliancesData();
              else
                {this.setState({ loading: false });}
            })
          } else {

            this.setState({ loading: false })
          }

        })
        .catch((error) =>{
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          })
          this.setState({loading:false})
        })   
    })

  }
  componentWillReceiveProps(props) {
    // if (props.navigation.state.isDrawerOpen) {
    //   // alert('hello');
    // }
  }
  async componentDidMount() {

    this.props.navigation.addListener('willFocus', async () => {     
      var update = await this.productAdded();
      this.getCartItemCount();
        this.fetchHomeData();
    });
    var token = await this.setToken();
    if (token) {
      this.loadData();
      
    }

  }
  setToken = async () => {
    var token = await authFun.checkAuth();
    this.setState({ token: token })
    return token;
  }

  closeActivityIndicator = () => setTimeout(() => this.setState({
    animating: false
  }))

  closeAddAppModal = () => {
    // alert('closed')
    this.setState({ showAddAppModal: false });
  }
  get pagination() {
    const {BannerdataSource, activeSlide} = this.state;
    return (<Pagination dotsLength={BannerdataSource.length} activeDotIndex={activeSlide} 
      dotStyle={{
        width: 10,
        height: 10,
        borderRadius: 5,
        borderWidth:1,
        borderColor:'white',
        backgroundColor: 'white'
      }} inactiveDotStyle={{
        width: 10,
        height: 10,
        borderWidth: 1,
        backgroundColor: "transparent",
        borderColor: 'white',}}
        dotContainerStyle={{marginRight:0}} inactiveDotOpacity={1} inactiveDotScale={1} />);
  }
  openDetail(item) {
    console.warn(item);
    this.props.navigation.navigate('Detail', {selectedItem: item});
  }

  async getCartItemCount() {
    await this.setToken();
    var url = constants.base_url+'/getCartCount?access_token='+this.state.token;
    url = url + "&customer_id="+this.state.customerID;
    axios.get(url).then((response) => {
      this.setState({CartItemCount: response.data.data});
      global.cartItemsCount = response.data.data;
    }).catch((error) => {
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){ 
        alert('Please check your internet connection.');
      }else{
        alert("Error : "+error);
      }
    })
      this.setState({gettingData:false, ShowAlert:false})
      //this.setState({gettingData: false});
    });
  }
  render() {
    if (this.state.confirmAll) {
      return (
        
        <ScrollView contentContainerStyle={{ alignItems: 'flex-start', backgroundColor: 'transparent', }}>
          <View style={{ height: 100,flex:1, paddingTop:Platform.OS=='ios'?30:0 }}>
            <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
              angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                backgroundColor: 'purple',
                alignSelf: 'center',
                top: -700,
                height: 800,
                width: 800,
                borderRadius: 400,
                position: 'absolute'
              }}
            >
            </LinearGradient>
            <View style={{
              justifyContent: 'center', flexDirection: 'row', alignItems: 'flex-start'
              , alignSelf: 'flex-start', marginTop: 10, marginLeft: 10
            }}>
              {/* <TouchableOpacity onPress={()=>this.setState({confirmAll:false})} >
                      <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30, }}/>
                  </TouchableOpacity> */}
              <Text style={{ alignSelf: 'center', textAlign: 'center', flex: 1, fontFamily: 'RubikBold', fontSize: 18, color: 'white', marginLeft: -10 }}>YOUR PRODUCT SUMMARY</Text>
            </View>
          </View>
          {this.state.addList ? this.state.addList.length ?
            <View style={{ width: '100%', padding: 35 }} >
              <Text style={{ fontFamily: 'RubikBold', fontSize: 22, marginBottom: 16 }}>{this.state.addList.length} SELECTED {this.state.addList.length > 1 ? "PRODUCTS" : "PRODUCT"}</Text>
              <FlatList
                data={this.state.addList}
                renderItem={({ item }) => {
                  return (
                    <View style={{ flexDirection: 'row', height: 95, borderRadius: 5, width: '100%', padding: 8, backgroundColor: '#caedfa', marginBottom: 8 }}>
                      {item.image?<Image source={{ uri: item.image }} style={{ height: 70, width: 70, resizeMode: 'contain' }} />:null}
                      <View style={{ padding: 12, flex: 1, alignItems: 'flex-start' }}>
                        <Text style={{ fontFamily: 'RubikBold', fontSize: 12, color: '#3875c9' }}>{item.product_name?item.product_name.toUpperCase():''}</Text>
                        <Text style={{ fontFamily: 'RubikBold', fontSize: 14, color: '#76787b' }}>{item.sub_product_name?item.sub_product_name.toUpperCase():''}</Text>
                        <Text style={{ fontFamily: 'RubikMedium', fontSize: 14, color: '#49494b' }}><Text style={{ fontFamily: 'RubikRegular' }}>Brand:</Text> eWarranty</Text>
                      </View>
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index + ''} />
            </View> : null : null
          }
          {this.state.delList ? this.state.delList.length ?
            <View style={{ width: '100%', padding: 35, paddingTop: 0 }} >
              <Text style={{ fontFamily: 'RubikBold', fontSize: 22, marginBottom: 16 }}> {this.state.delList.length} DELETED {this.state.delList.length > 1 ? "PRODUCTS" : "PRODUCT"}</Text>
              <FlatList
                data={this.state.delList}
                renderItem={({ item }) => {
                  return (
                    <View style={{ flexDirection: 'row', height: 95, borderRadius: 5, width: '100%', padding: 8, backgroundColor: '#fbd6e3', marginBottom: 8 }}>
                      {item.image?<Image source={{ uri: item.image }} style={{ height: 70, width: 70, resizeMode: 'contain' }} />:null}
                      <View style={{ flex: 1, padding: 12, alignItems: 'flex-start' }}>
                        <Text style={{ fontFamily: 'RubikBold', fontSize: 12, color: '#3875c9' }}>{item.product_name?item.product_name.toUpperCase():''}</Text>
                        <Text style={{ fontFamily: 'RubikBold', fontSize: 14, color: '#76787b' }}>{item.sub_product_name?item.sub_product_name.toUpperCase():''}</Text>
                        <Text style={{ fontFamily: 'RubikMedium', fontSize: 14, color: '#49494b' }}><Text style={{ fontFamily: 'RubikRegular' }}>Brand:</Text>E-warranty</Text>
                      </View>
                    </View>

                  );
                }}
                keyExtractor={(item, index) => index + ''} />
            </View>
            : null : null}

          <TouchableOpacity style={{ backgroundColor: 'blue', width: '80%', borderRadius: 10, shadowOffset: {
                    width: 1,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 7, marginBottom: 16, alignSelf: 'center' }}
            onPress={() => {
              this.sendFinalList();
            }
            }
          >
            <LinearGradient style={{ height: 60, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
              colors={["#ff619e", "#ff9fa6"]}
            >
              <Text style={{ fontFamily: 'RubikBold', fontSize: 17, color: 'white' }}>CONTINUE</Text></LinearGradient>
          </TouchableOpacity>
        </ScrollView>
      );
    } else
      return (

        <View style={{ flex: 1, }} >
          <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
          
          {this.state.loading ? <Loader /> : null}



          <ScrollView contentContainerStyle={{ justifyContent: 'flex-start', backgroundColor: 'white', }}>
            <View style={{ overflow:'visible', paddingTop:30}}>
              <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: -570,
                  height: 800,
                  width: 800,
                  borderRadius: 400,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 , marginTop:StatusBar.currentHeight}}>
                <TouchableOpacity onPress={() => drawer.current.open()}>
                  <Image source={require('../../assets/ic_home/align-left.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
                <View style={{flex:1, marginLeft:10, flexDirection:'row'}}>
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('Profile')} style={{ width:25, }}>
                    <Avatar
                      size={25}
                      rounded
                      activeOpacity={0.8}
                      source={{uri:global.profile_image?global.profile_image:'https://img.icons8.com/clouds/100/000000/user.png'}}
                    />
                  </TouchableOpacity>
                  
                </View>
                
                {this.state.CartItemCount?<TouchableOpacity
                  onPress={() => {this.props.navigation.navigate('MyCart');}}
                  style={{ height: 30, width: 30, alignItems: 'center', justifyContent: 'center',marginRight:5,  }}>
                    <Text style={{ color:'#ff7fa2',zIndex:1, fontSize:12, padding:2, paddingHorizontal:5,
                     position:'absolute', top:-3, right:-5, fontFamily:'RubikBold' }}>{this.state.CartItemCount}</Text>
                  <Image source={require('../../assets/cartWhite.png')} style={{ height: 20, width: 20, tintColor:'white', resizeMode:'contain' }} />
                 </TouchableOpacity>:null}

                <TouchableOpacity
                disabled={true}
                style={{ height:30, width:30, justifyContent:'center', alignItems:'center'}}
                onPress={()=>{
                      navigator.geolocation.getCurrentPosition(
                        (position) => {
                          try {
                            // Geocode.setApiKey("AIzaSyChljduE1LH_mgRv4gsCJGsCytmO4aMmkU");

                            fetch("https://maps.googleapis.com/maps/api/geocode/json?latlng="+position.coords.latitude+","+position.coords.longitude+"&key=AIzaSyChljduE1LH_mgRv4gsCJGsCytmO4aMmkU",
                              { headers: { 'Content-Type': 'application/json' } })
                        
                              .then(response => response.json())
                              .then((responseJson) => {
                                console.warn(responseJson);
                                // if (responseJson.status) {
                                // } else {
                                //   this.setState({ loading: false });
                                // }
                        
                              })
                              .catch((error) =>{
                                NetInfo.fetch().then(async (state)=>{
                                  if(!state.isConnected){
                                    alert('Please check your internet connection.');
                                  }else{
                                    alert(error);
                                  }
                                })
                                this.setState({loading:false})
                              }) 

                            // console.warn(position.coords.longitude,' : ',position.coords.latitude);
                            // Geocode.fromLatLng( position.coords.latitude, position.coords.longitude).then(
                            //   response => {
                            //     const address = response.results[0].formatted_address;
                            //     console.warn('address : ',address);
                            //   },
                            //   error => {
                            //     console.warn(error);
                            //   }
                            // );
                           } catch(err) {
                             console.warn('try block error: ',err);
                           }
                         },
                         (error) => this.setState({ error: error.message }),
                         { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
                       );
                    }
                  }>
                    
                  <Image source={require('../../assets/ic_home/bell.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
              </View>
              <View style={{ justifyContent: 'flex-start', paddingLeft: 10, marginBottom: 5 }}>
                <Text style={{ fontSize: 18, color: '#e9eaff', fontFamily: 'RubikBold' }}>Welcome {this.state.name?'back '+this.state.name+',':''}</Text>
                <Text style={{ fontSize: 14, color: '#e9eaff', fontFamily: 'RubikRegular' }}>What would you like to do today</Text>
              </View>
             
              <View style={{ height: 170 , borderRadius:5,marginBottom:10 }}>
              {this.state.BannerdataSource.length? 
                  <Swiper 
                    onIndexChanged={(index)=>this.setState({activeSlide:index})}
                    showsButtons={false} 
                    showsPagination={false}
                    loop={true} 
                    horizontal={true}
                    autoplayTimeout={3} 
                    autoplay={true}
                    >
                  {
                    this.state.BannerdataSource.map((item, i) => (
                      <View style={{height:'100%', flex:1, alignSelf:'stretch', margin:10, shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:7,borderRadius:10}}>
                          <Image key={i} source={{uri:item.image}} style={{ resizeMode: 'stretch',width:'100%', height: '100%',borderRadius:5 }}/>
                      </View>
                    
                  ))
                  }
                </Swiper>:null}
                <View style={{
                    position: 'absolute',
                    bottom: -10
                  }}>{this.pagination}</View>

              </View>
            </View>

            {/* Services Section Start */}
            <View style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 10,
              marginLeft: 10
            }}>

              <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-start', }}>
                <TouchableOpacity  style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }} onPress={() => this.props.navigation.navigate('ServiceRequest')}>
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/service_request.png')} style={{ height: 20, width: 20, resizeMode: 'contain' }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Request Service</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity  style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }} onPress={() => this.props.navigation.navigate('ServiceTrack')}>
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/service_track.png')} style={{ height: 20, width: 20, }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Track Service</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-start', }}>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }}onPress={()=>{NavigationService.navigate('AccessoriesNavigation')}} >
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/access.png')} style={{ height: 20, width: 20, }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Buy Accessories</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={()=>{NavigationService.navigate('AMCNavigation')}}
                 style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }} >
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/buy_amc.png')} style={{ height: 20, width: 20, }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Buy AMC</Text>
                  </View>
                </TouchableOpacity>


              </View>
              <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-start', }}>
                <TouchableOpacity style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }} >
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/offers.png')} style={{ height: 20, width: 20, }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Offers</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center', }} >
                  <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'flex-start', padding: 10 }}>
                    <Image source={require('../../assets/ic_home/more.png')} style={{ height: 20, width: 20, }} />
                    <Text style={{ paddingLeft: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>More</Text>
                  </View>
                </TouchableOpacity>
              </View>

            </View>

            {/* My Appliances Section Start */}
            {this.state.showAppliance ? this.state.customerProducts.length ?
              <View style={{marginTop:10}}>
                
                <View style={{ justifyContent: 'space-between', flexDirection: 'row', padding: 5, paddingBottom: 0 }}>

                  <Text style={{ paddingHorizontal: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>My {this.state.customerProducts.length?"Appliance":"Appliances"}</Text>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('My Appliance')}>
                    <Text style={{ paddingHorizontal: 10, color: '#4f4f4f', fontFamily: 'RubikMedium', textDecorationLine: 'underline' }}>View All</Text>
                  </TouchableOpacity>
                </View>
                <FlatList
                  horizontal
                  style={{ marginLeft: 10 }}
                  showsHorizontalScrollIndicator={false}
                  data={this.state.customerProducts}
                  renderItem={({ item: rowData }) => {
                    return (
                      <LinearGradient
                        colors={[ "#92d1e5", "#4790a9"]}
                        useAngle={true}
                        locations={[0.7, 1]}
                        angle={180}
                        angleCenter={{ x: 0, y: 0  }}
                        style={{  borderRadius: 10, width: 180, marginRight: 10, marginVertical: 10, padding:15 }}>
                          {/* {console.warn(rowData)} */}
                        <View style={{ flex:1,  borderRadius: 5,alignItems: 'stretch', }}>
                          <TouchableOpacity style={{flex:1,height:100,width:100, backgroundColor:'white', alignSelf:'center', borderRadius:5, marginBottom:5 }} 
                          onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:rowData.id,customer_id:this.state.customerID, applianceData:rowData})}>
                            {rowData.image?<Image source={{ uri: rowData.image }} style={{ height: '100%', width: '100%', resizeMode: 'cover', }} />:null}
                          </TouchableOpacity>
                          <View style={{ flex: 1, alignItems: 'center', alignSelf:'center' }}>
                            <TouchableOpacity style={{flex:1, alignItems:'center'}} onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:rowData.id,customer_id:this.state.customerID, applianceData:rowData})}>
                              <Text style={{ fontFamily: 'RubikMedium', fontSize: 17, color: 'white', marginBottom:5 }}>{rowData.product_name?rowData.product_name:''}</Text>
                              {rowData.brand_name?<Text style={{ fontFamily: 'RubikRegular', fontSize: 12, color: 'white',marginBottom:5  }}>Brand : {rowData.brand_name}</Text>:null}
                              <Text style={{ fontFamily: 'RubikRegular', fontSize: 12, color: 'white', marginBottom:5 }}>DOP : {authFun.getDate(rowData.purchase_date)}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{flexDirection:'row',  flex:1, alignItems:'center', justifyContent:'center',}} 
                            onPress={()=>NavigationService.navigate('AccessoriesNavigation', {product_group_id:rowData.product_group_id})}>
                              <Image source={require('../../assets/ic_home/basket_light.png')} style={{resizeMode:'contain', height:12, width:12}} />
                              <Text style={{ fontFamily: 'RubikRegular', fontSize: 12, color: 'white',  marginLeft:10, textDecorationLine:'underline'}}>Related accessories</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                        <View style={{  flexDirection: 'row', marginTop:15, }}>
                          <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:rowData})}} 
                          style={{ flex: 1, height: 35, borderColor: 'white',borderWidth:1, borderRadius: 2.5, alignItems: 'center', justifyContent: 'center', marginRight: 10, padding:5 }}>
                            <Text style={{ fontFamily: 'RubikBold', fontSize: 11, textAlign:'center', flexWrap:'wrap', color:'white' }}>Request Service</Text>
                          </TouchableOpacity>
                          <TouchableOpacity onPress={()=>{NavigationService.navigate('AMCNavigation')}} 
                          style={{ flex: 0.7, height: 35, borderColor: 'white', borderRadius: 5,borderWidth:1, justifyContent: 'center', padding:5 }}>
                            {/* <LinearGradient style={{ height: 35, borderRadius: 2.5, justifyContent: 'center', alignItems: 'center' }}
                              colors={["#ff619e", "#ff9fa6"]}
                            > */}
                              <Text style={{ fontFamily: 'RubikBold', fontSize: 11, color: 'white', flexWrap:'wrap', textAlign:'center' }}>Buy AMC</Text>
                            {/* </LinearGradient> */}
                          </TouchableOpacity>

                        </View>
                      </LinearGradient>

                    );
                  }}
                  keyExtractor={(item, index) => index + ''} />

              </View>
              : this.state.showAddView ?
                <LinearGradient style={{ height: 190, marginHorizontal: 15, borderRadius: 8, marginTop: 20, marginBottom: 10 }}
                  colors={["#5960e5", "#b785f8"]}
                  useAngle={true}
                  locations={[0.4, 1]}
                  angleCenter={{ x: 0, y: 0 }}
                >
                  <View style={{ alignSelf: 'center', justifyContent: 'flex-start', height: 50, width: '100%', marginBottom: 30 }}>
                    <Image source={require('../../assets/ic_home/modal_back.png')}
                      style={{ alignSelf: 'center', position: 'absolute', height: 80, width: 80, marginTop: 10 }} />
                    <View style={{ alignSelf: 'center', marginTop: 30, height: 32, width: 44 }}>
                      <Image source={require('../../assets/ic_home/box.png')} style={{ alignSelf: 'center', height: '100%', width: '100%' }} />
                      <Text style={{
                        backgroundColor: 'red',
                        fontFamily: 'RubikMedium', position: 'absolute',
                        fontSize: 15, width: 30, textAlign: 'center', borderRadius: 15, color: 'white', right: -10,
                      }}>{this.state.customerProducts.length ? this.state.customerProducts.length : this.state.productsList.length}</Text>
                    </View>
                  </View>
                  <Text style={{ fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'center', color: 'white', marginHorizontal: 15, marginTop: 10, marginBottom: 20 }}>We have found few products registered with your mobile number</Text>
                  <TouchableOpacity onPress={() => { this.setState({ showAppModal: true, showAppliance: false }) }} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} >
                      <Text style={{ fontSize: 25, color: 'white', fontFamily: 'RubikMedium' }}>+ </Text>
                      <Text style={{
                        textAlign: 'center', color: 'white', fontFamily: 'RubikMedium',
                        textAlignVertical: 'center', fontSize: 16
                      }}>
                        Let's add them here</Text>
                    </View>

                  </TouchableOpacity>
                </LinearGradient> :
                <LinearGradient style={{ height: 190, marginHorizontal: 15, borderRadius: 8, marginTop: 20, marginBottom: 10 }}
                  colors={["#5960e5", "#b785f8"]}
                  useAngle={true}
                  locations={[0.4, 1]}
                  angleCenter={{ x: 0, y: 0 }}
                >
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('AddAppliance')} style={{ alignSelf: 'center', justifyContent: 'flex-start', flex: 1, width: '100%' }}>
                    <View style={{ justifyContent: 'center', flex: 1, alignItems: 'center' }}>
                      <Image source={require('../../assets/ic_home/modal_back.png')}
                        style={{ alignSelf: 'center', position: 'absolute', height: 100, width: 100, }} />
                      <View style={{ marginRight: 20, height: 32, width: 44 }}>
                        <Image source={require('../../assets/ic_home/box.png')}
                          style={{ alignSelf: 'center', height: '100%', width: '100%' }} />
                        <Text style={{
                          backgroundColor: 'red',
                          fontFamily: 'RubikMedium', position: 'absolute',
                          fontSize: 15, width: 30, textAlign: 'center', borderRadius: 15,
                          color: 'white', right: -10,
                        }}>
                          {this.state.customerProducts.length ? this.state.customerProducts.length : '0'}</Text>

                      </View>
                    </View>
                    <Text style={{
                      textAlign: 'center', color: 'white', fontFamily: 'RubikMedium',
                      fontSize: 16, flex: 0.4
                    }}>
                      Let's start adding your first product</Text>
                  </TouchableOpacity>


                </LinearGradient> : null
            }
            {/* Accessories Section Start */}
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', padding: 5, paddingBottom: 0 }}>

              <Text style={{ paddingHorizontal: 10, color: '#4f4f4f', fontFamily: 'RubikMedium' }}>Accessories</Text>
              <TouchableOpacity onPress={()=>{NavigationService.navigate('AccessoriesNavigation')}}>
                <Text style={{ paddingHorizontal: 10, color: '#4f4f4f', fontFamily: 'RubikMedium', textDecorationLine: 'underline' }}>View All</Text>
              </TouchableOpacity>
            </View>
            <FlatList
              horizontal
              margin={10}
              showsHorizontalScrollIndicator={false}
              data={this.state.MyAccessoriesData}
              renderItem={({ item: rowData }) => {
                return (
                  <Card
                    title={null}
                    // height={140}
                    // width={120}
                    containerStyle={{ padding: 10, margin: 0, marginBottom: 10, marginRight: 5, alignItems: 'center', alignContent: 'center', alignSelf: 'center', borderRadius:5, marginRight:10, shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:6 }}>
                    <TouchableOpacity onPress={()=>this.openDetail(rowData)} style={{flex:1,justifyContent:'center', alignItems:'center',width:130}}>
                      {rowData.image?<Image source={{ uri: rowData.image }} style={{ height: 80, width: '90%', resizeMode:"center" , margin:5, }} />:null}
                      <Text numberOfLines={1} style={{ padding: 10,  color: '#000000', fontFamily: 'RubikRegular' }}>{rowData.accessories_name}</Text>
                      <View style={{flexDirection: 'row',alignItems: 'center',}}>
                        {
                          parseInt(rowData.special_price) > 0
                            ? <Text style={{
                                  textDecorationLine: parseInt(rowData.special_price) < 0
                                    ? null
                                    : 'line-through',
                                  fontFamily: "RubikRegular",
                                  color: 'gray',
                                  fontSize: 15,
                                  textAlign:'center',
                                  marginRight:10,
                                }}>₹ {
                                  parseInt(rowData.special_price) < 0
                                    ? Math.round(rowData.special_price)
                                    : Math.round(rowData.price)
                                }</Text>
                            : null
                        }
                        <Text style={{
                            fontFamily: "RubikMedium",
                            color: 'black',
                            fontSize: 18,
                            textAlign:'center',
                            textDecorationLine: parseInt(rowData.special_price) < 0
                              ? 'line-through'
                              : null
                          }}>
                          ₹{
                            parseInt(rowData.special_price) > 0
                              ? Math.round(rowData.special_price)
                              : Math.round(rowData.price)
                          }</Text>
                      </View>
                    </TouchableOpacity >
                  </Card>
                );
              }}
              keyExtractor={(item, index) => index + ''} />

          </ScrollView>

          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.showAppModal && this.state.productsList.length != 0}
            onRequestClose={() => {
              this.setState({ showFinalList: true })
            }}
          >
            <ProductScreen mobile={this.state.mNum} data={this.state.productsList} delApp={this.delApp}
              delData={this.state.delList} sendData={this.sendFinalList} addApp={this.addApp} addData={this.state.addList} confirmAll={this.state.confirmAll}
            />
          </Modal>
          
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.navigate('AddAppliance');
              // this.setState({ showAddAppModal: true })
          }}
            style={{ height: 60, width: 60, borderRadius: 35, position: 'absolute', alignItems: 'center', justifyContent: 'center', bottom: 20, right: 20 }}>
            {/* <View style={{height:70, width:70, borderRadius:35, shadowOffset: {
                    width: 1,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:15,backgroundColor:'red'}}> */}
            <Image source={require('../../assets/ic_home/plus.png')} style={{ height: 80, width: 80 }} />
            {/* </View> */}
          </TouchableOpacity>
        </View>


      );
  }

  progressOff() {
    this.setState({ showProgress: false });
  }

  progressOn() {
    this.setState({ showProgress: true });
  }
}


Home.propTypes = {
  user: PropTypes.object,

};


Home.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
