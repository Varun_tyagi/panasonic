

// eslint-disable-next-line no-unused-vars
import { StyleSheet, Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';


const widthFull = Dimensions.get('window').width;
// let heightFull = Dimensions.get('window').height;

const styles = StyleSheet.create({

  scrollerStyle: {
    height: 200,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 40,
  },
  containers: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.primary,
    padding: 40,
  },

  tblcontainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: Colors.white,
    padding: 10,
    height: 300,
    borderBottomColor: 'red',
    borderBottomWidth: 5,
    // height:'10%',
  },
  title: {
    fontSize: 16,
    color: '#000',
    justifyContent: 'center',
    alignItems: 'center',
  },
  AMCCategoryTitle: {
    fontSize: 14,
    color: '#FFF',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  AMCCategoryTitleHeader: {
    flex:1,
    fontSize: 25,
    color: '#AAAF',
    //justifyContent: 'center',
    // alignItems: 'center',
    // textAlign: 'center',
    fontWeight: 'bold',
    //backgroundColor: 'white',
    // height:40,

  },
  AMCCategoryDescHeader: {
    flex:1,
    fontSize: 12,
    color: 'red',
    fontWeight: 'bold',
    // height:20,
  justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  button_Text: {
    fontSize: 11,
    padding:2,
  },
  container_text: {
    flex: 1,
    flexDirection: 'column',
    marginLeft: 10,
    justifyContent: 'center',
  },
  description: {
    fontSize: 11,
    fontStyle: 'italic',
  },
  photo: {
    height: 70,
    width: 70,

  },
  photoLogo: {
    height: 30,
    // width: widthFull ,
    width: 50,

  },
  RowHeightcontainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: 'white',
    padding: 1,
    height: 200,
    borderBottomColor: '#EFEFF4',
    borderBottomWidth: 10,
    borderLeftWidth: 25,
    borderLeftColor: '#EFEFF4',
    borderRightColor: '#EFEFF4',
    borderRightWidth: 25,
    flexDirection: 'row',
  },
  ColumHeightcontainer: {
    flex: 1,
   // justifyContent: 'flex-start',
   // alignItems: 'center',
    color: 'blue',
    backgroundColor: 'transparent',
    padding: 10,
    height: 140,
    width: widthFull - 25,
    borderBottomColor: 'transparent',
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    borderLeftColor: 'transparent',
    borderRightColor: '#EFEFF4',
    borderRightWidth: 0,
    borderTopWidth: 0,
    borderTopColor: 'transparent',
    flexDirection: 'row',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
});

export default styles;

