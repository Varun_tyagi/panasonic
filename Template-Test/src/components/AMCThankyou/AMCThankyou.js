
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import TNC from '../TNC';
import ApplianceDetail from '../ApplianceDetail';

import Swiper from 'react-native-swiper';

const widthFull = Dimensions.get('window').width;

class AMCThankyou extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    // this.onPress = this.onPress.bind(this);
    
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  componentDidUpdate() {
    if (this.props.user === null) {
      this.props.navigation.navigate('Auth');
    }
    return null;
  }

  onPress(index, dataDict) {
    Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
  }
  onPressTnc(index,prodCatDict){
    this.props.navigation.navigate('TNC', { user: prodCatDict, index:index });

  }

  render() {
    return (
     // <ImageBackground source={require('../../assets/ic_home/pinkBG.png')} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >


    <View style={{ flex: 1, backgroundColor: '#5960e5' }} >
      <Header
        containerStyle={{ backgroundColor: 'transparent', height: '10%' }}
        backgroundColor={Colors.primary}
        leftContainerStyle={{ marginTop: -20 }}
        rightContainerStyle={{ marginTop: -20 }}
        centerContainerStyle={{ marginTop: -20 }}
        centerComponent={{ text: '', style: { color: '#fff', fontSize: 20 } }}
        leftComponent={
            {
              icon: 'arrow-back',
              color: '#FFFF',
              type: 'material',
              size: 30,
            onPress: () => { this.props.navigation.goBack() },

          }}

      />


      <ScrollView>

      <View style={{alignContent:"center", alignItems:"center"}}>

      <Image 
               source={require('../../assets/ic_home/extende_warranty.png')}
              style={{ resizeMode: 'contain', justifyContent:"center", alignItems: "center", height: 128, width: 145,marginTop:50 }}>
            </Image>

      <Text
        style={{alignItems: "center", color:'#e9eaff', fontSize:35 ,marginTop: 10, paddingLeft: 10, paddingRight: 10, marginTop: 20}}>
          {'Thank you' }
          </Text>

          <Text
        style={{alignItems: "center", alignContent:"center",justifyContent:"center",color:'#e9eaff', fontSize:16 ,fontWeight:"bold",marginTop: 10, paddingLeft: 10, paddingRight: 10, marginTop: 20,marginLeft:20, marginRight:20}}>
          {'Payment has been done  successfully for your 1  Year AMC next renew date is: 8TH may 2019' }
          </Text>

          <Image 
               source={require('../../assets/ic_home/extended_product.png')}
              style={{ resizeMode: 'contain', justifyContent:"center", alignItems: "center", height: 111, width: 166,marginTop:20 }}>
            </Image>

      <Text
        style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 ,marginTop: 10, paddingLeft: 10, paddingRight: 10, marginTop: 20}}>
          {'FROST FREE REFRIGERATOR' }
          </Text>

        <View style={{flexDirection:'row',marginTop:20}}>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 , }}>
            {'Type:' }
          </Text>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 ,marginLeft:10 }}>
          {'Double Door' }
          </Text>
        </View>

        <View style={{flexDirection:'row',}}>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 , }}>
            {'Serial Number:' }
          </Text>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 ,marginLeft:10  }}>
          {'987654321' }
          </Text>
        </View>

        <View style={{flexDirection:'row',marginBottom:20}}>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 , }}>
            {'Model Number.:' }
          </Text>
          <Text
            style={{alignItems: "center", color:'#e9eaff',fontWeight:"bold",fontSize:16 ,marginLeft:10 }}>
          {'123451' }
          </Text>
        </View>
      </View>

        

      </ScrollView>


    </View>
      // </ImageBackground>
  );
  }
}

AMCThankyou.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

AMCThankyou.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AMCThankyou);
