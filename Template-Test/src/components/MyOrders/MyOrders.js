import React, { Component } from 'react';
import { View,Platform, Text, TouchableOpacity, Image, BackHandler, AsyncStorage, FlatList, ImageBackground, StatusBar,SafeAreaView, ScrollView } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, Avatar, Card } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DateTimePicker from 'react-native-modal-datetime-picker';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import NavigationService from '../navigation/NavigationService';
import styles from './styles';
import ApplianceDetail from '../ApplianceDetail';
import { drawer } from '../navigation/AppNavigator';
import LinearGradient from 'react-native-linear-gradient';
import Shimmer from '../../helpers/Shimmer';
import * as authFun from '../../helpers/auth';
import * as constants from '../../helpers/constants';
import Loader from '../common/Loader';

class MyOrders extends Component {
  static navigationOptions = {
    header: null,

  };



  constructor(props) {
    super(props);
    this.state = {
      status: true,
      testval: 1,
      amc: true,
      loading:true,
      accessories: false,
      mainData:[],
      order_type:'amc',
      token:'',
      customerID:'',
      orderSelected:[],
    }
    this.loadData = this.loadData.bind(this);
    this.fetchOrders = this.fetchOrders.bind(this);
    this.setToken = this.setToken.bind(this);
    this.goBack = this.goBack.bind(this);
  }
  

  async fetchOrders(order_type) {
    await this.setToken();
    this.setState({ loading: true}, () => {
      var url = constants.base_url + 'getCustomerOrder';
      url = url + "?access_token=" + this.state.token + '&customer_id=' + this.state.customerID;
      fetch(url, { headers: { 'Content-Type': 'application/json' } })

        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.status==1) {
            let mainData = responseJson.data.map((item)=>{
              item['selected']=false;
              return item;
            });
            this.setState({mainData:mainData, loading:false})
          } else {

            this.setState({ loading: false })
          }

        })
        .catch(error => {console.warn(error); this.setState({loading:false})})
    })

  }
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
        loading: false,
      }, () => {
        this.fetchOrders('amc');
      });
    } else {
      this.setState({ loading: false });
    }
  }

  setToken = async () => {
    var token = await authFun.checkAuth();
    this.setState({ token: token })
    return token;
  }

  ShowHideTextComponentView = () => {

    if (this.state.status == true) {
      this.setState({ status: false })
    }
    else {
      this.setState({ status: true })
    }
  }
  getDate = (dt) => {
    // alert(dt);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-');
    return dt[2] + ' ' + months[dt[1][0] == 0 ? dt[1][1] : dt[1]] + ' ' + dt[0];
  }

  async componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.goBack);
    this.props.navigation.addListener('willFocus', async () => {
      this.loadData();
    });
        this.loadData();
  }

  componentWillUnmount(){
    this.backHandler.remove();
  }
  getWarrantyPeriod(product){
    let mainProduct = (Object.values(JSON.parse(product.plan))[0]);
    if(Object.prototype.toString.call(mainProduct).indexOf('Array')!=-1){
      mainProduct = mainProduct[0];
    }
      let warranty = mainProduct.WarrantyPeriod 
      if(warranty){
        warranty = parseInt((warranty.split(' ')[0])/12);
        let txt = warranty>1?' Years':' Year';
        return 'EWC - '+warranty+txt;
      }
  }

  selectOrderDetails(order){
    this.setState({orderSelected:[order]});
  }

  goBack(){
    if(this.state.orderSelected.length){
      this.setState({orderSelected:[]});
    }else{
      this.props.navigation.goBack(null);
    }
    return true;
  }

  render() {
    return (
      <View style={{flex:1, backgroundColor:'#edf4f6'}}>
        {this.state.loading ? <Loader /> : null}
        <ScrollView style={{flex:1,}}>
        {/* header start */}
        <View style={{height: this.state.mainData.length?50:120, width: '100%', overflow: 'visible',marginTop: StatusBar.currentHeight, alignSelf: 'center', justifyContent: 'flex-start', }}>
          <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
            angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45}
            style={{
              backgroundColor: 'purple',
              alignSelf: 'center',
              top: -980,
              height: 1100,
              width: 1100,
              borderRadius: 550,
              position: 'absolute', 
            }}
          >
          </LinearGradient>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 5, paddingTop: Platform.OS=='ios'?30:0, }}>
            <TouchableOpacity onPress={()=>{this.goBack()}} style={{ zIndex: 1 }}>
              <Image source={require('../../assets/ic_home/back.png')} style={{ height: 30, width: 30, }} />
            </TouchableOpacity>
            <Text style={{ flex: 1, color: '#e9eaff', fontFamily: 'RubikMedium', fontSize: 20, textAlign: 'center', marginLeft: -30 }}> My Order</Text>
          </View>
        </View>
        {/* end of header */}

       { this.state.mainData.length?
       <View>
          {/* Main data code start===================================================================================================================== */}
        
          <View style={{flex:1, alignItems:'stretch',}}>
            <FlatList
              data={this.state.orderSelected.length?this.state.orderSelected:this.state.mainData}
              showsVerticalScrollIndicator
              contentContainerStyle={{alignItems:'stretch', flex:1,}}
              renderItem={({ item }) => {
              return (
                <TouchableOpacity onPress={()=>this.selectOrderDetails(item)} disabled={this.state.orderSelected.length?true:false} style={{flex:1, alignItems:'stretch',  backgroundColor:'white', marginVertical:7.5, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                  {/* <Text style={{backgroundColor:'#e9e9e9', borderRadius:20, fontFamily:'RubikRegular', fontSize:15, paddingVertical:5, paddingHorizontal:15, color:'#48494a'}}>Order Id: {item.order_no}</Text> */}
                  <View style={{flex:1, flexDirection:'row',margin:10, marginBottom:0 }}>
                    <View style={{flex:1, }}>
                      <Text style={{ fontFamily:'RubikRegular', fontSize:15,  color:'#48494a'}}>ORDER ID: {item.order_no}</Text>
                      <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'#141313'}}>Order Placed: {authFun.getDate(item.created_at, ' ',)}</Text>
                    </View>
                    <View style={{flex:1, }}>
                      <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'#141313',textAlign:'right'}}>Total Amount </Text> 
                      <Text style={{ fontFamily:'RubikBold', fontSize:15,  color:'#141313', textAlign:'right'}}>₹ {item.grand_total}</Text>
                    </View>
                  </View>

                  {
                    item.order_items.length?
                    <FlatList
                      data={item.order_items}
                      showsVerticalScrollIndicator
                      contentContainerStyle={{alignItems:'stretch', flex:1, }}
                      renderItem={( product ) => {
                      return (
                        
                        <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingBottom:10, borderBottomColor:'#e6e8e9', borderBottomWidth:1,marginHorizontal:10}}>
                          <View style={{  borderRadius:5,  justifyContent:'center', alignItems:'center', height:120, width:120}}>
                            {product.item.image?<Image source={{uri:product.item.image}} style={{height:'100%', width:'100%', resizeMode:'center',}}/>:null}
                            {(product.item.qty>2)?<View style={{height:30, width:30, borderRadius:15,position:'absolute', bottom:0, left:0, backgroundColor:'#9176f0', justifyContent:'center', alignItems:'center', alignSelf:'flex-start'}}>
                              <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'white'}}>{product.item.qty}</Text></View>:null}
                          </View>
                          <View style={{flex:1, marginLeft:15}}>
                          {product.item.order_type=='amc'?<Text style={{fontFamily:'RubikMedium', fontSize:12, color:'#4f4f4f', }}>{this.getWarrantyPeriod(product.item)}</Text>:null}
                            {product.item.order_type=='amc'?<Text style={{fontFamily:'RubikBold', fontSize:13, color:'#3875c9', flexWrap:'wrap',marginBottom:5}}>{product.item.product_name?product.item.product_name.toUpperCase():''}</Text>:null}
                            <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#4f4f4f',marginBottom:5}}>{product.item.product_item_name}</Text>
                            {product.item.order_history_status?<View style={{ flexDirection:'row', alignItems:'center',marginBottom:5}}>
                              <View style={{height:8, width:8,marginRight:4, borderRadius:5, backgroundColor:(product.item.order_history_status.toLowerCase().indexOf('deliver')==-1) &&
                            (product.item.order_history_status.toLowerCase().indexOf('complete')==-1)?'#d70738':'#06d528'}}></View>
                              <Text style={{fontFamily:'RubikMedium', fontSize:12,
                            color:(product.item.order_history_status.toLowerCase().indexOf('deliver')==-1) &&
                            (product.item.order_history_status.toLowerCase().indexOf('complete')==-1)?'#d70738':'#06d528'}}>{product.item.order_history_status.toUpperCase()}</Text>
                            </View>:null}
                          </View>
                          <Text style={{fontFamily:'RubikMedium', fontSize:16, color:'#4f4f4f',}}>₹ {product.item.order_type=='amc'?product.item.plan_amount:product.item.price}</Text>
                          
                        </View>
                        )}} 
                    keyExtractor={(item, index) => index + ''}/>:null
                  }
                </TouchableOpacity>
                )}} 
            keyExtractor={(item, index) => index + ''}/>
          </View>
          
          {/* Main data code end======================================================================================================================= */}
        
          {/* address and payment summary */}
          {this.state.orderSelected.length?
            <View style={{flex:1, marginHorizontal:15, marginTop:20}}>
              {/* address */}
              <Text style={{fontFamily:'RubikRegular', fontSize:15,color:'#48494a'}}>SHIPPING ADDRESS</Text>
              
              <View style={{flex:1, borderRadius:5, backgroundColor:'white', padding:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginVertical:10}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15,}}>{this.state.orderSelected[0].address_type}</Text>
                <Text style={{fontFamily:'RubikRegular', fontSize:15,flexWrap:'wrap'}}>{this.state.orderSelected[0].address+', '+this.state.orderSelected[0].area_name+', '+this.state.orderSelected[0].city_name
              +', '+this.state.orderSelected[0].state_name+' - '+this.state.orderSelected[0].pincode+', '+this.state.orderSelected[0].country_name}</Text>
              </View>
              {/* address end */}

              {/* payment summary */}
              <Text style={{fontFamily:'RubikRegular', fontSize:15,color:'#48494a', marginTop:20}}>PAYMENT SUMMARY</Text>
              <View style={{flex:1, borderRadius:5, borderColor:'#c2c4c4', borderWidth:1, padding:10, marginVertical:10}}>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}} >
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',paddingBottom:5 }}>Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123',paddingBottom:5 }}>₹ {(this.state.orderSelected[0].grand_total) - this.state.orderSelected[0].tax + parseFloat(this.state.orderSelected[0].discount)}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}} >
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',paddingBottom:5 }}>Discount</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123',paddingBottom:5 }}>₹ {this.state.orderSelected[0].discount}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}} >
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',paddingBottom:5 }}>Sub Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123',paddingBottom:5 }}>₹ {this.state.orderSelected[0].grand_total - this.state.orderSelected[0].tax}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between'}} >
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',paddingBottom:5 }}>Estimated GST</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123',paddingBottom:5 }}>₹ {this.state.orderSelected[0].tax}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between', borderBottomColor:'#c2c4c4', borderBottomWidth:1.5}} >
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',paddingBottom:5 }}>Handling Charges</Text>
                  <Text  style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123',paddingBottom:10 }}>{this.state.orderSelected[0].shipping!='₹ 0.00'?'₹ '+this.state.orderSelected[0].shipping:'Free'}</Text>
                </View>
                <View style={{flex:1, flexDirection:'row', justifyContent:'space-between',marginTop:10}} >
                  <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#212123',paddingBottom:5 }}>Total Payable Amount</Text>
                  <Text  style={{fontFamily:'RubikBold', fontSize:15, color:'#212123',paddingBottom:5 }}>₹ {this.state.orderSelected[0].grand_total}</Text>
                </View>
              </View>
              {/* payment summary end*/}

            </View>
          :null}
          {/* address and payment summary end*/}
            {/* Go to listing button */}
          { this.state.orderSelected.length?<TouchableOpacity onPress={()=>this.goBack()} style={{alignSelf:'center', borderRadius:5, borderWidth:1.5, borderColor:'#9b9b9b',padding:10, marginVertical:20}}>
              <Text style={{fontFamily:'RubikRegular', fontSize:15, }}>Back To List</Text>
            </TouchableOpacity>:null}
            {/* Go to listing button end */}
          </View>:
          <View style={{flex:1, alignSelf:'center', marginTop:'45%'}}>
            <TouchableOpacity onPress={()=>{NavigationService.navigate('AccessoriesNavigation')}} style={{borderRadius:10, borderWidth:1, borderColor:'#3875c9'}}>
              <Text style={{fontFamily:'RubikMedium', fontSize:20, textDecorationLine:'underline', color:'#3875c9', padding:10}}>Make your first order</Text>
            </TouchableOpacity> 
          </View>
          }
        </ScrollView>
        
      </View>
    );

  }
}

MyOrders.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

MyOrders.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyOrders);

