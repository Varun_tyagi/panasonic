import { StyleSheet } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',    
    marginRight:10,
    marginLeft:10
  },
  linearGradient: {
    flex: 1,
    borderRadius: 5,
    marginRight:10,
    padding:10,
    paddingTop:0,
    margin:10,
    justifyContent:'center',
    alignItems:'center'
    // height:160
  },
  linearGradientSelected: {
    flex: 1,
    borderRadius: 5,
    marginRight:10,
    borderWidth:10,
    padding:10,
    paddingTop:0,
    margin:10,
    justifyContent:'center',
    alignItems:'center'
    // height:160
  },
 amcBtn:{
    justifyContent:'center',
    marginLeft:25,
    borderRadius:15,
    padding:10, 
    alignItems:'center',
    height:110, 
    width:100
  },

});


export default styles;
