/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
/* eslint-disable import/no-unresolved */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
// eslint-disable-next-line import/first
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
// eslint-disable-next-line import/order
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import AmcList from '../AmcList';

// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
import Swiper from 'react-native-swiper';

const widthFull = Dimensions.get('window').width;

class TNC extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    const {state} = props.navigation;
    this.state = {
      amcData: state.params.user,
      warrantyIndex: state.params.index,
    };

    this.warrantyIndex = state.params.index+1; 
    this.amcData =  state.params.user;

    console.log("\n\n\n Hello PROPS \n\n " + state.params.user + state.params.index);
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  componentDidUpdate() {
    if (this.props.user === null) {
      this.props.navigation.navigate('Auth');
    }
    return null;
  }

  // eslint-disable-next-line class-methods-use-this
  onPress(index, dataDict) {
   // Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
    this.props.navigation.navigate('AmcList');
  }

  render() {
    return (
      <ImageBackground source={require('../../assets/ic_home/pinkBG.png')} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >


    <View style={{ flex: 1, backgroundColor: 'transparent' }} >
      <Header
        containerStyle={{ backgroundColor: 'transparent', height: '10%' }}
        backgroundColor={Colors.primary}
        leftContainerStyle={{ marginTop: -20 }}
        rightContainerStyle={{ marginTop: -20 }}
        centerContainerStyle={{ marginTop: -20 }}
        centerComponent={{ text: '', style: { color: '#fff', fontSize: 20 } }}
      />

      <ScrollView>
           <View style={{ flex: 1, flexDirection: 'column', paddingLeft: 15, paddingRight: 15, }} >
      <Text style={styles.header}>
            { this.warrantyIndex + 'Year Warranty'}
      </Text>

       
            </View>


               
      </ScrollView>


    </View>
      </ImageBackground>
  );
  }
}

TNC.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

TNC.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TNC);
