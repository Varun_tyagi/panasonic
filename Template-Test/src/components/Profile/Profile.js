import React, { Component } from 'react';
import {View,Text,Platform,TouchableOpacity,Image,AsyncStorage,TextInput,FlatList,Dimensions,KeyboardAvoidingView,StatusBar,Picker,Alert,createFormData,Keyboard,Icon,} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, Avatar } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TextStyles from '../../helpers/TextStyles';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { ScrollView } from 'react-native-gesture-handler';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import axios from 'react-native-axios';
import styles from './styles';
import DatePicker from 'react-native-datepicker';
import Loader from '../common/Loader';
import LinearGradient from 'react-native-linear-gradient';
import * as constants from '../../helpers/constants';
import * as authFun from '../../helpers/auth';
import { drawer } from '../navigation/AppNavigator';
import ModalDropdown from 'react-native-modal-dropdown';

class Profile extends Component {
  static navigationOptions = {
    // title: strings.profile,
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      customerID: '',
      customerToken: '',
      fName: '',
      email: '',
      gender: '',
      mNum: '',
      aNum: '',
      occupation: '',
      birthday: '',
      genders:["Male", "Female"],
      selectedGender:'',
      anniversary: '',
      verifyANum:false,
      dataLoading: true,
      inputOnFocusStyle: { borderColor: '#5D61E6' },
      showDatePicker: false,
      avatarImageSource: 'https://reactnativeexample.com/content/images/2018/06/20180607093721.jpg',
      photo: undefined,
      filePath: '',
      showGenderPicker: false,
      profile_image:'',
      otp:['','','',''],
      showOtp:false,
      verifyOtp:false,
      oldAnum:'',
      aNumVer:false,
      maximumDate: new Date(),
      data:[],
      selected:[{label:'brand',val:0}, {label:'group', val:0}, {label:'subgroup', val:0}],
      selectedAdd:[{label:'country', val:0},{label:'state', val:0},{label:'city', val:0},{label:'area', val:0}, ],
      modelNum:'',
      myAddress:[],
      oldAddress:false,
      serialNum:'',
      invoiceDate:this.getDob(new Date()),
      warrantyDate:this.getDob(new Date()),
      IDupdated:true,
      WXupdated:true,
      fileList:[],
      dealerName:'',
      dealerAdd:'',
      invoiceNum:'',
      landmark:'',
      address:'',
      pincode:'',
      photo:'',
      country:[],
      state:[],
      city:[],
      area:[],
      brand:[],
      group:[],
      subgroup:[],
      token:'',
      newTest:false,
      radioButtons:[{label:'HOME', selected:true},{label: 'OFFICE', selected:false},{ label:'OTHER', selected:false}],
    };

    this.loadData = this.loadData.bind(this);
    this.selectRadioBtn = this.selectRadioBtn.bind(this)
    this.setToken = this.setToken.bind(this);
    this.setValue = this.setValue.bind(this);
    this.getArea = this.getArea.bind(this);
    
    this.updateAddress = this.updateAddress.bind(this);
   
  }
  
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }


  onFocus() {
    this.setState({
      borderColor: '#5D61E6',
      borderWidth: 1,
    });
  }

  endFocus() {
    this.setState({
      borderColor: 'transparent',
      borderWidth: 0,
    });
  }

  
  async componentDidMount() {
    this.props.navigation.addListener('willFocus', async () => {     
    var token = await  this.setToken();
    this.setState({newTest: false})
    if(token){
      this.loadData(); 
    }
  });
  }

  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    const token = await this.setToken();
    
    if (userData) {
      const data = JSON.parse(userData);
      // this.getDataofAddress(this.token, data.customer_id);
       console.warn('===========',data);
      global.profile_image=data.profile_image;
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        customerToken: data.customer_token ? data.customer_token : '',
        fName: data.name ? data.name : this.state.fName,
        email: data.email ? data.email : this.state.email,
        gender: data.gender ? data.gender : this.state.gender,
        selectedGender: data.gender ? data.gender : this.state.gender,
        mNum: data.mobile ? data.mobile : this.state.mNum,
        aNum: data.alternate_mobile_no ? data.alternate_mobile_no : this.state.aNum,
        occupation: data.occupation ? data.occupation : this.state.occupation,
        birthday: data.dob.indexOf('0000') == -1 ?data.dob : '',
        anniversary: data.anniversary.indexOf('0000') == -1 ? data.anniversary :'',
        dataLoading: false,
        profile_image:data.profile_image?data.profile_image:null,
      },()=>{this.setState({oldAnum:this.state.aNum});
      setTimeout(
        ()=>{
          if(this.genderSelector){
            this.genderSelector.select(this.state.selectedGender?this.state.genders.map((item)=>item.toLowerCase()).indexOf(this.state.selectedGender.toLowerCase()):-1)   
          }
        }
      ,200)
      
        
    });
    } else {
      this.setState({ dataLoading: false });
    }
  }

  sendOtp=()=>{
    this.setState({dataLoading:true});
    
    var url = constants.base_url+'send_otp';
    url=url+"?access_token="+this.state.token;

    // fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/send_otp', {
      fetch(url,{
          method: "POST",
          headers: { 
            // "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==",
          'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.aNum})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:true,},()=>{
              this.setState({dataLoading:false, verifyANum:false})
            });
          }else{
            
            alert(result.message);
            this.setState({dataLoading:false, verifyANum:false})
          }
          
        },
        (error) => { alert(error);
          this.setState({dataLoading:false, verifyANum:false})
        }
      );
  }
  verifyOtp=async ()=>{
    this.setState({dataLoading:true});
    await this.setToken();
    var url = constants.base_url+'verifySmsOtp';
    url=url+"?access_token="+this.state.token;
    fetch(url, {
          method: "POST",
          headers: { 'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.aNum,'otp':parseInt(this.state.otp.join(""))})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:false,aNumVer:true},()=>{
              this.setState({dataLoading:false})
            });
          }else{
            
            alert(result.message);
            this.setState({dataLoading:false})
          }
          
        },
        (error) => { alert(error);
          this.setState({dataLoading:false})
        }
      );
  }
  handleUploadPhoto = async (source) => {
    await this.setToken();
    const data = new FormData();
    let filePath = Platform.OS=='ios'?source.uri:`file://${source.path}`;
    let split_filePath = filePath.split('/');
    let fileName = Platform.OS=='ios'?split_filePath[split_filePath.length-1]:source.fileName;
    console.warn(split_filePath, fileName);
    data.append('customer_id', this.state.customerID);
    data.append('image', { uri: filePath, type: 'image/*', name:fileName });
    var url = constants.base_url+'upload_customerimg';
    url=url+"?access_token="+this.state.token;
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    })
      .then(response => response.json())
      .then((response) => {
        if (response.status) {
          global.profile_image = response.data.profile_image;
          this.setState({profile_image:response.data.profile_image})
        } else {
          Alert.alert(response.message);
        }
      })
      .catch((error) => {
        NetInfo.fetch().then(async (state)=>{
          if(!state.isConnected){
            alert('Please check your internet connection.');
          }else{
            // Alert.alert(`Upload failed!${JSON.stringify(error)}`);
            console.warn(error);
          }
        })
        
        console.log('\n\n\n\nupload error \n\n\n\n\n', error);
        
      });
  };


  logout = () => this.props.logout();

  isFormValidated = () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.fName.trim() === '') {
      alert('Please enter name');
      return false;
    } else if (this.state.email.trim() === '') {
      alert('Please enter email');
      return false;
    } else if (reg.test(this.state.email) === false) {
      alert('Please enter valid email');
      return false;
    } else if (this.state.gender.trim() == 0) {
      alert('Please select gender');
      return false;
    } else if (this.state.mNum.trim() === '') {
      alert('Please enter mobile number');
      return false;
    } else if (this.state.occupation.trim() === '') {
      alert('Please enter occupation');
      return false;
    }
    return true;
  };
  
  getDataofAddress() {
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerAddresses?access_token=" + this.state.token + "&customer_id=" + this.state.customerID).then((response) => {
      this.setState({myAddress:response.data.data,loading:false}, ()=>console.warn(this.state.myAddress))
    }).catch((error) => {
      NetInfo.fetch().then(async (state)=>{
        if(!state.isConnected){
          alert('Please check your internet connection.');
        }
      })
      
      this.setState({loading: false});
    });
  }

  updateData=async () => {
    if (this.isFormValidated())
{
    // this.setState({ dataLoading: true });
    // let birthday = this.state.birthday?this.state.birthday.indexOf('/')==-1?this.getDob(this.state.birthday):this.changeFormat(this.state.birthday):'';
    // let anniversary = this.state.anniversary?this.state.anniversary.indexOf('/')==-1?this.getDob(this.state.anniversary):this.changeFormat(this.state.anniversary):'';
    let url = constants.base_url+'update_profile';
    let bodyData = {
        customer_id: this.state.customerID,
        name: this.state.fName,
        email: this.state.email,
        alternate_mobile_no: this.state.aNum,
        gender: this.state.gender,
        dob: this.state.birthday,
        anniversary: this.state.anniversary,
        occupation: this.state.occupation,
      };
      await this.setToken();
    url=url+"?access_token="+this.state.token;
      fetch(url,{
      method: 'POST',
      headers: {'Content-Type': 'application/json',},
      body: JSON.stringify(bodyData),
    }).then(res => res.json())
      .then(
        (result) => {
          if (result.status) {
            this.setState({ dataLoading: false });
            AsyncStorage.setItem('userData', JSON.stringify(result.data), () => {
              alert('Data Updated Successfully.');
              this.loadData();
              this.setState({newTest:true},()=>{
                setTimeout(()=>{
                  this.getDataofAddress();
                  this.cleanAddress();
                  this.getArea();
                },100);
              })
            });
          } else {
            alert(result.message);
            this.setState({ dataLoading: false });
          }
        },
        (error) => { alert(error); },
      );
    }
  }

  changeFormat = (dt) => {
    var arr = dt.split('/');
    return arr[2] + '-' + arr[1] + '-' + arr[0];
  }
  getDob(date) {
    date = new Date(date);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return `${year}-${month < 10 ? `0${month}` : month}-${day < 10 ? `0${day}` : day}`;
  }

  // Image Picker
  chooseFile = () => {
    const options = {
      title: 'Select Image',mediaType: 'photo',noData: true,storageOptions: {skipBackup: true,}};
    
      ImagePicker.showImagePicker({title: 'Select Image',quality:0.8,maxHeight:400,maxWidth:400,mediaType: 'photo',noData: true,
      storageOptions: {skipBackup: true,}},
     (response) => {
      if (response.didCancel) {
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.warn('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = response;
        this.handleUploadPhoto(source);
        this.setState({
          filePath: source,

        });
      }
    });
  }

  // Image Picker end
  changeTextNameField(inputText, field) {
    const specialCharReg = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const emailReg = /^[0-9a-zA-Z._]*@{0,1}[0-9a-zA-Z.]*$/;
    const numReg = /^[0-9]*$/;
    const nameReg=/^[a-zA-Z\s]*$/;
    const numEntry = /[0-9]*/;
    if ((field === 'fName' | field==="occu") &  nameReg.test(inputText)) {
      if(field==='fName')
      this.setState({ fName: inputText });
      else
      this.setState({ occupation: inputText});
    }else if(field==='anum' && numReg.test(inputText)){
      this.setState({ aNum: inputText },()=>{
        if(this.state.aNum.length==10){
          this.setState({verifyANum:true})
        }else{
          this.setState({verifyANum:false, showOtp:false, otp:['','','','']})
        }
      });
    }else if( field==='email' && emailReg.test(inputText)){
      this.setState({email:inputText});
    }
  }
otpChanged = (value,id,callBack=undefined) => {
  var temp = this.state.otp;
  const testExp = /^[0-9]+$/;
  if(testExp.test(value) || value==''){
    temp[id]=value
    this.setState({ otp: temp },()=>{
      if(this.state.otp[0] && this.state.otp[1] && this.state.otp[2] && this.state.otp[3]){
        this.setState({verifyOtp:true});
      }else{
        this.setState({verifyOtp:false});
      }
      if(callBack){
        callBack();
    }
  });
  }
}


checkValidity(){
    
  var temp = this.state.selectedAdd;
  if(temp[0].val==0){
      alert('Please select country');
      return false;
  }else if(temp[1].val==0){
      alert('Please select state');
      return false;
  }else if(temp[2].val==0){
      alert('Please select city')
      return false;
  }else if(temp[3].val==0){
      alert('Please select area')
      return false;
  }
  // else if(this.state.landmark==''){
  //     alert('Please enter a landmark');
  //     return false;
  // }
  else if(this.state.address==''){
      alert('Please enter your address');
      return false;
  }else if(this.state.pincode==''){
      alert("Please enter a valid pincode");
      return false;
  }

return true;
}

updateAddress=async ()=>{
  if(this.checkValidity()){
      var data = new FormData();
      data.append('customer_id', this.state.customerID);
      this.state.selectedAdd.forEach((item)=>{
          if(item.label=='country')
          data.append('country_name',this.state.country.filter((name)=>name.country_id==item.val)[0].country_name);
          if(item.label=='state')
          data.append('state_name',this.state.state.filter((name)=>name.state_id==item.val)[0].state_name);
          if(item.label=='city')
          data.append('city_name',this.state.city.filter((name)=>name.city_id==item.val)[0].city_name);
          else if(item.label=='area')
          data.append('area_name',this.state.area.filter((name)=>name.area_id==item.val)[0].area_name);
      });
      data.append('address', this.state.address);
      data.append('address_type', this.state.radioButtons.filter((item)=>item.selected)[0].label);
      data.append('pincode', this.state.pincode+'');
      this.setState({loading:true});
      var url = constants.base_url+'addCustomerAddress';
      url=url+"?access_token="+this.state.token;
      fetch(url,{method:'POST',headers: {'Content-Type':'multipart/form-data'}, body:data})
          .then(response => response.json())
          .then((responseJson)=> {
            console.warn(responseJson);
            if(responseJson.status){
              alert('Address Updated successfully');
              this.setState({newTest:false})
              this.setState({loading:false}, ()=>{
              this.props.navigation.navigate('Main');
              });
            }else{
              alert(responseJson.message);
            }
              
             
          })
          .catch(error=>{alert(error);
              this.setState({loading:false})});
  }
}


setValue =  (screen, field, val, pinChange=false) => {
  var temp = screen == 1 ? this.state.selected : this.state.selectedAdd;
  temp = temp.map((item) => {
      if (item.label == field) {
          item.val = val;
          if(field == 'country' && pinChange){
              let countries = this.state.country.map((element)=>element.country_id);
              let selectedCountry = countries.indexOf(val);
              // this.countrySelector.select(selectedCountry);
          }
          if(field == 'state' && pinChange && this.stateSelector){
              let states = this.state.state.map((element)=>element.state_id);
              let selectedstate = states.indexOf(val);
              this.stateSelector.select(selectedstate);
          }
          if(field == 'city' && pinChange && this.citySelector){
              let cities = this.state.city.map((element)=>element.city_id);
              let selectedcity = cities.indexOf(val);
              this.citySelector.select(selectedcity);
          }
          if(field == 'area' && pinChange && this.areaSelector){
              let areas = this.state.area.map((element)=>element.area_id);
              let selectedarea = areas.indexOf(val);
              this.areaSelector.select(selectedarea);
          }
      }
      return item;
  });
  if (screen == 1)
      this.setState({ selected: temp });
  else
      this.setState({ selectedAdd: temp });

}

cleanAddress(){
  
  this.setState({ selectedAdd:[{label:'country', val:0},{label:'state', val:0},{label:'city', val:0},{label:'area', val:0}, ],address:'', pincode:'',
  radioButtons:[{label:'HOME', selected:true},{label: 'OFFICE', selected:false},{ label:'OTHER', selected:false}], oldAddress:false},()=>{
    this.getArea();
  });
  setTimeout(()=>{
    this.stateSelector.select(-1);
    this.citySelector.select(-1);
    this.areaSelector.select(-1);
  },100)
  
}
   
pincodeChanged =async ()=>{
  let pincode = this.state.pincode;
  await this.setToken();
  this.setState({loading:true})
  if(pincode!='' && pincode.length==6){
    let url = constants.base_url + 'pincodeBasedData';
    url = url + "?access_token=" + this.state.token + '&pincode=' + pincode;
    fetch(url,
      { headers: { 'Content-Type': 'application/json' } })
      .then(response => response.json())
      .then((responseJson) => {
        if (responseJson.status) {

          // this.setValue(2, 'country', responseJson.data.country_id, true);
          let temp = this.state.selectedAdd;
          temp[0].val = responseJson.data.country_id;
          this.setState({selectedAdd: temp});
          this.getArea('state', responseJson.data.country_id, responseJson.data.state_id, true);
          this.getArea('city',responseJson.data.state_id, responseJson.data.city_id);
          this.getArea('area', responseJson.data.city_id, responseJson.data.area_id, responseJson.data.area);
          this.setState({loading:false, });
        } else {
          this.setState({ loading: false,});
          alert('Could not locate '+this.state.pincode);
          this.cleanAddress();
        }

      })
      .catch(error => { console.log(error); this.setState({ loading: false }) })
  }else{
    alert('Enter a valid postal code.');
    this.setState({loading:false})
  }
}

  getArea = async (name = 'country', id = 0, pinChange = false, data = []) => {
      await this.setToken();
      this.setState({ loading: true });
      var location = id != 0 ? name == 'state' ? 'getState' : name == 'city' ? 'getCity' : 'getArea' : 'getCountry';
      var url = constants.base_url + location;
      var query = id != 0 ? name == 'state' ? '&country_id=' + '101' : name == 'city' ? '&state_id=' + id : '&city_id=' + id : '';
      url = url + "?access_token=" + this.state.token + query;

      fetch(url, { headers: { 'Content-Type': 'application/json' } })

          .then(response => response.json())
          .then((responseJson) => {

              if (responseJson.status) {
                  if (name == 'country') {
                      this.setState({ country: responseJson.data.length ? responseJson.data : [], pincode:'' }, () => {
                          let country_id = this.state.country.filter((item)=>item.country_name.toLowerCase() == 'india')[0].country_id;
                          this.setValue(2, 'country', country_id+'', true);
                          let temp = this.state.selectedAdd;
                          temp[0].val = country_id;
                          this.setState({selectedAdd:temp},()=>{
                            this.getArea('state', country_id);
                          });
                          
                      });
                  }
                  else if (name == 'state')
                      this.setState({ state: responseJson.data.length ? responseJson.data : [] }, ()=>{
                          if(pinChange){
                              this.setValue(2, 'state', pinChange, true);
                          }
                      });
                  else if (name == 'city')
                      this.setState({ city: responseJson.data.length ? responseJson.data : [] }, ()=>{
                          if(pinChange){
                              this.setValue(2, 'city', pinChange, true);
                          }
                      });
                  else
                      this.setState({ area: data.length?data:responseJson.data.length ? responseJson.data : [] }, ()=>{
                          // if(pinChange){
                          //     this.setValue(2, 'area', pinChange, true);
                          // }
                      });
              }
              this.setState({ loading: false })

          })
          .catch(error => {
              alert(error);
              this.setState({ loading: false })
          })
  }

selectRadioBtn=(index)=>{
  var temp = this.state.radioButtons
  temp.forEach((btn, ind)=>{
       if(ind==index){
           btn.selected=true
       }else{
           btn.selected=false
       }
  });
  this.setState({radioButtons:temp})
}


changeTextField(inputText, field) {
  const specialCharReg = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
  const numReg = /^[0-9]*$/;
  const serialNumReg = /^[0-9A-Z]*$/;
  const nameReg=/^[a-zA-Z\s ]*$/;
  const addReg = /^[a-zA-Z0-9\s-/, ]*$/;
  const invoiceNumReg = /^[a-zA-Z0-9\-#/]*$/;
  const numEntry = /[0-9]*/;
  const pincode = /^[0-9]*$/
  
 if(field=='landmark' && addReg.test(inputText)){
      this.setState({landmark:inputText})
  }else if(field=='pincode' && pincode.test(inputText)){
      this.setState({pincode:inputText})
  }else if(field=='address' && addReg.test(inputText)){
      this.setState({address:inputText})
  }
}

genFrame(style, length) {
  style.height = length > 7 ? 200 : - 1;
  style.left = Dimensions.get("screen").width*0.10;
  style.width = Dimensions.get('screen').width*0.80;
  // style.left = Dimensions.get("screen")*0.10;
  return style;
  }
  render() {
    let states = this.state.state.map((element)=>element.state_name);
    let cities = this.state.city.map((element)=>element.city_name);
    let countries = this.state.country.map((element)=>element.country_name);
    let areas = this.state.area.map((element)=>element.area_name);
    if(this.state.newTest){
      return(

        <View style={{flex:1, backgroundColor:"#e6e8e9"}} >
        {this.state.loading?<Loader/>:null}
       
    <ScrollView>
    <View style={{height:210,}}>
              <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
              angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
              backgroundColor:'purple', 
              alignSelf:'center',
              top:-360,
              height:500, 
              width:500,
              borderRadius:230,
              position:'absolute'
            }}
              >
            </LinearGradient>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10}}>
              
                <TouchableOpacity onPress={()=>this.setState({newTest:false},()=>this.loadData())}>
                  <Image source={require('../../assets/ic_home/back.png')} style={{height:25,width:20,marginTop:20}}/>
                </TouchableOpacity>
                <Text style={{fontSize:15,marginTop:25,color:'#e9eaff',fontFamily:'RubikMedium'}}>ADDRESS INFORMATION</Text>     
                <TouchableOpacity>
                  <Image  style={{height:20,width:20,}}/>
                </TouchableOpacity>
              </View>
  
                 {/* Header of profile page */}
            <View style={{ backgroundColor: 'transparent',height:150 , marginTop:35 }}>
      
              <Avatar
             containerStyle={{ alignSelf: 'center',  }}
             size={100}
             rounded
             activeOpacity={0.8}
            source={{uri:global.profile_image?global.profile_image:'https://img.icons8.com/clouds/100/000000/user.png'}}
            showEditButton
             overlayContainerStyle={{borderWidth:10, borderColor:'white'}}
            onEditPress={this.chooseFile.bind(this)}
             />
            </View>
            
           </View>
           <View style={{alignSelf:'stretch', flexDirection: 'row', alignItems:'center',  margin:15, marginBottom:0}}>
            {this.state.myAddress.length?<TouchableOpacity onPress={() =>{this.setState({oldAddress:true,});}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:this.state.oldAddress?'#c4c3c1':'transparent'}}>
                <Text  style={{color: this.state.oldAddress?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                  {"Existing Addresses"}
                </Text>
            </TouchableOpacity>:null}
            <TouchableOpacity onPress={() =>{this.setState({oldAddress:false,});this.cleanAddress();}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:(!this.state.oldAddress || !this.state.myAddress.length)?'#c4c3c1':'transparent'}}>
                <Text  style={{color:(!this.state.oldAddress || !this.state.myAddress.length)?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                  {"Add New Address"}
                </Text>
            </TouchableOpacity>
          </View> 
  
          {/* Main Content Screen 1 */}
          <KeyboardAvoidingView style={{...styles.inputContainerScreen3 }}>
                {
                   this.state.oldAddress && this.state.myAddress.length?<FlatList
                   data = {this.state.myAddress}
                   extraData={this.state}
                   keyExtractor={(index,item)=>index.toString()}
                   renderItem = {({item,index}) =>{
                     return(
                       <View key={index} activeOpacity={0.7}  style={{borderRadius:5, flexDirection:'row',justifyContent:'flex-start',alignItems:'center',marginHorizontal:15,marginVertical:10 }}>
                         <View style={{flex:1, backgroundColor:'white', padding:15, borderRadius:10, elevation:5}}>
                           <Text style={{fontFamily:'RubikMedium', fontSize:15,color:'black'}}>{item.address_type?item.address_type.toUpperCase():''}</Text>
                           <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.state_name}</Text>
                           <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.city_name}, {item.pincode}</Text>
                           <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.address}, {item.area_name} {item.near_by != ""? ", "+item.near_by:null}</Text>
                         </View>
                         {/* <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,elevation: 4,justifyContent: 'center', marginHorizontal:15}}>
                           <View style={{backgroundColor:this.state.SelectedAdressID == item.customer_address_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                         </View> */}
                       </View>
                       
                      )}
                 }
               />:
                <View style={{marginHorizontal:20, marginTop:10}}>
                   
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                      <View style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', elevation: 5,  }}>
                        <TextInput
                            autoCorrect={false}
                            style={{ backgroundColor:'white', 
                            color:'#707171', 
                            borderRadius:30, 
                            textAlign:'left', 
                            fontFamily:'RubikRegular',
                            fontSize:15,
                            padding:10, 
                            paddingHorizontal: 10, flex:1 }}
                            secureTextEntry={false}
                            contextMenuHidden
                            keyboardType='number-pad'
                            maxLength={6}
                            placeholder="Pincode*"
                            onEndEditing={()=>this.pincodeChanged()}
                            onChangeText={value => this.changeTextField(value, 'pincode')}
                            value={this.state.pincode}
                            ref={input => this.model = input}
                        />
                      </View>
                  
                  <TouchableOpacity activeOpacity={1} onPress={()=>this.stateSelector.show()} 
                  style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', elevation: 5, padding:5, marginLeft:10  }}>
                      <ModalDropdown
                          style={{backgroundColor:'white', flex:1, borderRadius:25}}
                          ref={ (ref) => this.stateSelector = ref}
                          defaultValue={'State'}
                          onSelect={(rowData,index) =>{ 
                              let state_id = this.state.state.filter((element)=>element.state_name==states[rowData])[0].state_id;
                              if (state_id != '0') {
                                  this.setValue(2, 'state', state_id);
                                  this.getArea('city', state_id);
                                  this.citySelector.select(-1);
                                  this.areaSelector.select(-1);
                                  let temp = this.state.selectedAdd;
                                  temp[2].val = 0;
                                  temp[3].val = 0;
                                  this.setState({pincode:'', selectedAdd:temp});
                              }
                          }}
                          options={states}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          adjustFrame={style => this.genFrame(style, states.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', elevation:5, marginTop:-15 }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      />
                      <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>
              </View> 
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', }}>
                    
                  <TouchableOpacity activeOpacity={1} onPress={()=>this.citySelector.show()} 
                  style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', elevation: 5, padding:5  }}>
                      <ModalDropdown
                          style={{backgroundColor:'white', flex:1, borderRadius:25}}
                          ref={ (ref) => this.citySelector = ref}
                          defaultValue={'City'}
                          onSelect={(rowData,index) =>{ 
                              let city_id = this.state.city.filter((element)=>element.city_name==cities[rowData])[0].city_id;
                              if (city_id != '0') {
                                  this.setValue(2, 'city', city_id);
                                  this.getArea('area', city_id);
                                  this.areaSelector.select(-1);
                                  let temp = this.state.selectedAdd;
                                  temp[3].val = 0;
                                  this.setState({pincode:'', selectedAdd:temp});
                              }
                          }}
                          options={cities}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          adjustFrame={style => this.genFrame(style, cities.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', elevation:5, marginTop:-15 }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      />
                      <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>
                  
                  <TouchableOpacity activeOpacity={1} onPress={()=>this.areaSelector.show()} 
                  style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',marginBottom: 15, alignItems:'center', elevation: 5, padding:5, marginLeft:10  }}>
                      {
                      //   this.state.area.length?<ModalDropdown
                      //     style={{backgroundColor:'white', flex:1, borderRadius:25}}
                      //     ref={ (ref) => this.areaSelector = ref}
                      //     defaultValue={'Area'}
                      //     onSelect={(rowData,index) =>{ 
                      //         let area_id = this.state.area.filter((element)=>element.area_name==areas[rowData])[0].area_id;
                      //         if (area_id != '0') {
                      //             this.setValue(2, 'area', area_id);
                      //             this.state.area.forEach((item) => {
                      //                 if (area_id == item.area_id) {
                      //                     this.setState({ pincode: item.postal_code })
                      //                 }
                      //             })
                      //         }
                      //     }}
                      //     options={areas}
                      //     dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                      //     textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                      //     adjustFrame={style => this.genFrame(style, areas.length)}
                      //     dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', elevation:5, marginTop:-15 }}
                      //     dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      // />:
                      <ModalDropdown
                          style={{backgroundColor:'white', flex:1, borderRadius:25}}
                          ref={ (ref) => this.areaSelector = ref}
                          defaultValue={'Area'}
                          onSelect={(rowData,index) =>{ 
                              let area_id = this.state.area.filter((element)=>element.area_name==areas[rowData])[0].area_id;
                              if (area_id != '0') {
                                  this.setValue(2, 'area', area_id);
                                  this.state.area.forEach((item) => {
                                      if (area_id == item.area_id ) {
                                        if(item.postal_code){
                                          this.setState({ pincode: item.postal_code })
                                        }
                                        
                                      }
                                  })
                              }
                          }}
                          options={areas}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          adjustFrame={style => this.genFrame(style, areas.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', elevation:5, marginTop:-15 }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      />}
                      <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>
              </View>
              
                <TextInput
                    style={{backgroundColor:'white', 
                    color:'#707171', 
                    borderRadius:30, 
                    textAlign:'left', 
                    fontFamily:'RubikRegular',
                    fontSize:15,
                    elevation:7, 
                    padding:10, 
                    marginBottom:15,
                    paddingHorizontal: 10,}}
                    secureTextEntry={false}
                    contextMenuHidden
                    placeholder="Landmark"
                    onChangeText={value => this.changeTextField(value, 'landmark')}
                    value={this.state.landmark}
                    ref={input => this.landmark = input}/>
                    <View style={{flex:1, flexDirection:'row', justifyContent:'flex-start', 
                    alignItems:'center', marginBottom:15}}>
                        <View style={{flex:1, flexDirection:'row', width:'100%', justifyContent:'flex-start', alignItems:'center'}}>
                        
                            {
                                this.state.radioButtons.map((btn, ind)=>{
                                    return (
                                        <TouchableOpacity key={ind} onPress={()=>this.selectRadioBtn(ind)} style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                                            <View style={{height:20, width:20, borderRadius:10,justifyContent:'center', alignItems:'center', marginRight:5,backgroundColor:'white', elevation:10}}>
                                                {btn.selected?<View style={{height:10, width:10, borderRadius:5, backgroundColor:'#5960e5'}}/>:null}
                                            </View>
                                            <Text style={{fontFamily:btn.selected?'RubikBold':'RubikRegular', fontSize:14}}>{btn.label}</Text>
                                        </TouchableOpacity>
                                    );
                                })
                            
                            }
                        </View>
                    </View>
                    <TextInput
                    style={{height:100,
                      backgroundColor:'white', 
                    color:'#707171', 
                    borderRadius:10, 
                    textAlign:'left',
                    textAlignVertica :'center', 
                    fontFamily:'RubikRegular',
                    fontSize:15,
                    elevation:7, 
                    padding:10, 
                     marginTop:5,
                    paddingHorizontal: 10,}}
                    secureTextEntry={false}
                    multiline={true}
                    numberOfLines={4}
                    placeholder="Address"
                    contextMenuHidden
                    textContentType="streetAddressLine1"
                    //  onEndEditing={()=>this.alterNum.focus()}
                    onChangeText={value => this.changeTextField(value, 'address')}
                    value={this.state.address}
                    ref={input => this.address = input}
                    />
                     {/* <TextInput
                      style={{backgroundColor:'white', 
                      color:'#707171', 
                      borderRadius:30, 
                      textAlign:'left', 
                      fontFamily:'RubikRegular',
                      fontSize:15,
                      elevation:7, 
                      padding:10, 
                      marginBottom:15,
                      paddingHorizontal: 10,marginTop:15}}
                      secureTextEntry={false}
                      contextMenuHidden
                      maxLength={6}
                      placeholder="Pincode"
                      onEndEditing={()=>this.pincodeChanged()}
                      onChangeText={value => this.changeTextField(value, 'pincode')}
                      value={this.state.pincode}
                      ref={input => this.model = input}
                    /> */}
                  
                    </View>
                } 
          </KeyboardAvoidingView>
  
           {/* update button code */}
          {!this.state.oldAddress? <TouchableOpacity
                onPress={this.updateAddress}
                style={styles.updateButton}
              >
                <Image
                  source={require('../../assets/ic_home/next.png')}
                  style={{ resizeMode: 'contain', height: '100%', width:'100%'  }}
                />
              </TouchableOpacity>:null}
        
    </ScrollView>  
        </View>
        )

    }else{

  
    return (        
        <View style={{ flex: 1 ,backgroundColor:'#e6e8e9' }}>
          <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
          {this.state.dataLoading?<Loader/>:null}
        <ScrollView>

        <View style={{height:210,marginBottom:15,paddingTop:30}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:-340,
            height:500, 
            width:500,
            borderRadius:230,
            position:'absolute'
          }}
            >
          </LinearGradient>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', padding:10, marginTop:StatusBar.currentHeight}}>
            
              <TouchableOpacity onPress={()=>drawer.current.open()}>
                <Image source={require('../../assets/ic_home/align-left.png')} style={{height:20,width:20,}}/>
              </TouchableOpacity>
              <Text style={{fontSize:15,color:'#e9eaff',fontFamily:'RubikMedium'}}>PERSONAL INFORMATION</Text>     
              <TouchableOpacity>
                <Image  style={{height:20,width:20,}}/>
              </TouchableOpacity>
            </View>

               {/* Header of profile page */}
          <View style={{ backgroundColor: 'transparent',height:150 , marginTop:45 }}>
    
            <Avatar
            containerStyle={{ alignSelf: 'center',  }}
            size={100}
            rounded
            activeOpacity={0.8}
           source={{uri:this.state.profile_image?this.state.profile_image:'https://img.icons8.com/clouds/100/000000/user.png'}}
           showEditButton
            overlayContainerStyle={{borderWidth:10, borderColor:'white'}}
           onEditPress={this.chooseFile.bind(this)}
           />
          </View>
          
         </View>

            {/* Containing Input */}
            <KeyboardAvoidingView style={{ ...styles.inputContainer }}>
              {/* <KeyboardAvoidingView> */}
              <TextInput
                style={{ ...styles.inputBox, borderColor: this.state.borderColor, borderWidth: this.state.borderWidth }}
                maxLength={50}
                contextMenuHidden
                placeholder="Full Name*"
                onChangeText={value => this.changeTextNameField(value, 'fName')}
                value={this.state.fName}
                ref={input => this.fName = input}
              />
              <TextInput
                style={styles.inputBox}
                keyboardType="email-address"
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Email ID*"
                onChangeText={value => this.changeTextNameField(value, 'email')}
                value={this.state.email}
                ref={input => this.email = input}
              />

                {/* <View
                  style={{
                        backgroundColor: 'white',
                        borderRadius: 30,
                        textAlign: 'left',
                        fontSize: 15,
                        elevation: 4,
                        color: '#707171',
                        marginBottom: 15,
                      }}
                  >

                <Picker
                mode='dropdown'
                style={{color:'#707171',padding:10,paddingHorizontal: 10,}}
                  selectedValue={this.state.gender}
                  onValueChange={(itemValue, itemIndex) =>
                  this.setState({ gender: itemValue, showGenderPicker: false })
                  }
                 >
                  <Picker.Item label="Gender*" value="0" />
                  <Picker.Item label="Male" value="Male" />
                  <Picker.Item label="Female" value="Female" />
                </Picker>
              </View> */}
              <TouchableOpacity activeOpacity={1} onPress={()=>this.genderSelector.show()} 
                  style={{ flex:1, flexDirection:'row', borderRadius:25, backgroundColor: 'white',
                  marginBottom: 15, alignItems:'center', elevation: 5,shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.62,   }}>
                      <ModalDropdown
                          style={{backgroundColor:'white', flex:1, borderRadius:25}}
                          ref={ (ref) => this.genderSelector = ref}
                          defaultValue={'Select Gender*'}
                          onSelect={(rowData,index) =>{ 
                            if(rowData!=-1){
                              this.setState({gender: this.state.genders[rowData], },()=>console.warn(this.state.gender))
                            }
                              
                          }}
                          options={this.state.genders}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          adjustFrame={style => this.genFrame(style, states.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', elevation:5, marginTop:Platform.OS=='ios'?0:-15 }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      />
                      <Image source={require('../../assets/arrowblack.png')} style={{tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>

              {/* {this._renderGenderPicker()} */}

              <TextInput
                style={styles.inputBox}
                maxLength={10}
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Mobile Number*"
                keyboardType="phone-pad"
                //  onEndEditing={()=>this.alterNum.focus()}
                onChangeText={value => this.setState({ mNum: value })}
                value={this.state.mNum}
                ref={input => this.mobile = input}
                editable={false}
              />
              <View style={{ paddingBottom:5,backgroundColor:this.state.showOtp?'rgba(0,0,0,0.1)':'transparent', borderRadius:20}}>
              <TextInput
                style={styles.inputBox}
                maxLength={10}
                secureTextEntry={false}
                keyboardType="phone-pad"
                contextMenuHidden
                placeholder="Alternate Number"
                // onEndEditing={this.sendOtp}
                // onChangeText={value => this.setState({ aNum: value })}
                onChangeText={value => this.changeTextNameField(value, 'anum')}
                value={this.state.aNum}
                ref={input => this.alterNum = input}
                
              />
              {this.state.aNumVer?<Text style={TextStyles.fieldTitleNote}>Number Verified Successfully.</Text>:null}
              {this.state.verifyANum?<TouchableOpacity
              onPress={this.sendOtp}
              style={styles.getOtp}
              ><Text style={{fontSize:16, color:'purple'}}>Verify Number</Text></TouchableOpacity>:null}
              {this.state.verifyANum?<Text style={TextStyles.fieldTitleNote}>Note: Only verified number will get saved.</Text>:null}
              {this.state.showOtp?
            <View style={{flex:1, flexDirection:'row', justifyContent:'center',alignItems:'center',marginBottom:10, marginTop:10}}>
              
              <TextInput

              style={{backgroundColor:'white',marginEnd:15, textAlign:'center', fontSize:15,width:40,height:40,}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',0)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,0,()=>this.otp2.focus())}
              value={this.state.otp[0]}
              ref={(input)=>this.otp1=input}
              />
              <TextInput
              style={{backgroundColor:'white',marginEnd:15, width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',1)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,1,()=>this.otp3.focus())}
              value={this.state.otp[1]}
              ref={(input)=>this.otp2=input}
              />
              <TextInput
              style={{backgroundColor:'white',marginEnd:15,width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',2)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,2,()=>this.otp4.focus())}
              value={this.state.otp[2]}
              ref={(input)=>this.otp3=input}
              />
              <TextInput
              style={{backgroundColor:'white', width:40,height:40, textAlign:'center', fontSize:15}}
              keyboardType="phone-pad"
              onFocus={()=>this.otpChanged('',3)}
              maxLength={1}
              secureTextEntry={true}
              onChangeText={(value)=>this.otpChanged(value,3) }
              value={this.state.otp[3]}
              ref={(input)=>this.otp4=input}
              />
             
              </View>:null
              }
              {this.state.showOtp?
              <View style={{flex:1, flexDirection:"column", justifyContent:'center'}} >
              {this.state.verifyOtp?
                <TouchableOpacity
              onPress={this.verifyOtp}
              style={styles.updateButton}
              >
              <Image
                source={require('../../assets/ic_home/next.png')}
                style={{ resizeMode: 'contain', height: 70 }}
              />
            </TouchableOpacity>
              :null}
              <Text style={TextStyles.fieldTitle}>
                Didn't receive the verification code?
              </Text>
              <TouchableOpacity onPress={this.sendOtp}>
                <Text style={{textAlign:'center', color:'purple', fontSize:15}}>Resend verification code.</Text>
              </TouchableOpacity>
              </View>
              :null}
              </View>
            
              <TextInput
                style={styles.inputBox}
                maxLength={50}
                secureTextEntry={false}
                contextMenuHidden
                placeholder="Occupation*"
          // onEndEditing={()=>this.birthday.focus()}
                onChangeText={value => this.changeTextNameField(value,'occu')}
                value={this.state.occupation}
                ref={input => this.occupation = input}
              />
             
              {/* <View style={{ ...styles.inputBox, }} >
              <Text style={{position:'relative',marginTop:-20, alignSelf:'flex-end'}} >Birthday</Text> */}
             <View style={styles.inputBoxDate}>
             <DatePicker
             style={{width:'100%'}}
             date={this.state.birthday?new Date(this.state.birthday):''}
              format="DD/MM/YYYY"
              mode="date"
              placeholder="Birthday"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              iconSource={require('../../assets/ic_home/cal.png')} 
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 5,
                  resizeMode:'contain',
                  top: 10,
                  height:15,
                  width:15,
                  marginLeft: 0
                },
                dateInput:{
                  borderWidth:0
                },
                placeholderText:{
                  fontSize:15,
                  color:'#707171',
                  padding: 10,
                  alignSelf:'flex-start'
                },
                dateText:{
                  fontSize:15,
                  padding: 10,
                  alignSelf:'flex-start'
                }
              }}
              onDateChange={(date) => {this.setState({birthday:this.changeFormat(date)})}}
            />
             </View>
              
             <View style={styles.inputBoxDate}>
             <DatePicker
             style={{width:'100%',}}
              date={this.state.anniversary?new Date(this.state.anniversary):''}
              mode="date"
              format="DD/MM/YYYY"
              placeholder="Anniversary"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"     
              iconSource={require('../../assets/ic_home/cal.png')}                                                                                                                                                                                                                                   
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 5,
                  resizeMode:'contain',
                  top: 10,
                  height:15,
                  width:15,
                  marginLeft: 0
                },
                dateInput:{
                  borderWidth:0
                },
                placeholderText:{
                  fontSize:15,
                  padding: 10,
                  color:'#707171',
                  alignSelf:'flex-start'
                },
                dateText:{
                  fontSize:15,
                  padding: 10,
                  alignSelf:'flex-start'
                }
              }}
              onDateChange={(date) => {this.setState({anniversary: this.changeFormat(date)})}}
            />
             </View>
            
            </KeyboardAvoidingView>

            {/* update button code */}
            <TouchableOpacity
              onPress={this.updateData}
              style={styles.updateButton}
            >
              <Image
                source={require('../../assets/ic_home/next.png')}
                style={{ resizeMode: 'contain', height: 70, }}
              />
            </TouchableOpacity>
          </ScrollView>
        </View>
      // </ImageBackground>
    );}
  }
}

Profile.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

Profile.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);

