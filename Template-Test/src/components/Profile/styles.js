import { StyleSheet } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'blue',
    backgroundColor: '#EAF0F0',
    padding: 40,
  },
  containers: {flex: 1,justifyContent: 'flex-start',alignItems: 'stretch',color: Colors.white,backgroundColor: Colors.white,padding: 20, },
  inputContainer:{ flex:1, justifyContent:'flex-start', alignItems:'stretch',marginTop:20, backgroundColor: 'transparent',marginHorizontal:25},
  welcome:{ textAlign:'center', fontSize:30,fontWeight:'800', color:'transparent'},
  sub:{ textAlign:'center', fontSize:15, color:'transparent'},
  
  inputBox:{  backgroundColor:'white', color:'#707171', borderRadius:30, textAlign:'left', fontSize:15,elevation:4, padding:10, marginBottom:15,paddingHorizontal: 10,
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,
      
      },
  inputBoxDate:{  backgroundColor:'white', color:'#707171', borderRadius:30, textAlign:'left', elevation:4, 
  padding:5, marginBottom:15, borderWidth:0, flex:1, alignItems:'stretch', 
  flexDirection:'column', justifyContent:'center'},
  inputTextBox:{  overflow: 'hidden' , backgroundColor:'white', borderRadius:30,color:'#707171', textAlign:'left', fontSize:15,elevation:4,  padding: 10 , marginBottom:0,paddingHorizontal: 10,

  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2,
  },
  shadowOpacity: 0.23,
  shadowRadius: 2.62,
      
      },
  
  inputBoxActive:{ backgroundColor:'white', borderRadius:30,color:'#707171', textAlign:'left', fontSize:15,elevation:4, padding:10,paddingHorizontal: 10,
  marginBottom:15, borderColor:'purple', borderWidth:1.5},
  
  updateButton:{flex:1, height:70, width:70,justifyContent:'center', alignItems:'center',alignSelf:'center', marginVertical:15 },
  getOtp:{flex:0.2,justifyContent:'center', alignItems:'center',alignSelf:'center' }
});

export default styles;
