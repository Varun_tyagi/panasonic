import React, { Component } from 'react';
import {View,Text,Image,FlatList,Platform,Switch,Picker,TouchableOpacity,ImageBackground,Dimensions,ScrollView,StatusBar,TextInput,AsyncStorage,} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import DatePicker from 'react-native-datepicker';
import ApplianceDetail from '../ApplianceDetail';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import ModalDropdown from 'react-native-modal-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import * as authFun from '../../helpers/auth';
import * as constants from '../../helpers/constants';
import Loader from '../common/Loader';


// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
const widthFull = Dimensions.get('window').width;
const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};

class ServiceTrackDetail extends Component {
 
  constructor(props) {
    super(props);
    this.state={
      loading:false,
      token:'',
      customerID:'',
      serviceTrackRecord:[],
      search:'',
    }
    
   }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

 
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
      },()=>{
       this.getData();

      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  
  onAfterSubmit(){

    this.setState({SelectedType:"HOME",stateName:"",area_name:"",pincode:"",address:"",landmark:"",SelectedAdressID:"",SelectedAddress:[]})

  }



  async componentDidMount(){
    var token = await  this.setToken();
     if(token){
       this.loadData(); 
       
     }
     
   }

   async getData() {
     this.setState({loading:true});
     let that = this;
     let item = this.props.navigation.state.params.item;
// alert(JSON.stringify(item));
   
    axios.get('https://www.ecarewiz.com/ewarrantyapi/trackServiceRequest?ascid='+item.ASCJobID+'&access_token='+this.state.token).then((response)=>{
      // alert(JSON.stringify(response.data.data))  
    if(response.data.status)
      that.setState({serviceTrackRecord:response.data.data})
      else
      alert(response.message);
      that.setState({loading:false});
    }).catch(function(error) {
      console.warn(error);
      that.setState({loading: false}); 
    });

  }

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example * */
    this.forceUpdate();
  };
  // eslint-disable-next-line class-methods-use-this
  
  capitalize=(txt)=>{
    return txt.split(' ').map((word)=>word.split('')[0].toUpperCase()+word.split('').splice(1,).join('')).join(' ');
  }
  getDate = (dt)=>{
    const months =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')
    
    return dt[2]+' '+months[dt[1][0]==0?dt[1][1]:dt[1]]+' '+dt[0];
  }
  render() {
    // let item = Object.keys(this.state.serviceTrackRecord).length?this.state.serviceTrackRecord:undefined;
    // console.warn(JSON.stringify(this.state.serviceTrackRecord));
    return (
      <View style={{flex:1, backgroundColor:"#edf4f6",justifyContent:'flex-start'}} >
       <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
       {this.state.loading?<Loader/>:null}
       
       <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#edf4f6'}}>
         <View style={{marginTop:StatusBar.currentHeight, flex:1,height:100,marginBottom:10,
           justifyContent:'flex-start', alignItems:'center',paddingBottom:Platform.OS=='ios'?0:15}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:-780,
            height:1000, 
            width:1000,
            borderRadius:500,
            position:'absolute'
          }}
            >
          </LinearGradient>
             <View style={{flexDirection:'row', justifyContent:'flex-start',
              alignItems:'center', padding:5, paddingTop:Platform.OS=='ios'?30:0, marginTop:15}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1 }} >
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'#e9eaff',fontFamily:'RubikMedium', fontSize:20, textAlign:'center', marginHorizontal:20}}>TRACK AND TRACE YOUR REQUEST</Text>
            
              </View>
                
         </View>
         {/* PRODUCT DETAILS */}
          {this.state.serviceTrackRecord.length?
            <FlatList
            data={this.state.serviceTrackRecord} 
            renderItem={({item, index}) => {
            return (
              <View style={{flex:1, margin:15,marginTop:0, alignItems:'stretch',}}>
              <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'white'}}>JOB ID: {item.JobNumber}</Text>
              
              <View style={{backgroundColor:'white', borderRadius:10, flex:1, padding:15, elevation:2, marginTop:10, alignItems:'stretch',  }}>
              {/* Status of Job */}
                <View style={{flex:1}}>
                  <View style={{flex:1, flexDirection:'row',alignItems:'flex-start',   borderLeftColor:'#3875c9',borderLeftWidth:1}}>
                    <View style={{height:6, width:6, borderRadius:3, backgroundColor:'#3875c9', marginRight:10, marginLeft:-3}}></View>
                    <Text style={{flex:0.5,color:'#c4c3c1', fontFamily:'RubikMedium',fontSize:12, marginTop:-5}}>Last Status</Text>
                    <Text style={{flex:1,color:'#000', fontFamily:'RubikMedium',fontSize:12, marginTop:-5}}>{item.JobstatusName?item.JobstatusName:'-'}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row',alignItems:'center', paddingVertical:5,  borderLeftColor:'#3875c9',borderLeftWidth:1}}>
                    <View style={{height:6, width:6, borderRadius:3, backgroundColor:'#3875c9', marginRight:10, marginLeft:-3}}></View>
                    <Text style={{flex:0.5,color:'#c4c3c1', fontFamily:'RubikMedium',fontSize:12}}>Creation Date</Text>
                    <Text style={{flex:1,color:'#000', fontFamily:'RubikMedium',fontSize:12}}>{authFun.getDate(item.JobCreationDate,'T')}</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row',alignItems:'flex-end', borderLeftColor:'#3875c9',borderLeftWidth:1,}}>
                    <View style={{height:6, width:6, borderRadius:3, backgroundColor:'#3875c9', marginRight:10, marginLeft:-3}}></View>
                    <Text style={{flex:0.5,color:'#c4c3c1', fontFamily:'RubikMedium',fontSize:12,marginBottom:-5,}}>Last Updated On</Text>
                    <Text style={{flex:1,color:'#000', fontFamily:'RubikMedium',fontSize:12,marginBottom:-5,}}>{authFun.getDate(item.LastUpdatedOn,'T')}</Text>
                  </View>
                </View>               
                
                {/* Product Information */}
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5, marginTop:15, padding:10}}>
                  <Text style={{fontFamily:'RubikMedium', fontSize:12, color:'#3875c9'}}>PRODUCT INFORMATION</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row',  }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Product</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.ProductName}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Product Category</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.SubProductGroupName}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Model Name</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.ModelName}</Text>
                </View>
                {item.serial_no?<View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Serial No.</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.SerialNumber?item.SerialNumber:'-'}</Text>
                </View>:null}
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Purchase Date</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.PurchaseDate?authFun.getDate(item.PurchaseDate,item.PurchaseDate.indexOf('T')!=-1?'T':' '):'-'}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Request Type</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.JobClassification}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row', }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Warranty</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.Warranty?item.Warranty:'-'}</Text>
                </View>
                {/* PERSONAL INFORMATION */}
                {/* <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5, padding:10}}>
                    <Text style={{fontFamily:'RubikMedium', fontSize:12, color:'#3875c9'}}>PERSONAL INFORMATION</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row',  }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Name</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.name}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row',  }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Mobile No.</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.mobile}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row',  }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Email-Id</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}>{item.email}</Text>
                </View>
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5,borderTopWidth:0,flexDirection:'row',  }}>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', paddingVertical:10,paddingLeft:10,backgroundColor:'rgba(230,232,233,0.3)'}}>Address</Text>
                  <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f',  paddingVertical:10, paddingLeft:10}}><Text style={{fontFamily:'RubikMedium'}}>{item.address_type+'\n'}</Text>{item.address+',\n'+item.area_name+','+item.city_name+',\n'+item.near_by+',\n'+item.state_name+' - '+item.pincode}</Text>
                </View> */}
                {/* Details of Request */}
                <View style={{flex:1, borderColor:'#c4c3c1', borderWidth:0.5, padding:10}}>
                    <Text style={{fontFamily:'RubikMedium', fontSize:12, color:'#3875c9'}}>DETAILS OF THE REQUEST</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', marginTop:10}}>{item.JobRemarks?item.JobRemarks:'-'}</Text>
                </View>
                
              </View>
              
              <View style={{backgroundColor:'white', borderRadius:10, flex:1, padding:15, elevation:2, marginTop:15, marginBottom:20}}>
                <View style={{flex:1,  padding:10}}>
                    <Text style={{fontFamily:'RubikMedium', fontSize:12, color:'#3875c9'}}>REMARK</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'#4f4f4f', marginTop:10}}>{item.JobRemarks?item.JobRemarks:'-'}</Text>
                </View>
              </View>
            </View>
            )}}/>
          :null}
         </ScrollView>
         </View>);
  }
}

ServiceTrackDetail.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ServiceTrackDetail.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTrackDetail);
