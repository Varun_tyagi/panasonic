import * as authFun from '../../helpers/auth';
import {  setConfiguration } from './utils/configuration';
import getUser from '../../selectors/UserSelectors';

export default setConfig = async ()=>{
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
}