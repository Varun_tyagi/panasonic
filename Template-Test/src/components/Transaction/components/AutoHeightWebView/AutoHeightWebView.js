import React, { Component } from 'react';
import { Dimensions,Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
const { width, height ,View} = Dimensions.get('window');
import AutoHeightWebView from 'react-native-autoheight-webview'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
export function AutoHeightWebViewCustom(html,color){
  //console.html)
  var fontname =   Platform.OS == "android"?'file:///android_asset/fonts/RubikBold.ttf':null;
  let wid = "100%"
  try {
  html.substring(0, 3) == "<p>" ? wid = "80%": wid = "98%";
}
catch(err) {
}

  return(
    <AutoHeightWebView
style={{ width: wid,fontSize: wp('4%'),
marginTop: wp('1.33%'),justifyContent: 'center',alignSelf:wid == "80%" ?'center':null
 }}
customScript={`document.body.style.background = '#edf4f6';`}
injectedJavaScript={
`const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.77, maximum-scale=1, user-scalable=1'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); true`}
// add custom CSS to the page's <head>
customStyle={`
* {
font-family: ${fontname};
}
p {
font-size: 15px;
}
`}
files={[{
href: 'cssfileaddress',
type: 'text/css',
rel: 'stylesheet'
}]}
source={{ html: html }}
zoomable={false}
/>
);
}