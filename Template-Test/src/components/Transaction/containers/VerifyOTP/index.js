
import { NavigationActions } from 'react-navigation';
import VerifyOTP from './VerifyOTP';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { verifyotpApi } from '../../modules/VerifyOTP';

import { Map } from 'immutable';


const mapStateToProps = state => ({
  isBusy: state.VerifyOTPReducer.isBusy,
  response: state.VerifyOTPReducer.response

});

//export default Login;

export default connect(
  mapStateToProps,
  dispatch => {
    return {
      verifyotpApi: bindActionCreators(verifyotpApi, dispatch),
      navigate: bindActionCreators(NavigationActions.navigate, dispatch)
    };
  }
)(VerifyOTP);

// export default VerifyOTP;
