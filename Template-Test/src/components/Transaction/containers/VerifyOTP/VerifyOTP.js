import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, Keyboard, ScrollView, SafeAreaView, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import {setConfiguration} from '../../utils/configuration';


import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class VerifyOTP extends React.Component {

   state = {
       phone: '',
       firstDigit: '',
       secondDigit: '',
       thirdDigit: '',
       fourthDigit: '',

   };


  componentDidMount()
  {
    const mobileNumber = this.props.navigation.getParam('mobileNumber', '');
console.log('mobileNumber mobileNumber', mobileNumber);
     this.setState({
       phone: mobileNumber
     });
  }



     verify(mobileNumber,otp) {
       Keyboard.dismiss();
       this.props.verifyotpApi(mobileNumber,otp)
         .then(() => this.afterVerifyotpApi())
       .catch(e => this.showAlert(e.message, 300));

     }

     afterVerifyotpApi() {
       console.log("isBusy value --- ",this.props.isBusy);
      console.log("response value --- ",this.props.response);
      console.log('customer id is -----', this.props.response.data.customer_id);
      var customer_id = this.props.response.data.customer_id;
      setConfiguration('customer_id', customer_id);
       this.props.navigation.navigate('ReferalCode');

     }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

goToNextScreen(){
     var otp = this.state.firstDigit + this.state.secondDigit + this.state.thirdDigit + this.state.fourthDigit;
     this.verify(this.state.phone, otp);
}

goToCorporate(){
  this.props.navigation.navigate('Login');

}

  render() {

    return (

      <View style={styles.container}>
      <Image resizeMode="cover" style = {{width: '100%', height: '100%', position: 'absolute', top:0, left: 0}} source = {require('../../../../assets/screenBG.png')}/>
        <View>

           <View style={{width: '100%', height: wp('71.33%'), backgroundColor: 'transparent'}}>
             <Image resizeMode="cover" style = {{width: '100%', height: '100%'}} source = {require('../../../../assets/referal.png')}/>
           </View>


           <View style={{width: '100%', height: 30, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

              <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
               <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
                        <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/fill.png')}/>
                <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
           </View>



           </View>

<ScrollView style={{marginBottom: 0, backgroundColor: 'transparent'}}>

           <View style={{width: '100%', height: 'auto',marginTop:30, backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

      <Text style={styles.txtTitle}> OTP VERIFICATION </Text>



      <View style={{width: '100%', height: 'auto', marginTop: wp('5.33%'), backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

       <Text style={styles.txtTitleDesc}> Please type the verification code sent to </Text>
       <Text style={styles.txtPrivacy}> +91 {this.state.phone} </Text>
       </View>

       <View style={{width: '100%', height: 'auto', marginTop: wp('5.33%'), flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

 <View style={{width: 40, height: 50, marginHorizontal:5, backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>

 <TextInput
    style={styles.txtFld}
    placeholder="-"
    returnKeyType = { "next" }
    autoFocus={true}
    blurOnSubmit={false}
    keyboardType={"phone-pad"}
    placeholderTextColor= 'black'
    maxLength = {1}
    onSubmitEditing={()=>{this.secondDigit.focus(),this.setState({secondDigit:""})}}
    onChangeText={(firstDigit) => {this.setState({firstDigit})}}
    value={this.state.firstDigit}
    />

 </View>


 <View style={{width: 40, height: 50, marginHorizontal:5, backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>
 <TextInput
    style={styles.txtFld}
    ref={(input) => { this.secondDigit = input; }}
    placeholder="-"
    keyboardType={"phone-pad"}
    blurOnSubmit={false}
    returnKeyType = { "next" }
    placeholderTextColor= 'black'
    maxLength = {1}
    onSubmitEditing={()=>{this.thirdDigit.focus(),this.setState({thirdDigit:""})}}
    onChangeText={(secondDigit) => {this.setState({secondDigit})}}
   value={this.state.secondDigit}
 />

 </View>

 <View style={{width: 40, height: 50, marginHorizontal:5, backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>

 <TextInput
    style={styles.txtFld}
    placeholder="-"
    keyboardType={"phone-pad"}
    blurOnSubmit={false}
    ref={(input) => { this.thirdDigit = input; }}
    placeholderTextColor= 'black'
    maxLength = {1}
    returnKeyType = { "next" }
    onSubmitEditing={()=>{this.fourthDigit.focus(),this.setState({fourthDigit:""})}}
    onChangeText={(thirdDigit) => {this.setState({thirdDigit})}}
   value={this.state.thirdDigit}
 />

 </View>

 <View style={{width: 40, height: 50, marginHorizontal:5, backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>

        <TextInput
           style={styles.txtFld}
           placeholder="-"
           ref={(input) => { this.fourthDigit = input; }}
           returnKeyType = { "done" }
           keyboardType={"phone-pad"}
           placeholderTextColor= 'black'
           maxLength = {1}

           onChangeText={(fourthDigit) => {this.setState({fourthDigit})}}
           value={this.state.fourthDigit}
        />

 </View>

       </View>


      </View>



      <View style={{width: '100%', height: 'auto', marginTop: wp('5.33%'), backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
     <TouchableOpacity>
     <Text style={styles.txtTitleDesc}> Didn't receive the verification code </Text>

      <Text style={[styles.txtPrivacy ,{textDecorationLine: 'underline',textAlign:'center'} ]}> Resend the code </Text>
      </TouchableOpacity>

 </View>



<View style={{width: '100%', height: 100, marginTop: wp('5.33%'), flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
<TouchableOpacity onPress={()=> this.goToNextScreen()}>
  <Image resizeMode="contain" style = {{width: 100, height: 100 }} source = {require('../../../../assets/next.png')}/>
  </TouchableOpacity>

</View>




</ScrollView>





           </View>



    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('8%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtCountryCode:{
        color: 'black',
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular"
      },
      txtPrivacy:{
        color: 'white',
        fontSize: wp('4.5%'),
        fontFamily: "RubikBold",
        borderColor: 'white',
        borderBottomWidth:0.6
      },
      txtUserLogin:{
        color: 'white',
        fontSize: wp('6.66%'),
        fontFamily: "RubikBold"

      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

             txtFld:{
               height: '90%',
               marginTop: 4,

               width:'90%',
              fontSize: wp('5.33%'),
              fontFamily: "RubikBold",
              textAlign: 'center',
              color: 'black'
             },


 })
