
import UserLogin from './UserLogin';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavigationActions } from 'react-navigation';
import { loginWithPhone } from '../../modules/auth';
import { getTokenAPI } from '../../modules/GetToken';

import { Map } from 'immutable';


const mapStateToProps = state => ({
  isBusy: state.AuthReducer.isBusy,
  response: state.AuthReducer,
  isBusyToken: state.GetTokenReducer.isBusy,
  responseToken: state.GetTokenReducer
});

//export default Login;

export default connect(
  mapStateToProps,
  dispatch => {
    return {
      loginWithPhone: bindActionCreators(loginWithPhone, dispatch),
      getTokenAPI: bindActionCreators(getTokenAPI, dispatch),
      navigate: bindActionCreators(NavigationActions.navigate, dispatch)
    };
  }
)(UserLogin);


//export default UserLogin;
