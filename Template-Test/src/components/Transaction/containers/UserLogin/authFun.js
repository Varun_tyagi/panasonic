import {AsyncStorage} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
// import * as constants from './constants';

export const checkAuth = async ()=>{
    var access='';
    var access_token = await AsyncStorage.getItem('access_token');
    var token_time = await AsyncStorage.getItem('token_time');
    if(access_token && parseInt(token_time) > (new Date().getTime())){
      access = access_token;
    }else{
       access = await getAuth();
    }

    return access;
}

export const getAuth =async ()=>{
  var access = ''
   access= fetch('http://neuronimbusapps.com/ewarranty/'+'token/authorize', {
          method: "POST",
          headers: {'Content-Type':'application/json'},
          body:JSON.stringify({
            "grant_type":"client_credentials",
            "client_id":"testclient",
            "client_secret":"testpass"
          })
        }).then(res => res.json())
        .then(async(result) => {
          if(Object.keys(result).length){
            access = result.access_token;

            await AsyncStorage.setItem('token_time',(new Date().getTime()+parseInt(parseInt(result.expires_in)*1000))+'');
            await AsyncStorage.setItem('access_token',result.access_token);
            return access;
          }else{

            alert(result.message);
            // access = '';
            return '';
          }

        },
        (error) => { alert(error);}
      );
      return access;
}
