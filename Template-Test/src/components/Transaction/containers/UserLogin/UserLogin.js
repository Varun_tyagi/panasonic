import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, ScrollView, SafeAreaView, FlatList, Keyboard, Alert, AsyncStorage } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import authFun  from './authFun';
import {setConfiguration} from '../../utils/configuration';



import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';


type Props = {
  navigate: PropTypes.func.isRequired
};
export default class UserLogin extends React.Component {

   state = {
       highlights: [],
       phone: ''
   };


  componentDidMount()
  {
     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });
  }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

getAccessToken(){

      var access = ''
       access= fetch('http://neuronimbusapps.com/ewarranty/'+'token/authorize', {
              method: "POST",
              headers: {'Content-Type':'application/json'},
              body:JSON.stringify({
                "grant_type":"client_credentials",
                "client_id":"testclient",
                "client_secret":"testpass"
              })
            }).then(res => res.json())
            .then(async(result) => {
              if(Object.keys(result).length){
                access = result.access_token;
                global.accessTokan = access;
                await AsyncStorage.setItem('token_time',(new Date().getTime()+parseInt(parseInt(result.expires_in)*1000))+'');
                await AsyncStorage.setItem('access_token',result.access_token);
                console.log(' access token:-----', access);
                return access;
              }else{

                alert(result.message);
                // access = '';
                return '';
              }

            },
            (error) => { alert(error);}
          );

          console.log(' accesssss token:-----', access);


          return access;

}


async goToNextScreen(){
  //fortest
  // this.afterVerifyotpApi();
  // var token = await this.getAccessToken();
  // console.log(' accesssss token token:-----', token);
  // setConfiguration('accessToken', token);
  // return;

  //original
   var token = await this.getAccessToken();
   console.log(' accesssss token token:-----', token);
   setConfiguration('accessToken', token);

 //this.callGetTokenAPI();

  this.login(token);
//  this.props.navigation.navigate('VerifyOTP');
}

afterVerifyotpApi() {

 //var customer_id = this.props.response.data.customer_id;
 setConfiguration('customer_id', "31");

  this.props.navigation.navigate('ReferalCode');

}
goToCorporate(){
  this.props.navigation.navigate('Login');

}
openPrivacy(){
  this.props.navigation.navigate('Privacy');
}




   login(token) {
     Keyboard.dismiss();
     this.props.loginWithPhone(this.state.phone,token)
       .then(() => this.afterLogin())
     .catch(e => this.showAlert(e.message, 300));

   }

   afterLogin() {
     console.log("isBusy value --- ",this.props.isBusy);
    console.log("response value --- ",this.props.response);
      this.props.navigation.navigate('VerifyOTP', {mobileNumber: this.state.phone});
   }


      callGetTokenAPI() {
        Keyboard.dismiss();
        this.props.getTokenAPI()
          .then(() => this.afterCallGetTokenAPI())
        .catch(e => this.showAlert(e.message, 300));
      }

      afterCallGetTokenAPI() {
        console.log("isBusy value --- ",this.props.isBusyToken);
       console.log("response value --- ",this.props.responseToken);
       // var id_token = this.props.response.response.id_token;
       //   setConfiguration('id_token', id_token);

     //  this.props.navigation.navigate('HomeLanding');


      }



  render() {

    return (

      <View style={styles.container}>
      <Image resizeMode="cover" style = {{width: '100%', height: '100%', position: 'absolute', top:0, left: 0}} source = {require('../../../../assets/screenBG.png')}/>
        <View >

           <View style={{width: '100%', height: wp('69.33%'), backgroundColor: 'transparent'}}>
             <Image resizeMode="cover" style = {{width: '100%', height: '100%'}} source = {require('../../../../assets/login.png')}/>
           </View>


           <View style={{width: '100%', height: 30, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

              <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
               <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
                        <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/fill.png')}/>
                <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
           </View>
           </View>
           <ScrollView style={{marginBottom: 0, backgroundColor: 'transparent'}}>

           <View style={{width: '100%', height: 'auto', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

           <Text style={styles.txtTitle}> USER LOGIN </Text>

           <View style={{width: '80%', flexDirection: 'row', height: wp('13.33%'), marginTop: wp('5.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>

           <Text style={styles.txtCountryCode}> +91 </Text>

           <TextInput
           style={styles.txtFld}
           placeholder="Enter your Mobile Number"
           placeholderTextColor= 'black'
           maxLength = {10}
           numberOfLines={1}
           onChangeText={(phone) => this.setState({phone})}
           value={this.state.phone}
           />

           </View>

           </View>



           <View style={{width: '100%', height: 'auto', marginTop: wp('5.33%'), backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

           <Text style={styles.txtTitleDesc}> By loging in, you agree to our Terms & Conditions </Text>

           </View>
           <View style={{width: '100%', height: 'auto', flexDirection: 'row', backgroundColor:'transparent', justifyContent:'center', alignItems:'center'}}>

           <Text style={styles.txtTitleDesc}> and that you have read our </Text>
           <TouchableOpacity onPress={()=> this.openPrivacy()} style={{marginTop: 4}}>
           <Text style={styles.txtPrivacy}> Privacy Policy</Text>
           </TouchableOpacity>

           </View>



           <View style={{width: '100%', height: 100, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
           <TouchableOpacity onPress={()=> this.goToNextScreen()}>
           <Image resizeMode="contain" style = {{width: 100, height: 100 }} source = {require('../../../../assets/next.png')}/>
           </TouchableOpacity>

           </View>


           <TouchableOpacity onPress={()=>this.goToCorporate()} style={{width: '100%', height: 'auto', flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
           <Image resizeMode="contain" style = {{width: 24, height: 24 }} source = {require('../../../../assets/corporate.png')}/>
           <Text style={styles.txtUserLogin}> Corporate Login </Text>

           </TouchableOpacity>
           </ScrollView>
           </View>




    );
  }
}








 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: 'transparent'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('8%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtCountryCode:{
        color: 'black',
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular"
      },
      txtPrivacy:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikBold",
        borderColor: 'white',
        borderBottomWidth:0.6,
        textDecorationLine: 'underline'

      },
      txtUserLogin:{
        color: 'white',
        fontSize: wp('6.66%'),
        fontFamily: "RubikBold"

      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

             txtFld:{
               height: '90%',
               marginTop: 4,
               marginLeft: wp('5%'),
               width:'70%',
              fontSize: wp('4.58%'),
              fontFamily: "RubikRegular",
              color: 'black'
             },


 })
