
      <View style={styles.container}>


      <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>


          <View style={styles.header}>
            <Image resizeMode="cover" style = {{width: '100%', height:'100%', position:'absolute'}} source = {require('../../../../assets/header.png')}/>
            <View style={{ width:'100%', height: wp('10.66%'), marginTop: wp('13.33%'),  backgroundColor:'transparent', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.txtTitle}> {this.state.oldAddress?"MY ADDRESSES":"ADDRESS"}  </Text>
              <TouchableOpacity onPress={()=>this.goBack()} style={styles.backTouchable}>
                <Image resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/backwhite.png')}/>
              </TouchableOpacity>


            </View>
            <View style={{width: '100%', height: wp('10.33%'), marginTop:wp('5%'), justifyContent:'center', alignItems: 'center'}}>
              <Image resizeMode="contain" style = {{width: wp('10.33%'), height:wp('10.33%'),tintColor:"white"}} source = {require('../../../../assets/locationicon.png')}/>

              <Image resizeMode="contain" style = {{width: wp('20%'),  height:30}} source = {require('../../../../assets/Addressbar.png')}/>
            </View>






          </View>


          <View style={{width: '100%', alignItems: 'center',  marginTop: 0, backgroundColor:'red'}}>
            <Image resizeMode="cover" style = {{width: '100%', height:wp('10.66%')}} source = {require('../../../../assets/curve.png')}/>
              {/* <Text style={{position: 'absolute',right:5}}></Text> */}
            <View  style={{width: '90%', marginTop: 10,  backgroundColor: 'transparent',}}>

                  {this.state.oldAddress?<FlatList
                    data = {this.state.Myaddresses}
                    extraData={this.state}
                    style={{backgroundColor:'blue', marginBottom:60}}
                    keyExtractor={(index,item)=>index.toString()}
                    renderItem = {({item,index}) =>{
                      return(<TouchableOpacity activeOpacity={0.5} onPress={()=>this.setState({SelectedAdressID:item.customer_address_id,SelectedAddress:[item]})} style={[styles.listTile,{flexDirection: 'row',alignItems: 'center'}]}>

                        <View style={{width: '90%',flexDirection: 'row',height: '100%',borderRadius: 10,marginTop: 0,backgroundColor: 'transparent'}}>

                          <View style={{width: '100%',justifyContent: 'center',backgroundColor: 'transparent',margin: 10,}}>

                            <Text style={{color: '#000000',fontSize: wp('4%'),fontFamily: "RubikBold",}}>
                              {item.address_type}
                            </Text>

                            <Text style={{color: '#000000',fontSize: wp('4%'),fontFamily: "RubikRegular",}}>
                              {item.state_name}
                            </Text>

                            <Text style={{color: 'black',fontSize: wp('4%'),fontFamily: "RubikRegular"
                              }}>{item.city_name}, {item.pincode}
                            </Text>
                            <Text style={{color: 'black',fontSize: wp('4%'),fontFamily: "RubikRegular"}}>
                            {item.address}, {item.area_name} {item.near_by != ""? ", "+item.near_by:null}
                            </Text>



                          </View>
                        </View>
                        <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}}>
                          <View style={{backgroundColor:this.state.SelectedAdressID == item.customer_address_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                        </View>
                      </TouchableOpacity>
                       )}
                  }
                />:
                <View>
                  <View style={{width: wp("85%"),height: hp("6%"),backgroundColor: "#d9dbdc",alignItems: 'center',alignSelf: 'center',borderRadius: 30,margin: 10,flexDirection: 'row'}}>
                <Image resizeMode="contain" style = {{width: wp('6.33%'), height:wp('6.33%'),tintColor:"gray",alignSelf: 'center',marginLeft:10}} source = {require('../../../../assets/locationicon.png')}/>
                <View style={{alignItems: 'flex-start',justifyContent: 'flex-start'}}>
                  <Text style={{
                    color: 'gray',
                    fontSize: wp('4%'),
                    marginLeft: wp('2%'),
                    fontFamily: "RubikRegular"
                  }}>Detect My Location</Text>
                  <Text style={{
                    color: 'gray',
                    fontSize: wp('4%'),
                    marginLeft: wp('2%'),
                    fontFamily: "RubikBold"
                  }}>{this.state.switch1Value == true?"ON":"OFF"}</Text></View>
                  <View style={{position: 'absolute',right: 10}}>
                    <Switch
                      thumbColor={this.state.switch1Value == false?"white": "#ff8ba3"}
                      trackColor={"white"}
                      trackColor={{ true: 'white', false:'#ffffff'  }}
                      onValueChange = {this.toggleSwitch1}
                      value = {this.state.switch1Value}/>
                    </View>
                  </View>

                  <View style={{justifyContent: 'space-around',alignItems: 'center',flexDirection: 'row',padding:  5}}>

                    <ModalDropdown
                      defaultValue={this.state.SelectedState}
                      defaultIndex ={-1}
                      options={StateName}
                      onSelect={(rowData,index) =>{ this.getData("CITY", this.state.StateName[rowData].state_id);this.setState({stateName:index,SelectedCity:"Select City"}); this.lookaheadFilter.select(-1); }}
                      dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                      style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, padding:5}}
                      textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',justifyContent: 'center',alignSelf: 'center',alignItems: 'center',margin: 5}}
                      animated={ true}
                      dropdownStyle={{width: '60%',height: hp("30%"),fontFamily: 'RubikRegular', marginTop:-20}}
                      dropdownTextHighlightStyle={{color:"blue",fontFamily: 'RubikRegular'}}
                    />



                      <ModalDropdown
                        ref={ (ref) => this.lookaheadFilter = ref}
                        defaultValue={this.state.SelectedCity}
                        onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.City[rowData].city_id);this.setState({cityname:index}); this.lookaheadFilter2.select(-1); }}
                        options={CityName}
                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                        style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', padding:5, alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}
                        textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',margin: 5}}
                        dropdownStyle={{width: '60%',fontFamily: 'RubikRegular', marginTop:-20}}
                      />





                  </View>
                  <View style={{justifyContent: 'space-around',alignItems: 'center',flexDirection: 'row',padding:  5}}>

                    <ModalDropdown
                      ref={ (ref) => this.lookaheadFilter2 = ref}
                      defaultValue={"Select Area"}
                      onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.Area[rowData].city_id);this.setState({pincode:this.state.Area[rowData].postal_code,area_name:this.state.Area[rowData].area_name});   }}
                      options={Area}
                      dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                      style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', padding:5, alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}
                      textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',margin:5}}
                      dropdownStyle={{width: '60%',fontFamily: 'RubikRegular', marignTop:-20}}
                    />




                                        <TextInput placeholder={"Pincode"} value={this.state.pincode}
                                          maxLength={6}
                                          onChangeText={(value) =>{
                                                 let num = value.replace(".", '');
                                                   if(isNaN(num)){
                                                       // Its not a number
                                                   }else{
                                                      this.setState({pincode:num})}}
                                                   }
                                                   placeholderTextColor={"#707171"}
                                          keyboardType={'numeric'}
                                        editable style={{width: '48%', flexDirection: 'row', borderRadius:wp('6.66%'), 
                                        backgroundColor: 'white',paddingLeft:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,fontSize: wp("4%"),height: wp('10.33%'),
                                        fontFamily: 'RubikRegular',alignSelf: 'center',color: "gray", }}/>





                  </View>
                  <View style={{width: '95%', flexDirection: 'row', height: wp('10.33%'),margin: 7, borderRadius:wp('6.66%'), backgroundColor: 'white',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}>


                    <TextInput
                      style={[styles.txtFld,{width: '95%'}]}
                      placeholder="Landmark"
                      placeholderTextColor= "#707171"
                      maxLength = {10}
                      onChangeText={(phone) => this.setState({landmark:phone})}
                      value={this.state.landmark}
                    />

                  </View>
                  <FlatList
                    data = {this.state.addressType}
                    extraData={this.state}
                    horizontal
                    renderItem = {({item}) =>
                    <TouchableOpacity  onPress={() => { this.onCheck(item.firstName) }} activeOpacity = {0.5} style={{margin: 7,flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',margin: 7}}>

                      <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}}>
                        <View style={{backgroundColor:this.state.SelectedType == item.firstName?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                      </View>
                      <Text style={{
                        color: 'gray',
                        fontSize: wp('4%'),
                        marginLeft: wp('2%'),
                        fontFamily: "RubikBold"
                      }}>{item.firstName}</Text>
                    </TouchableOpacity>
                  }
                />
                <View style={{width: '95%', margin: 7, borderRadius:wp('2.66%'), backgroundColor: 'white',  shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}>


                  <TextInput
                    placeholder="Enter Address"
                    onChangeText={password => this.setState({address : password})}
                    numberOfLines = {3}
                    //editable={this.state.onSubmit}
                    textAlignVertical={'top'}
                    value={this.state.address}
                    underlineColorAndroid='transparent'
                    style={{fontFamily: 'RubikRegular',
                    color:'#707171',
                    fontSize: wp("4%"),
                    height: hp("10%"),
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    width:'95%', paddingLeft:10}}
                    multiline

                  />

                </View>
              </View>

}





                </View>
              </View>
              <View style={{position: 'absolute',bottom: 5, width: wp("100%"),height: wp('13.33%'),flexDirection: 'row',alignItems:  'center',justifyContent: 'center',width: wp("100%")}}>
                <TouchableOpacity onPress={() =>this.check()} style={{
                    width: '45%',
                    borderColor: '#3b75c9',
                    borderWidth: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    margin: 5,
                    height: wp('13.33%'),
                    backgroundColor: 'white',
                    borderRadius: 10,alignSelf: 'center'
                  }}>
                  <Text style={{color: 'black',fontSize: wp('4%'),fontFamily: "RubikBold"}}> {this.state.oldAddress?"+ Add new address":"Exisiting address"} </Text>


                </TouchableOpacity>

                <TouchableOpacity onPress={()=>{this.AddAdress()}  } style={{width: '45%',alignSelf: 'center', justifyContent: 'center', alignItems: 'center',  overflow: 'hidden', height: wp('13.33%'),alignSelf: 'center',margin: 5
                  , backgroundColor: 'transparent', borderRadius: 10}}>
                  <Image resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}} source={require('../../../../assets/button.png')}/>

                  <Text style={{color: '#ffffff',fontSize: wp('4%'),fontFamily: "RubikBold"}}> CONTINUE </Text>

                </TouchableOpacity>
              </View>











            </SafeAreaView>
            {this.state.ActivityIndicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
              <ActivityIndicator color={"#6464e7"} size={"large"}/>
            </View>:null}


          </View>

