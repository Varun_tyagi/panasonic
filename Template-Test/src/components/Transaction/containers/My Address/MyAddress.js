import React from 'react';
import { StyleSheet, Text,View,Picker, TouchableOpacity,Dimensions, Image,TextInput,ScrollView,StatusBar,Platform, SafeAreaView,ActivityIndicator, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import RazorpayCheckout from 'react-native-razorpay';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import ModalDropdown from 'react-native-modal-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view'
import * as constants from '../../../../helpers/constants';
type Props = {
  navigate: PropTypes.func.isRequired
};




export default class MyAddress extends React.Component {
  constructor() {
    super()
    this.state = {
      token:'',
      Myaddresses:[],ActivityIndicator:false,highlights: [],itemCount:0,
      promocode: '',myCartData:[],refreshing: false,switch1Value: false,
      selectedUserType: '',user:"",dropdown_4_options: null,items:undefined,coupon:[],shipping:[],totalPrice:"",
      dropdown_4_defaultValue: 'loading...',SelectedType:"HOME",cityname:"",stateName:"",pincode:"",landmark:"",address:"",area_name:"",
      dropdown_6_icon_heart: true,addressType:[
        {firstName:'HOME',},
        {firstName:'OFFICE',},
        {firstName:'OTHER',}
      ],totalPrice:"",Indicator:false,
      StateName:[],City:[],Area:[],SelectedState:"Select State",SelectedCity:"Select City",oldAddress:true,SelectedAddress:[],SelectedAdressID:"",
    }
  }
  check(){
    if(this.state.oldAddress){
      this.setState({oldAddress:false})
    }else{
      this.setState({oldAddress:true})
    }
    this.onAfterSubmit();
  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }

async  componentDidMount()
  {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    this.setState({ActivityIndicator:true})
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    if(this.props.navigation.state.params != undefined){
      this.setState({myCartData:this.props.navigation.state.params.ProductInfo})
    }
    this.getDataofAddress(accessToken,customer_id)
    this.getData("state")

  }




  getDataofAddress(accessToken,customer_id) {
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerAddresses?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
      this.setState({Myaddresses:response.data.data,ActivityIndicator:false, oldAddress: response.data.data.length?true:false})
    }).catch((error) => {
      this.setState({ActivityIndicator: false});
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){
        alert('Please check your internet connection.');
      }else{
        alert(error);
      }
    })
    });
  }




    openDrawerClick() {
      this.props.navigation.dispatch(DrawerActions.openDrawer());
    }
    goBack(){
      this.props.navigation.pop();
    }

    onSubtract(index, item){
      if(parseInt(item.qty) > 1){
        --item.qty;
        this.setState({
          refresh: !this.state.refresh
        });
      }

    };
    onCheck(value){
      this.setState({SelectedType:value})
    }
    onAdd(index, item){
      ++item.qty;
      this.setState({
        refresh: !this.state.refresh
      });
    };
    toggleSwitch1 = (value) => {
      this.setState({switch1Value: value})
      console.warn('Switch 1 is: ' + value)
    }


    showAlert(message, duration) {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        alert(message);
      }, duration);
    }

    updateUser = (user) => {
      this.setState({ user: user })
    }


    async getData(MethodName,ID, selectId=null, data=[]) {
      //  alert(MethodName);
      var that = this;
      await this.setToken();
      //alert(' accesssss token token:-----'+ token);
      var  APINAME = "";
      var  TYPE = ""
      if(MethodName == "state"){
        APINAME =   "getState";
      }
      if(MethodName == "CITY"){
          APINAME = "getCity";
          TYPE = "state_id";
      }
      if(MethodName == "PINCODE"){
          APINAME = "getArea";
          TYPE = "city_id"
      }
  
      // Make a request for a user with a given ID
      axios.get('https://www.ecarewiz.com/ewarrantyapi/'+APINAME+"/?access_token="+this.state.token+"&"+TYPE+"="+ID).then((response)=>{
        if(MethodName == "state"){
           that.setState({StateName: response.data.data});
           if(selectId!=null){
             let id = '';
             this.state.StateName.forEach((item, index)=>{if(item.state_id==selectId)id= index;});
            // this.setState({stateName:selectId-1},()=>{
              this.stateSelector.select(id);
            // })
           }
        }
        if(MethodName == "CITY"){
           that.setState({City: response.data.data});
           if(selectId!=null){
            let id='';
            this.state.City.forEach((item, index)=>{if(item.city_id==selectId)id= index;});
            // this.setState({cityname:id},()=>{
              this.lookaheadFilter.select(id);
            // });
          }
        }
        if(MethodName == "PINCODE"){
           that.setState({Area:data.length?data: response.data.data});
           if(selectId!=null){
            let id ='';
            this.state.Area.forEach((item, index)=>{if(item.area_id==selectId)id= index;});
            // this.setState({area_name:id},()=>{
              // this.lookaheadFilter2.select(id);
            // });
          }
        }
  
      }).catch(function(error) {
        that.setState({gettingData: false});
        console.warn(error.code)
  
      });
  
    }
  
  async  AddAdress(){
      if(this.state.oldAddress){
        if(this.state.SelectedAddress.length > 0){
// console.warn(this.props.navigation.state.params.discount_mode);
          this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,
            discount_mode: this.props.navigation.state.params.discount_mode,
            DiscountedPrice:this.props.navigation.state.params.DiscountedPrice,
            couponCode:this.props.navigation.state.params.couponCode,
            couponDiscount:this.props.navigation.state.params.couponDiscount,
            SelectedAdressID:this.state.SelectedAdressID, 
            SelectedAddress:this.state.SelectedAddress,
            employeeId: this.props.navigation.state.params.employeeId,
            gst:this.props.navigation.state.params.gst})
            this.onAfterSubmit()
        }
        else{
          alert("Please select any address.")
        }
        return;
      }
      if(this.state.stateName == ""){
        alert("Please choose state name.");
        return;
      }
      if(this.state.cityname == ""){
        alert("Please choose city name.");
        return;
      }
      if(this.state.area_name == ""){
        alert("Please choose area name.");
        return;
      }
      if(this.state.pincode == ""){
        alert("Please choose pincode.");
        return;
      }
      if(this.state.address == ""){
        alert("Please enter address.");
        return;
      }
      



      this.setState({ActivityIndicator:true})
      var bodyFormData = new FormData();
      var token = await authFun.checkAuth();
      //alert(' accesssss token token:-----'+ token);
      setConfiguration('accessToken', token);
      var user = await getUser('state');
      setConfiguration('customer_id', user.customer_id); 
      const accessToken = getConfiguration('accessToken');
      const customer_id = getConfiguration('customer_id');
      bodyFormData.append('customer_id', customer_id);
      bodyFormData.append('address_type', this.state.SelectedType);
      bodyFormData.append('country_name', "INDIA");
      bodyFormData.append('state_name', this.state.stateName);
      bodyFormData.append('city_name', this.state.cityname);
      bodyFormData.append('area_name', this.state.area_name);
      bodyFormData.append('pincode', this.state.pincode);

      bodyFormData.append('address', this.state.address);
      bodyFormData.append('near_by', this.state.landmark);




      axios.post("https://www.ecarewiz.com/ewarrantyapi/addCustomerAddress", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
    ).then((response)=>{
      this.setState({Indicator:false})

      var address = {
            "customer_address_id": "response.data.data.customer_address_id",
            "address_type": this.state.SelectedType,
            "country_name": "INDIA",
            "state_name": this.state.stateName,
            "city_name":this.state.cityname,
            "area_name": this.state.area_name,
            "address":  this.state.address,
            "pincode": this.state.pincode,
            "near_by": this.state.landmark
        }
        this.setState({ActivityIndicator:false})
          this.onAfterSubmit()
      this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,
        totalPriceNew:this.props.navigation.state.params.totalPriceNew,
        DiscountedPrice:this.props.navigation.state.params.DiscountedPrice,couponCode:this.props.navigation.state.params.couponCode,couponDiscount:this.props.navigation.state.params.couponDiscount,
        SelectedAdressID:response.data.data.customer_address_id,SelectedAddress:[address],gst:this.props.navigation.state.params.gst})
      // this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,orderDetail:response.data.data,totalPriceNew:this.props.navigation.state.params.totalPriceNew,DiscountedPrice:this.props.navigation.state.params.DiscountedPrice})
    }).catch((response)=>{
      this.setState({Indicator:false, ActivityIndicator:false})
      alert(response.message)
      console.log(JSON.stringify(response));
      console.warn(JSON.stringify(response));
    })




    }

    onAfterSubmit(){

      this.setState({SelectedType:"HOME",stateName:"",area_name:"",cityname:"",pincode:"",address:"",landmark:"",SelectedAdressID:"",SelectedAddress:[]})

    }


  async  SendtoServerwithAddress(){
      //this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo})
      this.setState({Indicator:true})
      let totalQuantity = 0;
      let totalPrice = 0;
      let gst = 0;
      if (this.state.myCartData) {
        this.state.myCartData.forEach(item => {

          totalQuantity += item.price;

          totalPrice += item.qty * Math.trunc(item.price);

        });
      }

      var coupon = {"coupon_code":"","coupon_discount":""};
      let shipping = {
        "address_type":this.state.SelectedType,
        "country":"INDIA",
        "state":  this.state.stateName ,
        "city":this.state.cityname,
        "area":this.state.area_name,
        "pincode":this.state.pincode,
        "landmark":this.state.landmark,
        "address":this.state.address
      }



      var bodyFormData = new FormData();
      var token = await authFun.checkAuth();
      //alert(' accesssss token token:-----'+ token);
      setConfiguration('accessToken', token);
      var user = await getUser('state');
      setConfiguration('customer_id', user.customer_id); 
      const accessToken = getConfiguration('accessToken');
      const customer_id = getConfiguration('customer_id');
      bodyFormData.append('customer_id', customer_id);
      bodyFormData.append('order_type', "accessories");
      bodyFormData.append('currency', "INR");

      bodyFormData.append('subtotal',this.props.navigation.state.params.totalPriceNew);
      bodyFormData.append('grand_total',this.props.navigation.state.params.totalPriceNew);
      bodyFormData.append('tax', "0.0");
      bodyFormData.append('discount', this.props.navigation.state.params.DiscountedPrice);

      bodyFormData.append('shipping_charge', "0.0");
      bodyFormData.append( "coupon",JSON.stringify({"coupon_code":"","coupon_discount":""}));
      bodyFormData.append("shipping", JSON.stringify(this.state.shipping));
      bodyFormData.append('customer_address_id', "1");

      this.send(bodyFormData,accessToken,customer_id);
    }
    async  send(bodyFormData,accessToken,customer_id){
      await  axios.post("https://www.ecarewiz.com/ewarrantyapi/save_order", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
    ).then((response)=>{
      this.setState({Indicator:false, ActivityIndicator:false})
      // console.warn(response.data.data);
      this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,orderDetail:response.data.data,totalPriceNew:this.props.navigation.state.params.totalPriceNew,DiscountedPrice:this.props.navigation.state.params.DiscountedPrice,couponCode:this.props.navigation.state.params.couponCode,couponDiscount:this.props.navigation.state.params.couponDiscount,gst:this.props.navigation.state.params.gst})
    }).catch((response)=>{
      this.setState({Indicator:false, ActivityIndicator:false})
      alert(response.message)
    })
  }

  cleanAddress(){
    this.setState({ area_name:'', stateName:'', cityname:''});
    this.stateSelector.select(-1);
    this.lookaheadFilter.select(-1);
    this.lookaheadFilter2.select(-1);
  }

  
  
  pincodeChanged =async ()=>{
    let pincode = this.state.pincode;
    await this.setToken();
    this.setState({loading:true});
    if(pincode!='' && pincode.length==6){
      let url = constants.base_url + 'pincodeBasedData';
      url = url + "?access_token=" + this.state.token + '&pincode=' + pincode;
      fetch(url,
        { headers: { 'Content-Type': 'application/json' } })
        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.status) {
            // this.getData('area', responseJson.data.city_id, responseJson.data.area_id, responseJson.data.area);
            this.getData('state',null, parseInt(responseJson.data.state_id));
            this.getData('CITY',responseJson.data.state_id,parseInt(responseJson.data.city_id));
            this.getData('PINCODE', responseJson.data.city_id, parseInt(responseJson.data.area_id), responseJson.data.area);
            this.setState({loading:false, stateName: responseJson.data.state_name, cityname: responseJson.data.city_name,  countryName: responseJson.data.country_name});
            // console.warn(responseJson.data)
          } else {
            alert('Could not locate '+this.state.pincode);
            this.setState({ loading: false });
            this.cleanAddress();
          }
  
        })
        .catch(error => { alert('Could not locate '+this.state.pincode); this.setState({ loading: false }) })
    }else{
      alert('Enter a valid postal code.');
      this.setState({loading:false})
    }
  }

  genFrame(style, length) {
    style.height = length > 7 ? 200 : - 1;
    style.left = Dimensions.get("screen").width*0.10;
    style.width = Dimensions.get('screen').width*0.80;
    // style.left = Dimensions.get("screen")*0.10;
    return style;
    }

render() {

  let totalQuantity = 0;
  let totalPrice = 0;
  let gst = 0;
  const {params} = this.props.navigation.state;
  if (this.state.myCartData) {
    this.state.myCartData.forEach(item => {

      totalQuantity += item.price;

      totalPrice += item.qty * Math.trunc(item.price);

    });
    if(totalPrice != 0){gst =  0
      totalPrice = totalPrice+gst;}
    }
    var StateName = [];
      if(this.state.StateName.length >0){
      this.state.StateName.forEach((item,index)=>{

        StateName.push(item.state_name)
      })
    }else{
      StateName = [];
    }

    var CityName = [];
    if(this.state.City.length >0){
    this.state.City.forEach((item,index)=>{

      CityName.push(item.city_name)

    })
  }else{
    CityName = [];
  }
            var Area = [];
            if(this.state.Area.length > 0){
            this.state.Area.forEach((item,index)=>{
              Area.push(item.area_name)
            })
          }else{
            Area = [];
          }



    return (

      <View style={styles.container}>

    <ScrollView>
      <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>
        {/* header */}
      <View style={{flex:1, height:140,}}>
              <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: -660,
                  height: 800,
                  width: 800,
                  borderRadius: 400,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              {/* title and back button */}
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                <TouchableOpacity onPress={()=>this.goBack()} style={{zIndex:1, height: 20, width: 20,}}>
                  <Image source={require('../../../../assets/backwhite.png')} style={{ height: '100%', width: '100%', }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: '#e9eaff', textAlign:'center',flex:1,alignSelf:'stretch',marginLeft:-20, fontFamily: 'RubikMedium' }}>{this.state.oldAddress?"MY ADDRESSES":"ADDRESS"}</Text>
              </View>
              {/* icon and progress bar */}
              <Image resizeMode="contain" style = {{width: 35, height:40,tintColor:"white", alignSelf:'center', marginTop:10}} source = {require('../../../../assets/locationicon.png')}/>

              <Image resizeMode="contain" style = {{width: 80,  height:40, alignSelf:'center'}} source = {require('../../../../assets/Addressbar.png')}/>
          </View>
          {/* main content */}
          <View style={{marginTop:10, flex:1, alignSelf:'stretch',  padding:10, marginBottom:60}}>
            {
              this.state.oldAddress?<FlatList
                    data = {this.state.Myaddresses}
                    extraData={this.state}
                    keyExtractor={(index,item)=>index.toString()}
                    renderItem = {({item,index}) =>{
                      return(
                        <TouchableOpacity activeOpacity={0.7} onPress={()=>this.setState({SelectedAdressID:item.customer_address_id,SelectedAddress:[item]})} style={{borderRadius:5, padding:15, flexDirection:'row',
                        justifyContent:'flex-start',alignItems:'center',margin:5,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:2,shadowOffset: {
                    width: 1,
                    height: 1,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 2.0,backgroundColor:'white' }}>
                          <View style={{flex:1,  marginRight:15}}>
                            <Text style={{fontFamily:'RubikMedium', fontSize:15,color:'black'}}>{item.address_type?item.address_type.toUpperCase():''}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.state_name}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.city_name}, {item.pincode}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.address}, {item.area_name} {item.near_by != ""? ", "+item.near_by:null}</Text>
                          </View>
                          <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center',
                  shadowOffset: {
                    width: 1,
                    height: 1,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0, }}>
                            <View style={{backgroundColor:this.state.SelectedAdressID == item.customer_address_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                          </View>
                        </TouchableOpacity>
                        
                       )}
                  }
                />:
                <View style={{margin:5, flex:1, alignItems:'stretch', alignSelf:'stretch'}}>
                  {/* Detect Location */}
                  {/* <View style={{padding:10, backgroundColor:'#d9dbdc', borderRadius:26, flexDirection:'row', borderWidth:1.5, borderColor:'white'}}>
                    <Image resizeMode="contain" style = {{width: 25, height:25,tintColor:"gray",alignSelf: 'center'}} source = {require('../../../../assets/locationicon.png')}/>
                    <Text style={{flex:1, fontFamily:'RubikRegular', fontSize:12,marginLeft:15}}>Detect My Location{'\n'}<Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#5e6060'}}>{this.state.switch1Value == true?"ON":"OFF"}</Text></Text>
                    <Switch
                      thumbColor={this.state.switch1Value == false?"white": "#ff8ba3"}
                      trackColor={"white"}
                      trackColor={{ true: 'white', false:'#ffffff'  }}
                      onValueChange = {this.toggleSwitch1}
                      value = {this.state.switch1Value}/>
                  </View> */}

                  {/* Address Main */}
                  {/* State dropdown textinput */}
                  <View style={{flexDirection:'row', flex:1, marginTop:10}}>
                  <View style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, }}>
                        <TextInput placeholder={"Pincode"} value={this.state.pincode}
                          maxLength={6}
                          onChangeText={(value) =>{
                                let num = value.replace(".", '');
                                  if(isNaN(num)){
                                      // Its not a number
                                  }else{
                                      this.setState({pincode:num})}}
                                  }
                                  placeholderTextColor={"#707171"}
                          keyboardType={'numeric'}
                        editable 
                        onEndEditing={()=>this.pincodeChanged()}
                        style={{flex:1, padding:0,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white' }}/>
                      </View>

                    <TouchableOpacity activeOpacity={1} onPress={()=>this.stateSelector.show()} style={{ borderRadius:25, backgroundColor: 'white', marginLeft:5,alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginRight:5, justifyContent:'center'}}>
                      <ModalDropdown
                          ref={(stateSelector)=>this.stateSelector = stateSelector}
                          defaultValue={this.state.SelectedState}
                          defaultIndex ={-1}
                          options={StateName}
                          onSelect={(rowData,index) =>{ this.getData("CITY", this.state.StateName[rowData].state_id);this.setState({stateName:index,SelectedCity:"Select City", cityname:'', area_name:'',pincode:'' }); this.lookaheadFilter.select(-1);this.lookaheadFilter2.select(-1) }}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          animated={ true}
                          adjustFrame={style => this.genFrame(style, StateName.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20, }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                        />
                        <Image source={require('../../../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                      </TouchableOpacity>
                   
                  </View>
                  {/* pincode and area selector row */}
                  <View style={{flexDirection:'row', flex:1, marginTop:15}}>
                    
                  <TouchableOpacity activeOpacity={1} onPress={()=>this.lookaheadFilter.show()} style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1,  justifyContent:'center'}}>
                      <ModalDropdown
                        ref={ (ref) => this.lookaheadFilter = ref}
                        defaultValue={this.state.cityname?this.state.cityname:'Select City'}
                        onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.City[rowData].city_id);this.setState({cityname:index, area_name:'', pincode:''}); this.lookaheadFilter2.select(-1); }}
                        options={CityName}
                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}
                        adjustFrame={style => this.genFrame(style, CityName.length)}
                        textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                        dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20, }}
                        dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                        />
                        <Image source={require('../../../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                      </TouchableOpacity>
                 
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.lookaheadFilter2.show()} style={{ borderRadius:25, backgroundColor: 'white',justifyContent:'center', alignItems:'stretch', marginLeft:5 ,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginRight:5,}}>
                      <ModalDropdown
                          ref={ (ref) => this.lookaheadFilter2 = ref}
                          defaultValue={this.state.area_name?this.state.area_name:"Select Area"}
                          onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.Area[rowData].city_id);this.setState({pincode:this.state.Area[rowData].postal_code?this.state.Area[rowData].postal_code:this.state.pincode,area_name:this.state.Area[rowData].area_name});   }}
                          options={Area}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch', flex:1, }}
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          adjustFrame={style => this.genFrame(style, Area.length)}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20, }}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                        />
                        <Image source={require('../../../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                    </TouchableOpacity>
                   
                      
                       
                  </View>
                  
                  {/* landmark */}
                  <View style={{ borderRadius:25, alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginTop:15}}>
                    <TextInput
                        placeholder="Landmark"
                        placeholderTextColor= "#707171"
                        maxLength = {200}
                        onChangeText={(phone) => this.setState({landmark:phone})}
                        value={this.state.landmark}
                        style={{flex:1, paddingVertical:5,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white' }}/>
                  </View>

                  {/* Address Type */}
                  <FlatList
                    data = {this.state.addressType}
                    extraData={this.state}
                    horizontal
                    renderItem = {({item}, index) =>
                    <TouchableOpacity key={index}  onPress={() => { this.onCheck(item.firstName) }} activeOpacity = {0.5} 
                    style={{marginRight: 10,marginBottom:5, flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',marginTop:15}}>
                      <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}}>
                        <View style={{backgroundColor:this.state.SelectedType == item.firstName?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                      </View>
                      <Text style={{
                        color: 'gray',
                        fontSize: 15,
                        marginLeft: 10,
                        fontFamily: "RubikBold"
                      }}>{item.firstName}</Text>
                    </TouchableOpacity>
                  }
                />
                {/* Main address */}
                <View style={{ borderRadius:5, alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginTop:10, backgroundColor:'white'}}>

                  <TextInput
                      placeholder="Enter Address"
                      onChangeText={password => this.setState({address : password})}
                      numberOfLines = {3}
                      //editable={this.state.onSubmit}
                      textAlignVertical={'top'}
                      value={this.state.address}
                      underlineColorAndroid='transparent'
                      style={{fontFamily: 'RubikRegular',
                      color:'#707171',
                      fontSize: 15,
                      height:110,
                      alignItems: 'flex-start',
                      justifyContent: 'flex-start',
                      width:'95%', paddingLeft:10}}
                      multiline
                    />
                  </View>
                </View>
                }
            </View>
      
        </SafeAreaView>
      </ScrollView> 
                
      <View style={{alignSelf:'stretch', flexDirection: 'row', alignItems:'center',   position: 'absolute',bottom: 0,padding: 5,paddingTop:0,}}>
            
          <TouchableOpacity onPress={() =>this.check()}  style={{flex:1, borderRadius:5, padding:15, borderWidth:0.5, borderColor:'#c0c0c2', marginRight:7.5, backgroundColor:'white'}}>
              <Text  style={{color: 'black',fontSize: 15,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                {this.state.oldAddress?"+ Add new address":"Existing Address"}
              </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>{this.AddAdress()}  }  style={{ flex:1,borderRadius:5,marginLeft:7.5}}>
            <LinearGradient style={{  borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center' , padding:15}}
                        colors={["#ff619e", "#ff9fa6"]}>
                      <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                        Continue
                      </Text>
              </LinearGradient>
          </TouchableOpacity>
        </View> 
     
            {this.state.ActivityIndicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
              <ActivityIndicator color={"#6464e7"} size={"large"}/>
            </View>:null}


          </View>




        );
      }
    }



    const styles = StyleSheet.create({
      container: {
        flex:1,
        backgroundColor: '#edf4f6'
      },
      header: {
        width: '100%',
        height: wp('40%'),
        marginTop: -wp('13.33%'),
        backgroundColor: 'transparent'

      },
      innerView: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        position :'relative',
        alignItems: 'center'
      },



      backIcon: {

        width:wp('5.86%'),
        height: wp('5.86%'),

        backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
        width:wp('10.33%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
        fontSize: wp('4.58%'),
        fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
      listTile:{width: '100%',  shadowColor: '#000000',
      shadowOffset: {
        width: 0,
        height: 0
      },
      shadowRadius: 1,
      shadowOpacity: 0.8, borderRadius:10,  marginTop: wp('2.6%'), backgroundColor:'white'},
      txtFld:{
        // marginLeft: wp('1%'),
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular",
        color: 'gray', paddingLeft:10
      }
    })
