import React from 'react';
import { StyleSheet, TextInput,Text,ScrollView,View,Animated, ActivityIndicator, TouchableOpacity,ImageBackground, Dimensions,Image, SafeAreaView, FlatList, StatusBar, Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import {widthPercentageToDP as wp,heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import { NavigationEvents } from 'react-navigation';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import Swiper from 'react-native-swiper';

const SCREEN_WIDTH = Dimensions.get('window').width

type Props = {
  navigate: PropTypes.func.isRequired
};
class Home extends React.Component {

  state = {
    dataSource: [],
    dataSource2: [],
    SliderArray: [],
    activeSlide: 0,
    selectedCategory: false,
    responseData: '',
    gettingData: false,
    searchArray: [],
    filteredData: [],
    scrolled:false,
    CartItemCount: "",ShowAlert: false
  };

  componentDidMount() {
    
    this.callProductTypeAPI();
    this.callDataAPI();
    this.setState({ShowAlert: true});
    
    
    
    
  }

  SearchFilterFunction(text) {

    const newData = this.state.dataSource2.filter(function(item) {
      const itemData = item.accessories_name.toUpperCase()
      const textData = text.toUpperCase()
      return itemData.indexOf(textData) > -1
    });
    
    this.setState({searchArray: newData});
    // this.setState({filteredData: newData})
  }

  callProductTypeAPI() {
    this.props.getProductType().then(() => this.afterProductTypeAPI()).catch(e => this.showAlert(e.message, 300));
  }

  afterProductTypeAPI() {
    console.log("isBusy value --- ", this.props.isBusyProductType);
    console.log("response value --- ", this.props.responseProductType);

    var categories = this.props.responseProductType.data;

    this.setState({dataSource: categories});
    global.categoriesName = categories;
  }

  callDataAPI() {

    this.props.getAccessriesData('All').then(() => this.afterGetDataAPI()).catch(e => this.showAlert(e.message, 300));

  }

  afterGetDataAPI() {
    let product_group_id = this.props.navigation.state.params?this.props.navigation.state.params.product_group_id:null;
    this.setState({ShowAlert: false})
    
    console.log("isBusy value --- ", this.props.isBusy);
    console.log("response value --- ", this.props.response);
// alert(JSON.stringify(this.props.response.response.banners));

    var data = this.props.response.response.data;
    this.setState({dataSource2: data, searchArray: data, SliderArray:this.props.response.response.banners},()=>{
      if(product_group_id && !this.state.scrolled && this.state.dataSource.length && this.state.dataSource2.length){
        this.relatedAccessories(product_group_id);
      }
    });

  }

  parseData() {
    if (this.state.responseData != '') {
      var accessories = this.state.responseData.data.accessories;
      console.log('categories----', accessories);
      this.setState({dataSource: accessories});

      this.seleectCategory(accessories[0]);

    }

  }

  onFocusChange(catgory) {
    this.myRef.getNode().scrollTo({y: 545, animated: true});
  }
  seleectCategory(category) {
    this.onFocusChange(category);
    this.props.getAccessriesData(category).then(() => this.afterGetDataAPI()).catch(e => this.showAlert(e.message, 300));
  }

  openDrawerClick() {
    drawer.current.open();
  }
  goBack() {
    this.props.navigation.goBack();
  }

  openDetail(item) {
    this.props.navigation.navigate('Detail', {selectedItem: item});
  }
  NavigateToCart() {
    this.props.navigation.navigate('MyCart');
  }
  callAddToCartAPI(index, item,stringItem) {
    this.setState({ShowAlert: true});
    this.props.addCartApi(item).then(() => this.afterAddCartAPI(stringItem)).catch(e => {this.showAlert(e.message, 300);this.setState({ShowAlert:false})});

  }

  afterAddCartAPI(stringItem) {
    console.log("isBusy value --- ", this.props.isBusy);
    console.log("response value --- ", this.props.response);
    this.callGetCartAPI(stringItem);
  }

  callGetCartAPI(stringItem) {
    this.props.getCartApi().then(() => this.afterGetCartAPI(stringItem)).catch(e => this.showAlert(e.message, 300));
    this.getCartItemCount()
  }

  afterGetCartAPI(stringItem) {
    this.setState({ShowAlert: false})
    console.log("isBusy value --- ", this.props.isBusyGetCart);
    console.log("response value --- ", this.props.responseGetCart);

      if(stringItem == "buynow"){
        this.props.navigation.navigate('MyCart');
      }else{
        alert("Added to cart successfully.")
      }
    //

  }
  goToCart(index, item,stringItem) {

    this.callAddToCartAPI(index, item,stringItem);
    //  this.props.navigation.navigate('MyCart');
  }
  buyNow(index, item,stringItem) {

    let x = item;
    Object.assign(x, {qty: "1"})

    this.props.navigation.navigate('ReviewOrder', {ProductInfo: [x]});
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){
        alert('Please check your internet connection.');
      }else{
        alert(message);
      }
    })
      this.setState({gettingData:false, ShowAlert:false})
    }, duration);
  }
  get pagination() {
    const {SliderArray, activeSlide} = this.state;
    return (<Pagination dotsLength={SliderArray.length} activeDotIndex={activeSlide} 
      dotStyle={{
        width: 10,
        height: 10,
        borderRadius: 5,
        borderWidth:1,
        borderColor:'white',
        backgroundColor: 'white'
      }} inactiveDotStyle={{
        width: 10,
        height: 10,
        borderWidth: 1,
        backgroundColor: "transparent",
        borderColor: 'white',}}
        dotContainerStyle={{marginRight:0}} inactiveDotOpacity={1} inactiveDotScale={1} />);
  }
  async getCartItemCount() {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCartCount?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
      this.setState({CartItemCount: response.data.data});
      global.cartItemsCount = response.data.data;
    }).catch((error) => {
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){ 
        alert('Please check your internet connection.');
      }else{
        alert(error);
      }
    })
      this.setState({gettingData:false, ShowAlert:false})
      //this.setState({gettingData: false});
    });
  }
  relatedAccessories(product_group_id){ 
    
      this.setState({scrolled:true});
      let selectedProducts = [];
      let rejectedProducts = [];
      this.state.dataSource2.forEach((element)=>{ 
        if(element.product_group_id == product_group_id){
          selectedProducts.push(element);
        }else{
          rejectedProducts.push(element);
        }
      });
      this.setState({dataSource2:selectedProducts.concat(rejectedProducts)},()=>{
        setTimeout(()=>this.onFocusChange(),200);
      });
    
  }

  render() {
    // console.warn(this.state.dataSource2);
    
    

    const {selectedCategory} = this.state;
    _scrollView = React.createRef();
    scrollView = React.createRef();
    // alert(JSON.stringify(this.state.dataSource2))
    // let dotPlace = (parseInt((wp('100%')-20)/2));
    // alert(typeof(this.state.SliderArray[0].image))
    return (<View style={styles.container}>
      <StatusBar translucent={true} barStyle="light-content"/>
      <NavigationEvents onWillFocus={payload => {
          this.getCartItemCount()
        }}/>

    <Animated.ScrollView ref={c => (this.myRef = c)}>
      <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>
      {/* header */}
      <View style={{height:220,}}>
              <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: -630,
                  height: 800,
                  width: 800,
                  borderRadius: 400,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', 
              alignItems: 'center', padding: 10 }}>
                <TouchableOpacity onPress={() => drawer.current.open()}>
                  <Image source={require('../../../../assets/ic_home/align-left.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: '#e9eaff',  fontFamily: 'RubikMedium' }}>PRODUCT ACCESSORIES</Text>
                <TouchableOpacity>
                  <Image source={require('../../../../assets/ic_home/bell.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
              </View>

              {

              this.state.SliderArray.length
                ?  
                <View style={{ height: 160 , borderRadius:10,marginTop:10 }}>
                   <Swiper 
                    onIndexChanged={(index)=>this.setState({activeSlide:index})}
                    showsButtons={false} 
                    showsPagination={false}
                    loop={true} 
                    horizontal={true}
                    autoplayTimeout={2} 
                    autoplay={true}
                    >
                  {
                    this.state.SliderArray.map((item, i) => (
                      <View style={{height:'100%', flex:1, alignSelf:'stretch', margin:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:10,borderRadius:10}}>
                          <Image key={i} source={{uri:item.image}} style={{ resizeMode: 'stretch',width:'100%', height: '100%',borderRadius:10 }}/>
                      </View>
                    
                  ))
                  }
                </Swiper>
                <View style={{
                    position: 'absolute',
                    bottom: -10
                  }}>{this.pagination}</View>

              </View>
            // </View>
                : null
             }
          </View>
       
             {/* Filter options */}
             {(this.state.dataSource.length && this.state.dataSource2.length)?
              <View style={{flex:1,alignItems: 'center',justifyContent: 'center',marginBottom:10, }}>
            <FlatList data={this.state.dataSource} 
             extraData={this.state.selectedCategory}
             numColumns={3}
             columnWrapperStyle={{alignSelf:'center'}}
             renderItem={({item, index}) => <TouchableOpacity
             key={index+1} 
             style={{
                  backgroundColor: "white",
                  width:'30%',
                  borderColor: selectedCategory == item.product_type_id
                    ? "#925ddc"
                    : "white",
                  borderRadius: 5,
                  alignSelf:'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  overflow: 'hidden',
                  height: 95,
                  margin: 5,
                  alignItems: 'center',
                  alignItems: 'center',
                  borderWidth: 1.3
                }} onPress={() => {
                  this.setState({selectedCategory: item.product_type_id}),
                  this.seleectCategory(item.product_type_id);
                }}>
                <Image style={{resizeMode:'stretch', height:160, width:160, position:'absolute', top:-30, left:-20}}
                  source={selectedCategory  == item.product_type_id?require('../../../../assets/selected.png'):
                          require('../../../../assets/unSelected.png')} />

               {
                 item.product_type_image? 
                <Image resizeMode="contain" style={{tintColor: selectedCategory == item.product_type_id?"white":'#6fb9cd',alignSelf:"center"
                  ,width: 40,height: 40, resizeMode:'contain', }} source={{uri: item.product_type_image}}/>:null
                  }

                <Text style={{
                    paddingHorizontal:10,
                    textAlign: 'center',
                    marginTop: 5,
                    alignSelf:'center',
                    fontFamily: "RubikMedium",
                    color: selectedCategory == item.product_type_id
                      ? "white"
                      : 'black',
                    fontSize:12,
                    flexWrap:'wrap'
                  }}>{item.product_type_name}</Text>
               
              </TouchableOpacity>}/>
          </View>

              :null}
          {/* Search Bar */}
         {(this.state.dataSource.length && this.state.dataSource2.length)? <View style={{alignSelf: 'center',flexDirection: 'row',borderRadius: 25,height: 40,alignItems: 'center',backgroundColor: "white",justifyContent: 'space-between',borderWidth: 1,borderColor: 'white',margin:15, marginTop:0,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>

            <TextInput numberOfLines={1} ellipsizeMode='head' style={{ flex:1, alignSelf:'stretch',paddingHorizontal:10 }}
             placeholderTextColor="#aec0d2" onChangeText={(text) => this.SearchFilterFunction(text)} value={this.state.text} onFocus={() => this.onFocusChange()}
              // value={this.state.search}
              ref='Input' placeholder='Search' underlineColorAndroid='transparent' onSubmitEditing={(event) => {}}/>
            <TouchableOpacity style={{height:15, width:15, marginHorizontal:10}}>
              <FastImage source={require('../../../../assets/search_white.png')}
               style={{height:'100%', width:'100%', resizeMode:'contain'}}/>
            </TouchableOpacity>
          </View>:null}
            
            {/* Items  */}
          {  (this.state.dataSource.length && this.state.searchArray.length)?
          <View style={{flex: 1,borderRadius:20,alignItems: 'center',justifyContent: 'center',paddingHorizontal:10,marginBottom:40,}}>
            
            <FlatList
              data={this.state.searchArray}
              numColumns={2} 
              contentContainerStyle={{flex:1, alignItems:'center', }}
              extraData={(index) => index.toString()} 
              renderItem={({item, index}) => {
                return (
                <View style={{margin:5,height:240, width:wp('45%'),}}>
                  <TouchableOpacity onPress={() => this.openDetail(item)} style={{
                      width:'100%', 
                      height: '100%',
                      alignItems: 'center',
                      backgroundColor: 'white',
                      padding:10,
                      alignSelf: 'flex-start',
                      borderRadius: 7,
                      shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:6,
                    }}>
                    
                      {/* Item Image */}
                    <View style={{backgroundColor:'rgba(255, 255, 255,1)',alignSelf:'stretch', 
                    height:115,marginBottom:5, justifyContent:'center', alignItems:'center',borderRadius:5, borderWidth:2, borderColor:'rgba(240,240,240,0.5)'}}>
                     
                    {
                      item.offer_text != ""
                        ? <View style={{
                              width:'80%',
                              marginBottom:5,
                              alignSelf:'flex-start',
                              backgroundColor: 'rgba(0,0,0,0.4)',
                              borderTopRightRadius: 10,
                              borderBottomRightRadius: 10
                            }}>

                            <Text numberOfLines={1} ellipsizeMode="middle" style={{
                                // margin: 5,
                                alignSelf: 'center',
                                width: 'auto',
                                fontFamily: "RubikRegular",
                                color: 'white',
                                fontSize: 10
                              }}>Combo Offer
                              {/* {item.offer_text.trim()} */}
                              </Text>
                          </View>
                        : null
                    }
                      {item.images[0]?
                      <Image  style={{
                          flex:1,
                          width: '100%',
                          height: '100%',
                          alignSelf: 'center',
                          resizeMode:'contain'
                        }} source={{
                          uri: item.images[0]
                        }}/>:null
                        }
                    </View>
                   
                    {/* Item Details */}
                    <View style={{alignSelf:'stretch',alignItems:'flex-start', height:60,}}>
                     {/* Item Name */}
                      <Text numberOfLines={2} 
                        style={{
                          maxHeight:35,
                          textAlign:'left',
                          fontFamily: "RubikRegular",
                          color: 'black',
                          fontSize:15,
                        }}>{item.accessories_name.trim()}</Text>
                     {/* Item Price */}
                      <View style={{flexDirection: 'row',alignItems: 'center',height:25,}}>
                        {
                          parseInt(item.special_price) > 0
                            ? <Text style={{
                                  textDecorationLine: parseInt(item.special_price) < 0
                                    ? null
                                    : 'line-through',
                                  fontFamily: "RubikRegular",
                                  color: 'gray',
                                  fontSize: 15,
                                  textAlign:'center',
                                  marginRight:10,
                                }}>₹ {
                                  parseInt(item.special_price) < 0
                                    ? Math.round(item.special_price)
                                    : Math.round(item.price)
                                }</Text>
                            : null
                        }
                        <Text style={{
                            fontFamily: "RubikMedium",
                            color: 'black',
                            fontSize: 18,
                            textAlign:'center',
                            textDecorationLine: parseInt(item.special_price) < 0
                              ? 'line-through'
                              : null
                          }}>
                          ₹{
                            parseInt(item.special_price) > 0
                              ? Math.round(item.special_price)
                              : Math.round(item.price)
                          }</Text>
                      </View>
                    </View>
                 
                   {/* buttons */}
                    <View style={{flex:1, alignSelf:'stretch',flexDirection: 'row',justifyContent: 'flex-start',alignItems: 'flex-end', }}>
                        {/* Add To Cart button */}
                        <TouchableOpacity onPress={() => this.goToCart(index, item,"addtocart")} style={{flex:1, marginRight:2.5, }}>
                          <LinearGradient style={{borderRadius: 5, alignItems: 'center', justifyContent:'center',padding:8}}
                              colors={["#ff619e", "#ff9fa6"]}>
                                <Text style={{color: 'white',fontSize: 8,fontFamily: "RubikMedium",textAlign:'center', flexWrap:'nowrap'}}>
                                  Add To Cart
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                     
                      {/* buy now button */}
                      <TouchableOpacity onPress={() => this.goToCart(index, item,"buynow")}  style={{flex:0.6, marginLeft:2.5,padding:8, borderRadius: 5,borderWidth:0.5,borderColor:'#3b75c9'}}>
                        <Text style={{color:'#3b75c9', fontSize:8, fontFamily:'RubikMedium', textAlign:'center'}}>Buy Now</Text>
                      </TouchableOpacity>
                      
                    </View>
                   
                  </TouchableOpacity>
                </View>
              )}}
              keyExtractor={(item, index) => index}
              numColumns={2}/>



          </View>

        :null}

        
      </SafeAreaView>
      </Animated.ScrollView>

        {/* Floating Icon */}
      <TouchableOpacity style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          right: 10,
          bottom: 8,
          shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
        }}>
        <TouchableOpacity onPress={() => this.NavigateToCart()} style={{
            width: 60,
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
            borderRadius: 30,
            shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
          }}>
          <FastImage resizeMode="cover" style={{
              width: 100,
              height: 100,
              position: 'absolute',
              margin: 10
            }} source={require('../../../../assets/button.png')}/>

          <FastImage resizeMode="contain" style={{
              height: 33,
              width: 33,
              alignSelf: 'center'
            }} source={require('../../../../assets/cartWhite.png')}/>
          <View style={{
              height: 17,
              width: 17,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              top: 10,
              right: 8,
              overflow: 'hidden',
              backgroundColor: 'white',
              borderRadius: 20,
              shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
            }}>
            <Text style={{
                color: '#ff7fa2',
                fontSize: wp('2.3%'),
                fontFamily: "RubikBold"
              }}>{this.state.CartItemCount}</Text>
          </View>

        </TouchableOpacity>

      </TouchableOpacity>

      {
        this.state.gettingData 

          ? <Activity/>
          : null
      }
      {
        this.state.ShowAlert
          ? <View style={{
                position: "absolute",
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                alignItems: "center",
                justifyContent: 'center'
              }}>
              <ActivityIndicator color={"#6464e7"} size={"large"}/>
            </View>
          : null
      }
    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edf4f6'
  },
  header: {
    width: '100%',
    height: wp('18.66%'),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  innerView: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    position: 'relative',
    alignItems: 'center'
  },

  backTouchable: {
    position: 'absolute',
    width: wp('8.8%'),
    height: wp('10.66%'),
    bottom: wp('2.6%'),
    left: 0
  },
  backIcon: {

    width: wp('5.86%'),
    height: wp('5.86%'),
    marginTop: wp('4%'),
    backgroundColor: 'transparent'
  },
  backTouchable: {

    position: 'absolute',
    width: wp('5.86%'),
    height: '100%',
    top: 0,
    left: wp('4%'),
    backgroundColor: 'transparent'
  },
  txtTitle: {
    color: 'white',
    fontSize: wp('5.33%'),
    fontFamily: "RubikBold"
  },
  txtTitleDesc: {
    color: 'white',
    fontSize: wp('4%'),
    fontFamily: "RubikRegular"
  }

})

export default Home;

//
// const Screen = (props) => (
//   <View style={{ flex : 1, backgroundColor:"#369" }}>
//     <Text>{ props.text }</Text>
//   </View>
// );
//
// export default class App extends Component {
//
//   SCREENS = [
//     <Screen text='first screen' />,
//     <Screen text='second screen' />,
//     <Screen text='third screen' />
//   ]
//
//   constructor(props) {
//     super(props)
//     this.state = {
//       activeTab : 0
//     }
//   }
//
//   render() {
//     return (
//       <View style={styles.container}>
//
//

//
//
//       </View>
//     )
//   }
// }
//
// const styles = StyleSheet.create({
//   ww:{
//                   width: 10,
//                   height: 10,
//                   borderRadius: 5,
//                   marginHorizontal: 8, backgroundColor: 'rgba(255, 255, 255, 0.92)'
//               },
//   container: {
//     flex: 1,
//     paddingTop: 40
//   },
//   tabBar : {
//     position : 'absolute',
//     right : 0,
//     bottom : 0,
//     left : 0,
//     borderTopWidth : 1,
//     borderColor : '#ddd',
//     backgroundColor : '#fff'
//   },  // <TouchableOpacity onPress={()=> this.openDetail(item)} style={{ width:'45%', height:wp('60.33%'),alignItems: 'center', backgroundColor:'white', flexDirection: 'column', margin: 4 , marginLeft:wp('2.6%') }}>
  //
  // <Image resizeMode='contain'
  //  style={{width: '40%', height: '40%', marginVertical: 10, marginLeft:15,}}
  //       source =  {{uri: item.images[0]}}
  //     />
  //

  // </TouchableOpacity>)

//Setting the number of column
//   tabsContainer : {
//     flexDirection : 'row',
//     height : 50,
//     paddingTop : 0,
//     paddingBottom : 0
//   }
// })
