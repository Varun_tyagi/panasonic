
import { NavigationActions } from 'react-navigation';
import Home from './Home';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getAccessriesData } from '../../modules/GetAccessries';
import { getProductType } from '../../modules/GetProductType';
import { addCartApi } from '../../modules/AddToCart';
import { getCartApi } from '../../modules/GetCart';
import { getDetail } from '../../modules/ProductDetail';
import { Map } from 'immutable';


const mapStateToProps = state => ({
  isBusy: state.GetAccessriesReducer.isBusy,
  response: state.GetAccessriesReducer,
  isBusyProductType: state.GetProductTypeReducer.isBusy,
  responseProductType: state.GetProductTypeReducer.response,
  isBusyVerifyOtp: state.VerifyOTPReducer.isBusy,
  responseVerifyOtp: state.VerifyOTPReducer.response,
  isBusyGetCart: state.GetCartReducer.isBusy,
  responseGetCart: state.GetCartReducer,
  isBusyGetDetail: state.ProductDetailReducer.isBusy,
  responseGetDetail: state.ProductDetailReducer
});

//export default Login;

export default connect(
  mapStateToProps,
  dispatch => {
    return {
      getAccessriesData: bindActionCreators(getAccessriesData, dispatch),
      getProductType: bindActionCreators(getProductType, dispatch),
      addCartApi: bindActionCreators(addCartApi, dispatch),
      getCartApi: bindActionCreators(getCartApi, dispatch),
      getDetail: bindActionCreators(getDetail, dispatch),
      navigate: bindActionCreators(NavigationActions.navigate, dispatch)
    };
  }
)(Home);
