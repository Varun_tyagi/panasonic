import React from 'react';
import { StyleSheet, Text,View,Platform, TouchableOpacity, Dimensions,ScrollView, SafeAreaView, StatusBar,ActivityIndicator,  } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
const { width, height } = Dimensions.get('window');
import {AutoHeightWebViewCustom} from '../../components/AutoHeightWebView/AutoHeightWebView'
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import FastImage from 'react-native-fast-image';
import * as authFun from '../../../../helpers/auth';
import { NavigationEvents } from 'react-navigation';
import getUser from '../../../../selectors/UserSelectors';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class Detail extends React.Component {

  static navigationOptions = {
    //headerVisible: false,
    header: null,
  };
   state = {
       highlights: [],
       selectedItem: this.props.navigation.getParam('selectedItem', ''),CartItemCount:0,loadAddtoCart:false,
   };


  async componentDidMount()
  {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id);
     this.callGetDetailAPI();
     this.getCartItemCount();
     var sampleArray = [ this.state.selectedItem.highlight ];

     this.setState({
       highlights: sampleArray
     });
  }
  async getCartItemCount(){
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCartCount?access_token="+accessToken+"&customer_id="+customer_id)
    .then( (response)=>{
       this.setState({CartItemCount: response.data.data});
       global.cartItemsCount = response.data.data;
        // console.warn(response.data)
    })
    .catch( (error) =>{
      //this.setState({gettingData: false});
    });}


  callGetDetailAPI() {

    this.props.getDetail(this.state.selectedItem.id)
      .then(() => this.afterGetDetailAPI())
    .catch(e => this.showAlert(e.message, 300));


  }

  afterGetDetailAPI() {
    console.log("isBusy value --- ",this.props.isBusyGetDetail);
   console.warn("response value --- ",this.props.responseGetDetail);


  }





           callAddToCartAPI(buynow) {

             this.props.addCartApi(this.state.selectedItem,"HELLO")
               .then(() => {
                if(buynow=='buynow'){ 
                this.afterAddCartAPI(buynow)
                }else{
                  alert('Added to cart.');
                  this.setState({loadAddtoCart:false});
                  this.getCartItemCount();
                }
              })
             .catch(e => this.showAlert(e.message, 300));

           }

           afterAddCartAPI(buynow) {
             console.log("isBusy value --- ",this.props.isBusy);
            console.log("response value --- ",this.props.response);
              this.callGetCartAPI(buynow);
           }



                      callGetCartAPI(buynow) {

                        this.props.getCartApi()
                          .then(() => this.afterGetCartAPI(buynow))
                        .catch(e => this.showAlert(e.message, 300));
                        this.setState({loadAddtoCart:false})

                      }

                      afterGetCartAPI(buynow) {
                        console.log("isBusy value --- ",this.props.isBusyGetCart);
                       console.log("response value --- ",this.props.responseGetCart);
                      
                        this.props.navigation.navigate('MyCart',{type:"accessories"});
                       
                      }



           showAlert(message, duration) {
             clearTimeout(this.timer);
             this.timer = setTimeout(() => {
               alert(message);
             }, duration);
           }


  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack(null);
  }
  goToCart(buynow){

    this.setState({loadAddtoCart:true})
    this.callAddToCartAPI(buynow);
  //  this.props.navigation.navigate('MyCart');
  }
  buyNow(){

    let x = this.state.selectedItem;
    Object.assign(x,{qty:"1"})

    this.props.navigation.navigate('ReviewOrder',{ProductInfo:[x]});
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }


  render() {
    return (

           <View style={styles.container}>

<NavigationEvents
onWillFocus={payload => {
this.getCartItemCount()}}
// onDidFocus={payload => console.warn('onDidFocus',payload)} //
// onWillBlur={payload => console.warn('will blur',payload)}
// onDidBlur={payload => console.warn('did blur',payload)}
/>
        <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>


           <View style={styles.header}>
              <FastImage resizeMode="cover" style = {{width: '100%', height:'100%', position:'absolute',top:0}} source = {require('../../../../assets/header.png')}/>
              <View style={{ width:'100%', marginTop: hp("9.2%"),  backgroundColor:'transparent', justifyContent: 'center', alignItems: 'center'}}>
               <Text style={styles.txtTitle}> ACCESSORIES </Text>
               <TouchableOpacity onPress={()=>this.goBack()} style={styles.backTouchable}>
               <FastImage resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/backwhite.png')}/>
               </TouchableOpacity>

               <TouchableOpacity onPress={()=>this.afterGetCartAPI()} style={[styles.cartTouchable,{alignItems: 'center'}]}>

               <FastImage resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/cartWhite.png')}/>
               <View style={{height: hp("2.33%"), justifyContent: 'center', borderRadius: 10 , alignItems: 'center', width: wp("4.33%") ,  position:'absolute',  backgroundColor: 'red',right: 0,top:-2}}>
               <Text style={{color: 'white',  fontSize: wp("3%"),alignSelf: 'center'}} > {this.state.CartItemCount} </Text>
               </View>
               </TouchableOpacity>


               </View>
           </View>

           <View style={{width: '100%', height: 'auto',alignItems: 'center',  marginTop: 0}}>
              <FastImage resizeMode="cover" style = {{width: '100%', height:wp('10.66%'), position: 'absolute'}} source = {require('../../../../assets/curve.png')}/>

              <View style={{width: '85%', height: wp('40%'), justifyContent:'center', alignItems:'center', borderRadius:10, marginTop: -wp('8%'), backgroundColor:'white'}}>
                 <FastImage resizeMode="contain" style = {{width: '90%', height:'90%'}} source = {{uri: this.state.selectedItem.images?this.state.selectedItem.images[0]:this.state.selectedItem.image}}/>
              </View>


              </View>
              <ScrollView style={{flex:1, backgroundColor: 'transparent', paddingBottom: wp('4%'), marginBottom:0}}>

              <Text style={{
                color: '#3b75c9',
                fontSize: wp('4%'),
                marginTop: wp('5.33%'),
                marginLeft: wp('5.33%'),
                fontFamily: "RubikBold"
              }}>{this.state.selectedItem.product_name} </Text>




              <Text style={{
                color: '#000000',
                fontSize: wp('4.8%'),
                marginTop: 5,
                marginLeft: wp('5.33%'),
                fontFamily: "RubikMedium"
              }}>{this.state.selectedItem.accessories_name} </Text>




              <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'flex-start',marginTop: wp('3.33%'),}}>
                <Text style={{
                color: '#000000',
                fontSize: wp('5.9%'),
                marginLeft: wp('5.33%'),
                  textDecorationLine:parseInt(this.state.selectedItem.special_price )< 0?'line-through':null,
                fontFamily: "RubikMedium"}}>₹ {parseInt(this.state.selectedItem.special_price)>0?this.state.selectedItem.special_price:this.state.selectedItem.price}</Text>
                {parseInt(this.state.selectedItem.special_price )>0?<Text style={{
                color: '#b9b9bb',
                fontSize: wp('4.9%'),
                marginLeft: wp('2.33%'),
                textDecorationLine:parseInt(this.state.selectedItem.special_price ) > 0?'line-through':null,
                fontFamily: "RubikMedium"}}>₹ {this.state.selectedItem.price} </Text>:null}
            </View>


              <View   >{AutoHeightWebViewCustom(this.state.selectedItem.description.replace(/<a\b[^>]*>/g,' ').replace(/<\/\s*a\s*>/g,' '),'#000000',)}</View>

              {/* <Text style={{
                color: '#000000',
                fontSize: wp('4%'),
                marginTop: wp('5.33%'),
                marginLeft: wp('5.33%'),
                marginRight:wp('5.33%'),
                fontFamily: "RubikLight"
              }}> {this.state.selectedItem.description} </Text> */}

              <Text style={{
                color: '#000000',
                fontSize: wp('4%'),
                marginTop: wp('5.33%'),
                marginLeft: wp('5.33%'),
                fontFamily: "RubikBold"
              }}> HIGHLIGHTS </Text>


              {/* <View   > */}
                <View style={{width: '100%', height: 'auto',  marginTop: wp('2.6%')}} >
                  
                  {this.state.highlights.map((data) => {
                    return(AutoHeightWebViewCustom(data.replace(/<a\b[^>]*>/g,' ').replace(/<\/\s*a\s*>/g,' '),'#000000',))})}
                </View>
                  {/* {this.state.highlights.map((data) => {return(AutoHeightWebViewCustom(data,'#000000',))})} */}
               {/* </View> */}




             </ScrollView>

            </SafeAreaView>
            <View style={{width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems:'center', height: 'auto',  marginTop: wp('5.33%'),position: 'absolute',bottom: 0,backgroundColor: '#edf4f6',padding: 10, }}>

            <TouchableOpacity onPress={()=> this.goToCart("addtocart")} style={{width: '45%',  height: wp('13.33%'), justifyContent: 'center', alignItems: 'center', marginRight:5, overflow: 'hidden', backgroundColor: 'transparent', borderRadius: 10}}>
            <FastImage resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}} source = {require('../../../../assets/button.png')}/>

            <Text style={{
              color: '#ffffff',
              fontSize: wp('4%'),

              fontFamily: "RubikBold"
            }}> Add To Cart </Text>

             </TouchableOpacity>
             <TouchableOpacity onPress={()=> this.goToCart("buynow")} style={{width: '45%', borderColor: '#3b75c9', borderWidth:1, justifyContent: 'center', alignItems: 'center',  height: wp('13.33%'), marginLeft:5,  backgroundColor: 'transparent', borderRadius: 10}}>
             <Text style={{
               color: '#3b75c9',
               fontSize: wp('4%'),

               fontFamily: "RubikBold"
             }}> Buy Now </Text>

              </TouchableOpacity>


             </View>
            {this.state.loadAddtoCart?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
            <ActivityIndicator color={"#6464e7"} size={"large"}/>
          </View>:null}

           </View>




    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:40,
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
        cartTouchable: {

          position: 'absolute',
         width:wp('10.66%'),
          height: '100%',
          top: 0,
          right: wp('4%'),
          justifyContent: 'center',
          backgroundColor: 'transparent',
          },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4.2%'),
        fontFamily: "RubikRegular"
      }



 })

 // import RazorpayCheckout from 'react-native-razorpay';
 //
 // var options = {
 // description: 'Credits towards consultation',
 // image: 'https://i.imgur.com/3g7nmJC.png',
 // currency: 'INR',
 // key: 'rzp_test_cZHLesapF55rTE',
 // amount: '5000',
 // name: 'foo',
 // prefill: {
 //   email: 'void@razorpay.com',
 //   contact: '9191919191',
 //   name: 'Razorpay Software'
 // },
 // theme: {color: '#F37254'}
 // }
 // RazorpayCheckout.open(options).then((data) => {
 // // handle success
 // alert(`Success: ${data.razorpay_payment_id}`);
 // }).catch((error) => {
 // // handle failure
 // alert(`Error: ${error.code} | ${error.description}`);
 // });
