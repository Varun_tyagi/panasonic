import React from 'react';
import { StyleSheet, Text,View,StatusBar,Platform, TouchableOpacity,Switch, Image,TextInput, ScrollView, SafeAreaView,ActivityIndicator, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import RazorpayCheckout from 'react-native-razorpay';
import { getConfiguration, setConfiguration } from '../../utils/configuration';
import ModalDropdown from 'react-native-modal-dropdown';
import { KeyboardAwareView } from 'react-native-keyboard-aware-view';
import getUser from '../../../../selectors/UserSelectors';
import * as authFun from '../../../../helpers/auth';


type Props = {
  navigate: PropTypes.func.isRequired
};




export default class NewAddress extends React.Component {
  constructor() {
    super()
    this.state = {
      highlights: [],itemCount:0,
      promocode: '',myCartData:[],refreshing: false,switch1Value: false,
      selectedUserType: '',user:"",dropdown_4_options: null,items:undefined,coupon:[],shipping:[],totalPrice:"",
      dropdown_4_defaultValue: 'loading...',SelectedType:"HOME",cityname:"",stateName:"",pincode:"",landmark:"",address:"",area_name:"",
      dropdown_6_icon_heart: true,addressType:[
        {firstName:'HOME',},
        {firstName:'OFFICE',},
        {firstName:'OTHER',}
      ],totalPrice:"",Indicator:false,
      StateName:[],City:[],Area:[],SelectedState:"Select State",SelectedCity:"Select City"
    }
  }


async  componentDidMount()
  {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 


    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    console.warn(this.props.navigation.state.params);
    if(this.props.navigation.state.params != undefined){
      this.setState({myCartData:this.props.navigation.state.params.ProductInfo})
    }
    this.getData("state")

  }



  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.pop();
  }

  onSubtract(index, item){
    if(parseInt(item.qty) > 1){
      --item.qty;
      this.setState({
        refresh: !this.state.refresh
      });
    }

  };
  onCheck(value){
    this.setState({SelectedType:value})
  }
  onAdd(index, item){
    ++item.qty;
    this.setState({
      refresh: !this.state.refresh
    });
  };
  toggleSwitch1 = (value) => {
    this.setState({switch1Value: value})
    console.warn('Switch 1 is: ' + value)
  }


  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  updateUser = (user) => {
    this.setState({ user: user })
  }


async  getData(MethodName,ID) {
    var that = this;
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    const accessToken = getConfiguration('accessToken');
    var  APINAME = "";
    var  TYPE = ""
    if(MethodName == "state"){
      APINAME =   "getState";
    }
    if(MethodName == "CITY"){
        APINAME = "getCity";
        TYPE = "state_id";
    }
    if(MethodName == "PINCODE"){
        APINAME = "getArea";
        TYPE = "city_id"
    }

    // Make a request for a user with a given ID
    axios.get('https://www.ecarewiz.com/ewarrantyapi/'+APINAME+"/?access_token="+accessToken+"&"+TYPE+"="+ID).then((response)=>{
      if(MethodName == "state"){
         that.setState({StateName: response.data.data});
      }
      if(MethodName == "CITY"){
         that.setState({City: response.data.data});
      }
      if(MethodName == "PINCODE"){
         that.setState({Area: response.data.data});
      }

      // console.warn(response.data);
    }).catch(function(error) {
      that.setState({gettingData: false});

    });

  }

async  AddAdress(){
    if(this.state.stateName == ""){
      alert("Please choose state name.");
      return;
    }
    if(this.state.cityname == ""){
      alert("Please choose city name.");
      return;
    }
    if(this.state.pincode == ""){
      alert("Please choose pincode.");
      return;
    }
    if(this.state.landmark == ""){
      alert("Please enter landmark.");
      return;
    }
    if(this.state.address == ""){
      alert("Please enter address.");
      return;
    }

    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    var bodyFormData = new FormData();
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    bodyFormData.append('customer_id', customer_id);
    bodyFormData.append('address_type', this.state.SelectedType);
    bodyFormData.append('country_name', "INDIA");
    bodyFormData.append('state_name', this.state.stateName);
    bodyFormData.append('city_name', this.state.cityname);
    bodyFormData.append('area_name', this.state.area_name);
    bodyFormData.append('pincode', this.state.pincode);

    bodyFormData.append('address', this.state.address);
    bodyFormData.append('near_by', this.state.landmark);




    axios.post("https://www.ecarewiz.com/ewarrantyapi/addCustomerAddress", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
  ).then((response)=>{
    this.setState({Indicator:false})
    //  console.warn(response);
    // this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,orderDetail:response.data.data,totalPriceNew:this.props.navigation.state.params.totalPriceNew,DiscountedPrice:this.props.navigation.state.params.DiscountedPrice})
  }).catch((response)=>{
    this.setState({Indicator:false})
    alert(response.message)
    console.log(JSON.stringify(response));
    console.warn(JSON.stringify(response));
  })




  }




async SendtoServerwithAddress(){
    //this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo})
    this.setState({Indicator:true})
    let totalQuantity = 0;
    let totalPrice = 0;
    let gst = 0;
    if (this.state.myCartData) {
      this.state.myCartData.forEach(item => {

        totalQuantity += item.price;

        totalPrice += item.qty * Math.trunc(item.price);

      });
    }

    var coupon = {"coupon_code":"","coupon_discount":""};
    let shipping = {
      "address_type":this.state.SelectedType,
      "country":"INDIA",
      "state":  this.state.stateName ,
      "city":this.state.cityname,
      "area":this.state.area_name,
      "pincode":this.state.pincode,
      "landmark":this.state.landmark,
      "address":this.state.address
    }


    console.warn(shipping);
    var bodyFormData = new FormData();
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    bodyFormData.append('customer_id', customer_id);
    bodyFormData.append('order_type', "accessories");
    bodyFormData.append('currency', "INR");

    bodyFormData.append('subtotal',this.props.navigation.state.params.totalPriceNew);
    bodyFormData.append('grand_total',this.props.navigation.state.params.totalPriceNew);
    bodyFormData.append('tax', "0.0");
    bodyFormData.append('discount', this.props.navigation.state.params.DiscountedPrice);

    bodyFormData.append('shipping_charge', "0.0");
    bodyFormData.append( "coupon",JSON.stringify({"coupon_code":"","coupon_discount":""}));
    bodyFormData.append("shipping", JSON.stringify(this.state.shipping));
    bodyFormData.append('customer_address_id', "1");

    this.send(bodyFormData,accessToken,customer_id);
  }
  async  send(bodyFormData,accessToken,customer_id){
    await  axios.post("https://www.ecarewiz.com/ewarrantyapi/save_order", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
  ).then((response)=>{
    this.setState({Indicator:false})
    // console.warn(response.data.data);
    this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,orderDetail:response.data.data,totalPriceNew:this.props.navigation.state.params.totalPriceNew,DiscountedPrice:this.props.navigation.state.params.DiscountedPrice})
  }).catch((response)=>{
    this.setState({Indicator:false})
    alert(response.message)
    console.log(JSON.stringify(response));
    console.warn(JSON.stringify(response));
  })
}
render() {
  let totalQuantity = 0;
  let totalPrice = 0;
  let gst = 0;
  const {params} = this.props.navigation.state;
  if (this.state.myCartData) {
    this.state.myCartData.forEach(item => {

      totalQuantity += item.price;

      totalPrice += item.qty * Math.trunc(item.price);

    });
    if(totalPrice != 0){gst =  0
      totalPrice = totalPrice+gst;}
    }
    var StateName = [];
      if(this.state.StateName.length >0){
      this.state.StateName.forEach((item,index)=>{

        StateName.push(item.state_name)
      })
    }else{
      StateName = [];
    }

    var CityName = [];
    if(this.state.City.length >0){
    this.state.City.forEach((item,index)=>{

      CityName.push(item.city_name)

    })
  }else{
    CityName = [];
  }
            var Area = [];
            if(this.state.Area.length > 0){
            this.state.Area.forEach((item,index)=>{
              Area.push(item.area_name+" "+item.postal_code)
            })
          }else{
            Area = [];
          }



    return (

      <View style={styles.container}>


    <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>


          <View style={styles.header}>
            <Image resizeMode="cover" style = {{width: '100%', height:'100%', position:'absolute'}} source = {require('../../../../assets/header.png')}/>
            <View style={{ width:'100%', height: wp('10.66%'), marginTop: wp('13.33%'),  backgroundColor:'transparent', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.txtTitle}> ADDRESS  </Text>
              <TouchableOpacity onPress={()=>this.goBack()} style={styles.backTouchable}>
                <Image resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/backwhite.png')}/>
              </TouchableOpacity>


            </View>
            <View style={{width: '100%', height: wp('10.33%'), marginTop:wp('5%'), justifyContent:'center', alignItems: 'center'}}>
              <Image resizeMode="contain" style = {{width: wp('10.33%'), height:wp('10.33%'),tintColor:"white"}} source = {require('../../../../assets/locationicon.png')}/>

              <Image resizeMode="contain" style = {{width: wp('20%'),  height:30}} source = {require('../../../../assets/Addressbar.png')}/>
            </View>






          </View>


          <View style={{width: '100%', height: 'auto',alignItems: 'center',  marginTop: 0}}>
            <Image resizeMode="cover" style = {{width: '100%', height:wp('10.66%')}} source = {require('../../../../assets/curve.png')}/>

            <View showsVerticalScrollIndicator={false} style={{width: '90%', height: 'auto',marginTop: 0, marginBottom:hp("30%"), backgroundColor: 'transparent'}}>


              <View style={{width: wp("85%"),height: hp("6%"),backgroundColor: "#d9dbdc",alignItems: 'center',alignSelf: 'center',borderRadius: 30,margin: 10,flexDirection: 'row'}}>
                <Image resizeMode="contain" style = {{width: wp('6.33%'), height:wp('6.33%'),tintColor:"gray",alignSelf: 'center',marginLeft:10}} source = {require('../../../../assets/locationicon.png')}/>
                <View style={{alignItems: 'flex-start',justifyContent: 'flex-start'}}>
                  <Text style={{
                    color: 'gray',
                    fontSize: wp('4%'),
                    marginLeft: wp('2%'),
                    fontFamily: "RubikRegular"
                  }}>Detect My Location</Text>
                  <Text style={{
                    color: 'gray',
                    fontSize: wp('4%'),
                    marginLeft: wp('2%'),
                    fontFamily: "RubikBold"
                  }}>{this.state.switch1Value == true?"ON":"OFF"}</Text></View>
                  <View style={{position: 'absolute',right: 10}}>
                    <Switch
                      thumbColor={this.state.switch1Value == false?"white": "#ff8ba3"}
                      trackColor={"white"}
                      trackColor={{ true: 'white', false:'#ffffff'  }}
                      onValueChange = {this.toggleSwitch1}
                      value = {this.state.switch1Value}/>
                    </View>
                  </View>

                  <View style={{justifyContent: 'space-around',alignItems: 'center',flexDirection: 'row',padding:  5}}>

                    <ModalDropdown
                      defaultValue={this.state.SelectedState}
                      defaultIndex ={-1}
                      options={StateName}

                      onSelect={(rowData,index) =>{ this.getData("CITY", this.state.StateName[rowData].state_id);this.setState({stateName:index,SelectedCity:"Select City"}); this.lookaheadFilter.select(-1); }}
                      dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                      style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}
                      textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',justifyContent: 'center',alignSelf: 'center',alignItems: 'center',margin: 5}}
                      animated={ true}
                      dropdownStyle={{width: '60%',height: hp("30%"),fontFamily: 'RubikRegular', marginTop:-20}}
                      dropdownTextHighlightStyle={{color:"blue",fontFamily: 'RubikRegular'}}
                    />



                      <ModalDropdown

                        ref={ (ref) => this.lookaheadFilter = ref}
                        defaultValue={this.state.SelectedCity}
                        onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.City[rowData].city_id);this.setState({cityname:index}); this.lookaheadFilter2.select(-1); }}
                        options={CityName}
                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                        style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}
                        textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',margin: 5}}
                        dropdownStyle={{width: '60%',fontFamily: 'RubikRegular', marginTop:-20}}
                      />





                  </View>
                  <View style={{justifyContent: 'space-around',alignItems: 'center',flexDirection: 'row',padding:  5}}>

                    <ModalDropdown
                      ref={ (ref) => this.lookaheadFilter2 = ref}
                      defaultValue={"Select Pincode and Area"}
                      onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.Area[rowData].city_id);this.setState({pincode:this.state.Area[rowData].postal_code,area_name:this.state.Area[rowData].area_name});   }}
                      options={Area}
                      dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                      style={{width: '98%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}
                      textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular'}}
                      dropdownStyle={{width: '60%',fontFamily: 'RubikRegular', marginTop:-20}}
                    />




                  </View>
                  <View style={{width: '95%', flexDirection: 'row', height: wp('10.33%'),margin: 7, borderRadius:wp('6.66%'), backgroundColor: 'white',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}>


                    <TextInput
                      style={[styles.txtFld,{width: '95%'}]}
                      placeholder="Landmark"
                      placeholderTextColor= "#707171"
                      maxLength = {10}
                      onChangeText={(phone) => this.setState({landmark:phone})}
                      value={this.state.landmark}
                    />

                  </View>
                  <FlatList
                    data = {this.state.addressType}
                    extraData={this.state}
                    horizontal
                    renderItem = {({item}) =>
                    <TouchableOpacity  onPress={() => { this.onCheck(item.firstName) }} activeOpacity = {0.5} style={{margin: 7,flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',margin: 7}}>

                      <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}}>
                        <View style={{backgroundColor:this.state.SelectedType == item.firstName?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                      </View>
                      <Text style={{
                        color: 'gray',
                        fontSize: wp('4%'),
                        marginLeft: wp('2%'),
                        fontFamily: "RubikBold"
                      }}>{item.firstName}</Text>
                    </TouchableOpacity>
                  }
                />
                <View style={{width: '95%', margin: 7, borderRadius:wp('2.66%'), backgroundColor: 'white',  shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5}}>


                  <TextInput
                    placeholder="Enter Address"
                    onChangeText={password => this.setState({address : password})}
                    numberOfLines = {3}
                    //editable={this.state.onSubmit}
                    textAlignVertical={'top'}
                    value={this.state.address}
                    underlineColorAndroid='transparent'
                    style={{fontFamily: 'RubikRegular',
                    color:'#707171',
                    fontSize: wp("4%"),
                    height: hp("10%"),
                    alignItems: 'flex-start',
                    justifyContent: 'flex-start',
                    width:'95%'}}
                    multiline

                  />

                </View>










                </View>
              </View>
              <View style={{position: 'absolute',bottom: 5,left: 0,right: 0,  height: wp('13.33%'),}}>
                <TouchableOpacity onPress={()=>{this.AddAdress()}  } style={{width: '50%',alignSelf: 'center', justifyContent: 'center', alignItems: 'center', marginRight:5, overflow: 'hidden', height: wp('13.33%')
                  , backgroundColor: 'transparent', borderRadius: 10}}>
                  <Image resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}} source={require('../../../../assets/button.png')}/>

                  <Text style={{color: '#ffffff',fontSize: wp('4%'),fontFamily: "RubikBold"}}> CONTINUE </Text>

                </TouchableOpacity>
              </View>











            </SafeAreaView>
            {this.state.Indicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
              <ActivityIndicator color={"#6464e7"} size={"large"}/>
            </View>:null}


          </View>




        );
      }
    }



    const styles = StyleSheet.create({
      container: {
        flex:1,
        backgroundColor: '#edf4f6'
      },
      header: {
        width: '100%',
        height: wp('40%'),
        marginTop: -wp('13.33%'),
        backgroundColor: 'transparent'

      },
      innerView: {
        flex: 1,
        backgroundColor: '#f2f2f2',
        position :'relative',
        alignItems: 'center'
      },



      backIcon: {

        width:wp('5.86%'),
        height: wp('5.86%'),

        backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
        width:wp('10.33%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
      },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
        fontSize: wp('4.58%'),
        fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
      listTile:{width: '100%', height: wp('32%'), shadowColor: '#000000',
      shadowOffset: {
        width: 0,
        height: 0
      },
      shadowRadius: 1,
      shadowOpacity: 0.8, borderRadius:10,  marginTop: wp('2.6%'), backgroundColor:'white'},
      txtFld:{
        marginLeft: wp('1%'),
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular",
        color: 'gray'
      }
    })
