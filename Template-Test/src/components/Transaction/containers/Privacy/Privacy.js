import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, ScrollView, SafeAreaView, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class Privacy extends React.Component {

   state = {
       highlights: [],
       phone: ''
   };


  componentDidMount()
  {
     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });
  }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

goToNextScreen(){
  this.props.navigation.navigate('ReferalCode');
}

goToCorporate(){
  this.props.navigation.navigate('Login');

}

  render() {

    return (

           <View style={styles.container}>
            <Image resizeMode="cover" style = {{width: '100%', height: '100%', position: 'absolute', top:0, left: 0}} source = {require('../../../../assets/screenBG.png')}/>





           <View style={{width: '100%', height: 'auto',marginTop:30, backgroundColor: 'transparent', justifyContent:'center'}}>

      <Text style={styles.txtTitle}> PRIVACY POLICY </Text>

      </View>

      <View style={{flex:1, marginTop: wp('5.33%'), backgroundColor: 'transparent'}}>

       <Text style={styles.txtTitleDesc}>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.        </Text>
       </View>








<View style={{width: '100%', height: 100, marginTop: wp('5.33%'), flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
<TouchableOpacity onPress={()=> this.goBack()}>
  <Image resizeMode="contain" style = {{width: 100, height: 100 }} source = {require('../../../../assets/Close.png')}/>
  </TouchableOpacity>

</View>





           </View>




    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        fontFamily: "RubikBold",
        marginLeft: 20,
        marginTop: 20
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular",
        marginHorizontal: wp('5.33%')
      },
      txtCountryCode:{
        color: 'black',
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular"
      },
      txtPrivacy:{
        color: 'white',
        fontSize: wp('4.5%'),
        fontFamily: "RubikBold",
        borderColor: 'white',
        borderBottomWidth:0.6
      },
      txtUserLogin:{
        color: 'white',
        fontSize: wp('6.66%'),
        fontFamily: "RubikBold"

      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

             txtFld:{
               height: '90%',
               marginTop: 4,

               width:'90%',
              fontSize: wp('5.33%'),
              fontFamily: "RubikBold",
              textAlign: 'center',
              color: 'black'
             },


 })
