import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, ScrollView, SafeAreaView, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class ReferalCode extends React.Component {

   state = {
       highlights: [],
       phone: ''
   };


  componentDidMount()
  {
     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });
  }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

goToNextScreen(){
  this.props.navigation.navigate('SideMenu');
}

goToCorporate(){
  this.props.navigation.navigate('Login');

}

  render() {

    return (

           <View style={styles.container}>
            <Image resizeMode="cover" style = {{width: '100%', height: '100%', position: 'absolute', top:0, left: 0}} source = {require('../../../../assets/screenBG.png')}/>
           <View style={{width: '100%', height: wp('69.33%'), backgroundColor: 'transparent'}}>
             <Image resizeMode="cover" style = {{width: '100%', height: '100%'}} source = {require('../../../../assets/referal.png')}/>
           </View>


           <View style={{width: '100%', height: 30, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

              <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
               <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
                        <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/fill.png')}/>
                <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
           </View>

           <View style={{width: '100%', marginTop: wp('5%'), height: 'auto', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

      <Text style={styles.txtTitle}> DO YOU HAVE ANY REFERRAL CODE? </Text>
      <Text style={styles.txtTitleDesc}> Please enter here </Text>





       <View style={{width: '80%', flexDirection: 'row', height: wp('13.33%'), marginTop: wp('4.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>


       <TextInput
          style={styles.txtFld}
          placeholder="Enter your Referral Code"
          placeholderTextColor= 'black'
          maxLength = {10}
          onChangeText={(phone) => this.setState({phone})}
         value={this.state.phone}
       />

       </View>

      </View>





<View style={{width: '100%', height: 100, marginTop: wp('8.33%'), flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
<TouchableOpacity onPress={()=> this.goToNextScreen()}>
  <Image resizeMode="contain" style = {{width: 100, height: 100 }} source = {require('../../../../assets/next.png')}/>
  </TouchableOpacity>

</View>


<TouchableOpacity onPress={()=>this.goToNextScreen()} style={{position: 'absolute',bottom: 0,margin: 10,width: '100%', height: 'auto', flexDirection:'row', backgroundColor: 'transparent', justifyContent:'flex-end', alignItems:'center'}}>
 <View style={{marginRight: 15, borderColor: 'white', borderBottomWidth: 1.0}}>
  <Text style={styles.txtUserLogin}> SKIP </Text>
</View>
</TouchableOpacity>






           </View>




    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.20%%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtCountryCode:{
        color: 'black',
        fontSize: wp('4.58%'),
        fontFamily: "RubikRegular"
      },
      txtPrivacy:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikBold",
        borderColor: 'white',
        borderBottomWidth:0.6
      },
      txtUserLogin:{
        color: 'white',

        fontSize: wp('5.33%'),
        fontFamily: "RubikRegular"

      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

             txtFld:{
               height: '90%',
               marginTop: 4,
               marginLeft: wp('5%'),
               width:'70%',
              fontSize: wp('4.58%'),
              fontFamily: "RubikRegular",
              color: 'black'
             },


 })
