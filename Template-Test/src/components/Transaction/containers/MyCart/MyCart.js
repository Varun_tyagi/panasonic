import React from 'react';
import { StyleSheet, Text,View, ImageBackground,TouchableOpacity, Modal,Platform,Image,TextInput,AsyncStorage,  StatusBar, ScrollView, SafeAreaView, FlatList,ActivityIndicator } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import { setConfiguration ,getConfiguration } from '../../utils/configuration';
import * as authFun from '../../../../helpers/auth';
import getUser from '../../../../selectors/UserSelectors';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import NavigationService from '../../../navigation/NavigationService';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class MyCart extends React.Component {

  constructor() {
    super()
    this.state = {
      highlights: [],
      itemCount:0,
      customcoupon:"",
      couponDiscount:"",
      employeeId:'',
      promocode: '',
      myCartData:[],
      refreshing: false,
      TYPE:"",
      ShowAlert:false,
      ModalVisibleStatus:false,
      CouponCodes:[],
      indexoFHide:"",
      open:false,
      discount_mode:'',
      coupon_product_group_id:0,
      coupn_type:'',
      DiscountedPrice:0,
      PromoCodeName:"",
      totalPrice:0,
      coupon_type:''
    }
  }


async  componentDidMount()
  {
    this.setState({ShowAlert:true})
      if(this.props.navigation.state.params != undefined){
        this.setState({TYPE:this.props.navigation.state.params.type})
      }


     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });

     this.props.navigation.addListener('willFocus', async () => {   
        var token = await authFun.checkAuth();
        //alert(' accesssss token token:-----'+ token);
        setConfiguration('accessToken', token);
        var user = await getUser('state');
        setConfiguration('customer_id', user.customer_id); 
        const accessToken = getConfiguration('accessToken');
        const customer_id = getConfiguration('customer_id');
        axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerCart?access_token="+accessToken+"&customer_id="+customer_id)
              .then(response => {
                // this.state.myCartData.push(response.data.data)
                this.setState({myCartData:response.data.data,ShowAlert:false, DiscountedPrice:response.data.data.length>0?this.state.DiscountedPrice:0})
              })
              .catch((error) => {
                NetInfo.fetch().then(async (state)=>{
                if(!state.isConnected){
                  alert('Please check your internet connection.');
                }else{
                  alert(error);
                }
              })
                this.setState({ShowAlert:false})
              })
    
              axios.get("https://www.ecarewiz.com/ewarrantyapi/getCoupons?access_token="+accessToken+"&customer_id="+customer_id)
                  .then(response => {
                    //  console.warn(response.data.coupons)
                    this.setState({CouponCodes:response.data.coupons})
                  })
                  .catch((error) => {
                    NetInfo.fetch().then(async (state)=>{  
                    if(!state.isConnected){
                        alert('Please check your internet connection.');
                      }else{
                        alert(error);
                      }
                    })
                  })
       });

    
  }
  onSubtract(index, item){
    if(parseInt(item.qty) > 1){
      --item.qty;
      this.setState({
        refresh: !this.state.refresh
      });
    }
    this.cartCountUpdate(item,"update")

  };


async  cartCountUpdate(item,type){
  var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
  const accessToken = getConfiguration('accessToken');
  const customer_id = getConfiguration('customer_id');
          var bodyFormData = new FormData();
          bodyFormData.append('cart_id', item.cart_id);
          methodName = "";
          if(type == "update"){
            bodyFormData.append('qty', item.qty);
            methodName = "updateCart";
          }else{
            methodName = "deleteCart";
          }


       await axios.post("https://www.ecarewiz.com/ewarrantyapi/"+methodName, bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`} }
      ).then((response)=>{
        if(type == "delete"){
          // this.componentDidMount();
          axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerCart?access_token="+accessToken+"&customer_id="+customer_id)
          .then(response => {
            // this.state.myCartData.push(response.data.data)
            this.setState({myCartData:response.data.data,DiscountedPrice:response.data.data.length>0?this.state.DiscountedPrice:0,ShowAlert:false},()=>{
              if(!this.state.myCartData.length){
                this.setState({})
              }
            })
            global.cartItemsCount = response.data.data.length;
          })
          .catch((error) => {
            NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          })
          })

          axios.get("https://www.ecarewiz.com/ewarrantyapi/getCoupons?access_token="+accessToken+"&customer_id="+customer_id)
               .then(response => {
                //  console.warn(response.data.coupons)
                 this.setState({CouponCodes:response.data.coupons})
               })
               .catch((error) => {
                NetInfo.fetch().then(async (state)=>{  
                if(!state.isConnected){
                    alert('Please check your internet connection.');
                  }else{
                    alert(error);
                  }
                })
               })
          this.setState({
            refresh: !this.state.refresh
          });
        }
      // console.warn(response);
      }).catch((response)=>{
      // console.warn(response);
      })
  }



   onAdd(index, item){

      ++item.qty;
      this.setState({
        refresh: !this.state.refresh
      });
      this.cartCountUpdate(item,"update")
  };


  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }

  async validateCoupon(){
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    if(this.state.customcoupon!=""){
      const accessToken = getConfiguration('accessToken');
      const customer_id = getConfiguration('customer_id');
              var bodyFormData = new FormData();
              bodyFormData.append('customer_id',customer_id);
              bodyFormData.append('coupon_code',this.state.customcoupon);

      axios.post("https://www.ecarewiz.com/ewarrantyapi/validate_coupon_applicable", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`} }
     ).then(async (response)=>{
      if(response.data.status && (parseFloat(this.state.totalPrice) > parseFloat(response.data.data.discount))){
        await AsyncStorage.setItem('couponDetails', JSON.stringify(
          {DiscountedPrice:parseInt(response.data.data.discount)
            ,PromoCodeName:response.data.data.coupon_code,
            customcoupon:response.data.data.coupon_code,
            ModalVisibleStatus:false,
            indexoFHide:"",
            open:false, 
            couponDiscount:response.data.data.discount,
            discount_mode:response.data.data.discount_mode,
            coupn_type:response.data.data.coupon_type.toLowerCase(),
            coupon_product_group_id:response.data.data.product_group_id
          }
        ));
        
        this.setState({DiscountedPrice:parseInt(response.data.data.discount)
          ,PromoCodeName:response.data.data.coupon_code,
          customcoupon:response.data.data.coupon_code,
          ModalVisibleStatus:false,
          indexoFHide:"",
          open:false, 
          couponDiscount:response.data.data.discount,
          discount_mode:response.data.data.discount_mode,
          coupn_type:response.data.data.coupon_type.toLowerCase(),
          coupon_product_group_id:response.data.data.product_group_id
        });
      }else{
        alert('Invalid coupon');
      }
     }).catch(function (error) {
    if (error.response) {
      alert(error.response.data.message)
    } else if (error.request) { 
      console.warn("request",error.request);
    } else {
      console.warn('Error', error.message);
    }
    console.warn(error.config);
  });
    }else{alert("Please enter coupon code")}
  }




        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

async goToNextScreen(totalPriceNew,DiscountedPrice,gst){
  if(this.state.myCartData.length > 0){
    this.props.navigation.navigate('MyAddress',{ProductInfo:this.state.myCartData,totalPriceNew:totalPriceNew,DiscountedPrice:DiscountedPrice,gst:gst,couponCode:this.state.customcoupon, couponDiscount:this.state.couponDiscount, employeeId: this.state.employeeId, discount_mode: this.state.discount_mode});
}else{
  alert("Please add items into cart.")
}
}


  render() {
    const {TYPE} = this.state;
    let totalQuantity = 0;
    let totalPrice = 0;
    let gst = 0;
    let itemCount = this.state.myCartData.length;
    let itemDiscount = this.state.discount_mode=='percentage'?this.state.DiscountedPrice:(this.state.DiscountedPrice && this.state.myCartData.length?this.state.DiscountedPrice/itemCount:0);
    let   itemPrice = 0;
    let finalDiscount = 0;
    let totalPercentDiscount = 0 ;
    let totalPriceNew = 0;
    if (this.state.myCartData){
      this.state.myCartData.forEach((item,index) => {
        itemCount=index+1;
        let itemGst = 0;
        let itemTotalPrice = 0;
        let itemTotalDp = 0;
        let item_price = parseFloat(parseInt(item.special_price) > 0 ?item.special_price:item.price);
        itemTotalPrice = parseFloat(parseInt(item.qty )* item_price);
        totalPrice += itemTotalPrice;
        totalPercentDiscount += (this.state.discount_mode=='percentage'?itemTotalPrice*(itemDiscount/100):0);
        itemTotalDp = itemTotalPrice - (totalPercentDiscount?itemTotalPrice*(itemDiscount/100):itemDiscount);
        itemGst = parseFloat((itemTotalDp - (itemTotalDp * (100/(100+parseFloat(item.gst))))).toFixed(2));
        gst += parseFloat((itemTotalDp - (itemTotalDp * (100/(100+parseFloat(item.gst))))).toFixed(2));
      });
    }
    finalDiscount = (totalPercentDiscount?totalPercentDiscount:this.state.DiscountedPrice).toFixed(2);
    totalPrice = totalPrice.toFixed(2);
    totalPriceNew = totalPrice-(finalDiscount);
    return (

    <View style={styles.container}>
      <StatusBar translucent={true} hidden={this.state.ModalVisibleStatus} />
      <ScrollView  style={{flex:1,alignSelf:'stretch', backgroundColor: 'transparent', marginBottom:60}}>
        <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>
           {/* header */}
           <View>
           <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: -740,
                  height: 900,
                  width: 900,
                  borderRadius: 450,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                <TouchableOpacity onPress={()=>this.goBack()} style={{zIndex:1, height: 20, width: 20,}}>
                  <Image source={require('../../../../assets/backwhite.png')} style={{ height: '100%', width: '100%', }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: '#e9eaff', textAlign:'center',flex:1,alignSelf:'stretch',marginLeft:-20, fontFamily: 'RubikMedium' }}>MY CART</Text>
              </View>
           </View>
                {/* Items in cart */}
           <View style={{flex:1, alignSelf:'stretch',height: 'auto',alignItems: 'center',  marginTop: 10, paddingHorizontal:15,marginBottom:30 }}>
               {!this.state.ShowAlert ?
              <View style={{flex:1, alignSelf:'stretch', }}>
              <Text style={{
                 color: '#ffffff',
                 marginTop: 20,
                 fontSize: 15,
                 fontFamily: "RubikMedium"
               }}> {itemCount} {this.state.myCartData.length>1?"Items":"Item"} in your cart </Text>


              {/* Main list of cart */}
               <FlatList
               keyExtractor={(item, index) => index.toString()}
               extraData={this.state}
               data={this.state.myCartData}
               refreshing={this.state.refreshing}
               contentContainerStyle={{paddingBottom:5}}
                renderItem={({item, index}) =>
                {
                return (

                <View style={{alignSelf:'stretch',flexDirection:'row', borderRadius:10, overflow: 'hidden', marginTop: 10, backgroundColor:'white', padding:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, margin:2.5}}>
                  {/* Left -> product details */}
                  <View style={{backgroundColor:'transparent', flex:1, alignItems:'flex-start',justifyContent:'center', padding:5}}>
                    <View>
                      <Text style={{fontFamily:'RubikBold', fontSize:13, color:'#3875c9'}}>{item.product_name.toUpperCase()}</Text>
                      <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#4f4f4f', marginTop:8}}>{item.item_type=='amc'?item.sub_product_name:item.item_name}</Text>
                      {item.item_type=='amc'?<Text style={{fontFamily:'RubikRegular', fontSize:13, color:'#4f4f4f', marginTop:8}}>Model No: {item.model_name}</Text>:null}
                      <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#000', marginTop:12,}}>
                          ₹ {item.price}
                        </Text>
                        {parseInt(item.plan_amount)>0 && parseInt(item.plan_amount)!=parseInt(item.price)?
                          <Text style={{paddingLeft:10,fontFamily:'RubikRegular', fontSize:13, color:'#796dec', marginTop:12, textDecorationLine:'line-through'}}>
                            ₹ {item.plan_amount}
                          </Text>:null}
                      </View>
                    
                    </View>
                    {
                      item.item_type == "amc" ?null:
                      <View style={{alignSelf:'stretch', flexDirection: 'row', marginTop:5, backgroundColor:'transparent'}}>

                        <TouchableOpacity  onPress={item.qty==1?()=>{this.cartCountUpdate(item, "delete")}:this.onSubtract.bind(this, index, item)}
                          style={{width: 25, height:25, alignItems: 'center', justifyContent: 'center', borderRadius:12.5, borderColor: '#cbcbce', borderWidth: 1, backgroundColor:'transparent'}}>
                        <Text style={{
                          color:'#000',
                          fontSize: 12.5,
                          fontFamily: "RubikMedium",
                        }}>-</Text>

                      </TouchableOpacity>
                        <View style={{  alignItems: 'center', justifyContent: 'center', backgroundColor:'transparent'}}>
                        <Text style={{
                          alignSelf:'center',
                          color: '#000000',
                          fontSize: 15,
                          marginHorizontal:10,
                          fontFamily: "RubikMedium",
                        }}>{item.qty}</Text>

                        </View>

                        <TouchableOpacity   onPress={this.onAdd.bind(this, index, item)}
                        style={{width: 25, height:25, alignItems: 'center', justifyContent: 'center', borderRadius:12.5, borderColor: '#cbcbce', borderWidth: 1, backgroundColor:'transparent'}}>

                        <Text style={{
                          color:item.quantity==1?'#cbcbce':'#000',
                          fontSize: 12.5,
                          fontFamily: "RubikMedium",
                        }}>+</Text>

                        </TouchableOpacity>


                        </View>
                      }

                  </View>
                  {/* delete item button */}
                  <TouchableOpacity onPress={()=>{this.cartCountUpdate(item, "delete")}} style={{alignItems: 'center',justifyContent: 'center',padding: 5}}><FastImage resizeMode="contain" style = {{width: wp('5%'), height:wp('5%')}}
                    source = {require('../../../../assets/delete.png')}/>
                  </TouchableOpacity>
                  {/* image icon */}
                  <View style={{backgroundColor:'white', borderWidth:2, borderColor:'rgba(240,240,240,0.5)', height:110, width:110,borderRadius:5, alignItems:'center', justifyContent:'center', padding:5, margin:5}}>
                    {
                      item.item_type=='amc'?
                      <View style={{alignItems:'center', justifyContent:'center', }}>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>{item.plan.indexOf('FirstYearPlan')!=-1?1:item.plan.indexOf('SecondYearPlan')!=-1?2:3}</Text>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>Year</Text>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>Warranty</Text>
                      </View>:
                      item.image?
                      <FastImage resizeMode="contain" style = {{height:'100%',width:'100%'}} 
                      source = {item.image == ""?require('../../../../assets/image4.png'):{uri:item.image}}/>:null
                    }
                    {
                      item.item_type=='amc'?
                      <FastImage resizeMode="contain" style = {{position:"absolute",width: 25, height:25,right:-5,top:-5}} 
                        source = {require('../../../../assets/amc.png')}/>:null
                        }
                  </View>
                  
                </View>)
              }}/>
              {/* Apply promo button */}
              { 
                this.state.DiscountedPrice > 0?
                <View style={{alignSelf:'stretch', marginTop:15,height:60, justifyContent: 'space-between',flexDirection: 'row', alignItems: 'center', marginRight:5, overflow: 'hidden', backgroundColor: 'white', borderRadius: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
              <View style={{flexDirection: 'row',alignItems: 'center'}}>
              <FastImage resizeMode="contain" style = {{width: 30, height: 30,marginLeft:20}} source = {require('../../../../assets/ticketIcon.png')}/>

             <Text style={{
               color: 'black',
               fontSize: wp('4%'),
               marginLeft: 10,
               fontFamily: "RubikBold"
             }}>{this.state.PromoCodeName}</Text>
             <Text style={{
               color: 'black',
               fontSize: wp('4%'),
               fontFamily: "RubikRegular"
             }}> APPLIED</Text>
              <FastImage resizeMode="contain" style = {{width: 20, height: 20,marginLeft:20}} source = {require('../../../../assets/rightIcon.png')}/>
              </View>
              <TouchableOpacity onPress={async ()=>{this.setState({DiscountedPrice:0,PromoCodeName:'',customcoupon:'',indexoFHide:"",open:false, couponDiscount:'', coupon_type:'', discount_mode:'' });
                await AsyncStorage.removeItem('couponDetails');
              }} 
              style={{margin: 10,borderColor: 'black',borderWidth: 1,borderRadius:20,justifyContent: 'center',alignItems: 'center'}}>
              <Image resizeMode="contain" style = {{tintColor:"black",width: 10, height:10,alignSelf:"flex-end",margin:5}} source = {require('../../../../assets/closeWhite.png')}/>
              </TouchableOpacity>
              </View>:
              <View style={{alignSelf:'stretch'}}>
                <TouchableOpacity disabled={this.state.myCartData.length?false:true} onPress={()=>{this.setState({ ModalVisibleStatus: true, totalPrice:(totalPrice-gst).toFixed(2) })}} style={{marginTop:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,borderRadius:5, }}>
                <LinearGradient style={{ height: 60, borderRadius: 10, flexDirection:'row',alignSelf:'stretch', alignItems: 'center', justifyContent:'center',  }}
                            colors={["#ff619e", "#ff9fa6"]}>
                          <FastImage resizeMode="contain" style = {{marginRight:5,width:23,  height:20,}} 
                          source = {require('../../../../assets/ic_home/coupon.png')}/>
                          <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                            Apply Promo Code/ Voucher
                          </Text>
                  </LinearGradient>
                </TouchableOpacity>
                <View style={{backgroundColor:'#dedbdb', alignSelf:'stretch', padding:15, marginTop:10, borderRadius:10}}>
                  <View style={{flexDirection:'row', alignSelf:'stretch', justifyContent:'center', alignItems:'center'}}>
                          <FastImage resizeMode="contain" style = {{marginRight:5,width:25,  height:25,}} 
                          source = {require('../../../../assets/ic_home/emp_coupon.png')}/>
                          <Text style={{fontFamily:'RubikMedium', fontSize:15, }}>Use Employee ID Code</Text>
                  </View>
                  <View style={{flexDirection:'row', alignSelf:'stretch',backgroundColor:'white', borderRadius:20,marginHorizontal:5,marginTop:10, height:40, justifyContent:'center', alignItems:'center', paddingRight:10}}>
                          <TextInput
                          placeholder='Enter Code'
                          onChangeText={value => this.setState({employeeId : value})}
                          style={{flex:1,borderRadius:20,paddingHorizontal:10,fontFamily: "RubikRegular",alignSelf:'stretch',fontSize: 15, textAlign:'center'}}
                          value={ this.state.employeeId }
                          />
                          {/* <TouchableOpacity>
                            <Text style={{fontFamily:'RubikBold', fontSize:15,color:'black'}}>APPLY</Text>
                          </TouchableOpacity> */}
                  </View>
                  
                </View>
              </View>
              }

              {/* Payment Summary */}
              <Text style={{fontFamily:'RubikMedium', fontSize:20, marginTop:20, color:'#212123'}}>Payment Summary</Text>
              
              <View style={{padding:10,alignSelf:'stretch', borderRadius:10, backgroundColor:'white', alignItems:'stretch', marginTop:10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                
                <View style={{flexDirection:'row', paddingHorizontal:2.5}}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {(totalPrice-gst).toFixed(2)+''.replace(/^0+/, '')} </Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#796dec',flex:1}}>Discount</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#796dec', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>{this.state.DiscountedPrice?'-':''} ₹ {finalDiscount}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Sub Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {(totalPrice - gst - finalDiscount)?(totalPrice - gst - finalDiscount).toFixed(2):'0.00'}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Estimated GST</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {gst?(gst.toFixed(2)+'').replace(/^0+/, ''):'0.00'}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Handling Charges</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>Free</Text>
                </View>
                <View style={{height:0.5, backgroundColor:'rgba(0,0,0,0.4)', }}></View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5,  }}>
                  <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#212123',flex:1, textAlignVertical:'center'}}>Total Payable Amount</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.7,textAlign:'right',}}>₹ {totalPriceNew?totalPriceNew:'0.00'}</Text>
                </View>
              </View>
                </View>
                :null}
                {/* <View style={{marginTop:15, borderRadius:10, height:120, alignSelf:'stretch', backgroundColor:'white', alignItems:'stretch', justifyContent:'center'}}>
                  <FastImage resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute', borderRadius:10}} 
                  source = {require('../../../../assets/banner0.jpg')}/>
                  <Text style={{fontFamily:'RubikBold', color:'white', fontSize:17,textAlign:'center'}}>YOU MAY ALSO BUY ACCESSORIES</Text>
                  <TouchableOpacity style={{alignSelf:'stretch', marginTop:10}} onPress={()=>this.props.navigation.navigate('AccessoriesHomeScreen')}>
                    <Text style={{fontFamily:'RubikMedium', color:'white', fontSize:17,textAlign:'center', textDecorationLine:'underline'}} >VIEW ALL</Text>
                  </TouchableOpacity>
                </View> */}
              </View>               
             
              
            </SafeAreaView>
        </ScrollView>
        {!this.state.ShowAlert ?null:<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
        <ActivityIndicator color={"#6464e7"} size={"large"}/>
        </View>}

         <View style={{alignSelf:'stretch', flexDirection: 'row', alignItems:'center',   position: 'absolute',bottom: 0,padding: 5,paddingTop:0, backgroundColor: '#edf4f6'}}>


               <View style={{flex:1, justifyContent: 'center',  height: 60,   backgroundColor: 'transparent', borderRadius: 10}}>
               <Text style={{color: '#212123',fontSize: 20,fontFamily: "RubikBold",paddingLeft:5 }}> ₹ {totalPriceNew?totalPriceNew:'0.00'} </Text>

               <Text style={{color: '#212123',fontSize: 15,fontFamily: "RubikRegular",paddingLeft:5}}> Total Amount </Text>
                </View>
                 <TouchableOpacity disabled={this.state.myCartData.length?false:true} onPress={()=> this.goToNextScreen(totalPriceNew,this.state.DiscountedPrice,gst,)} style={{marginTop:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, flex:1,  alignSelf:'flex-start'}}>
                <LinearGradient style={{  borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center' , padding:10}}
                            colors={["#ff619e", "#ff9fa6"]}>
                          <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                            Continue
                          </Text>
                  </LinearGradient>
                </TouchableOpacity>
        </View> 



            <Modal animationType = {"slide"} transparent = {false}
              visible = {this.state.ModalVisibleStatus}
              onRequestClose = {() => { this.setState({ ModalVisibleStatus: false ,indexoFHide:"",open:false}) } }>
              <ImageBackground  resizeMode="cover" style = {{width: '100%', height:'100%', }} source = {require('../../../../assets/bgCoupon.png')}>
              <View style={{flex:1,alignItems: 'center'}}>
              <View style={styles.header}>

                 <View style={{ width:'100%', height: wp('10.66%'), marginTop: wp('13.33%'),marginBottom:  wp('7.33%'),  backgroundColor:'transparent', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={styles.txtTitle}> APPLY COUPON </Text>
                  <TouchableOpacity onPress={()=>this.setState({ ModalVisibleStatus: false,indexoFHide:"",open:false })} style={styles.backTouchable}>
                  <FastImage resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/closeWhite.png')}/>
                  </TouchableOpacity>
                  </View>
              </View>
              <View style={[styles.whiteBg,{marginBottom: 10}]}>
                <TextInput
                  placeholder='Enter Coupon Code'
                  onChangeText={customcoupon => this.setState({customcoupon : customcoupon})}
                  style={{margin: 5,fontFamily: "RubikRegular",width: wp("70%"),height: hp("10%"),fontSize: wp("4%")}}
                  value={ this.state.customcoupon }
                  />
                  <TouchableOpacity onPress={()=>{this.validateCoupon();}} style={{shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,fontFamily: "RubikRegular",justifyContent: 'center',alignItems: 'center',width: wp("20%"),height: hp("10%")}}><Text
                    style={{shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,textAlign: 'center',fontFamily: "RubikRegular",color: "black",fontSize: wp("4%")}}>Apply</Text></TouchableOpacity>
              </View>
              <FlatList
              keyExtractor={(item, index) => index.toString()}
              extraData={this.state}
              data={this.state.CouponCodes}

                renderItem={({item, index}) =>
                 {

                return (

                  <View style={[{overflow: 'hidden',margin: 10,
                    // height: index == this.state.indexoFHide && this.state.open == true ?hp("30%"):hp("10%"),
                    width: wp("90%"),
                    borderRadius:10,backgroundColor: "white"}]}>
                    <View style={[styles.whiteBg,{overflow: 'hidden'}]}>
                  <ImageBackground   style = {{width: wp("25%"), height:'100%', justifyContent:'center',alignItems:'center'}} source = {require('../../../../assets/s.png')}>
                  <Text
                    numberOfLines={2}
                    style={{shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,alignSelf: 'center',textAlign: 'center',fontFamily: "RubikBold",color: "black",fontSize: wp("3.7%")}}>USE CODE {item.coupon_code}</Text>
                  </ImageBackground>
                  <View style={{width:wp("62%"),alignItems: 'center', justifyContent:'space-between',flexDirection: 'row',}}>
                      <Text style={{shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,alignSelf: 'center',textAlign: 'center',fontFamily: "RubikBold",color: "black",fontSize: wp("3.7%")}}>SAVE   {item.discount_mode=='percentage'?(item.discount+' %'):('₹ '+item.discount)}</Text>

                      {/* <TouchableOpacity onPress={()=>{
                        if(this.state.open == true){
                          this.setState({indexoFHide:index,open:false})
                        }
                        else{
                          this.setState({indexoFHide:index,open:true})
                        }

                        //this.setState({indexoFHide:index,open:true})
                        }}>
                        <FastImage resizeMode="contain"  style = {{tintColor:'black',width: wp("3%"), height:hp("3%"),margin:20}}
                        source = { index == this.state.indexoFHide &&  this.state.open == true?require('../../../../assets/arrowup.png'):require('../../../../assets/arrowblack.png')}/></TouchableOpacity> */}
                  <TouchableOpacity onPress={()=> { this.setState({customcoupon:item.coupon_code},()=>this.validateCoupon()) }} 
                  style={{  justifyContent: 'center', alignItems: 'center',  overflow: 'hidden', backgroundColor: 'transparent', 
                  borderRadius: 10,alignSelf: 'flex-end',margin: 10}}>
                    <FastImage resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}}
                      source={require('../../../../assets/button.png')}/>
                    <Text numberOfLines={1} style={{color: '#ffffff',fontSize: wp('3%'),fontFamily: "RubikBold",margin:10}}>APPLY</Text>

                   </TouchableOpacity>
                  </View>
                </View>
                <View>
                {index == this.state.indexoFHide && this.state.open == true ?
                  <View style={{justifyContent: 'space-between',flexDirection: 'row'}}>
                    <Text
                      numberOfLines={2}
                      style={{shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,margin:10,alignSelf: 'center',textAlign: 'center',fontFamily: "RubikBold",color: "black",fontSize: wp("3.7%")}}>₹ {item.discount} off</Text>
                  <TouchableOpacity onPress={()=> { this.setState({customcoupon:item.coupon_code},()=>this.validateCoupon()) }} style={{width: '20%',  justifyContent: 'center', alignItems: 'center',  overflow: 'hidden', backgroundColor: 'transparent', borderRadius: 10,margin: 2,alignSelf: 'flex-end',margin: 10}}>
                  <FastImage resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}}
                     source={require('../../../../assets/button.png')}/>
                  <Text numberOfLines={1} style={{color: '#ffffff',fontSize: wp('3%'),fontFamily: "RubikBold",margin:10}}>APPLY</Text>

                   </TouchableOpacity>
                  </View>
                  :null}
                  <View>
                    <Text style={{color: 'black',fontSize: wp('4%'),fontFamily: "RubikRegular",margin: 10}}>{item.description.replace(/<\/*\w*>/g,'').replace(/&nbsp;/g,' ')}</Text>
                  </View>
            </View>
              </View>)}}/>
              </View>
              </ImageBackground>

           </Modal>

           </View>




    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   whiteBg:{width: wp("90%"),backgroundColor: "white",height: hp("10%"),borderRadius: 10,justifyContent: 'space-between',alignItems: 'center',flexDirection: 'row',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5},
   header: {
     alignSelf:'stretch',
     backgroundColor: 'transparent'
   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('4.50%'),
      height: wp('4.50%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: '#e9eaff',
        fontSize: 18,
        fontFamily: "RubikMedium"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      }



 })
