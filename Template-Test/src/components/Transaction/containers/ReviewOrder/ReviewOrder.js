import React from 'react';
import {StyleSheet,Text,View,TouchableOpacity,Image,BackHandler,ScrollView,SafeAreaView,ActivityIndicator,StatusBar,Platform,FlatList,Modal, AsyncStorage} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {DrawerActions} from 'react-navigation';
import axios from 'axios';
import FastImage from 'react-native-fast-image';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import RazorpayCheckout from 'react-native-razorpay';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import LinearGradient from 'react-native-linear-gradient';

type Props = {
  navigate: PropTypes.func.isRequired
};
export default class ReviewOrder extends React.Component {
  constructor() {
    super()
    this.state = {
      highlights: [],
      itemCount: 0,
      promocode: '',
      myCartData: [],
      successModal:false,
      failModal:false,
      refreshing: false,
      paymentError:'',
      orderDetails:[],
      Indicator:false,
      transactionId:'',
      PromoCodeName:'',
      orderNo:'',DiscountedPrice:0.0, couponeCode:'', couponDiscount:'', SelectedAddress:[], SelectedAdressID:'', employeeId:'', gst:'',totalPriceNew:'', dataChanged:false, discount_mode:''
    }
  }

  handleBackPress = ()=>{
    this.props.navigation.navigate('AccessoriesHomeScreen');
    return true;
  }

async componentDidMount() {
  
  let couponDetails = await AsyncStorage.getItem('couponDetails');
  couponDetails = JSON.parse(couponDetails);
  if (couponDetails) {
    let params = this.props.navigation.state.params;
      this.setState({
        PromoCodeName: couponDetails.customcoupon,
        DiscountedPrice: couponDetails.DiscountedPrice,
        couponCode: couponDetails.customcoupon,
        discount_mode: couponDetails.discount_mode,
        couponDiscount: couponDetails.couponDiscount,
        SelectedAdressID: params.SelectedAdressID,
        SelectedAddress: params.SelectedAddress,
        employeeId:params.employeeId,
        });
  }
  this.getCartData();
  // this.getCoupons();
  

    //
  }
  
  async getCartData(){
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
     const accessToken = getConfiguration('accessToken');
     const customer_id = getConfiguration('customer_id');
     axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerCart?access_token="+accessToken+"&customer_id="+customer_id)
          .then(response => {
            // this.state.myCartData.push(response.data.data)
            this.setState({myCartData:response.data.data,ShowAlert:false})
          })
          .catch((error) => {
            NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          })
            this.setState({ShowAlert:false})
          });
  }

  async getCoupons(){
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
     const accessToken = getConfiguration('accessToken');
     const customer_id = getConfiguration('customer_id');
     
     axios.get("https://www.ecarewiz.com/ewarrantyapi/getCoupons?access_token="+accessToken+"&customer_id="+customer_id)
     .then(response => {
      //  console.warn(response.data.coupons)
       this.setState({CouponCodes:response.data.coupons})
     })
     .catch((error) => {
      NetInfo.fetch().then(async (state)=>{  
      if(!state.isConnected){
          alert('Please check your internet connection.');
        }else{
          alert(error);
        }
      })
     })
  }


  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  async goBack() {
    // let data =await AsyncStorage.getItem('dataChanged');
    // console.warn(data);
    this.props.navigation.pop();
  }

  onSubtract(index, item) {
    if (parseInt(item.qty) > 1) {
      --item.qty;
      this.setState({
        refresh: !this.state.refresh
      });
    }

  };

  onAdd(index, item) {
    ++item.qty;
    this.setState({
      refresh: !this.state.refresh
    });
  };

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

async payment_success(razorpay_payment_id="") {


    var bodyFormData = new FormData();
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    bodyFormData.append('customer_id', customer_id);
    // bodyFormData.append('payment_type', "online-payment");
    bodyFormData.append('payment_type', 'cod');
    bodyFormData.append('order_id', this.state.orderDetails[0].order_id);
    bodyFormData.append('razorpay_order_id', razorpay_payment_id);
    //bodyFormData.append('subtotal',this.props.navigation.state.params.totalPriceNew);
    bodyFormData.append('razorpay_payment_id', razorpay_payment_id);
    axios.post("https://www.ecarewiz.com/ewarrantyapi/payment_success", bodyFormData, {
      headers: {
        "Authorization": `Bearer ${accessToken}`
      },
      'content-type': `multipart/form-data;`
    }).then((response) => {
      console.warn(response);
      this.setState({Indicator: false, successModal:true,orderNo:response.data.order_no,failModal:false, transactionId:razorpay_payment_id});
      // alert(response.data.message + ".\norder id " + razorpay_payment_id)
      // this.props.navigation.navigate('ReviewOrder',{ProductInfo:this.props.navigation.state.params.ProductInfo,orderDetail:response.data.data,totalPriceNew:this.props.navigation.state.params.totalPriceNew,DiscountedPrice:this.props.navigation.state.params.DiscountedPrice})
    }).catch((response) => {
      this.setState({Indicator: false})
      // alert(response.message);
    })
  }

async SendtoServerwithAddress(totalPriceNew,DiscountedPrice,gst){
    const {params} = this.props.navigation.state;
    this.setState({Indicator:true})
    // let totalQuantity = 0;
    // let totalPrice = 0;
    // let gst = 0;
    // if (this.state.myCartData) {
    //   this.state.myCartData.forEach(item => {

    //     totalQuantity += item.price;

    //     totalPrice += item.qty * Math.trunc(item.price);

    //   });
    // }

    var coupon = {"coupon_code":"","coupon_discount":""};
    let shipping = {
      "address_type":params.SelectedAddress[0].address_type,
      "country":"INDIA",
      "state":  params.SelectedAddress[0].state_name ,
      "city":params.SelectedAddress[0].city_name,
      "area":params.SelectedAddress[0].area_name,
      "pincode":params.SelectedAddress[0].pincode,
      "landmark":params.SelectedAddress[0].near_by,
      "address":params.SelectedAddress[0].address
    }
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 


    var bodyFormData = new FormData();
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    bodyFormData.append('customer_id', customer_id);
    bodyFormData.append('order_type', "accessories");
    bodyFormData.append('currency', "INR");

    bodyFormData.append('subtotal',totalPriceNew + DiscountedPrice);
    bodyFormData.append('grand_total',totalPriceNew);
    bodyFormData.append('tax', gst);
    bodyFormData.append('discount',DiscountedPrice);

    bodyFormData.append('shipping_charge', "0.0");
    bodyFormData.append( "coupon",JSON.stringify(this.state.couponCode?{"coupon_code":this.state.couponCode?this.state.couponCode:'',
    "coupon_discount":this.state.couponDiscount?this.state.couponDiscount:""}:{}));
    bodyFormData.append("shipping", JSON.stringify(shipping));
    bodyFormData.append('customer_address_id', this.state.SelectedAdressID);
    bodyFormData.append('employee_code', this.state.employeeId)
    // console.warn(bodyFormData);
    this.send(bodyFormData,accessToken,customer_id);
  }

  
  async  send(bodyFormData,accessToken,customer_id){
    await  axios.post("https://www.ecarewiz.com/ewarrantyapi/save_order", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
  ).then(async (response)=>{
    this.setState({Indicator:true,orderDetails:[response.data.data]});
    await AsyncStorage.removeItem('dataChanged');
    this.payment_success();
    // var options = {
    //   description: 'Credits towards consultation',
    //   image: 'https://i.imgur.com/3g7nmJC.png',
    //   currency: 'INR',
    //   key: 'rzp_test_cZHLesapF55rTE',
    //   amount: totalPriceNew * 100,
    //   name: 'Ecommerce',
    //   prefill: {
    //     email: 'void@razorpay.com',
    //     contact: '9191919191',
    //     name: 'Razorpay Software'
    //   },
    //   theme: {
    //     color: '#F37254'
    //   }
    // }
    // RazorpayCheckout.open(options).then((data) => {
    //   this.payment_success(data.razorpay_payment_id)
    // }).catch((error) => {
    //   this.setState({Indicator:false, failModal:true, successModal:false})
    //   NetInfo.fetch().then(async (state)=>{
    //   if(!state.isConnected){
    //     alert('Please check your internet connection.');
    //   }else{
    //     this.setState({paymentError:error.description})
    //   }
    // });
    // });
  }).catch((response)=>{
    this.setState({Indicator:false})
    alert(response.message)

  });
}

async  cartCountUpdate(item,type){
  var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
  const accessToken = getConfiguration('accessToken');
  const customer_id = getConfiguration('customer_id');
          var bodyFormData = new FormData();
          bodyFormData.append('cart_id', item.cart_id);
          methodName = "";
          if(type == "update"){
            bodyFormData.append('qty', item.qty);
            methodName = "updateCart";
          }else{
            methodName = "deleteCart";
          }


       await axios.post("https://www.ecarewiz.com/ewarrantyapi/"+methodName, bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`} }
      ).then((response)=>{
        if(type == "delete"){
           axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerCart?access_token="+accessToken+"&customer_id="+customer_id)
                .then(response => {
                  // this.state.myCartData.push(response.data.data)
                  this.setState({myCartData:response.data.data,ShowAlert:false,dataChanged:true },async ()=>{
                    await AsyncStorage.setItem('dataChanged', this.state.dataChanged+'');
                  });
                })
                .catch((error) => {
                  NetInfo.fetch().then(async (state)=>{
                    if(!state.isConnected){
                      alert('Please check your internet connection.');
                    }else{
                      alert(error);
                    }
                  })
                });
          this.setState({
            refresh: !this.state.refresh
          });
        }
      // console.warn(response);
      }).catch((response)=>{
      // console.warn(response);
      })
  }


  render() {
    const {TYPE} = this.state;
    let totalQuantity = 0;
    let totalPrice = 0;
    let gst = 0;
    let itemCount = this.state.myCartData.length;
    let itemDiscount = this.state.discount_mode=='percentage'?this.state.DiscountedPrice:(this.state.DiscountedPrice && this.state.myCartData.length?this.state.DiscountedPrice/itemCount:0);
    let   itemPrice = 0;
    let finalDiscount = 0.0;
    let totalPercentDiscount = 0.0 ;
    let totalPriceNew = 0.0;
    if (this.state.myCartData){
      this.state.myCartData.forEach((item,index) => {
        itemCount=index+1;
        let itemGst = 0;
        let itemTotalPrice = 0;
        let itemTotalDp = 0;
        let item_price = parseFloat(parseInt(item.special_price) > 0 ?item.special_price:item.price);
        itemTotalPrice = parseFloat(parseInt(item.qty )* item_price);
        totalPrice += itemTotalPrice;
        totalPercentDiscount += (this.state.discount_mode=='percentage'?itemTotalPrice*(itemDiscount/100):0);
        itemTotalDp = itemTotalPrice - (totalPercentDiscount?itemTotalPrice*(itemDiscount/100):itemDiscount);
        itemGst = parseFloat((itemTotalDp - (itemTotalDp * (100/(100+parseFloat(item.gst))))).toFixed(2));
        gst += parseFloat((itemTotalDp - (itemTotalDp * (100/(100+parseFloat(item.gst))))).toFixed(2));
      });
    }
    finalDiscount = (totalPercentDiscount?totalPercentDiscount:this.state.DiscountedPrice).toFixed(2);
    totalPrice = totalPrice.toFixed(2);
    totalPriceNew = totalPrice-(finalDiscount);

    return (
    <View style={styles.container}>
      <StatusBar translucent={!this.state.successModal}/>
      <ScrollView  style={{flex:1,alignSelf:'stretch', backgroundColor: 'transparent', marginBottom:60}}>
        <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>
           {/* header */}
           <View style={{height:160}}>
           <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: -740,
                  height: 900,
                  width: 900,
                  borderRadius: 450,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', padding: 10 }}>
                <TouchableOpacity onPress={()=>this.goBack()} style={{zIndex:1, height: 20, width: 20,}}>
                  <Image source={require('../../../../assets/backwhite.png')} style={{ height: '100%', width: '100%', }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: '#e9eaff', textAlign:'center',flex:1,alignSelf:'stretch',marginLeft:-20, fontFamily: 'RubikMedium' }}>REVIEW ORDER</Text>
              </View>
              <Image resizeMode="contain" style={{width: 40,height: 40, alignSelf:'center',marginTop:10}} source={require('../../../../assets/reviewOrder.png')}/>
              <Image resizeMode="contain" style={{width: 90, alignSelf:'center', height:8, marginTop:15}} source={require('../../../../assets/progressReview.png')}/>
           </View>
                {/* Address */}
           <View style={{flex:1, alignSelf:'stretch', justifyContent:'center', backgroundColor:'transparent', padding:5, alignItems:'stretch', margin:10, marginBottom:0}}>
               {this.state.SelectedAddress.length? <View style={{backgroundColor:'white', borderRadius:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, padding:15}}>
                    <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#555757'}}>{this.state.SelectedAddress[0].address_type.toUpperCase()}</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#555757'}}>{this.state.SelectedAddress[0].state_name}</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#555757'}}>{this.state.SelectedAddress[0].city_name}, {this.state.SelectedAddress[0].pincode}</Text>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#555757'}}>{this.state.SelectedAddress[0].address}, {this.state.SelectedAddress[0].area_name} {this.state.SelectedAddress[0].near_by != "" ? ", "+this.state.SelectedAddress[0].near_by:null}</Text>
                </View>:null}
           </View>

           {/* items */}
           
           <View style={{flex:1, alignSelf:'stretch',height: 'auto',alignItems: 'center',  marginTop: 10, paddingHorizontal:15,marginBottom:30 }}>
               {!this.state.ShowAlert ?
              <View style={{flex:1, alignSelf:'stretch', }}>


              {/* Main list of cart */}
               <FlatList
               keyExtractor={(item, index) => index.toString()}
               extraData={this.state}
               data={this.state.myCartData}
               refreshing={this.state.refreshing}
               contentContainerStyle={{paddingBottom:5}}
                renderItem={({item, index}) =>
                {
                return (

                <View style={{alignSelf:'stretch',flexDirection:'row', height: 150, borderRadius:10, overflow: 'hidden', marginTop: 10, backgroundColor:'white', padding:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, margin:2.5}}>
                  {/* Left -> product details */}
                  <View style={{backgroundColor:'transparent', flex:1, alignItems:'flex-start',justifyContent:'center', padding:5}}>
                    <View>
                      <Text style={{fontFamily:'RubikBold', fontSize:13, color:'#3875c9'}}>{item.product_name.toUpperCase()}</Text>
                      <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#4f4f4f', marginTop:8}}>{item.item_type=='amc'?item.sub_product_name:item.item_name}</Text>
                      {item.item_type=='amc'?<Text style={{fontFamily:'RubikRegular', fontSize:13, color:'#4f4f4f', marginTop:8}}>Model No: {item.model_name}</Text>:null}
                      <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#000', marginTop:12,}}>
                          ₹ {item.price}
                        </Text>
                        {parseInt(item.plan_amount)>0 && parseInt(item.plan_amount)!=parseInt(item.price)?
                          <Text style={{paddingLeft:10,fontFamily:'RubikRegular', fontSize:13, color:'#796dec', marginTop:12, textDecorationLine:'line-through'}}>
                            ₹ {item.plan_amount}
                          </Text>:null}
                      </View>
                    
                    </View>
                  </View>
                  {/* delete item button */}
                  <TouchableOpacity onPress={()=>{this.cartCountUpdate(item, "delete")}} style={{alignItems: 'center',justifyContent: 'center',padding: 5}}><FastImage resizeMode="contain" style = {{width: wp('5%'), height:wp('5%')}}
                    source = {require('../../../../assets/delete.png')}/>
                  </TouchableOpacity>
                  {/* image icon */}
                  <View style={{backgroundColor:'white',borderWidth:2, borderColor:'#f0f0f0', height:120, width:130,borderRadius:5, alignItems:'center', justifyContent:'center', padding:5}}>
                    {
                      item.item_type=='amc'?
                      <View style={{alignItems:'center', justifyContent:'center', }}>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>{item.plan.indexOf('FirstYearPlan')!=-1?1:item.plan.indexOf('SecondYearPlan')!=-1?2:3}</Text>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>Year</Text>
                        <Text style={{color:'#3e3d3d', fontFamily:'RubikBold', fontSize:15}}>Warranty</Text>
                      </View>:
                      item.image?
                      <Image style = {{height:'100%',width:'100%', resizeMode:'center'}} 
                      source = {item.image == ""?require('../../../../assets/image4.png'):{uri:item.image}}/>:null
                    }
                    {
                      item.item_type=='amc'?
                      <Image  style = {{position:"absolute",width: 25, height:25,right:-5,top:-5, resizeMode:'center'}} 
                        source = {require('../../../../assets/amc.png')}/>:null
                        }
                  </View>
                  
                </View>)
              }}/>
              {/* Apply promo button */}
              { 
               finalDiscount > 0?
                <View style={{alignSelf:'stretch', marginTop:15,height:60, justifyContent: 'space-between',flexDirection: 'row', alignItems: 'center', marginRight:5, overflow: 'hidden', backgroundColor: 'white', borderRadius: 10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
              <View style={{flexDirection: 'row',alignItems: 'center'}}>
              <FastImage resizeMode="contain" style = {{width: 30, height: 30,marginLeft:20}} source = {require('../../../../assets/ticketIcon.png')}/>

             <Text style={{
               color: 'black',
               fontSize: wp('4%'),
               marginLeft: 10,
               fontFamily: "RubikBold"
             }}>{this.state.PromoCodeName}</Text>
             <Text style={{
               color: 'black',
               fontSize: wp('4%'),
               fontFamily: "RubikRegular"
             }}> APPLIED</Text>
              <FastImage resizeMode="contain" style = {{width: 20, height: 20,marginLeft:20}} source = {require('../../../../assets/rightIcon.png')}/>
              </View>
              <TouchableOpacity onPress={async ()=>{ this.setState({DiscountedPrice:'', couponeCode:'', couponDiscount:'',});
              await AsyncStorage.removeItem('couponDetails')}} style={{margin: 10,borderColor: 'black',borderWidth: 1,borderRadius:20,justifyContent: 'center',alignItems: 'center'}}>
              <Image resizeMode="contain" style = {{tintColor:"black",width: 10, height:10,alignSelf:"flex-end",margin:5}} source = {require('../../../../assets/closeWhite.png')}/>
              </TouchableOpacity>
              </View>:null
              }

              {/* Payment Summary */}
              <Text style={{fontFamily:'RubikMedium', fontSize:20, marginTop:20, color:'#212123'}}>Payment Summary</Text>
              
              <View style={{padding:10,alignSelf:'stretch', borderRadius:10, backgroundColor:'white', alignItems:'stretch', marginTop:10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                
                <View style={{flexDirection:'row', paddingHorizontal:2.5}}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {(totalPrice-gst).toFixed(2)+''.replace(/^0+/, '')} </Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#796dec',flex:1}}>Discount</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#796dec', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>{this.state.DiscountedPrice?'-':''} ₹ {finalDiscount}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Sub Total</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {(totalPrice - gst - finalDiscount)?(totalPrice - gst - finalDiscount).toFixed(2):'0.00'}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Estimated GST</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>₹ {(gst.toFixed(2)+'').replace(/^0+/, '')}</Text>
                </View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5 }}>
                  <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#212123',flex:1}}>Handling Charges</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.8,textAlign:'right'}}>Free</Text>
                </View>
                <View style={{height:0.5, backgroundColor:'rgba(0,0,0,0.4)', }}></View>
                <View style={{flexDirection:'row',marginVertical:5, paddingHorizontal:2.5,  }}>
                  <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#212123',flex:1, textAlignVertical:'center'}}>Total Payable Amount</Text>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#212123', alignSelf:'flex-end',flex:0.7,textAlign:'right',}}>₹ {totalPriceNew}</Text>
                </View>
              </View>
                </View>
                :null}
              </View>               
     </SafeAreaView>
     </ScrollView>

     <View style={{alignSelf:'stretch', flexDirection: 'row', alignItems:'center',   position: 'absolute',bottom: 0,padding: 5,paddingTop:0, backgroundColor: '#edf4f6'}}>
      <View style={{flex:1, justifyContent: 'center',  height: 60,   backgroundColor: 'transparent', borderRadius: 10}}>
        <Text style={{color: '#212123',fontSize: 20,fontFamily: "RubikBold",paddingLeft:5 }}> ₹ {totalPriceNew} </Text>
        <Text style={{color: '#212123',fontSize: 15,fontFamily: "RubikRegular",paddingLeft:5}}> Total Amount </Text>
      </View>
        <TouchableOpacity onPress={()=> {this.state.myCartData.length?this.SendtoServerwithAddress(totalPriceNew,finalDiscount,gst):this.props.navigation.navigate('AccessoriesHomeScreen');}} style={{marginTop:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, flex:1, alignSelf:'flex-start'}}>
        <LinearGradient style={{borderRadius: 10, flexDirection:'row',alignSelf:'stretch', alignItems: 'center', justifyContent:'center' ,padding:10}}
                    colors={["#ff619e", "#ff9fa6"]}>
                  <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                    {this.state.myCartData.length?'Confirm Order':'Continue Shopping'}
                  </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
      {this.state.Indicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
        <ActivityIndicator color={"#6464e7"} size={"large"}/>
      </View>:null}
      <Modal animationType = {"slide"} transparent = {false} visible = {this.state.successModal || this.state.failModal} onRequestClose = {() => { this.setState({ ModalVisibleStatus: false ,indexoFHide:"",open:false}) } }>
              <LinearGradient style={{flex:1, justifyContent:'center', alignItems:'center'}} colors={["#5960e5", "#b785f8","#b785f8"]}  
                useAngle={true} 
                angle={45} 
                angleCenter={{x:0, y:0}}
                >
                  <View style={{justifyContent:'center', alignItems:'center', height:200, width:200}}>
                    <Image style={{resizeMode:'contain', height:'100%', width:'100%', position:'absolute'}} source={this.state.failModal?require('../../../../assets/ic_home/payment_failed.png'):require('../../../../assets/ic_home/finished_add_prod.png')} />
                  </View>
                  
                    <Text style={{fontFamily:'RubikBold', fontSize:35, color:'#e9eaff'}}>{this.state.failModal?'SORRY':'THANK YOU'} </Text>
                    <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#e9eaff',flexWrap:'wrap', margin:15, textAlign:'center'}}>Your order has been placed, our engineers will call you in 2-4 hours</Text>
                    {/* <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#e9eaff'}}>{this.state.failModal?this.state.paymentError?this.state.paymentError:'Payment has failed':'Payment has been done'}</Text> */}
                    {/* <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#e9eaff'}}>{this.state.failModal?"":'Successfully'}</Text> */}
                    {!this.state.failModal?
                    <View style={{alignItems:'stretch', margin:15}}>
                      <View style={{flexDirection:'row', }}>
                        <Text style={{fontFamily:'RubikRegular', color:'#e9eaff', fontSize:15}}>ORDER ID: </Text>
                        <Text style={{fontFamily:'RubikRegular', color:'#e9eaff', fontSize:15}}>{this.state.orderDetails.length?this.state.orderNo:''} </Text>
                      </View>
                      {/* <View style={{flexDirection:'row', }}>
                        <Text style={{fontFamily:'RubikRegular', color:'#e9eaff', fontSize:15}}>TRANSACTION ID: </Text>
                        <Text style={{fontFamily:'RubikRegular', color:'#e9eaff', fontSize:15}}>{this.state.transactionId?this.state.transactionId:''} </Text>
                      </View> */}
                    </View>:null
                  }
                  
                  <TouchableOpacity onPress={()=>this.props.navigation.navigate('AccessoriesHomeScreen')} style={{margin:30 , shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:10,  borderRadius:5, }}>
                  <LinearGradient style={{ borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center', padding:15 }}
                              colors={["#ff619e", "#ff9fa6"]}>
                            <Text style={{color: 'white',fontSize: 15,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                              Continue Shopping
                            </Text>
                    </LinearGradient>
                  </TouchableOpacity>
              </LinearGradient>
      </Modal>


    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edf4f6'
  },
  header: {
    width: '100%',
    height: wp('40%'),
    marginTop: -wp('13.33%'),
    backgroundColor: 'transparent'

  },
  innerView: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    position: 'relative',
    alignItems: 'center'
  },

  backIcon: {

    width: wp('5.86%'),
    height: wp('5.86%'),

    backgroundColor: 'transparent'
  },
  backTouchable: {

    position: 'absolute',
    width: wp('10.33%'),
    height: '100%',
    top: 0,
    left: wp('4%'),
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  txtTitle: {
    color: 'white',
    fontSize: wp('5.33%'),
    fontFamily: "RubikBold"
  },
  txtTitleDesc: {
    color: 'white',
    fontSize: wp('4%'),
    fontFamily: "RubikRegular"
  },
  txtUsername: {
    height: wp('10.66%'),
    marginLeft: wp('2.6%'),
    width: '70%',
    fontSize: wp('4.58%'),
    fontFamily: "RubikMedium"
  },
  tile: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 3,
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 'auto',
    backgroundColor: '#ffffff'
  },
  tileTitle: {
    color: '#000000',
    fontSize: wp('4.53%'),
    marginLeft: wp('2.6%'),
    fontFamily: "RubikLight"
  },
  tileValue: {
    color: '#000000',
    fontSize: wp('4.53%'),
    marginRight: wp('2.6%'),
    fontFamily: "RubikRegular"
  },
  listTile: {
    width: '100%',
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 1,
    shadowOpacity: 0.8,
    borderRadius: 10,
    marginTop: wp('2.6%'),
    backgroundColor: 'white'
  },purchase:{
      width: '100%',
      shadowColor: '#000000',
      shadowOffset: {
        width: 0,
        height: 0
      },
      shadowRadius: 1,
      shadowOpacity: 1.0,
      paddingVertical: wp('4%'),
      justifyContent: 'center',
      alignItems: 'center',
      height: 'auto',
      backgroundColor: '#ffffff',
      borderRadius: 10,
      marginTop: wp('5.33%')
    }

})
