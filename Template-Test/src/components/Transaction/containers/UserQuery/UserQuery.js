import React from 'react';
import { StyleSheet, Text, View, Modal,ImageBackground,Animated,ActivityIndicator, Platform,KeyboardAvoidingView ,TouchableOpacity, StatusBar, Image, TextInput, ScrollView, SafeAreaView, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import ModalDropdown from 'react-native-modal-dropdown';
import { setConfiguration, getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import * as authFun from '../../../../helpers/auth';
import LinearGradient from 'react-native-linear-gradient';
import {KeyboardAwareView} from 'react-native-keyboard-aware-view'



type Props = {
  navigate: PropTypes.func.isRequired
};

export default class UserQuery extends React.Component {
  constructor() {
    super()
    this.state = {
      highlights: [],Selected:"",
      itemCount: 0,
      promocode: '',
      myCartData: [],
      refreshing: false,
      switch1Value: false,
      selectedUserType: '',Indicator:false,
      user: "",
      dropdown_4_options: null,
      items: undefined,
      coupon: [],
      shipping: [],
      totalPrice: "",
      dropdown_4_defaultValue: 'loading...',
      SelectedType: "HOME",
      cityname: "",
      stateName: "",
      Phone: "",
      name: "",
      address: "",
      dropdown_6_icon_heart: true,ModalVisibleStatus:false,
      addressType: [
        {
          firstName: 'HOME'
        }, {
          firstName: 'OFFICE'
        }, {
          firstName: 'OTHER'
        }
      ],
      totalPrice: "",
      tempData: [
        {
          key: 0

        }
      ],textInputs: ["1"],
      Screenname: "",screen:"",out_of_warrenty :[],ENQUIRYProductWarrenty:[],comments:"",SuccessidOfWarrenty:"",CategoryName:[],SelectCategory:[]
    }
  }

async componentDidMount() {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 

    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    try {
      this.setState({screen: this.props.navigation.state.params.ScreenName})
      if (this.props.navigation.state.params.ScreenName == "ENQUIRY") {
        this.setState({Screenname: "ENQUIRE NOW"})
        this.setState({Indicator:true})

        axios.get('https://www.ecarewiz.com/ewarrantyapi/getProductGroup?'+"access_token="+accessToken+"&customer_id="+customer_id+"&brand_id=1").then((response)=>{
            this.setState({Indicator:false,CategoryName:response.data.data})
          //this.setState({out_of_warrenty:response.data.data})
          // console.warn(response.data);
        }).catch((error)=>{
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          })
        });
      } else {
        this.setState({Screenname: "OUT OF WARRANTY"})
        this.setState({Indicator:true})
        axios.get('https://www.ecarewiz.com/ewarrantyapi/getOutOfWarrantyCustomerProduct?'+"access_token="+accessToken+"&customer_id="+customer_id).then((response)=>{
            this.setState({Indicator:false})
          this.setState({out_of_warrenty:response.data.data})
          // console.warn(response.data);
        }).catch((error)=>{
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          });
        });

      }

    } catch (x) {}

  }



async  out_of_warrenty(){
    if(this.state.ENQUIRYProductWarrenty.length == 0){
        alert("Please choose warranty year of product's.");
        return;
    }
    if(this.state.comments == ""){
        alert("Please provide your comment.");
        return;
    }
    this.setState({Indicator:true})
    var bodyFormData = new FormData();
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');




    bodyFormData.append('customer_id', customer_id);
    bodyFormData.append('comment', this.state.comments);
    bodyFormData.append('enquiry_details',JSON.stringify(this.state.ENQUIRYProductWarrenty));

    //return
    axios.post("https://www.ecarewiz.com/ewarrantyapi/outOfWarrantyEnquiry", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
  ).then((response)=>{

    this.setState({SuccessidOfWarrenty:response.data.data})
    this.setState({ModalVisibleStatus:true})
  }).catch((response)=>{
    alert(response.message)
  }).finally(e=>{
    this.setState({Indicator:false})
  })

  }


async  enquirenow(){
  let flag=0;
    // console.warn("sz",this.state.SelectCategory.length);
    if(this.state.SelectCategory.length != 0 ){
      this.state.SelectCategory.forEach((item,index)=>{
        if(item == "" && index == this.state.SelectCategory.length-1){
          alert("Please choose your category.");
          flag=1;
        }
        if(parseInt(this.state.textInputs[index]) < 1){
          alert("Please enter quantity greater than zero.");
          flag=1;
        }
      })
    }
    else{
      alert("Please choose your category.");
      return;
    }
    if(flag){
      return;
    }
    if(this.state.comments == ""){
        alert("Please provide your comment.");
        return;
    }
    if(this.state.name == ""){
        alert("Please enter your name.");
        return;
    }
    if(this.state.Phone == ""){
        alert("Please enter your phone no.");
        return;
    }

    var product_info = [];
    this.state.SelectCategory.forEach((item,index)=>{
          product_info.push({"product_group_id": this.state.SelectCategory[index],"quantity": this.state.textInputs[index]})
      
    })
    if(product_info.length == 0){
      alert("Please choose your category.");
      return;
    }
    this.setState({Indicator:true})
    var bodyFormData = new FormData();
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');


    //var product_info =



    //return;
    bodyFormData.append('customer_id', customer_id);
    bodyFormData.append('comment', this.state.comments);
    bodyFormData.append('name', this.state.name);
    bodyFormData.append('mobile_no', this.state.Phone);
    bodyFormData.append('product_info',JSON.stringify(product_info));

    //return
    this.sendAPi(bodyFormData,accessToken)

  }
  sendAPi(bodyFormData,accessToken){
    axios.post("https://www.ecarewiz.com/ewarrantyapi/customerEnquiry", bodyFormData,{ headers: {"Authorization" : `Bearer ${accessToken}`,},'content-type': `multipart/form-data;` }
  ).then((response)=>{
    //console.warn(response.data.message);
    alert(
  response.data.message,
  [
    {text: 'OK', onPress: () => this.props.navigation.pop()},
  ],
  {cancelable: false},
);
      this.setState({Indicator:false})
    //this.setState({SuccessidOfWarrenty:response.data.data})
    //this.setState({ModalVisibleStatus:true})
  }).catch((response)=>{
    alert(response.message)
  }).finally(e=>{
    this.setState({Indicator:false})
  })
  }
  checkMultiple(item,index,SeletedYear){
    let enquiry_details = {
    "customer_product_id": item.customer_product_id,
    "warranty": SeletedYear
    }


    this.state.ENQUIRYProductWarrenty.forEach((value,index)=>{
        if(value.customer_product_id == item.customer_product_id){
          this.state.ENQUIRYProductWarrenty.splice(index, 1);
        }
    })
    this.state.ENQUIRYProductWarrenty.push(enquiry_details)
    this.setState({
      refreshing: !this.state.refreshing
    });

    //SelectedProductWarrenty
  }

  render() {


    // if(this.state.ModalVisibleStatus){
    //   setTimeout(() => {
    //     //this.props.navigation.pop();
    //       //this.setState({ModalVisibleStatus:false});
    //   }, 5000);
    // }
    const { textInputs,SelectCategory } = this.state;
    var CategoryName = [];
    // try{
    //     global.categoriesName.forEach((e)=>{
    //       CategoryName.push(e.product_type_name)
    //     })
    // }catch(x){
    //
    // }

    if(this.state.CategoryName.length == 0){
      CategoryName = [];
    }
    else{
      this.state.CategoryName.forEach((e)=>{
        CategoryName.push(e.product_name)
      })
    }
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
      <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>
      <View style={{ height: 100, backgroundColor:'transparent'}}>
      <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']}
               locations={[0.1, 0.5, 0.2, 0.2]}
              angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                backgroundColor: 'purple',
                alignSelf: 'center',
                top: -700,
                height: 800,
                width: 800,
                borderRadius: 400,
                position: 'absolute'
              }}
            >
            </LinearGradient>
            {/* <View style={{
              width: '100%',
              height: wp('10.66%'),
              marginTop: wp('13.33%'),
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center'
            }}> */}
            <View style={{
              justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'
              ,  marginLeft: 10
            }}>
            <TouchableOpacity onPress={() => this.props.navigation.pop()}
             style={{width:30, zIndex:1}}
             >
              <Image resizeMode="contain" style={{height:25, width:25}} source={require('../../../../assets/backwhite.png')}/>
            </TouchableOpacity>
            <Text 
            style={{alignSelf:'center',  flex:1, textAlign:'center', marginLeft:-30, fontFamily:'RubikBold', color:'white', fontSize:20}}
            >
              {this.state.Screenname}
            </Text>
            

          </View>
            </View>
        

        <View style={{
            width: '100%',
            height: 'auto',
            alignItems: 'center',
            marginTop: 20
          }}>
          {/* <Image resizeMode="cover" style={{
              width: '100%',
              height: wp('10.66%')
            }} source={require('../../../../assets/curve.png')}/> */}

          <Animated.ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{
            flexGrow: 1}}>
            <View style={{  height: "auto",
              width: wp("90%"),marginBottom: hp("18%"),backgroundColor: 'transparent',justifyContent: 'center',alignItems: 'center'}}>
           {( this.state.screen=="ENQUIRY" ) || (this.state.out_of_warrenty.length && this.state.screen!="ENQUIRY")? <View style={{width:'100%', flexDirection:'row',  marginHorizontal:7 }}>
              <Text style={{flex:1, fontFamily:'RubikRegular',paddingLeft:20, fontSize:15}}>Select Product</Text>
              <Text style={{flex:1, fontFamily:'RubikRegular',paddingLeft:5, fontSize:15,}}> Enter Quantity</Text>
            </View>:null}
            {
              this.state.screen == "ENQUIRY"
                ? <FlatList data={this.state.tempData} keyExtractor={(item) => item.key.toString()} extraData={this.state}
                  renderItem={({item,index}) =>{
                    if(this.state.tempData.length-1){if(textInputs[index] == undefined)
                    {textInputs.push("1")}
                    if(this.state.SelectCategory[index] == undefined){
                      SelectCategory.push("")
                    }}
                    return(
             <View style={{justifyContent: 'space-between',alignItems: 'center',flexDirection: 'column',height:index == this.state.tempData.length-1? hp("15%"):hp("7%")}}>
             <View style={{justifyContent: 'space-between',alignItems: 'center',flexDirection: 'row',margin: 7}}>

          <ModalDropdown
            defaultValue={"Select Category"}
            onSelect={(rowData,index) =>{
              SelectCategory.push(this.state.CategoryName[rowData].product_group_id);
            console.warn(this.state.CategoryName[rowData]);}}
          options={CategoryName}
          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
          style={{width: '48%', flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', paddingLeft:10,alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,marginRight: 5}}
          textStyle={{fontSize: wp("4%"),fontFamily: 'RubikRegular',}}
          dropdownStyle={{width: '60%',fontFamily: 'RubikRegular', marginTop:-20}}
            />
        <View style={{width: '48%',flexDirection: 'row', height: wp('10.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', alignItems:'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,marginLeft:2}}>


        <TextInput
           style={[styles.txtFld,{width: '90%',color:"#707171" ,textAlign: 'center', }]}
           value={"1"}
           placeholderTextColor= "#707171"
           maxLength = {2}
           numberOfLines={1}
           editable={true}

           onChangeText={(value) =>{
                  let num = value.replace(".", '');
                    if(isNaN(num)){
                        // Its not a number
                    }else{
                      textInputs[index] = value;
                      this.setState({
                        textInputs,
                      });
                    }}
                    }
                  value={this.state.textInputs[index]}
        />
        </View>
          </View>
        {index == this.state.tempData.length-1?<TouchableOpacity onPress={()=>{
          var indexx = item.key+1;
          this.setState(prevState => ({tempData: [...prevState.tempData, {"key": indexx}]}))
          }}
          style={{width: wp("85%"),height: hp("6%"),backgroundColor: "#d9dbdc",alignItems: 'center',alignSelf: 'center',borderRadius: 30,margin: 10,flexDirection: 'row',justifyContent: 'center',marginTop: hp("1%")}}>

        <Text style={{
          color: 'black',
          fontSize: wp('4%'),
          marginLeft: wp('2%'),
          fontFamily: "RubikRegular",textAlign: 'center'
        }}>+  Add More</Text></TouchableOpacity>:null}


      </View>)}
            }/>
                :  
              
              this.state.out_of_warrenty.length>0?
                <View style={{width: wp("90%"),backgroundColor: "white",height: "auto",borderRadius:10,overflow: 'hidden',marginTop: 10,}}>
                  <View style={{width: wp("90%"),backgroundColor: "white",height: "auto",flexDirection: 'row',justifyContent: 'center',alignItems: 'center'}}>
                    <View style={{width: wp("40%"),height: hp("6%"),}}></View>
                    <View style={{width: wp("17%"),height: hp("6%"),borderLeftWidth:  1,borderColor: "#f3f4f4",justifyContent: 'center',alignItems: 'center'}}>
                      <Text style={{fontFamily: "RubikBold",color: "#014d67",alignSelf: 'center'}}>1 YEAR</Text>
                    </View>
                    <View style={{width: wp("17%"),height: hp("6%"),borderLeftWidth:  1,borderColor: "#f3f4f4",justifyContent: 'center',alignItems: 'center'}}><Text style={{fontFamily: "RubikBold",color: "#014d67"}}>2 YEAR</Text></View>
                    <View style={{width: wp("17%"),height: hp("6%"),borderLeftWidth:  1,borderColor: "#f3f4f4",justifyContent: 'center',alignItems: 'center'}}><Text style={{fontFamily: "RubikBold",color: "#014d67"}}>3 YEAR</Text></View>
                  </View>
                  {this.state.out_of_warrenty.length<0?null:
                  <FlatList
                    data = {this.state.out_of_warrenty}
                    refreshing={this.state.refreshing}
                    extraData={this.state}
                      keyExtractor={item => item.model_name+''}
                    renderItem = {({item,index}) =>{
                    let year = "";
                    let customer_product_id = 0;

                return(
                  <View style={{width: wp("90%"),borderWidth: 1,borderColor: "#f3f4f4",backgroundColor: "white",height: "auto",flexDirection: 'row',justifyContent: 'center',alignItems: 'center',borderWidth:1,borderColor: "#f3f4f4"}}>
                    {this.state.ENQUIRYProductWarrenty.forEach((value,index)=>{
                        if(value.customer_product_id == item.customer_product_id){
                          year = value.warranty;
                          customer_product_id = value.customer_product_id;
                        }
                    })}
                    <View style={{width: wp("40%"),padding:5}}>
                      <View style={{width: wp("40%"),margin: 7,justifyContent: 'space-around'}}>
                        <Text style={{fontFamily: "RubikBold",fontSize: wp("4%"),color:"#3875c9",margin: 2}}>{item.product_name}</Text>
                          <Text style={{fontFamily: "RubikBold",fontSize: wp("4.3%"),color:"black",margin: 2}}>{item.sub_product_name}</Text>
                        <Text style={{fontFamily: "RubikRegular",fontSize: wp("4%"),color:"black",margin: 2}}>Model No: {item.model_name}</Text>
                      </View>
                    </View>
                    <TouchableOpacity onPress={() => { this.checkMultiple(item,index,"1")}} style={styles.yearSelection}>
                      <View   activeOpacity = {0.5} style={styles.yearView}>
                        <View style={styles.View}>
                          <View style={{backgroundColor:year == "1" &&  customer_product_id == item.customer_product_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => {this.checkMultiple(item,index,"2") }} style={styles.yearSelection}>
                      <View   activeOpacity = {0.5} style={styles.yearView}>

                        <View style={styles.View}>
                          <View style={{backgroundColor:year == "2" &&  customer_product_id == item.customer_product_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                        </View>
                      </View>
                    </TouchableOpacity>
                    <TouchableOpacity  onPress={() => { this.checkMultiple(item,index,"3") }} style={styles.yearSelection}>
                      <View  activeOpacity = {0.5} style={styles.yearView}>

                        <View style={styles.View}>
                          <View style={{backgroundColor:year == "3" &&  customer_product_id == item.customer_product_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                )}

             }
              />}
              </View>:
              <Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20, alignSelf:'center'}}>No data found.</Text>
            

            }




            {( this.state.screen=="ENQUIRY" ) || (this.state.out_of_warrenty.length && this.state.screen!="ENQUIRY")?<View style={{width:this.state.screen == "ENQUIRY"?wp("84%"):wp("89%"),alignSelf: 'center',borderRadius: wp('2.66%'),backgroundColor: 'white',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,margin:this.state.screen == "ENQUIRY"?10:10,justifyContent: 'center',alignItems: 'center'}}>

              <TextInput placeholder="COMMENTS" onChangeText={comments => this.setState({comments: comments})} numberOfLines={3}
                //editable={this.state.onSubmit}
                textAlignVertical={'top'} value={this.state.comments} placeholderTextColor="#707171" underlineColorAndroid='transparent'
                 style={{
                  fontFamily: 'RubikRegular',
                  color: '#707171',
                  fontSize: wp("4%"),
                  height: hp("10%"),
                  alignItems: 'flex-start',
                  justifyContent: 'flex-start',
                  width: '95%'
                }} multiline/>

            </View>:null}
            {
              this.state.screen == "ENQUIRY"
                ? <View style={{
                      width: '95%',
                      flexDirection: 'row',
                      height: wp('10.33%'),
                      margin: 7,
                      borderRadius: wp('6.66%'),
                      backgroundColor: 'white',
                      shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5
                    }}>

                    <TextInput style={[
                        styles.txtFld, {
                          width: '95%', paddingLeft:10
                        }
                      ]} placeholder="Name" placeholderTextColor="#707171" maxLength={100} 
                      onChangeText={(phone) => this.setState({name: phone})} value={this.state.name}/>

                  </View>
                : null
            }

            {
              this.state.screen == "ENQUIRY"
                ? <View style={{
                      width: '95%',
                      flexDirection: 'row',
                      height: wp('10.33%'),
                      margin: 7,
                      borderRadius: wp('6.66%'),
                      backgroundColor: 'white',
                      alignItems: 'center',
                      shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5
                    }}>

                    <TextInput style={[
                        styles.txtFld, {
                          width: '95%', paddingLeft:10 
                        }
                      ]} placeholder="Phone no." placeholderTextColor="#707171" maxLength={10} value={this.state.Phone} onChangeText={(value) => {
                        let num = value.replace(".", '');
                        if (isNaN(num)) {
                          // Its not a number
                        } else {
                          this.setState({Phone: value})
                        }
                      }
                    }/>

                  </View>
                : null
            }
          {( this.state.screen=="ENQUIRY" ) || (this.state.out_of_warrenty.length && this.state.screen!="ENQUIRY")?
            <View style={{width: wp("100%"),flexDirection: 'row',justifyContent: 'center',alignItems: 'center',height: 'auto',marginTop: 10}}>

              <TouchableOpacity onPress={() => this.props.navigation.pop()} style={{
                  width: '44%',
                  borderColor: '#3b75c9',
                  borderWidth: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: 2,
                  height: wp('11.33%'),
                  backgroundColor: 'transparent',
                  borderRadius: 10
                }}>
                <Text style={{
                    color: '#3b75c9',
                    fontSize: wp('3.50%'),

                    fontFamily: "RubikBold"
                  }}>
                  CANCEL
                </Text>

              </TouchableOpacity>
              <TouchableOpacity onPress={() =>this.state.screen == "ENQUIRY"?this.enquirenow():this.out_of_warrenty()} style={{
                  width: '44%',
                  height: wp('11.33%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                  overflow: 'hidden',
                  backgroundColor: 'transparent',
                  borderRadius: 10,
                  margin: 2
                }}>
                <Image resizeMode="cover" style={{
                    width: '100%',
                    height: '100%',
                    position: 'absolute'
                  }} source={require('../../../../assets/button.png')}/>

                <Text numberOfLines={1} style={{
                    color: '#ffffff',
                    fontSize: wp('3.50%'),

                    fontFamily: "RubikBold"
                  }}>
                  SEND REQUEST
                </Text>

              </TouchableOpacity>
            </View>
          :null}
            </View>
          </Animated.ScrollView>
        </View>


      </SafeAreaView>

      {this.state.Indicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
        <ActivityIndicator color={"#6464e7"} size={"large"}/>
      </View>:null}
      <Modal animationType = {"slide"} transparent = {false}
        visible = {this.state.ModalVisibleStatus}

        onRequestClose = {() => {this.setState({ ModalVisibleStatus: false }),this.props.navigation.pop()} }>
        <TouchableOpacity
            activeOpacity={1}
            onPressOut={() => {this.setState({ ModalVisibleStatus: false }),this.props.navigation.pop()}}
          ><ImageBackground  resizeMode="cover" style = {{width: '100%', height:'100%', }} source = {require('../../../../assets/bgCoupon.png')}>
        <View style={{alignItems: 'center',justifyContent: 'center',position: 'absolute',left:15,top:70,right: 0}}>
          <Image resizeMode="contain" style={{
              width: 200,
              height: 200,alignSelf: 'center'
            }} source={require('../../../../assets/Successorder.png')}/>

            <Text numberOfLines={1} style={{
                color: '#ffffff',
                fontSize: wp('8%'),

                fontFamily: "RubikBold"
              }}>
              YOUR TICKET NO
            </Text>
            <Text numberOfLines={1} style={{
                color: '#ffffff',
                fontSize: wp('7%'),

                fontFamily: "RubikBold"
              }}>
              {this.state.SuccessidOfWarrenty}
            </Text>
            <Text numberOfLines={1} style={{
                color: '#ffffff',
                fontSize: wp('6%'),
                marginTop: 10,
                fontFamily: "RubikRegular"
              }}>
              Our representative will contact
            </Text>
            <Text numberOfLines={1} style={{
                color: '#ffffff',
                fontSize: wp('6%'),
                marginTop: 10,
                fontFamily: "RubikRegular"
              }}>
              you shortly
            </Text>

        </View>
      </ImageBackground></TouchableOpacity>

     </Modal>

    </KeyboardAvoidingView>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#edf4f6'
  },
  header: {
    width: '100%',
    height: wp('30%'),
    marginTop: -wp('13.33%'),
    backgroundColor: 'transparent'

  },
  innerView: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    position: 'relative',
    alignItems: 'center'
  },

  backIcon: {

    width: wp('5.86%'),
    height: wp('5.86%'),

    backgroundColor: 'transparent'
  },
  backTouchable: {

    // position: 'absolute',
    width: wp('10.33%'),
    height: '100%',
    // top: 0,
    alignSelf:"flex-start",
    left: wp('4%'),
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  txtTitle: {
    color: 'white',
    fontSize: wp('5.33%'),
    fontFamily: "RubikBold"
  },
  txtTitleDesc: {
    color: 'white',
    fontSize: wp('4%'),
    fontFamily: "RubikRegular"
  },
  txtUsername: {
    height: wp('10.66%'),
    marginLeft: wp('2.6%'),
    width: '70%',
    fontSize: wp('4.58%'),
    fontFamily: "RubikMedium"
  },
  tile: {
    width: '100%',
    flexDirection: 'row',
    paddingVertical: 3,
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 'auto',
    backgroundColor: '#ffffff'
  },
  tileTitle: {
    color: '#000000',
    fontSize: wp('4.53%'),
    marginLeft: wp('2.6%'),
    fontFamily: "RubikLight"
  },
  tileValue: {
    color: '#000000',
    fontSize: wp('4.53%'),
    marginRight: wp('2.6%'),
    fontFamily: "RubikRegular"
  },
  listTile: {
    width: '100%',
    height: wp('32%'),
    shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 1,
    shadowOpacity: 0.8,
    borderRadius: 10,
    marginTop: wp('2.6%'),
    backgroundColor: 'white'
  },
  yearSelection:{width: wp("17%"),justifyContent: 'center',alignItems: 'center',borderLeftWidth: 1,height: hp("12%"),borderColor: "#f3f4f4"},
  txtFld: {
    // marginLeft: wp('1%'),
    // textAlign:'left',
    fontSize: wp('4.58%'),
    fontFamily: "RubikRegular",
    color: 'gray'
  },
  yearView:{margin: 7,flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',margin: 7},View:{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}
})
