import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, ScrollView, SafeAreaView, FlatList, Platform, StatusBar } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';







type Props = {
  navigate: PropTypes.func.isRequired
};
export default class AMCCart extends React.Component {

   state = {
       highlights: [],
       promocode: ''
   };


  componentDidMount()
  {
     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });
  }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

goToNextScreen(){
  this.props.navigation.navigate('ReviewOrder');
}


  render() {
        const cartData = this.state.highlights.map((data) => {
      return (

                      <View style={{width: '100%', height: wp('37.33%'), borderRadius:10, overflow: 'hidden', marginTop: wp('2.6%'), backgroundColor:'white'}}>

                      <View style={{width: '100%', flexDirection: 'row', height: '100%', borderRadius:10, marginTop: 0, backgroundColor:'transparent'}}>

                      <View style={{width: '70%', height: '100%', justifyContent: 'center',  backgroundColor:'transparent' }}>
                      <Text style={[styles.txtDaysLeft, {color: '#5e62e6'}]}>REFRIGERATOR</Text>
                      <Text style={[styles.txtDaysLeft, {fontSize: wp('4.53%')}]}>Frost Free</Text>
                      <Text style={[styles.txtDaysLeft, {fontFamily: "RubikRegular"}]}>Model No.: 12125</Text>


                      <View style={{width: '100%', height: wp('8%'), marginTop: 5, backgroundColor:'transparent'}}>
                      <Text style={{
                        color: '#000000',
                        fontSize: wp('5%'),
                        marginTop: 0,
                        marginLeft: wp('2.6%'),
                        fontFamily: "RubikMedium"
                      }}> ₹ 456 </Text>

                      </View>







                      </View>


                      <View style={{width: '30%', height: '100%', flexDirection: 'row', marginTop: wp('6.66%'), backgroundColor:'transparent'}}>
                         <Image resizeMode="contain" style = {{width: wp('21.33%'), borderColor: 'gray', borderWidth: 0.8, borderRadius: 10, height:wp('21.33%')}} source = {require('../../../../assets/image4.png')}/>

                            <Image resizeMode="cover" style = {{width: wp('8%'), height:wp('8%'), position: 'absolute', top:-10, right:0 }} source = {require('../../../../assets/amc.png')}/>

                      </View>



                      </View>






                      </View>

      )
    });
    return (

           <View style={styles.container}>


        <SafeAreaView style={{flex:1, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0}}>


           <View style={styles.header}>
              <Image resizeMode="cover" style = {{width: '100%', height:'100%', position:'absolute'}} source = {require('../../../../assets/header.png')}/>
              <View style={{ width:'100%', height: wp('10.66%'), marginTop: wp('13.33%'),  backgroundColor:'transparent', justifyContent: 'center', alignItems: 'center'}}>
               <Text style={styles.txtTitle}> MY CART </Text>
               <TouchableOpacity onPress={()=>this.goBack()} style={styles.backTouchable}>
               <Image resizeMode="contain" style = {styles.backIcon} source = {require('../../../../assets/backwhite.png')}/>
               </TouchableOpacity>
               </View>
           </View>

           <View style={{width: '100%', height: 'auto',alignItems: 'center',  marginTop: 0}}>
              <Image resizeMode="cover" style = {{width: '100%', height:wp('10.66%'), position: 'absolute'}} source = {require('../../../../assets/curve.png')}/>

               <ScrollView style={{width: '90%', height: 'auto',marginTop: -wp('13.33%'), marginBottom:100, backgroundColor: 'transparent'}}>

               <Text style={{
                 color: '#ffffff',
                 marginTop: wp('2.6%'),
                 fontSize: wp('4%'),
                 fontFamily: "RubikMedium"
               }}> 3 ITEMS IN YOUR CART </Text>

               {cartData}

               <TouchableOpacity style={{width: '100%', marginTop:wp('5.33%'),  height: wp('13.33%'), justifyContent: 'center', alignItems: 'center', marginRight:5, overflow: 'hidden', backgroundColor: 'transparent', borderRadius: 10}}>
               <Image resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}} source = {require('../../../../assets/button.png')}/>

               <Text style={{
                 color: '#ffffff',
                 fontSize: wp('4%'),

                 fontFamily: "RubikBold"
               }}> APPLY PROMO CODE/ VOUCHER </Text>

                </TouchableOpacity>

                   <View style={{width: '100%',  justifyContent: 'center', alignItems:'center', height: wp('26.66%'), backgroundColor:'#dfdbdc', borderRadius:10,  marginTop: wp('5.33%')}}>

                   <Text style={{
                     color: '#000000',
                     fontSize: wp('4%'),

                     fontFamily: "RubikMedium"
                   }}> USE EMPLOYEE ID CODE </Text>


                     <View style={{width: '85%',  flexDirection: 'row', alignItems:'center', height: wp('10.66%'), backgroundColor:'white', borderRadius: wp('5.33%'),  marginTop: 10}}>
                       <TextInput
                          style={styles.txtUsername}
                          placeholder="Enter Code"
                          onChangeText={(promocode) => this.setState({promocode})}
                         value={this.state.promocode}
                       />

                          <TouchableOpacity style={{width: '30%', alignItems:'center', justifyContent: 'center', height: wp('10.66%'), backgroundColor:'transparent'}}>
                          <Text style={{
                            color: '#000000',
                            fontSize: wp('4%'),
                            fontFamily: "RubikMedium"
                          }}> APPLY </Text>
                          </TouchableOpacity>

                     </View>
                   </View>
                   <Text style={{
                     color: '#000000',
                     marginTop: wp('2.6%'),
                     fontSize: wp('4%'),
                     fontFamily: "RubikMedium"
                   }}> Payment Summary </Text>



                      <View style={{width: '100%',shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 1,
    shadowOpacity: 0.8,  paddingVertical:wp('4%'),  justifyContent: 'center', alignItems:'center', height: 'auto', backgroundColor:'#ffffff', borderRadius:10,  marginTop: wp('5.33%')}}>

                      <View style={styles.tile}>
                      <Text style={styles.tileTitle}> Total </Text>
                      <Text style={styles.tileValue}> ₹ 839.00 </Text>
                      </View>

                      <View style={styles.tile}>
                      <Text style={styles.tileTitle}> Discount </Text>
                      <Text style={styles.tileValue}> ₹ 00.00 </Text>
                      </View>

                      <View style={styles.tile}>
                      <Text style={styles.tileTitle}> Sub Total </Text>
                      <Text style={styles.tileValue}> ₹ 839.00 </Text>
                      </View>


                      <View style={styles.tile}>
                      <Text style={styles.tileTitle}> Estimated GST </Text>
                      <Text style={styles.tileValue}> ₹ 139.00 </Text>
                      </View>

                      <View style={styles.tile}>
                      <Text style={styles.tileTitle}> Handling Charges </Text>
                      <Text style={styles.tileValue}> Free </Text>
                      </View>

                      <View style={{width: '90%', backgroundColor: 'gray', height:1, marginTop:10}}>

                      </View>


                      <View style={[styles.tile , {marginTop: wp('5.33%')}]}>
                      <Text style={[styles.tileTitle, {fontFamily: "RubikRegular"}]}> Total Payable Amount </Text>
                      <Text style={styles.tileValue}> ₹ 978.00 </Text>
                      </View>
                      </View>


                      <View style={{width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems:'center', height: 'auto',  marginTop: wp('5.33%')}}>


                       <TouchableOpacity style={{width: '50%',  justifyContent: 'center',  height: wp('13.33%'), marginLeft:5,  backgroundColor: 'transparent', borderRadius: 10}}>
                       <Text style={{
                         color: '#000000',
                         fontSize: wp('6%'),

                         fontFamily: "RubikMedium"
                       }}> ₹ 839.00 </Text>

                       <Text style={{
                         color: '#000000',
                         fontSize: wp('3.2%'),

                         fontFamily: "RubikLight"
                       }}> Total Amount </Text>

                        </TouchableOpacity>




                        <TouchableOpacity onPress={()=> this.goToNextScreen()} style={{width: '45%',  height: wp('13.33%'), justifyContent: 'center', alignItems: 'center', marginRight:5, overflow: 'hidden', backgroundColor: 'transparent', borderRadius: 10}}>
                        <Image resizeMode="cover" style = {{width: '100%', height: '100%',  position: 'absolute'}} source = {require('../../../../assets/button.png')}/>

                        <Text style={{
                          color: '#ffffff',
                          fontSize: wp('4%'),

                          fontFamily: "RubikBold"
                        }}> CONTINUE </Text>

                         </TouchableOpacity>




                       </View>

                       <View style={{height:20}}>
                       </View>

                </ScrollView>
              </View>










            </SafeAreaView>

           </View>




    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')}



 })
