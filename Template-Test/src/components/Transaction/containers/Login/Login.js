import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,TextInput, ScrollView, SafeAreaView, FlatList } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';



type Props = {
  navigate: PropTypes.func.isRequired
};
export default class Login extends React.Component {

   state = {
       highlights: [],
       email: '',
       password: ''
   };


  componentDidMount()
  {

     var sampleArray = ['Material PVC',  'Depth 101.6 cm'];
     this.setState({
       highlights: sampleArray
     });
  }



  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }





        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

goToNextScreen(){
  this.props.navigation.navigate('ReviewOrder');
}

goToUserLogin(){
  this.props.navigation.navigate('UserLogin');

}


  render() {

    return (
         <View style={styles.container}>
         <Image resizeMode="cover" style = {{width: '100%', height: '100%', position: 'absolute', top:0, left: 0}} source = {require('../../../../assets/screenBG.png')}/>
           <View>

           <View style={{width: '100%', height: wp('69.33%'), backgroundColor: 'transparent'}}>
             <Image resizeMode="cover" style = {{width: '100%', height: '100%'}} source = {require('../../../../assets/corporateLoginBG-2.png')}/>
           </View>


           <View style={{width: '100%', height: 30, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

              <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
               <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
                        <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/fill.png')}/>
                <Image resizeMode="cover" style = {{width: 10, marginHorizontal:2.5, height: 10}} source = {require('../../../../assets/unfill.png')}/>
           </View>



                      </View>

           <ScrollView style={{marginBottom: 0, backgroundColor: 'transparent'}}>

           <View style={{width: '100%', height: 'auto', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

      <Text style={styles.txtTitle}> CORPORATE LOGIN </Text>

       <View style={{width: '80%', height: wp('13.33%'), marginTop: wp('5.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>
       <TextInput
          style={styles.txtFld}
          placeholder="Enter your Email Id"
          placeholderTextColor= 'black'
          onChangeText={(email) => this.setState({email})}
         value={this.state.email}
       />
         <Image resizeMode="contain" style = {{width: 15, position: 'absolute', right: 20, top: 3, marginHorizontal:2.5, height: '100%'}} source = {require('../../../../assets/email.png')}/>

       </View>



       <View style={{width: '80%', height: wp('13.33%'), marginTop: wp('5.33%'), borderRadius:wp('6.66%'), backgroundColor: 'white', justifyContent:'center', alignItems:'center'}}>
       <TextInput
          style={styles.txtFld}
          placeholder="Password"
          placeholderTextColor= 'black'
          onChangeText={(password) => this.setState({password})}
         value={this.state.password}
       />
          <Image resizeMode="contain" style = {{width: 15, position: 'absolute', right: 20, top: 3, marginHorizontal:2.5, height: '100%'}} source = {require('../../../../assets/password.png')}/>

       </View>
      </View>



      <View style={{width: '100%', height: 'auto', marginTop: wp('5.33%'), backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>

 <Text style={styles.txtTitleDesc}> By loging in, you agree to our Terms & Conditions </Text>

 </View>
 <View style={{width: '100%', height: 'auto', flexDirection: 'row', backgroundColor:'transparent', justifyContent:'center', alignItems:'center'}}>

<Text style={styles.txtTitleDesc}> and that you have read our </Text>
<Text style={styles.txtPrivacy}> Privacy Policy</Text>

</View>



<View style={{width: '100%', height: 100, flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
<TouchableOpacity>
  <Image resizeMode="contain" style = {{width: 100, height: 100 }} source = {require('../../../../assets/next.png')}/>
  </TouchableOpacity>

</View>


<TouchableOpacity onPress={()=> this.goToUserLogin()} style={{width: '100%', height: 'auto', flexDirection:'row', backgroundColor: 'transparent', justifyContent:'center', alignItems:'center'}}>
  <Image resizeMode="contain" style = {{width: 24, height: 24 }} source = {require('../../../../assets/user.png')}/>
  <Text style={styles.txtUserLogin}> User Login </Text>

</TouchableOpacity>

</ScrollView>





           </View>


    );
  }
}



 const styles = StyleSheet.create({
   container: {
     flex:1,
     backgroundColor: 'red'
   },
   header: {
     width: '100%',
     height: wp('40%'),
     marginTop: -wp('13.33%'),
     backgroundColor: 'transparent'

   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),

      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('10.66%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        justifyContent: 'center',
        backgroundColor: 'transparent',
        },
      txtTitle:{
        color: 'white',
        fontSize: wp('8%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      txtPrivacy:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikBold",
        borderColor: 'white',
        borderBottomWidth:0.6,
        textDecorationLine: 'underline'
      },
      txtUserLogin:{
        color: 'white',
        fontSize: wp('6.66%'),
        fontFamily: "RubikBold"

      },
      txtUsername:{
        height: wp('10.66%'),
        marginLeft: wp('2.6%'),
        width: '70%',
       fontSize: wp('4.58%'),
       fontFamily: "RubikMedium"
      },
      tile:{width: '100%', flexDirection:'row', paddingVertical:3,  justifyContent: 'space-between', alignItems:'center', height: 'auto', backgroundColor:'#ffffff'},
      tileTitle:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginLeft: wp('2.6%'),
        fontFamily: "RubikLight"
      },
      tileValue:{
        color: '#000000',
        fontSize: wp('4.53%'),
        marginRight: wp('2.6%'),
        fontFamily: "RubikRegular"
      },
             txtDaysLeft:{marginTop: 2,marginLeft:wp('4%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

             txtFld:{
               height: '90%',
               marginLeft: wp('5%'),
               width:'70%',
              fontSize: wp('4.58%'),
              fontFamily: "RubikRegular",
              color: 'black',
              textAlign: 'center'
             },


 })
