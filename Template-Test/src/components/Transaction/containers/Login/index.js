
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { NavigationActions } from 'react-navigation';
import Login from './Login';
import { loginWithPhone } from '../../modules/auth';
import { Map } from 'immutable';


const mapStateToProps = state => ({
  isBusy: state.AuthReducer.isBusy,
  response: state.AuthReducer
});

//export default Login;

export default connect(
  mapStateToProps,
  dispatch => {
    return {
      loginWithPhone: bindActionCreators(loginWithPhone, dispatch),
      navigate: bindActionCreators(NavigationActions.navigate, dispatch)
    };
  }
)(Login);



//
// export default Login;
