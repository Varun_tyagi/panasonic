import React from 'react';
import { StyleSheet, Text,View, TouchableOpacity, Image,ScrollView, ActivityIndicator, SafeAreaView, FlatList, Platform, StatusBar } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration, getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import FastImage from 'react-native-fast-image';


type Props = {
  navigate: PropTypes.func.isRequired
};
class AllEwcPlans extends React.Component {

   state = {
     dataSource: [],
     dataSource2: [],
     selectedCategory: 2,
     responseData: '',
     gettingData: false,
     filter: false
   };


  componentDidMount()
  {
    this.setState({gettingData:true});
    var tempData= [
      {key: 1, image: require('../../../../assets/ac.png'), name: 'All Applications'},
      {key: 2, image: require('../../../../assets/beauty.png'), name: 'Refrigrators'},
      {key: 3, image: require('../../../../assets/mobile.png'), name: 'Air conditioners'},
      {key: 4, image: require('../../../../assets/entertainment.png'), name: 'Home Entertainment'}
    ];
    this.setState({dataSource: tempData});
     this.getData();
     return
    var tempData2= {
   "1 ton":[
            {
                "id": "7936",
                "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                "PlanName": "test",
                "Description": "",
                "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                "ProductGroupName": "Aircon",
                "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                "CSProductCategoryName": "1 ton",
                "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                "SubProductGroupName": "Split",
                "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                "ModelName": "CU-UC12QKY2",
                "WarrantyPeriod": "0",
                "Amount": "1000.00",
                "EWCType": "Comprehensive",
                "status": "1",
                "is_deleted": "0",
                "created_at": "2019-07-30 13:24:28",
                "updated_at": "2019-07-30 07:55:27"
            },{
                "id": "7936",
                "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                "PlanName": "test",
                "Description": "",
                "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                "ProductGroupName": "Aircon",
                "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                "CSProductCategoryName": "1 ton",
                "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                "SubProductGroupName": "Split",
                "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                "ModelName": "CU-UC12QKY2",
                "WarrantyPeriod": "0",
                "Amount": "1000.00",
                "EWCType": "Comprehensive",
                "status": "1",
                "is_deleted": "0",
                "created_at": "2019-07-30 13:24:28",
                "updated_at": "2019-07-30 07:55:27"
            },{
                "id": "7936",
                "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                "PlanName": "test",
                "Description": "",
                "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                "ProductGroupName": "Aircon",
                "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                "CSProductCategoryName": "1 ton",
                "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                "SubProductGroupName": "Split",
                "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                "ModelName": "CU-UC12QKY2",
                "WarrantyPeriod": "0",
                "Amount": "1000.00",
                "EWCType": "Comprehensive",
                "status": "1",
                "is_deleted": "0",
                "created_at": "2019-07-30 13:24:28",
                "updated_at": "2019-07-30 07:55:27"
            }
          ],"3 ton":[
                   {
                       "id": "7936",
                       "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                       "PlanName": "test",
                       "Description": "",
                       "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                       "ProductGroupName": "Aircon",
                       "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                       "CSProductCategoryName": "1 ton",
                       "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                       "SubProductGroupName": "Split",
                       "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                       "ModelName": "CU-UC12QKY2",
                       "WarrantyPeriod": "0",
                       "Amount": "1000.00",
                       "EWCType": "Comprehensive",
                       "status": "1",
                       "is_deleted": "0",
                       "created_at": "2019-07-30 13:24:28",
                       "updated_at": "2019-07-30 07:55:27"
                   },{
                       "id": "7936",
                       "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                       "PlanName": "test",
                       "Description": "",
                       "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                       "ProductGroupName": "Aircon",
                       "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                       "CSProductCategoryName": "1 ton",
                       "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                       "SubProductGroupName": "Split",
                       "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                       "ModelName": "CU-UC12QKY2",
                       "WarrantyPeriod": "0",
                       "Amount": "1000.00",
                       "EWCType": "Comprehensive",
                       "status": "1",
                       "is_deleted": "0",
                       "created_at": "2019-07-30 13:24:28",
                       "updated_at": "2019-07-30 07:55:27"
                   },{
                       "id": "7936",
                       "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                       "PlanName": "test",
                       "Description": "",
                       "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                       "ProductGroupName": "Aircon",
                       "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                       "CSProductCategoryName": "1 ton",
                       "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                       "SubProductGroupName": "Split",
                       "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                       "ModelName": "CU-UC12QKY2",
                       "WarrantyPeriod": "0",
                       "Amount": "1000.00",
                       "EWCType": "Comprehensive",
                       "status": "1",
                       "is_deleted": "0",
                       "created_at": "2019-07-30 13:24:28",
                       "updated_at": "2019-07-30 07:55:27"
                   },{
                       "id": "7936",
                       "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                       "PlanName": "test",
                       "Description": "",
                       "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                       "ProductGroupName": "Aircon",
                       "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                       "CSProductCategoryName": "1 ton",
                       "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                       "SubProductGroupName": "Split",
                       "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                       "ModelName": "CU-UC12QKY2",
                       "WarrantyPeriod": "0",
                       "Amount": "1000.00",
                       "EWCType": "Comprehensive",
                       "status": "1",
                       "is_deleted": "0",
                       "created_at": "2019-07-30 13:24:28",
                       "updated_at": "2019-07-30 07:55:27"
                   }
                 ],
                 "5 ton":[
                          {
                              "id": "7936",
                              "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                              "PlanName": "test",
                              "Description": "",
                              "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                              "ProductGroupName": "Aircon",
                              "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                              "CSProductCategoryName": "1 ton",
                              "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                              "SubProductGroupName": "Split",
                              "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                              "ModelName": "CU-UC12QKY2",
                              "WarrantyPeriod": "0",
                              "Amount": "1000.00",
                              "EWCType": "Comprehensive",
                              "status": "1",
                              "is_deleted": "0",
                              "created_at": "2019-07-30 13:24:28",
                              "updated_at": "2019-07-30 07:55:27"
                          },{
                              "id": "7936",
                              "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                              "PlanName": "test",
                              "Description": "",
                              "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                              "ProductGroupName": "Aircon",
                              "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                              "CSProductCategoryName": "1 ton",
                              "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                              "SubProductGroupName": "Split",
                              "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                              "ModelName": "CU-UC12QKY2",
                              "WarrantyPeriod": "0",
                              "Amount": "1000.00",
                              "EWCType": "Comprehensive",
                              "status": "1",
                              "is_deleted": "0",
                              "created_at": "2019-07-30 13:24:28",
                              "updated_at": "2019-07-30 07:55:27"
                          },{
                              "id": "7936",
                              "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                              "PlanName": "test",
                              "Description": "",
                              "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                              "ProductGroupName": "Aircon",
                              "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                              "CSProductCategoryName": "1 ton",
                              "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                              "SubProductGroupName": "Split",
                              "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                              "ModelName": "CU-UC12QKY2",
                              "WarrantyPeriod": "0",
                              "Amount": "1000.00",
                              "EWCType": "Comprehensive",
                              "status": "1",
                              "is_deleted": "0",
                              "created_at": "2019-07-30 13:24:28",
                              "updated_at": "2019-07-30 07:55:27"
                          },{
                              "id": "7936",
                              "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                              "PlanName": "test",
                              "Description": "",
                              "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                              "ProductGroupName": "Aircon",
                              "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                              "CSProductCategoryName": "1 ton",
                              "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                              "SubProductGroupName": "Split",
                              "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                              "ModelName": "CU-UC12QKY2",
                              "WarrantyPeriod": "0",
                              "Amount": "1000.00",
                              "EWCType": "Comprehensive",
                              "status": "1",
                              "is_deleted": "0",
                              "created_at": "2019-07-30 13:24:28",
                              "updated_at": "2019-07-30 07:55:27"
                          }
                        ],"8 ton":[
                                 {
                                     "id": "7936",
                                     "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                                     "PlanName": "test",
                                     "Description": "",
                                     "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                                     "ProductGroupName": "Aircon",
                                     "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                                     "CSProductCategoryName": "1 ton",
                                     "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                                     "SubProductGroupName": "Split",
                                     "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                                     "ModelName": "CU-UC12QKY2",
                                     "WarrantyPeriod": "0",
                                     "Amount": "1000.00",
                                     "EWCType": "Comprehensive",
                                     "status": "1",
                                     "is_deleted": "0",
                                     "created_at": "2019-07-30 13:24:28",
                                     "updated_at": "2019-07-30 07:55:27"
                                 },{
                                     "id": "7936",
                                     "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                                     "PlanName": "test",
                                     "Description": "",
                                     "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                                     "ProductGroupName": "Aircon",
                                     "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                                     "CSProductCategoryName": "1 ton",
                                     "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                                     "SubProductGroupName": "Split",
                                     "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                                     "ModelName": "CU-UC12QKY2",
                                     "WarrantyPeriod": "0",
                                     "Amount": "1000.00",
                                     "EWCType": "Comprehensive",
                                     "status": "1",
                                     "is_deleted": "0",
                                     "created_at": "2019-07-30 13:24:28",
                                     "updated_at": "2019-07-30 07:55:27"
                                 },{
                                     "id": "7936",
                                     "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                                     "PlanName": "test",
                                     "Description": "",
                                     "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                                     "ProductGroupName": "Aircon",
                                     "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                                     "CSProductCategoryName": "1 ton",
                                     "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                                     "SubProductGroupName": "Split",
                                     "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                                     "ModelName": "CU-UC12QKY2",
                                     "WarrantyPeriod": "0",
                                     "Amount": "1000.00",
                                     "EWCType": "Comprehensive",
                                     "status": "1",
                                     "is_deleted": "0",
                                     "created_at": "2019-07-30 13:24:28",
                                     "updated_at": "2019-07-30 07:55:27"
                                 },{
                                     "id": "7936",
                                     "EWCItemId": "62d32aa1-5eda-e711-812d-005056886d93",
                                     "PlanName": "test",
                                     "Description": "",
                                     "ProductGroupId": "27bc7f9e-a7d9-e711-812d-005056886d93",
                                     "ProductGroupName": "Aircon",
                                     "CSProductCategoryId": "24d9cb3d-5bda-e711-812d-005056886d93",
                                     "CSProductCategoryName": "1 ton",
                                     "SubProductGroupId": "e28dfab4-6dda-e711-812d-005056886d93",
                                     "SubProductGroupName": "Split",
                                     "ModelId": "b7285f31-06f6-e711-8133-005056886d93",
                                     "ModelName": "CU-UC12QKY2",
                                     "WarrantyPeriod": "0",
                                     "Amount": "1000.00",
                                     "EWCType": "Comprehensive",
                                     "status": "1",
                                     "is_deleted": "0",
                                     "created_at": "2019-07-30 13:24:28",
                                     "updated_at": "2019-07-30 07:55:27"
                                 }
                               ]
}
            ;


    this.setState({dataSource2: tempData2});
    //this.getData();

    //  this.setState({gettingData: true});

  }

    parseData(){
      if(this.state.responseData!= ''){
        var accessories = this.state.responseData.data.accessories;
        console.log('categories----',accessories);
        this.setState({dataSource: accessories});

            //this.seleectCategory(accessories[0]);

      }
    }



  async getData(){
    var token = await authFun.checkAuth();
  //alert(' accesssss token token:-----'+ token);
  setConfiguration('accessToken', token);
  var user = await getUser('state');
  setConfiguration('customer_id', user.customer_id); 
     // Make a request for a user with a given ID
     const accessToken = getConfiguration('accessToken');
await axios.get("https://www.ecarewiz.com/ewarrantyapi/getEwcItems?access_token="+accessToken+"&customer_id="+user.customer_id,{headers: {
            'content-type': 'multipart/form-data'
        }})
  .then((response)=>{
    //  console.warn(response.data.data);
     this.setState({dataSource2:response.data.data,gettingData:false})
   }).catch((error)=>{
     this.setState({gettingData:false})
     NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){
        alert('Please check your internet connection.');
      }
    });
   });


   }
  seleectCategory(category){
    var product_data = category.product_data;
      this.setState({dataSource2: product_data});
  }


  openDrawerClick() {
     this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack(){
    this.props.navigation.goBack();
  }

  openDetail(){
    this.props.navigation.navigate('Detail');
  }
  goToCart(){
      this.props.navigation.navigate('MyCart');
  }

        showAlert(message, duration) {
          clearTimeout(this.timer);
          this.timer = setTimeout(() => {
            alert(message);
          }, duration);
        }

        buyNow(){

        }

        addToCart(){
           this.props.navigation.navigate('AMCCart');
        }
        filterButtonClick(){
          if(this.state.filter == true){
            this.setState({filter : false});

          }else{
            this.setState({filter : true});
          }
        }


  render() {


//       for (const [key, value] of Object.entries(tempData2)) {
//   console.warn(`${key} ${value[0].PlanName}`,"gfchvbngvfcdgjhknbhvc"); // "a 5", "b 7", "c 9"
// }



    return (

           <View style={styles.container}>
            <View style={{ height: 120, backgroundColor:'transparent'}}>
      <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']}
               locations={[0.1, 0.5, 0.2, 0.2]}
              angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                backgroundColor: 'purple',
                alignSelf: 'center',
                top: -680,
                height: 800,
                width: 800,
                borderRadius: 400,
                position: 'absolute'
              }}
            >
            </LinearGradient>
            {/* <View style={{
              width: '100%',
              height: wp('10.66%'),
              marginTop: wp('13.33%'),
              backgroundColor: 'transparent',
              justifyContent: 'center',
              alignItems: 'center'
            }}> */}
            <View style={{
              justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center'
              ,  marginLeft: 10, marginTop:Platform.OS=='android'?StatusBar.currentHeight:0,
              paddingTop:Platform.OS=='ios'?30:0
            }}>
            <TouchableOpacity onPress={() => this.props.navigation.pop()}
             style={{width:30, zIndex:1}}
             >
              <Image resizeMode="contain" style={{height:25, width:25}} source={require('../../../../assets/backwhite.png')}/>
            </TouchableOpacity>
            <Text 
            style={{alignSelf:'center',  flex:1, textAlign:'center', marginLeft:-30, fontFamily:'RubikBold', color:'white', fontSize:20}}
            >
              AMC PLANS
            </Text>
            

          </View>
            </View>
             <View style={{width: '100%', backgroundColor:'transparent'}}>
                {/* <Image resizeMode="cover" style = {{width: '100%', height:50, position:'absolute'}} source = {require('../../../../assets/header.png')}/> */}
                <SafeAreaView style={{flex:1, height:'100%', width:'100%', }}>


                         <View style={[styles.header, this.state.filter ? {height: wp('18.66%'), backgroundColor: 'transparent'}  : {height: 0, backgroundColor: 'transparent'}]}>
                             {/* <Image resizeMode="cover" style = {{width: '100%', height:'100%', position:'absolute'}} source = {require('../../../../assets/header.png')}/> */}
                         <FlatList
                           data={this.state.dataSource}
                           horizontal= {true}
                           renderItem={({item}) =>
                           <TouchableOpacity  style={{width: wp('21.33%'), alignItems:'center', justifyContent: 'center', height: 100,marginHorizontal:5, backgroundColor: 'transparent'}}>

                              <View style={{width: '100%', height:'100%', alignItems:'center', justifyContent: 'center'}}>
                           <Image resizeMode="contain" style = {{width: '80%', height: '40%'}} source = {require('../../../../assets/homeApp.png')} />
                           <Text style={{textAlign: 'center', marginTop: 5, height: wp('16%'), color: 'white', fontSize: 12}}>{item.name}</Text>
                           </View>


                           </TouchableOpacity>
                          }
                         />

                         </View>


                  <View style={[{width: '100%', height: 30, overflow: 'hidden', flexDirection: 'row', backgroundColor: 'pink'}, this.state.filter ? {height: 30}  : {height: 0}    ]}>
                    <Image resizeMode="stretch" style = {{width: '100%', height:'100%', position:'absolute'}} source = {require('../../../../assets/header.png')}/>

                    <View style={[styles.tile, {marginLeft: 10}]} >
                         <Image resizeMode="stretch" style = {{width: wp('4.58%'), height: wp('4.58%')}} source = {require('../../../../assets/check.png')}/>
                           <Text style={styles.tileTitle}>All </Text>

                      </View>

                      <View style={styles.tile} >
                         <Image resizeMode="stretch" style = {{width: wp('4.58%'), height: wp('4.58%')}} source = {require('../../../../assets/uncheck.png')}/>
                           <Text style={styles.tileTitle}>In Warranty </Text>

                      </View>

                      <View style={styles.tile} >
                         <Image resizeMode="stretch" style = {{width: wp('4.58%'), height: wp('4.58%')}} source = {require('../../../../assets/uncheck.png')}/>
                           <Text style={styles.tileTitle}>Extended Warranty </Text>

                      </View>



                   </View>


                         <View style={{width: '100%', height: wp('10.66%'),alignItems: 'center',  marginTop: 0}}>
                            {/* <Image resizeMode="cover" style = {{width: '100%', height:wp('10.66%')}} source = {require('../../../../assets/curve.png')}/> */}
                            </View>


                            {this.state.dataSource2 != 0? <View style={{width: wp("85%"),alignSelf: 'center',marginTop: 10}}>
                              <Text style={{color:"#fb84b0",fontFamily: "RubikRegular",fontSize: wp("4%")}}>GST/Taxes applicable extra</Text>
                            </View>:null}
                              {this.state.dataSource2 != 0?
                              <View style={{width: wp("85%"),alignSelf: 'center',borderTopLeftRadius:10,borderTopRightRadius:10,borderWidth: 0.1,borderColor: "transparent",shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 2,overflow: 'hidden',borderColor: "#9ba2a4",}}>
                            <View style={{backgroundColor: "#dadcdc",width: "100%",height: hp("7%"),flexDirection: 'row'}}>
                              <View style={{width: wp("40%"),borderRightWidth: 0.5,borderColor: "#9ba2a4",justifyContent: 'center',height: "100%"}}>
                                <Text style={{textAlign: 'center',  color: 'black', fontSize: wp("4%"),fontFamily: "RubikRegular"}}>Product Description</Text>
                              </View>
                              <View style={{width: wp("22.5%"),borderRightWidth: 0.5,borderColor: "#9ba2a4",justifyContent: 'center',height: "100%"}}>
                                <Text style={{textAlign: 'center',  color: 'black', borderColor: "#9ba2a4",fontSize: wp("4%"),fontFamily: "RubikRegular"}}>Plan Term</Text>
                              </View>
                              <View style={{width: wp("22.5%"),justifyContent: 'center',height: "100%"}}>
                                <Text style={{textAlign: 'center',  color: 'black', fontSize: wp("4%"),fontFamily: "RubikRegular"}}>MRP</Text>
                              </View>
                            </View>
                          </View>:null }

                          {this.state.dataSource2 != 0?<ScrollView style={{flex:1,marginBottom: 10, backgroundColor: 'transparent'}}>

                            <View style={{width: wp("85%"),alignSelf: 'center',borderBottomLeftRadius: 10,
                              borderBottomRightRadius: 10,borderWidth: 0.1,borderColor: "transparent",shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 2,overflow: 'hidden',borderColor: "#9ba2a4",}}>
                            {/* <FlatList
                              data={Object.entries(tempData2)}
                              horizontal= {true}
                              renderItem={([key, value,index]) =>console.warn(key,value,index)}/> */}




                            {Object.entries(this.state.dataSource2).map(([key, value],index) =>
                              {return(
                                <View style={{backgroundColor: "#dadcdc",width: "100%",flexDirection: 'row'}}>
                                  <View style={{width: wp("40%"),borderRightWidth: 0.5,justifyContent: 'center',height: "auto",borderColor: "#9ba2a4",backgroundColor: "white",borderBottomWidth: 0.5,borderColor: "#9ba2a4",}}>
                                    <Text style={{textAlign: 'center',  color: 'black', fontSize: wp("4%"),fontFamily: "RubikBold",}}>{key}</Text>
                                  </View>
                                  <FlatList
                                    data={value}
                                    renderItem={({item}) =>
                                    <View style={{backgroundColor: "white",width: "100%",height: wp("10%"),flexDirection: 'row',borderBottomWidth: 0.4,borderColor: "#9ba2a4"}}>

                                      <View style={{width: wp("22.5%"),borderRightWidth: 0.5,borderColor: "#9ba2a4",justifyContent: 'center',height: "100%"}}>
                                        <Text style={{textAlign: 'center',  color: 'black', fontSize: wp("4%"),fontFamily: "RubikRegular"}}>{item.WarrantyPeriod}</Text>
                                      </View>
                                      <View style={{width: wp("22.5%"),justifyContent: 'center',height: "100%"}}>
                                        <Text style={{textAlign: 'center',  color: 'black', fontSize: wp("4%"),fontFamily: "RubikRegular"}}>{item.Amount}</Text>
                                      </View>

                                    </View>
                                   }
                                  /></View>
                                )}
                                  )

                                  }
                                  </View>
                                  <View style={{
                                    width: wp("85%"),alignSelf: 'center',marginTop: 10,
                                      flexDirection: 'row',

                                    }}>
                                    <FastImage resizeMode="contain" style={{
                                        width: '7%',
                                        height: '100%'
                                      }} source={require('../../../../assets/info.png')}/>
                                    <Text style={{
                                        color: "#8d8d90",
                                        fontFamily: "RubikRegular",
                                        fontSize: wp("3.5%")
                                      }}>All Terms and Conditions</Text>

                                  </View>


                               </ScrollView>:null}
             </SafeAreaView>
             </View>
             {this.state.gettingData ?<View style={{position: "absolute", left: 0, right: 0,top:100,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
               <ActivityIndicator color={"#6464e7"} size={"large"}/>
             </View>: null }

           </View>




    );
  }
}

class MyListItem extends React.PureComponent {
  render() {
    return (
        <View
                style={{
                  paddingVertical: 10,
                }}>
                  <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
              angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                backgroundColor: 'purple',
                alignSelf: 'center',
                top: -680,
                height: 800,
                width: 800,
                borderRadius: 400,
                position: 'absolute'
              }}
            >
            </LinearGradient>
                  <TouchableOpacity onPress={() => null}>
                    <Text
                      style={{
                        color: '#000',
                        height: 40,
                        justifyContent: 'center'
                      }}>
                      {this.props.produto.descricao}
                    </Text>
                  </TouchableOpacity>
                </View>
    )
  }
}



 const styles = StyleSheet.create({
   container: {
     width: '100%',
     height: '100%',
     backgroundColor: '#edf4f6'
   },
   header: {
     width: '100%',
     height: wp('18.66%'),
     backgroundColor: 'transparent',
     alignItems:'center'
   },
   innerView: {
    flex: 1,
     backgroundColor: '#f2f2f2',
     position :'relative',
     alignItems: 'center'
   },



   backTouchable:{
     position: 'absolute',
     width:wp('8.8%'),
     height: wp('10.66%'),
     bottom: wp('2.6%'),
     left: 0
    },
    backIcon: {

     width:wp('5.86%'),
      height: wp('5.86%'),
      marginTop: wp('4%'),
      backgroundColor: 'transparent',
      },
      backTouchable: {

        position: 'absolute',
       width:wp('5.86%'),
        height: '100%',
        top: 0,
        left: wp('4%'),
        backgroundColor: 'transparent',
        },
        filterTouchable: {

          position: 'absolute',
         width:wp('5.86%'),
          height: '100%',
          top: 0,
          right: wp('4%'),
          backgroundColor: 'transparent',
          },
      txtTitle:{
        color: 'white',
        fontSize: wp('5.33%'),
        marginTop: wp('2.6%'),
        fontFamily: "RubikBold"
      },
      txtTitleDesc:{
        color: 'white',
        fontSize: wp('4%'),
        fontFamily: "RubikRegular"
      },
      tileTitle:{
        color: 'white',
        fontSize: wp('4%'),
        marginLeft: 10,
        marginTop:2,
        fontFamily: "RubikRegular"
      },
      tile:{width: 'auto', paddingRight: 15, marginLeft: 3, flexDirection: 'row', alignItems:'center',  height: 'auto', marginTop: 10, flexDirection: 'row', backgroundColor: 'transparent'},

      listDetail:{
        width: '100%',
        height: 'auto',
        marginBottom: 0,
        backgroundColor: 'transparent',
        marginTop:10

      },
      loadDetail:{
        width:'auto',
        height:'auto',
        alignItems: 'center',
        paddingBottom: 10,
        backgroundColor:'white',
        flexDirection: 'column',
        margin: 4 ,
        marginHorizontal:wp('2.6%'),
        shadowColor: '#000000',
        shadowOffset: {
          width: 0,
          height: 0
         },
         shadowRadius: 1,
         shadowOpacity: 1.0 },

         loadDetailTile1:{width:'95%',marginTop: 10, flexDirection: 'row', alignItems: 'center', backgroundColor:'transparent',height: 130},

       loadDetailTile2:{width:'95%',marginTop: 10, borderRadius:7, overflow: 'hidden', flexDirection: 'row', alignItems: 'center', backgroundColor:'transparent',height: 90},

         imageBG:{width:'20%', borderRadius:7, justifyContent: 'center', alignItems: 'center', backgroundColor:'transparent',height: '90%', marginLeft: 10},
          imageBGCheck:{width:'20%', borderRadius:7,  alignItems: 'center', backgroundColor:'transparent',height: '90%', marginLeft: 10},

         productDetail: {flex:1,   backgroundColor:'transparent',height: '90%', marginLeft: 10},

         txtDaysLeft:{marginTop: 2,marginLeft:wp('2.6%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: 'black', fontSize: wp('4%')},

         txtWarranty  :{marginTop: 10,marginLeft:wp('4%'),height: 'auto', width: 'auto', fontFamily: "RubikMedium", color: '#3a687f', fontSize: wp('4.8%')},

         txtPrice  :{marginTop: 0, marginLeft:wp('2.6%'),height: 'auto', width: 'auto', fontFamily: "RubikMedium", color: '#716da0', fontSize: wp('4%')},

          txtGST  :{marginTop: 0,marginLeft:wp('4%'),height: 'auto', width: 'auto', fontFamily: "RubikRegular", color: '#fa8ab2', fontSize: wp('4.8%')},

        txtRate  :{marginTop: 0,marginLeft:wp('2.6%'),height: 'auto', width: 'auto', fontFamily: "RubikRegular", color: '#000000', fontSize: wp('4%')},

         txtAMCPlans:{marginTop: 10,marginLeft:wp('10%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikMedium", color: '#c1c7c7', fontSize: wp('8%')},

          txtOFF:{marginTop: 2,marginLeft:wp('2.6%'),height: 'auto', marginRight:wp('2.6%'), width: '100%', fontFamily: "RubikBold", color: 'white', fontSize: wp('8%')}



 })


export default AllEwcPlans;
