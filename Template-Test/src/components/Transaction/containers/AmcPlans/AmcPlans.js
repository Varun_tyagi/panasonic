import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, ScrollView, Image, SafeAreaView, FlatList, Platform, StatusBar } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { DrawerActions } from 'react-navigation';
import axios from 'axios';
import LinearGradient from 'react-native-linear-gradient';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import Activity from '../../components/Activity/Activity';
import { getConfiguration, setConfiguration } from '../../utils/configuration';
import * as authFun from '../../../../helpers/auth';
import getUser from '../../../../selectors/UserSelectors';
import { drawer } from '../../../navigation/AppNavigator';
import { NavigationEvents } from 'react-navigation';
import FastImage from 'react-native-fast-image';


type Props = {
  navigate: PropTypes.func.isRequired
};

class AmcPlans extends React.Component {


  constructor(props) {
    // var SelectedPlanForAdd = new Array();
    // var SelectedPlanKey =

    super(props);

    this.state = {
      dataSource: [],
      dataSource2: [],
      customer_amc: [],
      action:'',
      SelectedPlan: "",
      SelectedPlanPrice: "", loadIndicator: false,loadAddtoCart:false,
      selectedCategory: 2, SelectedPlanForAdd: new Array(), SelectedPlanKey: new Array(),
      responseData: '',
      gettingData: false, CartItemCount: "0",
      filter: false,
      filterOptions:[{product_group_id:'all',product_name:'All Appliances', icon:require('../../../../assets/ic_home/house_light.png'), selected:true}],
      radioButtons:[{name:'All', selected:true, type:'all'}, {name:'In Warranty', selected:false, type:'In-Warranty'}, {name:'Extended Warranty', selected:false, type:'EWC'}],
      filteredData:[],
    };
    //this.componentDidMount();
  }

  NavigateToCart() {
    this.props.navigation.navigate('MyCart');
  }
  componentDidMount() {
    this.setState({ loadIndicator: true })
    this.getData()
    this.props.navigation.addListener('willFocus', async () => {     
        this.getCartItemCount();
    });


    var tempData = [
      {
        key: 1,
        image: require('../../../../assets/ac.png'),
        name: 'All Applications'
      }, {
        key: 2,
        image: require('../../../../assets/beauty.png'),
        name: 'Refrigrators'
      }, {
        key: 3,
        image: require('../../../../assets/mobile.png'),
        name: 'Air conditioners'
      }, {
        key: 4,
        image: require('../../../../assets/entertainment.png'),
        name: 'Home Entertainment'
      }
    ];
    this.setState({ dataSource: tempData });

    var tempData2 = [
      {
        key: 1,
        image: require('../../../../assets/image3.png'),
        dayLeft: 'AMC EXPIRED:: 22 DAYS LEFT ',
        name: 'REFRIGERATOR',
        type: 'Frost Free',
        Model: '12125',
        brand: 'ABC',
        Date: '06 June 2019'
      }, {
        key: 1,
        image: require('../../../../assets/image4.png'),
        dayLeft: 'AMC EXPIRED:: 22 DAYS LEFT ',
        name: 'REFRIGERATOR',
        type: 'Frost Free',
        Model: '12125',
        brand: 'ABC',
        Date: '06 June 2019'
      }
    ];
    this.getCartItemCount()
    this.setState({ dataSource2: tempData2 });

    //  this.setState({gettingData: true});

  }

  parseData() {
    if (this.state.responseData != '') {
      var accessories = this.state.responseData.data.accessories;
      console.log('categories----', accessories);
      this.setState({ dataSource: accessories });
      this.seleectCategory(accessories[0]);
    }
  }

  async getData() {
    var that = this;
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id);
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerAmcItems?access_token=" + accessToken + "&customer_id=" + customer_id).then(response => {
      // filter data code
      console.warn(response.data)
      let filter = [{product_group_id:'all',product_name:'All Appliances', icon:require('../../../../assets/ic_home/house_light.png'), selected:true}];
      filter.push(...response.data.filter_product.map((item)=>{item.selected=false;return item;}))
      // AMC Plans code
      let amc = response.data.customer_amc;
      // amc = amc.map((item)=>{
      //   if(item.pl)
      // });
      this.setState({filterOptions:filter, customer_amc: response.data.customer_amc, filteredData:response.data.customer_amc, SelectedPlanForAdd: new Array(parseInt(this.state.customer_amc.length)), SelectedPlanKey: new Array(parseInt(this.state.customer_amc.length)) });
      this.setState({ loadIndicator: false })
    }).catch((error) => {
      
      this.setState({ loadIndicator: false })
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){
        alert('Please check your internet connection.');
      }
    })
    })

  }
  seleectCategory(category) {
    var product_data = category.product_data;
    this.setState({ dataSource2: product_data });
  }

  openDrawerClick() {
    drawer.current.open();
    // this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  goBack() {
    this.props.navigation.goBack();
  }

  showAlert(message, duration) {
    this.setState({ loadAddtoCart: false });
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }



  callAddToCartAPI(bodyFormData) {

    this.props.addCartApi(bodyFormData, "amc").then(() => this.afterAddCartAPI()).catch(e => this.showAlert(e.message, 300));

  }

  afterAddCartAPI() {
    console.log("isBusy value --- ", this.props.isBusy);
    console.log("response value --- ", this.props.response);
    if(this.state.action=='buy')
    this.callGetCartAPI();
    else
    this.getCartItemCount();
    this.setState({action:'', loadAddtoCart:'false'})
  }

  callGetCartAPI() {

    this.props.getCartApi().then(() => this.afterGetCartAPI()).catch(e => this.showAlert(e.message, 300));

  }

  afterGetCartAPI() {
    console.log("isBusy value --- ", this.props.isBusyGetCart);
    console.log("response value --- ", this.props.responseGetCart);

    this.setState({ loadAddtoCart: false, action:'' });
    this.props.navigation.navigate('MyCart', { type: "accessories" });
  }
  componentWillUnmount() {

  }
  async getCartItemCount() {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var user = await getUser('state');
    setConfiguration('customer_id', user.customer_id);
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCartCount?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
      this.setState({ CartItemCount: response.data.data, loadIndicator:false, loadAddtoCart:false, action:'' });
      global.cartItemsCount = response.data.data;
    }).catch((error) => {
      this.setState({gettingData: false});
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){
        alert('Please check your internet connection.');
      }
    });
    });
  }
  filterButtonClick() {
    if (this.state.filter == true) {
      this.setState({ filter: false });

    } else {
      this.setState({ filter: true });
    }
  }

  addToCart(item, index) {


    const { SelectedPlanForAdd, SelectedPlanKey } = this.state;
    // let keyname = ;
    // let x = {[SelectedPlanKey[index]]:SelectedPlanForAdd[index]}
    // console.warn("sd");
    if(!SelectedPlanForAdd.length && !SelectedPlanForAdd[index])
    {
      alert('Please select any plan.');
      return;
    }
    var gst = 0;
    // alert(JSON.stringify(item));
    gst = item.gst == null ? 0 : item.gst;
    // alert(JSON.stringify(item));

    if(Object.prototype.toString.call(SelectedPlanForAdd[index]).indexOf('Array')==-1){
      SelectedPlanForAdd[index] = [SelectedPlanForAdd[index]];   
    }
    var cartDataStr = {
      "item_type": "amc",
      "product_item_id": item.product_id,
      "item_name": item.product_name,
      "product_group_id": item.product_group_id,
      "sub_product_group_id": item.sub_product_group_id,
      "model_id": item.model_id,
      "serial_no": item.serial_no,
      "qty": "1",
      "gst": gst,
      "price": this.state.SelectedPlanPrice,
      "plan": { [SelectedPlanKey[index]]: SelectedPlanForAdd[index] },
      "plan_amount": this.state.SelectedPlanPrice
    };

    if (SelectedPlanForAdd[index]) {
      this.setState({ loadAddtoCart: true });
      this.callAddToCartAPI(cartDataStr);
    } 
    
    
  }
  viewAllEwc() {
    this.props.navigation.navigate('AllEwcPlans');
  }

  filterData(){
    let temp = this.state.customer_amc;
    let selectedProductType = this.state.filterOptions.filter((item)=>item.selected)[0].product_group_id;
    let selectedWarrantyType = this.state.radioButtons.filter((item)=>item.selected)[0].type;
    // alert(selectedWarrantyType);
    if(selectedProductType!='all')
    temp = temp.filter((temp)=>temp.product_group_id == selectedProductType);
    if(selectedWarrantyType!='all')
    temp = temp.filter((temp)=>temp.warranty_type == selectedWarrantyType);
    this.setState({filteredData:temp});
  }

  getDate = (dt) => {
    // alert(dt);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')

    return dt[2] + ' ' + months[dt[1][0] == 0 ? dt[1][1] : dt[1]] + ' ' + dt[0];
  }

  render() {
    // alert(JSON.stringify(this.state.customer_amc));
    const { SelectedPlanForAdd, SelectedPlanKey } = this.state;
    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1, alignSelf: 'stretch', backgroundColor: 'transparent', marginBottom: 60 }}>
          <SafeAreaView style={{ flex: 1, marginTop: Platform.OS == 'android' ? StatusBar.currentHeight : 0 }}>
            {/* header */}
            <View style={{ height: this.state.filter ? 250 : 100 }}>
              <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
                angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                  backgroundColor: 'purple',
                  alignSelf: 'center',
                  top: this.state.filter ? -650 : -800,
                  height: 900,
                  width: 900,
                  borderRadius: 450,
                  position: 'absolute'
                }}
              >
              </LinearGradient>
              <View style={{
                flexDirection: 'row', justifyContent: 'space-between',
                alignItems: 'center', padding: 10
              }}>
                <TouchableOpacity onPress={() => drawer.current.open()}>
                  <Image source={require('../../../../assets/ic_home/align-left.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
                <Text style={{ fontSize: 18, color: '#e9eaff', fontFamily: 'RubikMedium' }}>BUY AMC</Text>
                <TouchableOpacity onPress={() => this.setState({ filter: !this.state.filter })}>
                  <Image source={!this.state.filter ? require('../../../../assets/filter.png') : require('../../../../assets/closeWhite.png')} style={{ height: 20, width: 20, }} />
                </TouchableOpacity>
              </View>
              <View style={{flex:1, alignSelf:'stretch', alignItems:'center', justifyContent:'flex-start',}}>
                {
                  this.state.filterOptions && this.state.filter
                    ? <FlatList 
                    data={this.state.filterOptions} 
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{height:120,}}
                    extraData={this.state} 
                    renderItem={({item, index}) => {
                      return(
                        <TouchableOpacity onPress={()=>{
                          let temp = this.state.filterOptions;
                          temp = temp.map((item)=>{
                            item.selected = false;
                            return item;
                          });
                          temp[index].selected = true;
                          this.setState({filterOptions:temp},()=>this.filterData());
                        }}
                           style={{height: '100%', width:100,padding:5, borderRadius:10, overflow: 'hidden',backgroundColor:item.selected?'#4325f4':'transparent', marginLeft:10, alignItems:'center', justifyContent:'flex-start'}}>
                          <View style={{height:60, width:60, alignItems:'center',justifyContent:'center',margin:5}}>
                            <Text style={{fontFamily:'RubikMedium', fontSize:10, color:'#4325f4',backgroundColor:item.selected?'#5dfbff':'white',height:20, width:20,
                             borderRadius:10, position:'absolute', top:-5, right:0,textAlign:'center', textAlignVertical:'center', zIndex:1}}>{index?item.amc_count:this.state.customer_amc.length}</Text>
                            {item.icon?<Image source={item.product_group_id=='all'?item.icon:{uri:item.icon}} style={{resizeMode:'contain',tintColor:item.selected?'#5dfbff':null, height:45, width:45,}}/>:null}
                          </View>
                          <Text style={{flexWrap:'wrap', fontFamily:'RubikRegular', color:item.selected?'#5dfbff':'white', fontSize:12, textAlign:'center'}}>{item.product_name}</Text>
                        </TouchableOpacity>
                      );
                    }}
                        />:null
                  }
                  {
                  this.state.filterOptions && this.state.filter
                    ? <FlatList 
                    data={this.state.radioButtons} 
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    contentContainerStyle={{ height:30,paddingHorizontal:10, justifyContent:'center' , }}
                    extraData={this.state} 
                    renderItem={({item, index}) => {
                      return(
                        <TouchableOpacity onPress={()=>{
                          let temp = this.state.radioButtons;
                          temp = temp.map((item)=>{
                            item.selected = false;
                            return item;
                          });
                          temp[index].selected = true;
                          this.setState({radioButtons:temp},()=>this.filterData());
                        }}
                           style={{ marginHorizontal:5, alignItems:'center', justifyContent:'flex-start', flexDirection:'row', }}>
                          <View style={{height:20, width:20, borderRadius:10, borderWidth:2, borderColor:'white', padding:5, justifyContent:'center', alignItems:'center'}}>
                              {
                                item.selected?
                                <View style={{height:10, width:10,backgroundColor:'white', borderRadius:5}}></View>:null
                              }
                          </View>
                          <Text style={{flexWrap:'wrap', fontFamily:'RubikMedium', color:'white', fontSize:12, marginLeft:5}}>{item.name}</Text>
                        </TouchableOpacity>
                      );
                    }}
                        />:null
                  }
              </View>
            
            </View>

            {/* Items */}
            <View style={{flex:1, alignSelf:'stretch', alignItems:'stretch', marginTop:10,padding:10 }}>
            {
            !this.state.loadIndicator && this.state.filteredData.length
              ? <FlatList data={this.state.filteredData} 
              // style={styles.listDetail} 
              contentContainerStyle={{alignItems:'stretch', flex:1, padding:5}}
              extraData={this.state} 
              renderItem={({item, index}) => {
                return(
                  <View style={{borderRadius:10,marginTop: 10, backgroundColor:'white', padding:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,alignItems:'stretch'}}>
                    {/* Details */}
                    <View style={{flexDirection:'row',justifyContent:'flex-start',}}>
                      {/* Image */}
                      <View style={{height:160,width:100,backgroundColor:'white', alignItems:'center', }}>

                        <View style={{backgroundColor:'#f0f0f0',height:'100%',width:'100%', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                          {item.product_location?<View style={{backgroundColor:'#796dec', height:40, width:40, borderRadius:20, position:'absolute', left:-15, bottom:-15, justifyContent:'center', alignItems:'center', padding:5}}>
                            <Image source={item.product_location.toLowerCase()=='kitchen'?
                              require('../../../../assets/ic_home/kitchen_light.png'):item.product_location.toLowerCase()=='bedroom'?
                              require('../../../../assets/ic_home/bedroom_light.png'):item.product_location.toLowerCase()=='bathroom'?
                              require('../../../../assets/ic_home/bathroom_light.png'):item.product_location.toLowerCase()=='studyroom'?
                              require('../../../../assets/ic_home/studyroom_light.png'):require('../../../../assets/ic_home/waitingroom_light.png')} style={{height:20, width:20, resizeMode:'contain'}}/>
                          </View>:null}
                          {item.image?<Image source={{uri:item.image}} style={{height:110, width:110, resizeMode:'cover', }}/>:null}
                        </View>
                      </View>

                      {/* Details */}
                      <View style={{flex:1,backgroundColor:'white', marginLeft:10, justifyContent:'center'}}>
                        <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#2d2c2c'}}>{item.amc_expired>0?'EXPIRY IN '+item.amc_expired+' DAYS':'EXPIRED'}</Text>
                        <Text style={{fontFamily:'RubikBold', fontSize:15, color:'#3875c9', marginTop:5}}>{item.product_name?item.product_name.toUpperCase():''}</Text>
                        <Text style={{fontFamily:'RubikMedium', fontSize:18, color:'#4f4f4f', marginTop:5}}>{item.sub_product_name}</Text>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#4f4f4f', marginTop:5}}>Model : {item.model_name}</Text>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#4f4f4f',marginTop:5}}>Brand: {item.brand_name?item.brand_name.toUpperCase():''}</Text>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#4f4f4f',marginTop:5}}>Date of Purchase: {item.purchase_date?authFun.getDate(item.purchase_date):''}</Text>
                      </View>
                    </View>
                    {/* Plans */}
                    <View style={{flex:1,flexDirection:'row',  justifyContent:'center',marginTop:15}}>
                      {
                        
                        [[item.plan[0].FirstYearPlan[0],item.plan[0].FirstYearPlan.length?item.plan[0].FirstYearPlan[0].TotalAmount:0], [item.plan[0].SecondYearPlan, item.plan[0].SecondYearTotalAmount], [item.plan[0].ThirdYearPlan, item.plan[0].ThirdYearTotalAmount]].map((plan,ind)=>{
                          return(
                            <TouchableOpacity
                              onPress={() => {
                                SelectedPlanForAdd[index] = plan[0],
                                SelectedPlanKey[index] = ind==0?"FirstYearPlan":ind==1?"SecondYearPlan":"ThirdYearPlan",
                                this.setState({
                                  // SelectedPlan: plan[0].EWCItemId,
                                  SelectedPlanPrice: plan[1].toFixed(2)
                                });
                              }}
                              disabled={plan[1]?false:true}  style={{ flex:1, backgroundColor:SelectedPlanForAdd[ind] == plan[0] && plan[1]?'#f8fdfe':'white',marginRight:ind!=2?5:0,  borderRadius:5, 
                              borderColor:SelectedPlanForAdd[index] == plan[0] && plan[1]?'#91796dec':'#91b0acac', borderWidth:1.5, alignItems:'center',justifyContent:'center', padding:10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:SelectedPlanForAdd[index] == plan[0] && plan[1]?5:0 }}>
                              <Text style={{color:'#014d67', fontFamily:'RubikBold', fontSize:15,textAlign:'center',opacity:plan[1]?1:0.5 }}>{ind+1} YEAR</Text>
                              <Text style={{color:'#757578', fontFamily:'RubikBold', fontSize:11,marginTop:8,textAlign:'center',opacity:plan[1]?1:0.5}}>WARRANTY</Text>
                              <Text style={{color:'#656363', fontFamily:'RubikBold', fontSize:13,marginTop:12 ,textAlign:'center',opacity:plan[1]?1:0.5}}>₹ {plan[1]?plan[1].toFixed(2):'0.00'}</Text>
                            </TouchableOpacity>
                          );
                        })
                        
                      }
                    </View>
                    {/* T&C */}
                    <TouchableOpacity style={{flexDirection:'row', height:20, marginTop:10,}}>
                      <Text style={{backgroundColor:'#8d8d90', textAlign:'center',padding:5, borderRadius:10, width:20,color:'white',
                      fontFamily:'RubikBold', textAlignVertical:'center', fontSize:12, marginRight:5}}>i</Text>
                      <Text>All Terms and Conditions</Text>
                    </TouchableOpacity>
                    {/* buttons */}
                    {
                      item.plan[0].SecondYearPlan.length || item.plan[0].ThirdYearPlan.length || item.plan[0].FirstYearPlan.length?
                      <View style={{flexDirection:'row',  marginTop:15, }}>
                        <TouchableOpacity onPress={() => {this.setState({action:'add', },()=>this.addToCart(item, index));}} 
                          style={{ flex:1, alignItems: 'stretch', justifyContent:'center' , borderRadius:5, backgroundColor:'red',marginRight:7.5 }}>
                          <LinearGradient style={{  flex:1,borderRadius: 5, alignItems: 'stretch', justifyContent:'center' , padding:10}} 
                                      colors={["#ff619e", "#ff9fa6"]}>
                                    <Text style={{color: 'white',fontSize: 15,fontFamily: "RubikMedium",textAlign:'center'}}>
                                      ADD TO CART
                                    </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {this.setState({action:'buy'},()=>this.addToCart(item, index));}} style={{ flex:0.8,marginLeft:7.5,  borderRadius:5,alignItems: 'stretch' ,padding:10, justifyContent:'center' ,  borderWidth:1.5, borderColor:'#6665e8'}}>
                            <Text style={{color: '#6665e8',fontSize: 15,fontFamily: "RubikMedium",textAlign:'center'}}>
                              BUY NOW
                            </Text>
                        </TouchableOpacity>
                      </View>:
                      <View style={{flexDirection:'row',  marginTop:15,justifyContent:'center' }}>
                        <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"ENQUIRY"})} 
                        style={{  borderRadius:5,alignItems: 'center', justifyContent:'center' , padding:15, borderWidth:1.5, borderColor:'#6665e8'}}>
                            <Text style={{color: '#6665e8',fontSize: 15,fontFamily: "RubikMedium",}}>
                              ENQUIRE NOW
                            </Text>
                        </TouchableOpacity>
                      </View>
                    }
                  </View>
                );
              }}
                  />:<Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20, alignSelf:'center'}}>No data found.</Text>  }
            </View>
          </SafeAreaView>
          
        </ScrollView>
          {/* Floating Icon */}
      <TouchableOpacity style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          right: 10,
          bottom: 60,
          shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
        }}>
        <TouchableOpacity onPress={() => this.NavigateToCart()} style={{
            width: 60,
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
            borderRadius: 30,
            shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
          }}>
          <FastImage resizeMode="cover" style={{
              width: 100,
              height: 100,
              position: 'absolute',
              margin: 10
            }} source={require('../../../../assets/button.png')}/>

          <FastImage resizeMode="contain" style={{
              height: 33,
              width: 33,
              alignSelf: 'center'
            }} source={require('../../../../assets/cartWhite.png')}/>
          <View style={{
              height: 17,
              width: 17,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              top: 10,
              right: 8,
              overflow: 'hidden',
              backgroundColor: 'white',
              borderRadius: 20,
              shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
            }}>
            <Text style={{
                color: '#ff7fa2',
                fontSize: wp('2.3%'),
                fontFamily: "RubikBold"
              }}>{this.state.CartItemCount}</Text>
          </View>

        </TouchableOpacity>

      </TouchableOpacity>

        <View style={{
              width: '100%',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              position: 'absolute',bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              height: 'auto',
              marginTop: 10
            }}>
              <View style={{alignItems: 'center',justifyContent: 'space-around',width: "100%",backgroundColor: "white",height:60,flexDirection: 'row', }}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"WARRENTY"})} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: 25,
                          height: 25,
                          tintColor: "black", 
                        }} source={require('../../../../assets/out_of_warrenty.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: 12,
                          fontFamily: "RubikRegular", marginTop:5
                        }}>
                        Out Of Warranty
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.viewAllEwc()} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: 25,
                          height: 25,
                          tintColor: "black"
                        }} source={require('../../../../assets/all_plans.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: 12,

                          fontFamily: "RubikRegular", marginTop:5
                        }}>
                        View All Plans
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"ENQUIRY"})} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: 25,
                          height: 25,
                          tintColor: "black"
                        }} source={require('../../../../assets/enquirenow.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: 12,

                          fontFamily: "RubikRegular", marginTop:5
                        }}>
                        Enquire Now
                      </Text>
                    </TouchableOpacity>
                  </View>

          </View>
          {
          this.state.loadIndicator || this.state.loadAddtoCart
          ? 
              <ActivityIndicator color={"#6464e7"} size={"large"} style={{position:'absolute', top:hp('50%'),right:wp('45%'), left:wp('45%') }}/>
          : null
      }
      </View>
    );}
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#edf4f6',
  },
  header: {
    width: '100%',
    height: wp('18.66%'),
    backgroundColor: 'transparent',
    overflow: 'hidden',
    alignItems: 'center'
  },
  innerView: {
    flex: 1,
    backgroundColor: '#f2f2f2',
    position: 'relative',
    alignItems: 'center'
  },

  backTouchable: {
    position: 'absolute',
    width: wp('8.8%'),
    height: wp('10.66%'),
    bottom: wp('2.6%'),
    left: 0
  },
  backIcon: {

    width: wp('5.86%'),
    height: wp('5.86%'),
    marginTop: wp('4%'),
    backgroundColor: 'transparent'
  },
  backTouchable: {

    position: 'absolute',
    width: wp('5.86%'),
    height: '100%',
    top: 0,
    left: wp('4%'),
    backgroundColor: 'transparent'
  },
  filterTouchable: {

    position: 'absolute',
    width: wp('5.86%'),
    height: '100%',
    top: 0,
    right: wp('4%'),
    backgroundColor: 'transparent'
  },
  txtTitle: {
    color: 'white',
    fontSize: wp('5.33%'),
    marginTop: wp('2.6%'),
    fontFamily: "RubikBold"
  },
  txtTitleDesc: {
    color: 'white',
    fontSize: wp('4%'),
    fontFamily: "RubikRegular"
  },
  tileTitle: {
    color: 'white',
    fontSize: wp('4%'),
    marginLeft: 10,
    marginTop: 2,
    fontFamily: "RubikRegular"
  },
  tile: {
    width: 'auto',
    paddingRight: 15,
    marginLeft: 3,
    flexDirection: 'row',
    alignItems: 'center',
    height: 'auto',
    marginTop: 10,
    flexDirection: 'row',
    backgroundColor: 'transparent'
  },

  listDetail: {
    width: '100%',
    height: wp("20%"),
    marginBottom: 0,
    backgroundColor: 'transparent',
    marginTop: 10

  },
  loadDetail: {
    width: 'auto',
    height: 'auto',
    borderWidth: 1,
    margin: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingBottom: 10,
    borderColor: "transparent",
    backgroundColor: 'white',
    marginHorizontal: wp('2.6%'),
    shadowColor: '#000000',
    shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10,
    borderRadius: 10,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 1,
    shadowOpacity: 1.0
  },

  loadDetailTile1: {
    width: '95%',
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 160,
  },

  loadDetailTile2: {
    width: '95%',
    marginTop: 10,
    borderRadius: 7,
    overflow: 'hidden',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: 90
  },

  imageBG: {
    width: '20%',
    borderRadius: 7,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: '90%',
    marginLeft: 10
  },
  imageBGCheck: {
    width: '20%',
    borderRadius: 7,
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: '90%',
    marginLeft: 10
  },

  productDetail: {
    flex: 1,
    backgroundColor: 'transparent',
    height: '90%',
    marginLeft: 10
  },

  txtDaysLeft: {
    marginTop: 2,
    marginLeft: wp('2.6%'),
    height: 'auto',
    marginRight: wp('2.6%'),
    width: '100%',
    fontFamily: "RubikMedium",
    color: 'black',
    fontSize: wp('4%')
  },

  txtWarranty: {
    marginTop: 10,
    marginLeft: wp('4%'),
    height: 'auto',
    width: 'auto',
    fontFamily: "RubikMedium",
    color: '#3a687f',
    fontSize: wp('4.8%')
  },

  txtPrice: {
    marginTop: 0,
    marginLeft: wp('2.6%'),
    height: 'auto',
    width: 'auto',
    fontFamily: "RubikMedium",
    color: '#716da0',
    fontSize: wp('4%')
  },

  txtGST: {
    marginTop: 0,
    marginLeft: wp('4%'),
    height: 'auto',
    width: 'auto',
    fontFamily: "RubikRegular",
    color: '#fa8ab2',
    fontSize: wp('4.8%')
  },

  txtRate: {
    marginTop: 0,
    marginLeft: wp('2.6%'),
    height: 'auto',
    width: 'auto',
    fontFamily: "RubikRegular",
    color: '#000000',
    fontSize: wp('4%')
  },

  txtAMCPlans: {
    marginTop: 10,
    marginLeft: wp('10%'),
    height: 'auto',
    marginRight: wp('2.6%'),
    width: '100%',
    fontFamily: "RubikMedium",
    color: '#c1c7c7',
    fontSize: wp('5.33%')
  },

  txtOFF: {
    marginTop: 2,
    marginLeft: wp('2.6%'),
    height: 'auto',
    marginRight: wp('2.6%'),
    width: '100%',
    fontFamily: "RubikBold",
    color: 'white',
    fontSize: wp('8%')
  }

})

export default AmcPlans;
