
    <View style={[styles.container,{justifyContent: 'center',alignItems: 'center'}]}>
    <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
            angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
              backgroundColor: 'purple',
              alignSelf: 'center',
              top: -680,
              height: 800,
              width: 800,
              borderRadius: 400,
              position: 'absolute'
            }}
          >
          </LinearGradient>
    <NavigationEvents
    onWillFocus={payload => {
    this.componentDidMount()}}
    // onDidFocus={payload => console.warn('onDidFocus',payload)} //
    // onWillBlur={payload => console.warn('will blur',payload)}
    // onDidBlur={payload => console.warn('did blur',payload)}
  />


    <View style={{
        width: '100%',flex:1,
        backgroundColor: 'transparent',  marginTop:Platform.OS=='android'?StatusBar.currentHeight:0
      }}>


      <SafeAreaView style={{
          height: '100%',
          width: '100%',
          backgroundColor: 'transparent',
        }}>

        <View style={[
            styles.header, {
              height: hp("7%"),justifyContent:'center'
            }
          ]}>

          <Text style={[
              styles.txtTitle, {
                textAlign: 'center',
                alignSelf: 'center'
              }
            ]}>
            BUY AMC
          </Text>

          <TouchableOpacity onPress={() => this.openDrawerClick()} style={styles.backTouchable}>
            <FastImage resizeMode="contain" style={styles.backIcon} source={require('../../../../assets/ic_home/align-left.png')}/>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.filterButtonClick()} style={styles.filterTouchable}>
            {
              this.state.filter == true
                ? <FastImage resizeMode="contain" style={styles.backIcon} source={require('../../../../assets/closeWhite.png')}/>
                : <FastImage resizeMode="contain" style={styles.backIcon} source={require('../../../../assets/filter.png')}/>
            }

          </TouchableOpacity>

        </View>

        <View style={[
            styles.header, this.state.filter
              ? {
                height: hp('13%'),
                backgroundColor: 'transparent',
                justifyContent: 'center',
                alignItems: 'center'
              }
              : {
                height: 0,
                backgroundColor: 'transparent'
              }
          ]}>
          


          <FlatList data={this.state.dataSource} horizontal={true} 
          renderItem={({item}) => <TouchableOpacity onPress={() => this.seleectCategory(item)} style={{
                width: wp('21.33%'),
                height: hp("20%"),
                alignItems: 'center',
                justifyContent: 'center',
                marginHorizontal: 5,
                backgroundColor: 'transparent'
              }}>
              <FastImage resizeMode="contain" style={{
                  width: '70%',
                  height: '30%'
                }} source={require('../../../../assets/homeApp.png')}/>
              <Text style={{
                  textAlign: 'center',
                  margin: 5,
                  height: wp('16%'),
                  color: 'white',
                  fontSize: wp("2.9%")
                }}>{item.name}</Text>

            </TouchableOpacity>}/>

        </View>

        <View style={[
            {
              width: '100%',
              height: 30,
              overflow: 'hidden',
              flexDirection: 'row',
              backgroundColor: 'pink'
            },
            this.state.filter
              ? {
                height: 30
              }
              : {
                height: 0
              }
          ]}>
          <FastImage resizeMode="stretch" style={{
              width: '100%',
              height: '100%',
              position: 'absolute'
            }} source={require('../../../../assets/header.png')}/>

          <View style={[
              styles.tile, {
                marginLeft: 10
              }
            ]}>
            <FastImage resizeMode="stretch" style={{
                width: wp('4.58%'),
                height: wp('4.58%')
              }} source={require('../../../../assets/check.png')}/>
            <Text style={styles.tileTitle}>All
            </Text>

          </View>
          <View style={styles.tile}>
            <FastImage resizeMode="stretch" style={{
                width: wp('4.58%'),
                height: wp('4.58%')
              }} source={require('../../../../assets/uncheck.png')}/>
            <Text style={styles.tileTitle}>In Warranty
            </Text>

          </View>
          <View style={styles.tile}>
            <FastImage resizeMode="stretch" style={{
                width: wp('4.58%'),
                height: wp('4.58%')
              }} source={require('../../../../assets/uncheck.png')}/>
            <Text style={styles.tileTitle}>Extended Warranty
            </Text>

          </View>

        </View>

        <View style={{
            width: '100%',
            height: wp('10.66%'),
            alignItems: 'center',
            marginTop: 0
          }}>
          {/* <FastImage resizeMode="cover" style={{
              width: '100%',
              height: wp('10.66%')
            }} source={require('../../../../assets/curve.png')}/> */}
        </View>

        <View style={{
            flex: 1,
            marginVertical: 10,
            backgroundColor: 'transparent'
          }}>
          {
            !this.state.loadIndicator
              ? <FlatList data={this.state.customer_amc} style={styles.listDetail} extraData={this.state} 
              renderItem={({item, index}) => {
                    return (
                      <View>
                          <View style={[styles.loadDetail]}>
                        <View style={styles.loadDetailTile1}>
                          <View style={styles.imageBG}>
                            <FastImage style={{
                                width: '100%',
                                height: '100%',
                                backgroundColor: 'rgba(250,250,250,0.8)'
                              }} source={item.image}/>
                              <FastImage resizeMode='contain' style={{
                                  width: '33%',
                                  height: '33%',
                                  position: 'absolute',
                                  bottom: 0,
                                  left: 0,
                                }} source={require("../../../../assets/amc.png")}/>
                          </View>

                          <View style={styles.productDetail}>
                            <Text style={styles.txtDaysLeft}>AMC EXPIRED:: {item.amc_expired}</Text>
                            <Text style={[
                                styles.txtDaysLeft, {
                                  color: '#3875c9',fontFamily:"RubikBold"
                                }
                              ]}>{item.product_name}</Text>
                            <Text style={[
                                styles.txtDaysLeft, {
                                  fontSize: wp('4.53%')
                                }
                              ]}>{item.sub_product_name}</Text>
                            <Text style={[
                                styles.txtDaysLeft, {
                                  fontFamily: "RubikRegular"
                                }
                              ]}>Model No: {item.model_name}</Text>
                            <Text style={[
                                styles.txtDaysLeft, {
                                  fontFamily: "RubikRegular"
                                }
                              ]}>Brand: {item.brand_name}</Text>
                            <Text style={[
                                styles.txtDaysLeft, {
                                  fontFamily: "RubikRegular"
                                }
                              ]}>Date Of Purchase: {item.purchase_date.substr(0,11)}</Text>
                          </View>
                        </View>

                        <View style={{
                            flexDirection: 'row', height:120
                          }}>
                          {
                            item.plan[0].FirstYearPlan.length > 0
                              ? <View style={{
                                    width: wp("30%"),
                                    height: wp("30%"),
                                    justifyContent: 'space-between',
                                    margin: 10,
                                    alignItems: 'center',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10,
                                  }}>
                                  <TouchableOpacity onPress={() => {

                                      SelectedPlanForAdd[index] = item.plan[0].FirstYearPlan[0],
                                      SelectedPlanKey[index] = "FirstYearPlan",

                                      this.setState({
                                        SelectedPlan: item.plan[0].FirstYearPlan[0].EWCItemId,
                                        SelectedPlanPrice: Math.round(item.plan[0].FirstYearPlan[0].TotalAmount)
                                      })
                                    }} style={{
                                      borderWidth: 0.5,
                                      flexDirection: 'column',
                                      borderRadius: 5,
                                      backgroundColor:SelectedPlanForAdd[index] == item.plan[0].FirstYearPlan[0]?
                                      'rgba(245, 245, 245, 0.8)':null,
                                      borderColor: SelectedPlanForAdd[index] == item.plan[0].FirstYearPlan[0]
                                        ? "#6461dd"
                                        : "#dbd9d9"
                                    }}>
                                    <View style={{
                                        margin: 10,
                                        width: wp("21%"),
                                        height: hp("11%"),
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                      }}>
                                      <Text style={{
                                          color: "#014d67",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("4%")
                                        }}>{item.plan[0].FirstYearPlan[0].WarrantyPeriod}</Text>
                                      <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹{Math.round(item.plan[0].FirstYearPlan[0].TotalAmount)}</Text>
                                      {/* <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹ {Math.round(item.plan[0].FirstYearPlan[0].TotalAmount)}</Text>
                                      <Text style={{
                                          color: "#ff679f",
                                          fontFamily: "RubikRegular",
                                          fontSize: wp("3%")
                                        }}>10% Discount</Text> */}
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              : null
                          }
                          {
                            item.plan[0].SecondYearPlan.length > 0
                              ? <View style={{
                                    width: wp("25%"),
                                    height: wp("25%"),
                                    justifyContent: 'space-between',
                                    margin: 10,
                                    alignItems: 'center'
                                  }}>
                                  <TouchableOpacity onPress={() => {
                                      SelectedPlanForAdd[index] = item.plan[0].SecondYearPlan[0],
                                      SelectedPlanKey[index] = "SecondYearPlan",
                                      this.setState({
                                        SelectedPlan: item.plan[0].SecondYearPlan[0].EWCItemId,
                                        SelectedPlanPrice: Math.round(item.plan[0].SecondYearPlan[0].TotalAmount)
                                      })
                                    }} style={{
                                      borderWidth: 0.5,
                                      flexDirection: 'column',
                                      borderRadius: 5,
                                      backgroundColor:SelectedPlanForAdd[index] == item.plan[0].SecondYearPlan[0]?
                                      'rgba(245, 245, 245, 0.8)':null,
                                      borderColor: SelectedPlanForAdd[index] == item.plan[0].SecondYearPlan[0]
                                        ? "#6461dd"
                                        : "#dbd9d9"
                                    }}>
                                    <View style={{
                                        margin: 10,
                                        width: wp("21%"),
                                        height: hp("11%"),
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                      }}>
                                      <Text style={{
                                          color: "#014d67",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("4%")
                                        }}>{item.plan[0].SecondYearPlan[0].WarrantyPeriod}</Text>
                                      <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹{Math.round(item.plan[0].SecondYearPlan[0].TotalAmount)}</Text>
                                      {/* <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹ {Math.round(item.plan[0].SecondYearPlan[0].TotalAmount)}</Text>
                                      <Text style={{
                                          color: "#ff679f",
                                          fontFamily: "RubikRegular",
                                          fontSize: wp("3%")
                                        }}>10% Discount</Text> */}
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              : null
                          }
                          {
                            item.plan[0].ThirdYearPlan.length > 0
                              ? <View style={{
                                    width: wp("25%"),
                                    height: wp("25%"),
                                    justifyContent: 'space-between',
                                    margin: 10,
                                    alignItems: 'center'
                                  }}>
                                  <TouchableOpacity onPress={() => {
                                      SelectedPlanForAdd[index] = item.plan[0].SelectedPlanForAdd[0],
                                      SelectedPlanKey[index] = "ThirdYearPlan",
                                      this.setState({
                                        SelectedPlan: item.plan[0].ThirdYearPlan[0].EWCItemId,
                                        SelectedPlanPrice: Math.round(item.plan[0].ThirdYearPlan[0].TotalAmount)
                                      })
                                    }} style={{
                                      borderWidth: 0.5,
                                      flexDirection: 'column',
                                      borderRadius: 5,
                                      backgroundColor:SelectedPlanForAdd[index] == item.plan[0].SecondYearPlan[0]?
                                      'rgba(245, 245, 245, 0.8)':null,
                                      borderColor: SelectedPlanForAdd[index] == item.plan[0].SecondYearPlan[0]
                                        ? "#6461dd"
                                        : "#dbd9d9"
                                    }}>
                                    <View style={{
                                        margin: 10,
                                        width: wp("21%"),
                                        height: hp("11%"),
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                      }}>
                                      <Text style={{
                                          color: "#014d67",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("4%")
                                        }}>{item.plan[0].ThirdYearPlan[0].WarrantyPeriod}</Text>
                                      <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹{Math.round(item.plan[0].ThirdYearPlan[0].TotalAmount)}</Text>
                                      {/* <Text style={{
                                          color: "#656363",
                                          fontFamily: "RubikBold",
                                          fontSize: wp("5%")
                                        }}>₹ {Math.round(item.plan[0].ThirdYearPlan[0].TotalAmount)}</Text>
                                      <Text style={{
                                          color: "#ff679f",
                                          fontFamily: "RubikRegular",
                                          fontSize: wp("3%")
                                        }}>10% Discount</Text> */}
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              : null
                          }
                        </View>

                        <View style={{
                            justifyContent: 'space-between',
                            flexDirection: 'row',
                            width: wp("85%"),
                            justifyContent: 'flex-start'
                          }}>
                          <FastImage resizeMode="contain" style={{
                              width: '7%',
                              height: '100%'
                            }} source={require('../../../../assets/info.png')}/>
                          <Text style={{
                              color: "#8d8d90",
                              fontFamily: "RubikRegular",
                              fontSize: wp("3.5%")
                            }}>All Terms and Conditions</Text>

                        </View>

                        <View style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: 'auto',
                            marginTop: wp('2.33%')
                          }}>

                          <TouchableOpacity onPress={() => this.addToCart(item, index)} style={{
                              width: '45%',
                              height: wp('13.33%'),
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginRight: 5,
                              overflow: 'hidden',
                              backgroundColor: 'transparent',
                              borderRadius: 10
                            }}>
                            <FastImage resizeMode="cover" style={{
                                width: '100%',
                                height: '100%',
                                position: 'absolute'
                              }} source={require('../../../../assets/button.png')}/>

                            <Text style={{
                                color: '#ffffff',
                                fontSize: wp('4%'),

                                fontFamily: "RubikBold"
                              }}>
                              ADD TO CART
                            </Text>

                          </TouchableOpacity>
                          <TouchableOpacity onPress={() => this.addToCart(item, index)} style={{
                              width: '45%',
                              borderColor: '#3b75c9',
                              borderWidth: 1,
                              justifyContent: 'center',
                              alignItems: 'center',
                              height: wp('13.33%'),
                              marginLeft: 5,
                              backgroundColor: 'transparent',
                              borderRadius: 10
                            }}>
                            <Text style={{
                                color: '#3b75c9',
                                fontSize: wp('4%'),

                                fontFamily: "RubikBold"
                              }}>
                              BUY NOW
                            </Text>

                          </TouchableOpacity>

                        </View>

                      </View>
                      {
                        index == this.state.customer_amc.length - 1
                          ? <View style={{
                                alignItems: 'center',
                                justifyContent: 'space-around',
                                width: "100%",
                                height: hp("9%"),
                                flexDirection: 'row'
                              }}/>
                              :null}

                    </View>)
                  }}
                  //Setting the number of column
                  numColumns={1} keyExtractor={(item, index) => index}/>
              :null
          }


          

        </View>

      </SafeAreaView>
      <View style={{
              width: '100%',
              backgroundColor: 'transparent',
              flexDirection: 'row',
              position: 'absolute',bottom: 0,
              justifyContent: 'center',
              alignItems: 'center',
              height: 'auto',
              marginTop: 10
            }}>
              <View style={{
                      alignItems: 'center',
                      justifyContent: 'space-around',
                      width: "100%",
                      backgroundColor: "white",
                      height: hp("8%"),
                      flexDirection: 'row', 
                    }}>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"WARRENTY"})} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: wp("3.5%"),
                          height: hp("3.5%"),
                          tintColor: "black"
                        }} source={require('../../../../assets/out_of_warrenty.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: wp('4%'),

                          fontFamily: "RubikRegular"
                        }}>
                        Out Of Warranty
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.viewAllEwc()} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: wp("4%"),
                          height: hp("4%"),
                          tintColor: "black"
                        }} source={require('../../../../assets/all_plans.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: wp('4%'),

                          fontFamily: "RubikRegular"
                        }}>
                        View All Plans
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("UserQuery",{ScreenName:"ENQUIRY"})} style={{
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center'
                      }}>
                      <FastImage resizeMode="contain" style={{
                          width: wp("4%"),
                          height: hp("4%"),
                          tintColor: "black"
                        }} source={require('../../../../assets/enquirenow.png')}/>
                      <Text style={{
                          color: 'black',
                          fontSize: wp('4%'),

                          fontFamily: "RubikRegular"
                        }}>
                        Enquire Now
                      </Text>
                    </TouchableOpacity>
                  </View>

          </View>
      <TouchableOpacity style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          position: 'absolute',
          right: 10,
          bottom: 100,
          shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
        }}>
        <TouchableOpacity onPress={() => this.NavigateToCart()} style={{
            width: 60,
            height: 60,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
            borderRadius: 30,
            shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
          }}>
          <FastImage resizeMode="cover" style={{
              width: 100,
              height: 100,
              position: 'absolute',
              margin: 10
            }} source={require('../../../../assets/button.png')}/>

          <FastImage resizeMode="contain" style={{
              height: 33,
              width: 33,
              alignSelf: 'center'
            }} source={require('../../../../assets/cartWhite.png')}/>
          <View style={{
              height: 17,
              width: 17,
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              top: 10,
              right: 8,
              overflow: 'hidden',
              backgroundColor: 'white',
              borderRadius: 20,
              shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10
            }}>
            <Text style={{
                color: '#ff7fa2',
                fontSize: wp('2.3%'),
                fontFamily: "RubikBold"
              }}>{this.state.CartItemCount}</Text>
          </View>

        </TouchableOpacity>

      </TouchableOpacity>
    </View>

    {
      this.state.gettingData
        ? <Activity/>
        : null
    }
    {this.state.loadIndicator?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
    <ActivityIndicator color={"#6464e7"} size={"large"}/>
  </View>:null}
  {this.state.loadAddtoCart?<View style={{position: "absolute", left: 0, right: 0,top:0,bottom: 0,alignItems: "center",justifyContent: 'center'}}>
  <ActivityIndicator color={"#6464e7"} size={"large"}/>
</View>:null}

  </View>);
