import { NavigationActions } from 'react-navigation';
import AmcPlans from './AmcPlans';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addCartApi } from '../../modules/AddToCart';
import { getCartApi } from '../../modules/GetCart';
import { getDetail } from '../../modules/ProductDetail';



const mapStateToProps = state => ({
  isBusy: state.AddCartReducer.isBusy,
  response: state.AddCartReducer,
  isBusyGetCart: state.GetCartReducer.isBusy,
  responseGetCart: state.GetCartReducer,
  isBusyGetDetail: state.ProductDetailReducer.isBusy,
  responseGetDetail: state.ProductDetailReducer
});

//export default Login;

export default connect(
  mapStateToProps,
  dispatch => {
    return {
      addCartApi: bindActionCreators(addCartApi, dispatch),
      getCartApi: bindActionCreators(getCartApi, dispatch),
      getDetail: bindActionCreators(getDetail, dispatch),

      navigate: bindActionCreators(NavigationActions.navigate, dispatch)
    };
  }
)(AmcPlans);
