// @flow
import {
  getDetail
} from './actions';
import reducer from './reducer';

export {
  getDetail
};

export default reducer;
