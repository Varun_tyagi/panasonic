// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  DETAIL_REQUEST,
  DETAIL_SUCCESS,
  DETAIL_FAILURE
} from './types';
import * as authFun from '../../../../helpers/auth';
import {  get } from '../../utils/api';
import { getConfiguration, setConfiguration } from '../../utils/configuration';



export const getDetail = async (accessories_id : string) => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: DETAIL_REQUEST
  });

  try {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    const accessToken = token;
    const customer_id = getConfiguration('customer_id');
   var url = 'accessoriesDetail?access_token='+accessToken+'&accessories_id='+accessories_id;
  const user = await get(url);

     return dispatch({
       type: DETAIL_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: DETAIL_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
