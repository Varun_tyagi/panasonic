// @flow
import { Map } from 'immutable';
import {
  DETAIL_REQUEST,
  DETAIL_SUCCESS,
  DETAIL_FAILURE
} from './types';
import type State from './types';




const INITIAL_STATE = [{
  error: null,
  response: null,
  isBusy: false
}];



const reducer = (state: State = INITIAL_STATE, action) => {
  switch (action.type) {
    case DETAIL_REQUEST:
    return {
        ...state,
        isBusy: true,
        response: null
      };
      //return state.update('isBusy', () => true);
    case DETAIL_SUCCESS:
    return {
        ...state,
        isBusy: false,
        response: action.payload
      };


      case DETAIL_FAILURE:
      return {
          ...state,
          isBusy: false,
          response: null
        };
    default:
      return state;
  }
};

export default reducer;
