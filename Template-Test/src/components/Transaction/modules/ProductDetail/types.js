export const DETAIL_REQUEST = 'ProductDetail/DETAIL_REQUEST';
export const DETAIL_SUCCESS = 'ProductDetail/DETAIL_SUCCESS';
export const DETAIL_FAILURE = 'ProductDetail/DETAIL_FAILURE';



export type State = {
  error: string | null
};
