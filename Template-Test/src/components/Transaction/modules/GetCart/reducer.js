// @flow
import { Map } from 'immutable';
import {
  GETCART_REQUEST,
  GETCART_SUCCESS,
  GETCART_FAILURE
} from './types';
import type State from './types';




const INITIAL_STATE = [{
  error: null,
  response: null,
  isBusy: false
}];



const reducer = (state: State = INITIAL_STATE, action) => {
  switch (action.type) {
    case GETCART_REQUEST:
    return {
        ...state,
        isBusy: true,
        response: null
      };
      //return state.update('isBusy', () => true);
    case GETCART_SUCCESS:
    return {
        ...state,
        isBusy: false,
        response: action.payload
      };


      case GETCART_FAILURE:
      return {
          ...state,
          isBusy: false,
          response: null
        };
    default:
      return state;
  }
};

export default reducer;
