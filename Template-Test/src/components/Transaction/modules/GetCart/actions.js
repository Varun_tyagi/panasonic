// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  GETCART_REQUEST,
  GETCART_SUCCESS,
  GETCART_FAILURE
} from './types';
import {  get } from '../../utils/api';

import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';

export const getCartApi = async () => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: GETCART_REQUEST
  });

  try {
    var token = await authFun.checkAuth();
  //alert(' accesssss token token:-----'+ token);
  setConfiguration('accessToken', token);
  var userData = await getUser('state');
  setConfiguration('customer_id', userData.customer_id); 
    const accessToken = token;
    const customer_id = getConfiguration('customer_id');
   var url = 'getCustomerCart?access_token='+accessToken+'&customer_id='+customer_id;
  const user = await get(url);

     return dispatch({
       type: GETCART_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: GETCART_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
