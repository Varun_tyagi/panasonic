export const GETCART_REQUEST = 'GetCart/GETCART_REQUEST';
export const GETCART_SUCCESS = 'GetCart/GETCART_SUCCESS';
export const GETCART_FAILURE = 'GetCart/GETCART_FAILURE';



export type State = {
  error: string | null
};
