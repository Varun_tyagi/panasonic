// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  PRODUCTYPE_REQUEST,
  PRODUCTYPE_SUCCESS,
  PRODUCTYPE_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';
import {  get } from '../../utils/api';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';




export const getProductType = async () => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: PRODUCTYPE_REQUEST
  });

  try {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var userData = await getUser('state');
    setConfiguration('customer_id', userData.customer_id); 
    const accessToken = token;
      const user = await get('getProductType?access_token='+accessToken);

     return dispatch({
       type: PRODUCTYPE_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: PRODUCTYPE_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
