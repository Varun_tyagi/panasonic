// @flow
import { Map } from 'immutable';

import type State from './types';
import {
  PRODUCTYPE_REQUEST,
  PRODUCTYPE_SUCCESS,
  PRODUCTYPE_FAILURE
} from './types';



const INITIAL_STATE = [{
  error: null,
  response: null,
  isBusy: false
}];



const reducer = (state: State = INITIAL_STATE, action) => {
  switch (action.type) {
    case PRODUCTYPE_REQUEST:
    return {
        ...state,
        isBusy: true,
        response: null
      };
      //return state.update('isBusy', () => true);
    case PRODUCTYPE_SUCCESS:
    return {
        ...state,
        isBusy: false,
        response: action.payload
      };


      case PRODUCTYPE_FAILURE:
      return {
          ...state,
          isBusy: false,
          response: null
        };
    default:
      return state;
  }
};

export default reducer;
