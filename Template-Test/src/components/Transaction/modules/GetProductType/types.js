export const PRODUCTYPE_REQUEST = 'GetProductType/PRODUCTYPE_REQUEST';
export const PRODUCTYPE_SUCCESS = 'GetProductType/PRODUCTYPE_SUCCESS';
export const PRODUCTYPE_FAILURE = 'GetProductType/PRODUCTYPE_FAILURE';



export type State = {
  error: string | null
};
