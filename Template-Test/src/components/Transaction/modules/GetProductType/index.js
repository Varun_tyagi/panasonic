// @flow
import {
  getProductType
} from './actions';
import reducer from './reducer';

export {
  getProductType
};

export default reducer;
