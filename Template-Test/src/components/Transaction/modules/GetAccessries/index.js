// @flow
import {
  getAccessriesData
} from './actions';
import reducer from './reducer';

export {
  getAccessriesData
};

export default reducer;
