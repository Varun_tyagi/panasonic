export const GETACCESSRIES_REQUEST = 'GetAccessries/GETACCESSRIES_REQUEST';
export const GETACCESSRIES_SUCCESS = 'GetAccessries/GETACCESSRIES_SUCCESS';
export const GETACCESSRIES_FAILURE = 'GetAccessries/GETACCESSRIES_FAILURE';



export type State = {
  error: string | null
};
