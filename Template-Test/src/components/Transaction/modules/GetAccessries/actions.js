// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  GETACCESSRIES_REQUEST,
  GETACCESSRIES_SUCCESS,
  GETACCESSRIES_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';
import {  get } from '../../utils/api';
import * as authFun from '../../../../helpers/auth';
import { getConfiguration, setConfiguration } from '../../utils/configuration';



export const getAccessriesData = async (productType) => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: GETACCESSRIES_REQUEST
  });

  try {

    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    await setConfiguration('accessToken', token);
    const accessToken = token;

      var user;
      if(productType == 'All'){
        user = await get('getAllAccessories?access_token='+accessToken);

      }else{
        var url = 'getAllAccessories?access_token='+accessToken + '&product_type_id='+productType;
        user = await get(url);

      }

     return dispatch({
       type: GETACCESSRIES_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: GETACCESSRIES_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
