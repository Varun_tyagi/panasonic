// @flow
import { Map } from 'immutable';

import type State from './types';
import {
  GETACCESSRIES_REQUEST,
  GETACCESSRIES_SUCCESS,
  GETACCESSRIES_FAILURE
} from './types';



const INITIAL_STATE = [{
  error: null,
  response: null,
  isBusy: false
}];



const reducer = (state: State = INITIAL_STATE, action) => {
  switch (action.type) {
    case GETACCESSRIES_REQUEST:
    return {
        ...state,
        isBusy: true,
        response: null
      };
      //return state.update('isBusy', () => true);
    case GETACCESSRIES_SUCCESS:
    return {
        ...state,
        isBusy: false,
        response: action.payload
      };


      case GETACCESSRIES_FAILURE:
      return {
          ...state,
          isBusy: false,
          response: null
        };
    default:
      return state;
  }
};

export default reducer;
