// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  LOGIN_WITH_PHONE_REQUEST,
  LOGIN_WITH_PHONE_SUCCESS,
  LOGIN_WITH_PHONE_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';


export const loginWithPhone = async (mobile: string, token: string) => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: LOGIN_WITH_PHONE_REQUEST
  });

  try {
     let details = {
       'mobile': mobile,
       'access_token': token
     };

     let formBody = [];
    for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
}
     formBody = formBody.join("&");
     console.log('formBody: ', formBody);

      const user = await postAPI('login',formBody);

     return dispatch({
       type: LOGIN_WITH_PHONE_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: LOGIN_WITH_PHONE_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
