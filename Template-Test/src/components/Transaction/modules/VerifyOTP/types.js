export const VERIFYOTP_REQUEST = 'VerifyOTP/VERIFYOTP_REQUEST';
export const VERIFYOTP_SUCCESS = 'VerifyOTP/VERIFYOTP_SUCCESS';
export const VERIFYOTP_FAILURE = 'VerifyOTP/VERIFYOTP_FAILURE';



export type State = {
  error: string | null
};
