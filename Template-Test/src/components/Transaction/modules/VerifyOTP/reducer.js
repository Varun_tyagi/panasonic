// @flow
import { Map } from 'immutable';
import {
  VERIFYOTP_REQUEST,
  VERIFYOTP_SUCCESS,
  VERIFYOTP_FAILURE
} from './types';
import type State from './types';




const INITIAL_STATE = [{
  error: null,
  response: null,
  isBusy: false
}];



const reducer = (state: State = INITIAL_STATE, action) => {
  switch (action.type) {
    case VERIFYOTP_REQUEST:
    return {
        ...state,
        isBusy: true,
        response: null
      };
      //return state.update('isBusy', () => true);
    case VERIFYOTP_SUCCESS:
    return {
        ...state,
        isBusy: false,
        response: action.payload
      };


      case VERIFYOTP_FAILURE:
      return {
          ...state,
          isBusy: false,
          response: null
        };
    default:
      return state;
  }
};

export default reducer;
