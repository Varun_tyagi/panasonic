// @flow
import {
  verifyotpApi
} from './actions';
import reducer from './reducer';

export {
  verifyotpApi
};

export default reducer;
