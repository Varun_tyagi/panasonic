// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  VERIFYOTP_REQUEST,
  VERIFYOTP_SUCCESS,
  VERIFYOTP_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';
import { getConfiguration } from '../../utils/configuration';



export const verifyotpApi = async (mobile: string, otp: string) => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: VERIFYOTP_REQUEST
  });

  try {
    const accessToken = getConfiguration('accessToken');

     let details = {
       'mobile': mobile,
       'otp': otp,
       'access_token':accessToken
     };

     let formBody = [];
    for (let property in details) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
}
     formBody = formBody.join("&");
     console.log('formBody: ', formBody);

      const user = await postAPI('verifyOtp',formBody);

     return dispatch({
       type: VERIFYOTP_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: VERIFYOTP_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
