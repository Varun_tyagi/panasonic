// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  GET_TOKEN_REQUEST,
  GET_TOKEN_SUCCESS,
  GET_TOKEN_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';


export const getTokenAPI = async () => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: GET_TOKEN_REQUEST
  });

  try {

    let details = {
      'grant_type': 'client_credentials',
      'client_id':'testclient',
      'client_secret':'testpass'
    };
      const user = await postAPI('http://salescrm.neuronimbus.in/ewarranty/token/authorize', JSON.stringify(details));

     return dispatch({
       type: GET_TOKEN_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: GET_TOKEN_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};
