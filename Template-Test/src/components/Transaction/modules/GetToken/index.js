// @flow
import {
  getTokenAPI
} from './actions';
import reducer from './reducer';

export {
  getTokenAPI
};

export default reducer;
