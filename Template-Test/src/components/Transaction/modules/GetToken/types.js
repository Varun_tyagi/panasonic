export const GET_TOKEN_REQUEST = 'GetToken/GET_TOKEN_REQUEST';
export const GET_TOKEN_SUCCESS = 'GetToken/GET_TOKEN_SUCCESS';
export const GET_TOKEN_FAILURE = 'GetToken/GET_TOKEN_FAILURE';



export type State = {
  error: string | null
};
