// @flow
import { Platform } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import axios from 'react-native-axios';
import {
  ADDCART_REQUEST,
  ADDCART_SUCCESS,
  ADDCART_FAILURE
} from './types';
import {  postAPI } from '../../utils/api';
import * as authFun from '../../../../helpers/auth';
import { setConfiguration,getConfiguration } from '../../utils/configuration';
import getUser from '../../../../selectors/UserSelectors';



export const addCartApi = async (cart_data: string,type:string) => async (
  dispatch: ReduxDispatch
) => {
  dispatch({
    type: ADDCART_REQUEST
  });

  try {
    var token = await authFun.checkAuth();
    //alert(' accesssss token token:-----'+ token);
    setConfiguration('accessToken', token);
    var userData = await getUser('state');
    setConfiguration('customer_id', userData.customer_id); 
    const accessToken = getConfiguration('accessToken');
    const customer_id = getConfiguration('customer_id');

    //console.warn(' cart data :- ',type);
    
    var cartDataStr = type == "amc"? cart_data:
      {
      "item_type": "accessories" ,
      "product_item_id": cart_data.id,
      "item_name":cart_data.accessories_name,
      "product_group_id": cart_data.product_group_id,
      "sub_product_group_id": "0",
      "model_id":"0",
      "serial_no":"",
      "qty":"1",
      "price":parseInt(cart_data.special_price) > 0 ?cart_data.special_price:cart_data.price,
      "plan": "",
      "plan_amount":parseInt(cart_data.special_price) > 0 ?cart_data.price:cart_data.special_price,
      "gst":cart_data.gst
      };


     var str = JSON.stringify(cartDataStr);
     let details = {
       'cart_data': str,
       'customer_id':customer_id
     };

     let formBody = [];
    for (let property in details) {
        let encodedKey = property;   //encodeURIComponent(property);
        let encodedValue =  details[property];   //encodeURIComponent(details[property]);
        formBody.push(encodedKey + "=" + encodedValue);
}
     formBody = formBody.join("&");
     console.log('formBody: ', formBody);
      var url = 'addToCart?access_token='+accessToken
      const user = await postAPI(url,formBody);

     return dispatch({
       type: ADDCART_SUCCESS,
       payload: user
     });
  } catch (e) {
    dispatch({
      type: ADDCART_FAILURE,
      payload: e && e.message ? e.message : e
    });

    throw e;
  }
};