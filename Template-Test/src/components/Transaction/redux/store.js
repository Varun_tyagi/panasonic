
//import { createStore, combineReducers, applyMiddleware } from 'redux';
import AuthReducer from '../modules/auth';
import GetTokenReducer from '../modules/GetToken';
import VerifyOTPReducer from '../modules/VerifyOTP';
import GetAccessriesReducer from '../modules/GetAccessries';
import GetProductTypeReducer from '../modules/GetProductType';
import AddCartReducer from '../modules/AddToCart';
import GetCartReducer from '../modules/GetCart';
import ProductDetailReducer from '../modules/ProductDetail';


// import RegisterReducer from '../modules/Register';
// import ProfileReducer from '../modules/ProfileRanking';
// import ChhanelsReducer from '../modules/Channels';
// import QuesGroupReducer from '../modules/QuesGroup';
// import AnswerReducer from '../modules/Answer';
// import HistoryReducer from '../modules/History';






import {applyMiddleware, createStore, compose, combineReducers} from 'redux';
import * as reduxLoop from 'redux-loop-symbol-ponyfill';
import middleware from './middleware';
import reducer from './reducer';


const enhancers = [
  applyMiddleware(...middleware),
  reduxLoop.install()
];



const rootReducer = combineReducers({
  AuthReducer,
  GetTokenReducer,
  VerifyOTPReducer,
  GetAccessriesReducer,
  GetProductTypeReducer,
  AddCartReducer,
  GetCartReducer,
  ProductDetailReducer
  // RegisterReducer,
  // ProfileReducer,
  // ChhanelsReducer,
  // QuesGroupReducer,
  // AnswerReducer,
  // HistoryReducer
})


const store = createStore(rootReducer,
                          applyMiddleware(...middleware));


console.log(store.getState());

const composeEnhancers = (
	__DEV__ &&
	typeof (window) !== 'undefined' &&
	window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
	) || compose;
// /* eslint-enable no-undef */
//
const enhancer = composeEnhancers(...enhancers);

export default store;
