import React, { Component } from 'react';
import {View,Text,Image,FlatList,Alert,Platform,Picker,TouchableOpacity,KeyboardAvoidingView,Dimensions,ScrollView,StatusBar,TextInput,AsyncStorage,} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import DatePicker from 'react-native-datepicker';
import ApplianceDetail from '../ApplianceDetail';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import ModalDropdown from 'react-native-modal-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import * as authFun from '../../helpers/auth';
import * as constants from '../../helpers/constants';
import Loader from '../common/Loader';


// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
const widthFull = Dimensions.get('window').width;
const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};

class ServiceTrack extends Component {
  
  constructor(props) {
    super(props);
    this.state={
      loading:false,
      token:'',
      customerID:'',
      serviceRecord:[],
      alternateServiceRecord:[],
      filter:[],
      otp:['','','',''],
      otpFull:null,
      search:'',
      showOtp:false,
      showMobile:false,
      std:'+91',
      mobile:'',
      alternateMobile:'',
      noRecordFound:false,
      alternateMobileVerified:false,
    }
    
   }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

 
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mobile: data.mobile ? data.mobile : this.state.mNum,
      },()=>{
       this.getData();
      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  
  onAfterSubmit(){

    this.setState({SelectedType:"HOME",stateName:"",area_name:"",pincode:"",address:"",landmark:"",SelectedAdressID:"",SelectedAddress:[]})

  }



  async componentDidMount(){
    var token = await  this.setToken();
     if(token){
       this.loadData(); 
       
     }
     
   }

   async getData(mobile=this.state.mobile, alternateMobile=false) {
     let that = this;
     this.setState({loading:true});
    // Make a request for a user with a given ID
    // https://www.ecarewiz.com/ewarrantyapi/trackServiceRequestWithMobile?mobile=8588867109
    axios.get('https://www.ecarewiz.com/ewarrantyapi/trackServiceRequestWithMobile?mobile='+mobile+'&access_token='+this.state.token).then((response)=>{
      
      if(response.data.status){
        if(alternateMobile){
          that.setState({alternateServiceRecord:response.data.data,  showMobile:false, showOtp:false, alternateMobileVerified:false,otp:['','','',''], noRecordFound:false});  
        }else{
          that.setState({serviceRecord:response.data.data,alternateServiceRecord:[], filter:response.data.data, showMobile:false, showOtp:false, alternateMobileVerified:false,otp:['','','',''], noRecordFound:false});
        }

      }
      else
      alert(response.message);
      this.setState({loading:false})
    }).catch(function(error) {
      that.setState({loading: false, noRecordFound:alternateMobile?true:false, }); 
    });

  }

  sendOtp=()=>{
    this.setState({loading:true});
    
    var url = constants.base_url+'send_otp';
    url=url+"?access_token="+this.state.token;

    // fetch('http://salescrm.neuronimbus.in/E-warranty/E-warrantyapi/send_otp', {
      fetch(url,{
          method: "POST",
          headers: { 
            // "Authorization": "Basic cGFuYXNvbmljOk5ldXJvQDEyMw==",
          'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.alternateMobile})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:true,alternateMobileVerified:false},()=>{
              this.setState({loading:false,})
            });
          }else{
            
            alert(result.message);
            this.setState({loading:false,})
          }
          
        },
        (error) => { alert(error);
          this.setState({loading:false,})
        }
      );
  }
  verifyOtp=async ()=>{
    this.setState({loading:true});
    await this.setToken();
    var url = constants.base_url+'verifySmsOtp';
    url=url+"?access_token="+this.state.token;
    fetch(url, {
          method: "POST",
          headers: { 'Content-Type':'application/json'},
          body:JSON.stringify({"alternate_mobile_no":this.state.alternateMobile,'otp':parseInt(this.state.otp.join(""))})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({showOtp:false,alternateMobileVerified:true,  otp:['','','','']},()=>{
              this.setState({loading:false})
            });
          }else{
            
            alert(result.message);
            this.setState({loading:false})
          }
        },
        (error) => { alert(error);
          this.setState({loading:false})
        }
      );
  }
  componentWillReceiveProps(nextProps) { 
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example * */
    this.forceUpdate();
  };
  // eslint-disable-next-line class-methods-use-this
  mobileChanged = value => /^[0-9]*$/.test(value)||value==''?this.setState({ alternateMobile: value },()=>{
    if(this.state.alternateMobile.length!=10){
      this.setState({showOtp:false, otp:['','','',''],alternateMobileVerified:false})
    }
  }):null;
  otpChanged = (value,id,callBack=undefined) => {
    var temp = this.state.otp;
    const testExp = /^[0-9]*$/;
    if(testExp.test(value)){
      temp[id]=value
    this.setState({ otp: temp },()=>{
      if(callBack){
        callBack();
      }
    });
    }
    
}
  capitalize=(txt)=>{
    return txt.split(' ').map((word)=>word.split('')[0].toUpperCase()+word.split('').splice(1,).join('')).join(' ');
  }
  getDate = (dt)=>{
    const months =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')
    
    return dt[2]+' '+months[dt[1][0]==0?dt[1][1]:dt[1]]+' '+dt[0];
  }
   
  trackService(){
    if(!this.state.alternateMobileVerified){
      alert('Please verify the mobile number.')
    }else{
      this.getData(this.state.alternateMobile, true);
    }
  }
  render() {
    // console.warn(JSON.stringify(this.state.customerProducts[0]));
    return (
      <View style={{flex:1, backgroundColor:"#edf4f6",justifyContent:'flex-start'}} >
       <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
       {this.state.loading?<Loader/>:null}
       
       <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#edf4f6'}}>
         <View style={{marginTop:StatusBar.currentHeight, flex:1,height:100,marginBottom:10,
           justifyContent:'flex-start', alignItems:'center',paddingBottom:Platform.OS=='ios'?0:15}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:Platform.OS=='ios'?-880:-900,
            height:1000, 
            width:1000,
            borderRadius:500,
            position:'absolute'
          }}
            >
          </LinearGradient>
             <View style={{flex:1, flexDirection:'row', justifyContent:'flex-start',
              alignItems:'center', padding:5, marginTop:Platform.OS=='ios'?30:0,}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1 }} >
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'#e9eaff',fontFamily:'RubikMedium',
                 fontSize:20, textAlign:'center', }}>TRACK AND TRACE YOUR REQUESTS</Text>
            
              </View>
                
         </View>
         {/* product filter */}
         <View style={{flex:1, alignSelf:'stretch', margin:15, flexDirection:'row'}}>
            {!this.state.alternateServiceRecord.length?<TextInput
            placeholder="Job Id"
            onChangeText={(value)=>{this.setState({search:value,filter:this.state.serviceRecord.filter((item)=>item.JobNumber.indexOf(value)!=-1)})}}
            style={{flex:1, marginRight:10, backgroundColor:'white', fontSize:15,fontFamily:'RubikRegular', padding:10, borderRadius:30, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}
            />:<Text style={{flex:1,fontFamily:'RubikMedium', fontSize:15, color:'#707171'}}>Result of <Text style={{color:'black'}}>{this.state.alternateMobile}</Text></Text>
          }
           {!this.state.alternateServiceRecord.length? <TouchableOpacity onPress={()=>this.setState({showMobile:!this.state.showMobile, alternateMobile:'', otp:['','','',''],alternateMobileVerified:false, showOtp:false, alternateServiceRecord:[]})} 
            style={{borderRadius:30, borderWidth:1.5, borderColor:'#bcbaba', backgroundColor:this.state.showMobile?'#c4c3c1':'transparent', justifyContent:'center', alignItems:'center', flexDirection:'row', paddingHorizontal:20}}>
              <Image source={require('../../assets/ic_home/mobile.png')} style={{resizeMode:'contain', height:20, width:20, tintColor:this.state.showMobile?'white':'#bcbaba'}} />
              <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:8, textAlign:'center', color:this.state.showMobile?'white':'#bcbaba'}}>Alternate {'\nMobile No'}</Text>
            </TouchableOpacity>:
            <TouchableOpacity onPress={()=>this.setState({showMobile:!this.state.showMobile, alternateMobile:'',alternateServiceRecord:[], otp:['','','',''],alternateMobileVerified:false, showOtp:false})}>
              <Text style={{color:'black', fontSize:12, textAlignVertical:'top', textDecorationLine:'underline', fontFamily:'RubikMedium'}}>Change Mobile No</Text>
            </TouchableOpacity>}
         </View>
         {/* PRODUCT DETAILS */}
         {  
            !this.state.showMobile?
              this.state.filter.length || this.state.alternateServiceRecord.length?
            <FlatList
              data={this.state.alternateServiceRecord.length?this.state.alternateServiceRecord:this.state.filter} 
              renderItem={({item, index}) => {
                  return (
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('ServiceTrackDetail', {item})} style={{flex:1,borderRadius:5,alignItems:'stretch', backgroundColor:'white', padding:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,flexDirection:'row', alignSelf:'stretch',margin:15}}>
                  <View style={{ height:120,width:100, borderRadius:5, padding:10, borderColor:'#f0f0f0', borderWidth:2, }}>
                    {item.image?<Image source={{uri:item.image}} style={{resizeMode:'contain',height:'100%', width:'100%'}}/>:null}
                  </View>
                  <View style={{height:120, marginLeft:10, padding:10,justifyContent:'center',flex:1,  }}>
                    <Text style={{color:'#3875c9', fontFamily:'RubikBold', fontSize:17, marginBottom:5}}>{(item.ProductName+'').toUpperCase()}</Text>                
                    {item.SubProductGroupName?<Text style={{color:'#4f4f4f', fontFamily:'RubikMedium', fontSize:15}}>{item.SubProductGroupName+''}</Text>:null}                
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Job Id:{' '+item.JobNumber} </Text> 
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Model Name:{' '+item.ModelName} </Text> 
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Created Date: {authFun.getDate(item.JobCreationDate,'T')} </Text>
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Request Type: {item.JobClassification} </Text>                
                    <Text style={{color:'#198816', fontFamily:'RubikMedium', fontSize:15}}>Status: {item.JobstatusName!=null?item.JobstatusName:'Pending'} </Text>     
                  </View>
                  </TouchableOpacity>)}}/>
                  :
                  this.state.serviceRecord.length?
                  <View style={{height:150, flex:1, justifyContent:'center', alignItems:'center', marginHorizontal:15}}>
                    <Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20,textAlign:'center'}}>Please make sure that Job Id is correct.</Text>  
                  </View>
                  :<View style={{height:150, flex:1, justifyContent:'center', alignItems:'center'}}>
                    <Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20}}>No service registered yet.</Text>  
                  </View>:
                  <View style={{flex:1, padding:15, backgroundColor:'white', borderRadius:10, margin:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3, alignItems:'stretch'}}>
                    <Text style={{fontFamily:'RubikBold', fontSize:15, textAlign:'center', color:'#707171'}}>ENTER MOBILE NUMBER</Text>
                   {/* <ScrollView> */}
                    <KeyboardAvoidingView behavior="position" enabled >
                    <View style={{backgroundColor:'white',marginTop:15, paddingHorizontal:15, borderRadius:30, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4, flexDirection:'row', justifyContent:'flex-start'}}>
                      <TextInput
                      style={{textAlign:'center',fontFamily:'RubikMedium', fontSize:16, flex:0.2, }} 
                      keyboardType="phone-pad"
                      editable={false}
                      placeholderTextColor="black"
                      contextMenuHidden={true}
                      placeholder={this.state.std}
                      />
                      <View style={{height:20, width:1, backgroundColor:'#2b26242a', alignSelf:'center'}}></View>
                      <TextInput
                      style={{textAlign:'center',fontFamily:'RubikMedium', fontSize:16, flex:1}} 
                      keyboardType="phone-pad"
                      maxLength={10}
                      secureTextEntry={false}
                      contextMenuHidden={true}
                      placeholder="Mobile number"
                      onChangeText={(value)=>this.mobileChanged(value)}
                      value={this.state.alternateMobile}
                      ref={(input)=>this.mobile=input}
                      />
                    </View>
                          
                    </KeyboardAvoidingView>
                    {this.state.showOtp?
                      <View style={{marginTop:20}}>
                        <Text style={{fontFamily:'RubikBold', fontSize:15, textAlign:'center', color:'#707171',}}>OTP VERIFICATION</Text>
                        {this.state.showOtp?
                          <View style={{flexDirection:'row',alignSelf:'center', marginTop:10,}}>
                            <TextInput
                            style={{backgroundColor:'white',marginEnd:15, textAlign:'center', fontSize:20, fontFamily:'RubikBold',padding:10, paddingVertical:15, borderRadius:5, borderWidth:1, borderColor:'#3a8f8f8f'}}
                            keyboardType="phone-pad"
                            onFocus={()=>this.otpChanged('',0)}
                            maxLength={1}
                            secureTextEntry={true}
                            input={"[1-9]{1}[0-9]{9}"}
                            pattern={"[1-9]{1}[0-9]{9}"}
                            onChangeText={(value)=>this.otpChanged(value,0,()=>this.otp2.focus())}
                            value={this.state.otp[0]}
                            ref={(input)=>this.otp1=input}
                            />
                            <TextInput
                            style={{backgroundColor:'white',marginEnd:15, padding:10, paddingVertical:15, textAlign:'center', fontSize:20, fontFamily:'RubikBold', borderRadius:5, borderWidth:1, borderColor:'#3a8f8f8f'}}
                            keyboardType="phone-pad"
                            onFocus={()=>this.otpChanged('',1)}
                            maxLength={1}
                            secureTextEntry={true}
                            // onKeyPress={()=>{
                            //   this.onBackSpace(this.otp1)
                            // }}
                            onChangeText={(value)=>this.otpChanged(value,1,()=>this.otp3.focus())}
                            value={this.state.otp[1]}
                            ref={(input)=>this.otp2=input}
                            />
                            <TextInput
                            style={{backgroundColor:'white',marginEnd:15,padding:10, paddingVertical:15, textAlign:'center', fontSize:20, fontFamily:'RubikBold', borderRadius:5, borderWidth:1, borderColor:'#3a8f8f8f'}}
                            keyboardType="phone-pad"
                            onFocus={()=>this.otpChanged('',2)}
                            maxLength={1}
                            secureTextEntry={true}
                            // onKeyPress={()=>{
                            //   this.onBackSpace(this.otp2)
                            // }}
                            onChangeText={(value)=>this.otpChanged(value,2,()=>this.otp4.focus())}
                            value={this.state.otp[2]}
                            ref={(input)=>this.otp3=input}
                            />
                            <TextInput
                            style={{backgroundColor:'white', padding:10, paddingVertical:15, textAlign:'center', fontSize:20, fontFamily:'RubikBold', borderRadius:5, borderWidth:1, borderColor:'#3a8f8f8f'}}
                            keyboardType="phone-pad"
                            onFocus={()=>this.otpChanged('',3)}
                            maxLength={1}
                            // onKeyPress={()=>{
                            //   this.onBackSpace(this.otp3)
                            // }}
                          // onEndEditing={this.state.showOtp?()=>this.verifyOtp():()=>this.loginAPi()}
                            secureTextEntry={true}
                            onChangeText={(value)=>this.otpChanged(value,3) }
                            value={this.state.otp[3]}
                            ref={(input)=>this.otp4=input}
                            />
                          </View>:null
                          }
                        
                      </View>:null}
                      <TouchableOpacity onPress={()=>
                        {
                          if(this.state.alternateMobile.length==10)
                          if(this.state.showOtp)
                          this.verifyOtp();
                          else
                          this.sendOtp();
                          else{
                            alert('Please enter a valid mobile number.');
                            this.mobile.focus();
                          }
                          
                        }} 
                      style={{ height:60, width:60,alignSelf:'center',  marginTop:15}} >
                        <Image  source={require('../../assets/ic_home/next.png')}  
                        style={{ resizeMode:'contain',height:'100%',width:'100%', }}
                        />
                      </TouchableOpacity>
                      {
                          this.state.alternateMobileVerified?
                          <Text style={{fontFamily:'RubikMedium', fontSize:15, textAlign:'center', color:'#ADFF2F',marginTop:15}}>Mobile number verified successfully.</Text>:null
                        }
                    
                  </View>

                  }
                  {
                    this.state.showMobile?
                    <TouchableOpacity onPress={()=>{ this.trackService(); }  } disabled={!this.state.alternateMobileVerified} style={{ flex:1,borderRadius:5, alignSelf:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,marginVertical:20}}>
                      <LinearGradient style={{  borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center' , padding:15}}
                                  colors={["#ff619e", "#ff9fa6"]}>
                                <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                                  TRACK REQUEST
                                </Text>
                        </LinearGradient>
                    </TouchableOpacity>:null
                  }
                  {
                    this.state.noRecordFound?
                    <Text style={{fontFamily:'RubikMedium', fontSize:15, textAlign:'center'}}>No records found.</Text>:null
                  }
         </ScrollView>
         </View>);
  }
}

ServiceTrack.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ServiceTrack.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTrack);
