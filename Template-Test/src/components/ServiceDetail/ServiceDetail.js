import React, { Component } from 'react';
import {View,Text,Image,FlatList,Modal,Switch,Picker,TouchableOpacity,ImageBackground,Dimensions,ScrollView,StatusBar,TextInput,AsyncStorage,} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import DatePicker from 'react-native-datepicker';
import ApplianceDetail from '../ApplianceDetail';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import ModalDropdown from 'react-native-modal-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import * as authFun from '../../helpers/auth';
import * as constants from '../../helpers/constants';
import Loader from '../common/Loader';
import NavigationService from '../navigation/NavigationService';


// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
const widthFull = Dimensions.get('window').width;
const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};

class ServiceDetail extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state={
      loading:false,
      token:'',
      customerID:'',
      customerProducts:[],
      customerProductsAmcExpired:[],
      Myaddresses:[],
      ActivityIndicator:false,
      highlights: [],
      itemCount:0,
      promocode: '',
      serialNum:'',
      myCartData:[],
      refreshing: false,
      switch1Value: false,
      selectedUserType: '',
      user:"",
      dropdown_4_options: null,
      items:undefined,
      coupon:[],
      shipping:{},
      totalPrice:"",
      modelNum:'',
      selectedAddressType:'HOME',
      filterValues:[],
      dropdown_4_defaultValue: 'loading...',
      cityname:"",
      stateName:"",
      pincode:"",
      landmark:"",
      address:"",
      area_name:"",
      group:[],
      subgroup:[],
      models:[],
      date:'',
      purchase_date:'',
      time:'',
      warrantyTypes:[],
      jobTypes:[],
      remark:'',
      warranty:'',
      job:'',
      JobId:'',
      showPMSNote:false,
      selected: [ { label: 'group', val: 0 }, { label: 'subgroup', val: 0 }],
      allowed_brands:['Panasonic', 'Sanyo', 'Others'],
      dropdown_6_icon_heart: true,
      successModal:false,
      acceptRequest:true,
      addressType:[
        {firstName:'HOME',},
        {firstName:'OFFICE',},
        {firstName:'OTHER',}
      ],totalPrice:"",Indicator:false,
      StateName:[],City:[],Area:[],oldAddress:true,SelectedAddress:[],SelectedAdressID:"",
    }
    this.capitalize = this.capitalize.bind(this);
    this.getDate = this.getDate.bind(this);
    this.productAdded = this.productAdded.bind(this);
    this.pincodeChanged = this.pincodeChanged.bind(this);
    this.submitRequest = this.submitRequest.bind(this); 
   }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  productAdded = async ()=>{
    const result = await AsyncStorage.getItem('productAdded');
    if(result==1){
      await AsyncStorage.removeItem('productAdded');
      return true;
    }else{
    return false;
    }
  }
  setValue = (screen, field, val) => {
    var temp = screen == 1 ? this.state.selected : this.state.selectedAdd;
    temp = temp.map((item) => {
        if (item.label == field) {
            item.val = val
        }
        return item;
    });
    if (screen == 1)
        this.setState({ selected: temp });
    else
        this.setState({ selectedAdd: temp });

}
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
      },()=>{
       this.getDataofAddress(this.state.token,this.state.customerID);

       this.getData("state");
       let previousProductData = this.props.navigation.state.params?this.props.navigation.state.params.productDetails:'';
       this.getBrand(previousProductData);
      
       this.getJobWarrantyData();
       
      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }

  getDataofAddress(accessToken,customer_id) {
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCustomerAddresses?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
      this.setState({Myaddresses:response.data.data,ActivityIndicator:false})
    }).catch((error) => {
      NetInfo.fetch().then(async (state)=>{
        if(!state.isConnected){
          alert('Please check your internet connection.');
        }
      })
      
      this.setState({ActivityIndicator: false});
    });
  }

  toggleSwitch1 = (value) => {
    this.setState({switch1Value: value});
  }
  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  
  async  submitRequest(){
    let shipping={};
    let productKnown = this.props.navigation.state.params?this.props.navigation.state.params.productDetails:null;
    let body = new FormData();
    if(!productKnown){
      if(!this.state.selected[0].val){
        alert("Please select the product group.");
        return;
      }
      if(!this.state.selected[1].val){
        alert("Please select the product category.");
        return;
      }
      // if(!this.state.modelNum){
      //   alert("Please enter a valid model name.");
      //   return;
      // }
      // if(!this.state.serialNum){
      //   alert("Please enter a valid Serial number.");
      //   return;
      // }
      if(!this.state.purchase_date){
        alert("Please select the date of purchase.");
        return;
      }
      if(!this.state.warranty){
        alert("Please select warranty type.");
        return;
      }
      
    }
    
    if(!this.state.job){
      alert("Please select request type.");
      return;
    }
    // if(!this.state.date){
    //   alert("Please select preferred date.");
    //   return;
    // }
    // if(!this.state.time){
    //   alert("Please select preferred time.");
    //   return;
    // }
    if(!this.state.remark){
      alert("Please enter the details of request.");
      return;
    }
    if(this.state.oldAddress  && this.state.Myaddresses.length){
      if(this.state.SelectedAdressID){
          shipping={};
          // this.onAfterSubmit();
      }
      else{
        alert("Please select any address.");
        return;
      }
    }else{
    if(this.state.address == ""){
      alert("Please enter address.");
      return;
    }
    if(this.state.stateName == ""){
      alert("Please choose state name.");
      return;
    }
    if(this.state.cityname == ""){
      alert("Please choose city name.");
      return;
    }
    if(this.state.area_name == ""){
      alert("Please choose area name.");
      return;
    }
    if(this.state.pincode == ""){
      alert("Please choose pincode.");
      return;
    }
    shipping={
      address_type:this.state.selectedAddressType,
      country:"India",
      state:this.state.stateName,
      city:this.state.cityname,
      area:this.state.area_name,
      pincode:this.state.pincode,
      landmark:this.state.landmark,
      address:this.state.address
    }
  }
  

  body.append('customer_id', this.state.customerID);
    if(productKnown){
      body.append('customer_product_id', productKnown.id);
      body.append('product_group_id', productKnown.product_group_id);
      body.append('sub_product_group_id',productKnown.sub_product_group_id);
      body.append('model_id',productKnown.model_id);
      body.append('purchase_date', productKnown.purchase_date);
      body.append('warranty_type', productKnown.warranty_type);
    }else{
      let model_id = this.state.models.filter((model)=>model.model_name==this.state.modelNum);
      console.warn(model_id)
      if(model_id.length){
        model_id = model_id[0].model_id;
      }else if(this.state.modelNum){
          alert('Please enter a valid model name or leave it blank.');
          return ;
      }else{
        model_id = '';
      }
      body.append('customer_product_id', '');
      body.append('product_group_id', this.state.selected[0].val);
      body.append('sub_product_group_id',this.state.selected[1].val);
      body.append('model_id', model_id);
      body.append('serial_no', this.state.serialNum.toUpperCase());
      body.append('purchase_date', this.changeFormat(this.state.purchase_date));
      body.append('warranty_type', this.state.warranty);
    }
    
    body.append('job_classification', this.state.job);
    body.append('preferred_date',this.state.date?this.changeFormat(this.state.date):'0000-00-00 00:00:00');
    body.append('preferred_time', this.state.time);
    body.append('remark', this.state.remark);
    body.append('customer_address_id',Object.keys(shipping).length?'':this.state.SelectedAdressID);
    body.append('shipping', JSON.stringify(shipping));
    // console.warn('Data to be sent ', body);
    
    await this.setToken();
    this.setState({loading:true});
    let url = constants.base_url + 'serviceRequest'
    url = url + "?access_token=" + this.state.token;
    console.warn(url, body);
    axios.post(url, body,{timeout:30*1000} ,{ headers: {'content-type': `multipart/form-data;`} }, 
  ).then((response)=>{
      this.setState({loading:false});
      console.warn(response.data)
      if(response.data.status){
        this.setState({JobId: response.data.ASCJOBID})
        this.onAfterSubmitData();
      }else{
        alert(response.data.message);
      }
  }).catch((response)=>{
    this.setState({Indicator:false, loading:false});
    if(response.message.indexOf('timeout')!=-1){
      alert('timeout'+response.message);
      this.setState({timeout:true},()=>{
        this.onAfterSubmitData();
      });
    }else if(response.message.indexOf("Network")!=-1)
    {
      alert('Please check your internet connection.');
    }else{
      console.warn(response);
    }
  })
  }

  onAfterSubmit(goBack=true){
    
    this.setState({selectedAddressType:"HOME",stateName:"",area_name:"",pincode:"",address:"",landmark:"",SelectedAdressID:"",SelectedAddress:[], });
    if(goBack){
      if(this.warrantySelector)
      this.warrantySelector.select(-1);
      if(this.jobSelector)
      this.jobSelector.select(-1);
      this.props.navigation.goBack(null);
    }
    

  }
  onAfterSubmitData(){
    
    this.setState({selectedAddressType:"HOME",stateName:"",area_name:"",pincode:"",address:"",landmark:"",SelectedAdressID:"",SelectedAddress:[], warranty:'', job:'', date:'', purchase_date:'', time:'', successModal:true});
    if(this.warrantySelector)  
    this.warrantySelector.select(-1);
    if(this.jobSelector)
    this.jobSelector.select(-1);
      
    

  }



  async componentDidMount(){
    var token = await  this.setToken();
     if(token){
       this.loadData(); 
       
     }
     
   }

   async getData(MethodName,ID, selectId=null, data=[]) {
    //  alert(MethodName);
    var that = this;
    await this.setToken();
    //alert(' accesssss token token:-----'+ token);
    var  APINAME = "";
    var  TYPE = ""
    if(MethodName == "state"){
      APINAME =   "getState";
    }
    if(MethodName == "CITY"){
        APINAME = "getCity";
        TYPE = "state_id";
    }
    if(MethodName == "PINCODE"){
        APINAME = "getArea";
        TYPE = "city_id"
    }

    // Make a request for a user with a given ID
    axios.get('https://www.ecarewiz.com/ewarrantyapi/'+APINAME+"/?access_token="+this.state.token+"&"+TYPE+"="+ID).then((response)=>{
      if(MethodName == "state"){
         that.setState({StateName: response.data.data});
         if(selectId!=null){
           let id = '';
           this.state.StateName.forEach((item, index)=>{if(item.state_id==selectId)id= index;});
          // this.setState({stateName:selectId-1},()=>{
            this.stateSelector.select(id);
          // })
         }
      }
      if(MethodName == "CITY"){
         that.setState({City: response.data.data});
         if(selectId!=null){
          let id='';
          this.state.City.forEach((item, index)=>{if(item.city_id==selectId)id= index;});
          // this.setState({cityname:id},()=>{
            this.lookaheadFilter.select(id);
          // });
        }
      }
      if(MethodName == "PINCODE"){
         that.setState({Area:data.length?data: response.data.data});
         if(selectId!=null){
          let id ='';
          this.state.Area.forEach((item, index)=>{if(item.area_id==selectId)id= index;});
          // this.setState({area_name:id},()=>{
            // this.lookaheadFilter2.select(id);
          // });
        }
      }

    }).catch(function(error) {
      that.setState({gettingData: false});
      console.warn(error.code)

    });

  }

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example * */
    this.forceUpdate();
  };
  // eslint-disable-next-line class-methods-use-this
  
  capitalize=(txt)=>{
    return txt.split(' ').map((word)=>word.split('')[0].toUpperCase()+word.split('').splice(1,).join('')).join(' ');
  }
  getDate = (dt)=>{
    const months =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')
    
    return dt[2]+' '+months[dt[1][0]==0?dt[1][1]:dt[1]]+' '+dt[0];
  }
  // check(){
  //   if(this.state.oldAddress){
  //     this.setState({oldAddress:false})
  //   }else{
  //     this.setState({oldAddress:true})
  //   }
  //   this.onAfterSubmit();
  // }
  changeFormat = (dt) => {
    var arr = dt.split('/');
    return arr[2] + '-' + arr[1] + '-' + arr[0]+' 00:00:00'
  }

  cleanAddress(){
    this.setState({ area_name:'', statename:'', cityname:''});
    this.stateSelector.select(-1);
    this.lookaheadFilter.select(-1);
    this.lookaheadFilter2.select(-1);
  }
     
  
  pincodeChanged =async ()=>{
    let pincode = this.state.pincode;
    await this.setToken();
    this.setState({loading:true});
    if(pincode!='' && pincode.length==6){
      let url = constants.base_url + 'pincodeBasedData';
      url = url + "?access_token=" + this.state.token + '&pincode=' + pincode;
      fetch(url,
        { headers: { 'Content-Type': 'application/json' } })
        .then(response => response.json())
        .then((responseJson) => {
          if (responseJson.status) {
            // this.getData('area', responseJson.data.city_id, responseJson.data.area_id, responseJson.data.area);
            this.getData('state',null, parseInt(responseJson.data.state_id));
            this.getData('CITY',responseJson.data.state_id,parseInt(responseJson.data.city_id));
            this.getData('PINCODE', responseJson.data.city_id, parseInt(responseJson.data.area_id), responseJson.data.area);
            this.setState({loading:false, stateName: responseJson.data.state_name, cityname: responseJson.data.city_name,  countryName: responseJson.data.country_name});
            // console.warn(responseJson.data)
          } else {
            alert('Could not locate '+this.state.pincode);
            this.setState({ loading: false });
            this.cleanAddress();
          }
  
        })
        .catch(error => { alert('Could not locate '+this.state.pincode); this.setState({ loading: false }) })
    }else{
      alert('Enter a valid postal code.');
      this.setState({loading:false})
    }
  }
  // data related to products that could not be found in list
  getCategory = (id=1) => {
    this.setState({ loading: true })
    var url = constants.base_url + 'getProductGroupService';
    url = url + "?access_token=" + this.state.token + '&brand_id=' + id;

    fetch(url, { headers: { 'Content-Type': 'application/json' } })

        .then(response => response.json())
        .then((responseJson) => {
          console.warn(responseJson)
            if (responseJson.status) {
                this.setState({ group: responseJson.data })
            }
            this.setState({ loading: false })

        })
        .catch(error => {
            alert(error);
            this.setState({ loading: false })
        })
}
getBrand = (previousProductData) => {
  this.setState({ loading: true })
  var url = constants.base_url + 'getBrand';
  url = url + "?access_token=" + this.state.token + '&is_service_request=' + 1;

  fetch(url, { headers: { 'Content-Type': 'application/json' } })

      .then(response => response.json())
      .then((responseJson) => {
        console.warn(responseJson)
          if (responseJson.status) {
              this.setState({ allowed_brands: responseJson.data },()=>{
                if(previousProductData){
                  this.setState({acceptRequest: this.state.allowed_brands.filter((item)=>item.brand_name.toLowerCase()==previousProductData.brand_name.toLowerCase()).length?true:false});
                    
               }
              });
          }
          this.setState({ loading: false })

      })
      .catch(error => {
          alert(error);
          this.setState({ loading: false })
      })
}
getJobWarrantyData = () => {
  this.setState({ loading: true })
  var url = constants.base_url + 'getJobWarranty';
  url = url + "?access_token=" + this.state.token;

  fetch(url, { headers: { 'Content-Type': 'application/json' } })

      .then(response => response.json())
      .then((responseJson) => {
        console.warn(responseJson.job_classifications.length)
          if (responseJson.status) {
              this.setState({ jobTypes: responseJson.job_classifications, warrantyTypes:responseJson.warranty_type })
          }
          this.setState({ loading: false })

      })
      .catch(error => {
          alert(error);
          this.setState({ loading: false })
      })
}

getAppliance = (id) => {
    this.setState({ loading: true });
    var url = constants.base_url + 'getSubProductGroup';
    url = url + "?access_token=" + this.state.token + '&product_group_id=' + id;

    fetch(url, { headers: { 'Content-Type': 'application/json' } })

        .then(response => response.json())
        .then((responseJson) => {
            if (responseJson) {
                this.setState({ subgroup: responseJson.data });
            }
            this.setState({ loading: false })

        })
        .catch(error => {
            alert(error);
            this.setState({ loading: false })
        })
}

getModels = (id) => {
    var url = constants.base_url + 'getModels';
    url = url + "?access_token=" + this.state.token + '&sub_product_group_id=' + id;
    fetch(url, { headers: { 'Content-Type': 'application/json' } })
        .then(response => response.json())
        .then((responseJson) => {
            if (responseJson.status) {
                if (responseJson.data.length) {
                  // alert(JSON.stringify(responseJson))
                    this.setState({
                        models: responseJson.data,
                    })
                } else {
                    alert('No models present for the selected Appliance. ');
                }
            }
            this.setState({ loading: false })

        })
        .catch(error => {
            alert(error);
            this.setState({ loading: false })
        })
}
genFrame(style, length) {
  style.height = length > 7 ? 200 : - 1;
  style.left = Dimensions.get("screen").width*0.10;
  style.width = Dimensions.get('screen').width*0.80;
  // style.left = Dimensions.get("screen")*0.10;
  return style;
  }
  render() {
        var StateName = [];
        if(this.state.StateName.length >0){
        this.state.StateName.forEach((item,index)=>{

          StateName.push(item.state_name)
        })
      }else{
        StateName = [];
      }

      var CityName = [];
      if(this.state.City.length >0){
      this.state.City.forEach((item,index)=>{

        CityName.push(item.city_name)

      })
    }else{
      CityName = [];
    }
              var Area = [];
              if(this.state.Area.length > 0){
              this.state.Area.forEach((item,index)=>{
                Area.push(item.area_name)
              })
            }else{
              Area = [];
            }

      let item = this.props.navigation.state.params?this.props.navigation.state.params.productDetails:null;
      let showNote = false;
      if(item){
        showNote = item.warranty_type?item.warranty_type.toLowerCase()=='out of warranty'?true:false:false;
      }else{
        let warranty_type = this.state.warranty;
        showNote = warranty_type && warranty_type.toLowerCase()=='out of warranty'?true:false;
      }
      // console.warn(item)
      // console.warn(this.state.Myaddresses)
      
    return (
      <View style={{flex:1, backgroundColor:"#edf4f6",justifyContent:'flex-start'}} >
       <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
       {this.state.loading?<Loader/>:null}
       
       <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#edf4f6', }}>
         <View style={{marginTop:StatusBar.currentHeight, flex:1,justifyContent:'flex-start', alignItems:'center',paddingBottom:15,}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:item?-820:-900,
            height:1000, 
            width:1000,
            borderRadius:500,
            position:'absolute'
          }}
            >
          </LinearGradient>
             <View style={{flexDirection:'row', justifyContent:'flex-start',alignItems:'center', padding:5, paddingTop: Platform.OS == 'ios'? 20:0, marginTop:15}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1 }} >
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'#e9eaff',fontFamily:'RubikMedium', fontSize:20, textAlign:'center', marginLeft:-30}}>SERVICE REQUEST</Text>
              </View>
              
              {/* PRODUCT DETAILS */}
              {item?
                <View style={{flex:1,borderRadius:5,alignItems:'stretch', backgroundColor:'white', padding:15, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,flexDirection:'row', alignSelf:'stretch',margin:15}}>
                  <View style={{ height:120,width:100, borderRadius:5, padding:10, borderColor:'#f0f0f0', borderWidth:2, }}>
                  {
                          item.product_location?
                        <View style={{backgroundColor:'blue',position:'absolute', bottom:-10, left:-10,height:30, width:30, alignItems:'center', justifyContent:'center', borderRadius:15}}>
                        <Image source={
                          item.product_location.toLowerCase()=='kitchen'?
                        require('../../assets/ic_home/kitchen_light.png'):item.product_location.toLowerCase()=='bedroom'?
                        require('../../assets/ic_home/bedroom_light.png'):item.product_location.toLowerCase()=='bathroom'?
                        require('../../assets/ic_home/bathroom_light.png'):item.product_location.toLowerCase()=='studyroom'?
                        require('../../assets/ic_home/studyroom_light.png'):require('../../assets/ic_home/waitingroom_light.png')} style={{height:25, width:25, resizeMode:'center',}}/>
                        </View>:null
                        
                      }
                    <Image source={{uri:item.image}} style={{resizeMode:'contain',height:'100%', width:'100%'}}/>
                  </View>
                  <View style={{height:120, marginLeft:10, padding:10,justifyContent:'center',flex:1,  }}>
                    <Text style={{color:'#3875c9', fontFamily:'RubikBold', fontSize:17, marginBottom:5}}>{(item.product_name+'').toUpperCase()}</Text>                
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikMedium', fontSize:15}}>{item.sub_product_name+''}</Text>                
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Model No:{' '+item.model_name} </Text> 
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Serial No:{' '+item.serial_no} </Text> 
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Brand: {item.brand_name} </Text>
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Purchase Date: {authFun.getDate(item.order_type=='amc'?item.payment_date_time:item.purchase_date)} </Text>
                    <Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Warranty: {item.warranty_type} </Text>                
                    {/*item.amc_days?<Text style={{color:'#198816', fontFamily:'RubikMedium', fontSize:15}}>{item.amc_days>0?"Buy AMC: ":""}{item.amc_days==0?"AMC Expired":item.amc_days+" Days Left"} </Text>:null*/}     
                  </View>
                </View>:null
              }
                       
         </View>
         {!item?
         <View style={{marginBottom:this.state.acceptRequest?0:15,marginHorizontal:15}}>
          <Text style={{marginLeft:12, marginBottom:5,marginTop:item?0:70,  fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Select Brand*</Text>
              <TouchableOpacity activeOpacity={1} onPress={()=>this.productGroupSelector.show()} 
                style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, justifyContent:'center',}}>
                    <ModalDropdown
                        ref={(ref)=>this.brandSelector = ref}
                        defaultValue={'Select Brand'}
                        defaultIndex ={-1}
                        options={this.state.allowed_brands.map((item)=>item.brand_name)}
                        onSelect={(rowData,index) =>{ 
                            this.getCategory(this.state.allowed_brands[rowData].brand_id);
                        }}
                        dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                        textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                        animated={ true}
                        adjustFrame={style => this.genFrame(style, this.state.allowed_brands.length)}
                        dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20,}}
                        dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                      />
                      <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>
         </View>
         :null}
          {/* Main Content Screen 1 */}
          { this.state.acceptRequest?
            <View>  
                <View style={{flex:1, margin:15,}}>
                
                {!item?
                  <View style={{flex:1, }}>
                     
                    <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Product Group*</Text>
                  <TouchableOpacity activeOpacity={1} onPress={()=>this.productGroupSelector.show()} 
                   style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, justifyContent:'center',}}>
                        <ModalDropdown
                            ref={(ref)=>this.productGroupSelector = ref}
                            defaultValue={'Select Product Group'}
                            defaultIndex ={-1}
                            options={this.state.group.map((item)=>item.product_name)}
                            onSelect={(rowData,index) =>{ 
                              {
                                let itemID = this.state.group[rowData].product_group_id;
                                let temp = this.state.selected;
                                temp[1].val=0;
                                this.setState({ modelNum: '', serialNum: '', filterValues: [], selected:temp })
                                    this.setValue(1, 'group', itemID);
                                    this.productCategorySelector.select(-1);
                                    
                                    this.getAppliance(itemID);
                            }
                              // this.setState({warranty:this.state.warrantyTypes.filter((item,index)=>index==rowData)[0].name});  
                            }}
                            dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                            textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                            animated={ true}
                            adjustFrame={style => this.genFrame(style, this.state.group.length)}
                            dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                            dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                          />
                          <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                        </TouchableOpacity>
                        <Text style={{marginLeft:12,marginTop:15, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Product Category*</Text>
                        <TouchableOpacity activeOpacity={1} onPress={()=>this.productCategorySelector.show()} 
                        style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, justifyContent:'center',}}>
                              <ModalDropdown
                                  ref={(ref)=>this.productCategorySelector = ref}
                                  defaultValue={'Select Product Category'}
                                  defaultIndex ={-1}
                                  options={this.state.subgroup.map((item)=>item.sub_product_name)}
                                  onSelect={(rowData,index) =>{ 
                                    {
                                      let itemID = this.state.subgroup[rowData].sub_product_group_id;
                                      this.setState({ modelNum: '',  filterValues: [] })
                                      this.setValue(1, 'subgroup', itemID);
                                      this.getModels(itemID);
                                  }
                                    // this.setState({warranty:this.state.warrantyTypes.filter((item,index)=>index==rowData)[0].name});  
                                  }}
                                  dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                                  textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                                  animated={ true}
                                  adjustFrame={style => this.genFrame(style, this.state.subgroup.length)}
                                  dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                                  dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                                />
                                <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                              </TouchableOpacity>

                  <Text style={{marginLeft:12,  fontFamily:'RubikRegular', fontSize:15, color:'#707171',marginTop:15, marginBottom:5 }}>Model Name</Text>
                <TextInput autoCorrect={false}
                   style={{flex:1, padding:5,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5 }}
                    contextMenuHidden
                    placeholder="Model Name"
                    //  onEndEditing={()=>this.alterNum.focus()}
                    onChangeText={value => {
                      const modelNumReg = /^[0-9A-Za-z-]*$/;
                      if(modelNumReg.test(value)){
                          this.setState({ modelNum: value}, () => {
                            // this.state.brand[this.state.selected[0].val].toLowerCase()=='E-warranty' && 
                            if (this.state.models.length && this.state.modelNum != '') {
                                this.setState({ filterValues: this.state.models.filter((item) => item.model_name.toLowerCase().indexOf(this.state.modelNum.toLowerCase()) != -1) })
                            }
                        });
                      }
                        
                    }
                    }
                    value={this.state.modelNum}
                    ref={input => this.model = input}
                />
                
                {
                    (this.state.filterValues.length && this.state.filterValues.length == 1 && this.state.filterValues.filter((val) => val == this.state.modelNum).length == 0)
                        || this.state.filterValues.length && (this.state.filterValues.length != 1) ?
                        <View style={{ backgroundColor: 'white', width: '100%', alignItems: 'center', marginTop: 10, borderRadius: 10, padding: 5 }}>
                            {
                                this.state.filterValues.map((item, index) =>
                                    <TouchableOpacity key={index} onPress={() => {
                                        this.setState({
                                            modelNum: item.model_name, warrantyPeriod: item.warranty_period,
                                            filterValues: [item.model_name]
                                        })
                                    }} style={{ width: '100%' }}>
                                        <Text style={{ fontFamily: 'RubikRegular', fontSize: 15, textAlign: 'center', width: '100%' }} >
                                            {item.model_name}
                                        </Text></TouchableOpacity>)
                            }

                        </View> : null
                }
                <Text style={{marginLeft:12,  fontFamily:'RubikRegular', fontSize:15, color:'#707171',marginTop:15, marginBottom:5 }}>Serial Number</Text>
                <TextInput autoCorrect={false}
                   style={{flex:1, padding:5,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5 }}
                    contextMenuHidden
                    placeholder="Serial Number"
                    //  onEndEditing={()=>this.alterNum.focus()}
                    onChangeText={value => {
                      const serialNumReg = /^[0-9A-Za-z-]*$/;
                        if(serialNumReg.test(value))
                          this.setState({ serialNum: value });
                    }
                    }
                    value={this.state.serialNum}
                    ref={input => this.serial = input}
                />
                   <Text style={{marginLeft:12,  fontFamily:'RubikRegular', fontSize:15, color:'#707171',marginTop:15 }}>Date of Purchase*</Text>
             <View style={{backgroundColor:'white', color:'#707171', borderRadius:30, textAlign:'left', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4, 
                marginBottom:15,marginTop:5, borderWidth:0, flex:1, alignItems:'stretch', 
               flexDirection:'column', justifyContent:'center'}}>
             <DatePicker
              style={{width:'100%',}}
              date={this.state.purchase_date}
              mode="date"
              maxDate={new Date()}
              format="DD/MM/YYYY"
              placeholder="Select Date"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"  
              onDateChange={(purchase_date)=>this.setState({purchase_date})}   
              iconSource={require('../../assets/ic_home/cal.png')}                                                                                                                                                                                                                                   
              customStyles={{dateIcon: {position: 'absolute',right: 5,resizeMode:'contain',top: 10,height:15,width:15,marginLeft: 0},
                dateInput:{borderWidth:0},
                placeholderText:{fontSize:15,padding: 10,color:'#707171',alignSelf:'flex-start'},
                dateText:{fontSize:15,padding: 10,alignSelf:'flex-start'}
              }}
            />
             </View>
               
               

                </View>
                :null
              }
              {!item?
              <View>
                <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Warranty*</Text>
                <TouchableOpacity activeOpacity={1} onPress={()=>this.warrantySelector.show()} style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, justifyContent:'center'}}>
                    <ModalDropdown
                      ref={(ref)=>this.warrantySelector = ref}
                      defaultValue={'Select Warranty Type'}
                      defaultIndex ={-1}
                      options={this.state.warrantyTypes.map((item)=>item.name)}
                      onSelect={(rowData,index) =>{ this.setState({warranty:this.state.warrantyTypes.filter((item,index)=>index==rowData)[0].name});  }}
                      dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}
                      textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                      animated={ true}
                      adjustFrame={style => this.genFrame(style, this.state.warrantyTypes.length)}
                      dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                      dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                    />
                    <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                  </TouchableOpacity>
              </View>:null}
            
                  <View style={{flex:1,justifyContent:'space-between', marginTop:15}}>

                  <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Request Type*</Text>
                    <TouchableOpacity activeOpacity={1} onPress={()=>this.jobSelector.show()} style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1,  justifyContent:'center'}}>
                        <ModalDropdown
                            ref={(ref)=>this.jobSelector = ref}
                            defaultValue={'Select Request Type'}
                            defaultIndex = {-1}
                            options={this.state.jobTypes.map((item)=>item.name)}
                            onSelect={(rowData,index) =>{ 
                              let selectedJob = (this.state.jobTypes.filter((item,index)=>index==rowData)[0].name);
                              this.setState({job:selectedJob, showPMSNote:((selectedJob.toLowerCase().indexOf('preventive')!=-1) && (selectedJob.toLowerCase().indexOf('service')!=-1))?true:false});
                            }}
                            dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}
                            textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                            animated={ true}
                            adjustFrame={style => this.genFrame(style, this.state.jobTypes.length)}
                            dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                            dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                          />
                          <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                        </TouchableOpacity>  
        
                  </View>

          <View style={{flex:1, flexDirection:'row', justifyContent:'space-between',marginTop:15}}>
            
                
          <View style={{flex:1,justifyContent:'space-between',}}> 
          <Text style ={{color:'#707171',marginLeft:10}}> Preferred Date</Text>
             <View style={{backgroundColor:'white', color:'#707171', borderRadius:30, textAlign:'left', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4, 
                marginBottom:15,marginTop:5, borderWidth:0, flex:1, alignItems:'stretch', 
               flexDirection:'column', justifyContent:'center'}}>
             <DatePicker
             style={{width:'100%',}}
              date={this.state.date}
              mode="date"
              minDate={new Date()}
              format="DD/MM/YYYY"
              placeholder="Select Date"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"  
              onDateChange={(date)=>this.setState({date})}   
              iconSource={require('../../assets/ic_home/cal.png')}                                                                                                                                                                                                                                   
              customStyles={{dateIcon: {position: 'absolute',right: 5,resizeMode:'contain',top: 10,height:15,width:15,marginLeft: 0},
                dateInput:{borderWidth:0},
                placeholderText:{fontSize:15,padding: 10,color:'#707171',alignSelf:'flex-start'},
                dateText:{fontSize:15,padding: 10,alignSelf:'flex-start'}
              }}
            />
             </View>
             </View>

             <View style={{flex:1,justifyContent:'space-between',}}> 
            <Text style ={{color:'#707171',marginLeft:10}}>Preferred Time</Text>

             <View style={{backgroundColor:'white', color:'#707171', borderRadius:30, textAlign:'left', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4, 
                marginBottom:15,marginTop:5, borderWidth:0, flex:1, alignItems:'stretch', marginLeft:10,
               flexDirection:'column', justifyContent:'center'}}>
             <DatePicker
             style={{width:'100%',}}
              date={this.state.time}
              mode="time"
              is24Hour={true}
              onDateChange={(time)=>this.setState({time})}
              placeholder="Select Time"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"     
              iconSource={require('../../assets/ic_home/cal.png')}                                                                                                                                                                                                                                   
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  right: 5,
                  resizeMode:'contain',
                  top: 10,
                  height:15,
                  width:15,
                  marginLeft: 0
                },
                dateInput:{
                  borderWidth:0
                },
                placeholderText:{
                  fontSize:15,
                  padding: 10,
                  color:'#707171',
                  alignSelf:'flex-start'
                },
                dateText:{
                  fontSize:15,
                  padding: 10,
                  alignSelf:'flex-start'
                }
              }}/>
             </View>

             </View>
             
              </View>
              <View style={{flex:1,justifyContent:'space-between',marginTop:15, }}> 
            <Text style ={{color:'#707171',marginLeft:10}}> Details of the Request*</Text>
             
                    <TextInput
                    style={{backgroundColor:'white',color:'#707171', borderRadius:10, textAlign:'left',textAlignVertical:'top', 
                    fontFamily:'RubikRegular',fontSize:15,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, padding:10, marginTop:5,paddingHorizontal: 10,height:100}}
                    multiline={true}
                    onChangeText={(value)=>this.setState({remark:value})}
                    numberOfLines={4}
                    autoCorrect={false}
                    placeholder=""
                    contextMenuHidden
                    textContentType="streetAddressLine1"/>
                </View>
              </View>
             
        
        {/* address area */}
               
        <View style={{alignSelf:'stretch', flexDirection: 'row', alignItems:'center',  margin:15, marginBottom:0}}>
          {this.state.Myaddresses.length?<TouchableOpacity onPress={() =>{this.setState({oldAddress:true,});this.onAfterSubmit(false);}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:this.state.oldAddress?'#c4c3c1':'transparent'}}>
              <Text  style={{color: this.state.oldAddress?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                {"Choose Your Address"}
              </Text>
          </TouchableOpacity>:null}
          <TouchableOpacity onPress={() =>{this.setState({oldAddress:false,});this.onAfterSubmit(false);}}  style={{flex:1, borderRadius:25, padding:15, borderWidth:0.5, borderColor:'#c4c3c1', marginRight:7.5, backgroundColor:(!this.state.oldAddress || !this.state.Myaddresses.length)?'#c4c3c1':'transparent'}}>
              <Text  style={{color:(!this.state.oldAddress || !this.state.Myaddresses.length)?'white':'#c4c3c1',fontSize: 12,fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                {"Add New Address"}
              </Text>
          </TouchableOpacity>
        </View> 
       
        <View style={{marginTop:15}}>
            
          <View style={{marginTop:10, flex:1, alignSelf:'stretch',  }}>
            {
              this.state.oldAddress && this.state.Myaddresses.length?<FlatList
                    data = {this.state.Myaddresses}
                    extraData={this.state}
                    keyExtractor={(index,item)=>index.toString()}
                    renderItem = {({item,index}) =>{
                      return(
                        <TouchableOpacity key={index} activeOpacity={0.7} onPress={()=>this.setState({SelectedAdressID:item.customer_address_id,SelectedAddress:[item]})} style={{borderRadius:5, flexDirection:'row',justifyContent:'flex-start',alignItems:'center',marginHorizontal:15,marginVertical:10 }}>
                          <View style={{flex:1, backgroundColor:'white', padding:15, borderRadius:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5}}>
                            <Text style={{fontFamily:'RubikMedium', fontSize:15,color:'black'}}>{item.address_type?item.address_type.toUpperCase():''}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.state_name}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.city_name}, {item.pincode}</Text>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15,}}>{item.address}, {item.area_name} {item.near_by != ""? ", "+item.near_by:null}</Text>
                          </View>
                          <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center', marginHorizontal:15}}>
                            <View style={{backgroundColor:this.state.SelectedAdressID == item.customer_address_id?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                          </View>
                        </TouchableOpacity>
                        
                       )}
                  }
                />:
                <View style={{marginHorizontal:15, marginBottom:10, flex:1, alignItems:'stretch', alignSelf:'stretch'}}>
                  
                {/* Main address */}
                <View style={{ borderRadius:5, alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginTop:10, backgroundColor:'white'}}>

                  <TextInput
                      placeholder="Enter Address"
                      onChangeText={password => this.setState({address : password})}
                      numberOfLines = {3}
                      autoCorrect={false}
                      textAlignVertical={'top'}
                      value={this.state.address}
                      underlineColorAndroid='transparent'
                      style={{fontFamily: 'RubikRegular',
                      color:'#707171',
                      fontSize: 15,
                      height:110,
                      alignItems: 'flex-start',
                      justifyContent: 'flex-start',
                      width:'95%', paddingLeft:10}}
                      multiline
                    />
                  </View>


                  {/* Address Main */}
                  {/* State dropdown textinput */}
                  <View style={{flexDirection:'row', flex:1, marginTop:10}}>
                    
                  <View style={{flex:1}}>
                      <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Postal Code</Text>
                      <View style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginLeft:5,}}>
                        <TextInput 
                          autoCorrect={false}
                          placeholder={"Pincode"} value={this.state.pincode}
                          maxLength={6}
                          onEndEditing={()=>{this.pincodeChanged()}}
                          onChangeText={(value) =>{
                                let num = value.replace(".", '');
                                  if(isNaN(num)){
                                      // Its not a number
                                  }else{
                                      this.setState({pincode:num})}}
                                  }
                                  placeholderTextColor={"#707171"}
                          keyboardType={'numeric'}
                        editable 
                        style={{flex:1, padding:0,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white' }}/>
                      </View>
                    </View>
                     
                    <View style={{flex:1, marginLeft:10}}>
                      <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>State</Text>
                      <TouchableOpacity activeOpacity={1} onPress={()=>this.stateSelector.show()} style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginRight:5, justifyContent:'center'}}>
                        <ModalDropdown
                            ref={(stateSelector)=>this.stateSelector = stateSelector}
                            defaultValue={this.state.stateName?this.state.stateName:'Select State*'}
                            defaultIndex ={-1}
                            options={StateName}
                            onSelect={(rowData,index) =>{ this.getData("CITY", this.state.StateName[rowData].state_id);this.setState({stateName:this.state.StateName[rowData].state_name,SelectedCity:"Select City", cityname:'',area_name:'', pincode:''}); this.lookaheadFilter.select(-1);this.lookaheadFilter2.select(-1) }}
                            dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                            textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                            animated={ true}
                            adjustFrame={style => this.genFrame(style, StateName.length)}
                            dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                            dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                          />
                          <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                        </TouchableOpacity>    
                    </View>
                     </View>
                  {/* pincode and area selector row */}
                  <View style={{flexDirection:'row', flex:1, marginTop:15}}>
                    
                  <View style={{flex:1}}>
                      <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>City</Text>
                      <TouchableOpacity activeOpacity={1} onPress={()=>this.lookaheadFilter.show()} style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginLeft:5, justifyContent:'center'}}>
                        <ModalDropdown
                          ref={ (ref) => this.lookaheadFilter = ref}
                          defaultValue={this.state.cityname?this.state.cityname:'Select City*'}
                          onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.City[rowData].city_id);this.setState({cityname:this.state.City[rowData].city_name, area_name:'', pincode:''}); this.lookaheadFilter2.select(-1); }}
                          options={CityName}
                          adjustFrame={style => this.genFrame(style, CityName.length)}
                          dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                          textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                          dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                          dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                          />
                          <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                        </TouchableOpacity>
                      </View>
                 
                    <View style={{flex:1, marginLeft:10}}>
                    <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171'}}>Area</Text>
                      <TouchableOpacity activeOpacity={1} onPress={()=>this.lookaheadFilter2.show()} style={{ borderRadius:25, backgroundColor: 'white',justifyContent:'center', alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, marginRight:5,}}>
                        <ModalDropdown
                            ref={ (ref) => this.lookaheadFilter2 = ref}
                            defaultValue={this.state.area_name?this.state.area_name:"Select Area"}
                            onSelect={(rowData,index) =>{ this.getData("PINCODE", this.state.Area[rowData].city_id);this.setState({pincode:this.state.Area[rowData].postal_code?this.state.Area[rowData].postal_code:this.state.pincode,area_name:this.state.Area[rowData].area_name});   }}
                            options={Area}
                            dropdownTextStyle={{width:'100%',backgroundColor: '#fff',fontSize:17, fontFamily: 'RubikRegular'}}/*Change font family here*/
                            style={{ borderRadius:25, backgroundColor: 'white', alignItems:'stretch', flex:1, }}
                            adjustFrame={style => this.genFrame(style, Area.length)}
                            textStyle={{fontSize: 15,fontFamily: 'RubikRegular',textAlign:'left', flex:1,paddingHorizontal:12, paddingVertical:10, borderRadius:25}}
                            dropdownStyle={{flex:1, alignSelf:'stretch',fontFamily: 'RubikRegular', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5, marginTop:-20}}
                            dropdownTextHighlightStyle={{color:"#6364e7",fontFamily: 'RubikRegular'}}
                          />
                          <Image source={require('../../assets/arrowblack.png')} style={{position:'absolute', tintColor:'gray', height:8, width:13, resizeMode:'contain', right:12,}}/>
                      </TouchableOpacity>
                    </View>
                      
                  </View>
                  
                  {/* Address Type */}
                  <FlatList
                    data = {this.state.addressType}
                    extraData={this.state}
                    horizontal
                    renderItem = {({item}, index) =>
                    <TouchableOpacity key={index}  onPress={() => { this.setState({selectedAddressType:item.firstName}) }} activeOpacity = {0.5} 
                    style={{marginRight: 10,marginBottom:5, flexDirection: 'row',justifyContent: 'space-between',alignItems: 'center',marginTop:15}}>
                      <View style={{backgroundColor: "white",height: 20,width: 20,borderRadius: 10,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 4,justifyContent: 'center'}}>
                        <View style={{backgroundColor:this.state.selectedAddressType == item.firstName?"#6364e7":"white",height: 10,width: 10,alignSelf: 'center',borderRadius:5}}/>
                      </View>
                      <Text style={{
                        color: 'gray',
                        fontSize: 15,
                        marginLeft: 10,
                        fontFamily: "RubikBold"
                      }}>{item.firstName}</Text>
                    </TouchableOpacity>
                  }
                />
                 {/* landmark */}
                 <Text style={{marginLeft:12, marginBottom:5, fontFamily:'RubikRegular', fontSize:15, color:'#707171', marginTop:10}}>LandMark</Text>
                 <View style={{ borderRadius:25, alignItems:'stretch',shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5, flex:1, }}>
                    <TextInput
                      autoCorrect={false}
                      placeholder="Landmark"
                      placeholderTextColor= "#707171"
                      maxLength = {200}
                      onChangeText={(value) => this.setState({landmark:value})}
                      value={this.state.landmark}
                      style={{flex:1, paddingVertical:5,borderRadius:25,fontSize: 15, paddingHorizontal:15, fontFamily: 'RubikRegular',color: "gray",backgroundColor:'white' }}/>
                  </View>

                </View>
                }
            </View>
      
        </View>
       
     
        {showNote || this.state.showPMSNote? <Text style={{fontFamily:"RubikRegular", fontSize:15, marginHorizontal:15,  textAlign:'center', color:'red', textDecorationLine:'underline'}}>Note: Services charges may apply for out of warranty or preventive maintenance services.</Text>:null}
        <TouchableOpacity onPress={()=>{this.submitRequest();}  }  style={{ flex:1,borderRadius:5, alignSelf:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,marginBottom:20, marginTop:10}}>
            <LinearGradient style={{  borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center' , padding:15}}
                        colors={["#ff619e", "#ff9fa6"]}>
                      <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                        SUBMIT
                      </Text>
              </LinearGradient>
          </TouchableOpacity>
          </View>
      :<Text style={{fontFamily:'RubikMedium', fontSize:15, marginHorizontal:20, textAlign:'center'}}>Service Request cannot be raised for other brands.</Text>}   
        </ScrollView>
        
      <Modal animationType = {"slide"} transparent = {false} visible = {this.state.successModal} onRequestClose = {() => { this.props.navigation.navigate('Main') } }>
        <TouchableOpacity onPress={()=> this.props.navigation.goBack(null)} style={{flex:1}}>
          <LinearGradient style={{flex:1, justifyContent:'center', alignItems:'center'}} colors={["#5960e5", "#b785f8","#b785f8"]}  useAngle={true} angle={45} angleCenter={{x:0, y:0}}>
                    <View style={{justifyContent:'center', alignItems:'center', height:200, width:200}}>
                      <Image style={{resizeMode:'contain', height:'100%', width:'100%', position:'absolute'}} source={this.state.failModal?require('../../assets/ic_home/payment_failed.png'):require('../../assets/ic_home/finished_add_prod.png')} />
                    </View>
                      <Text style={{fontFamily:'RubikBold', fontSize:35, color:'#e9eaff', textAlign:'center'}}>THANKS </Text>
                      <Text style={{fontFamily:'RubikMedium', fontSize:20, color:'#e9eaff', textAlign:'center'}}>FOR REGISTERING THE CALL</Text>
                      {this.state.JobId?
                       <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#e9eaff', textAlign:'center', marginTop:20}}>Job Id: {this.state.JobId}</Text> 
                      :<Text style={{fontFamily:'RubikMedium', fontSize:15, color:'#e9eaff', textAlign:'center', marginTop:20}}>Your request has been submitted, your job id will be shared through sms.</Text>
                      }
                      <TouchableOpacity onPress={()=>NavigationService.navigate('Main')} style={{alignSelf:'stretch',  justifyContent:'center', alignItems:'center', flexDirection:'row', marginTop:20}}>
                        <Image source={require('../../assets/ic_home/house_dark.png')} style={{resizeMode:'center', height:25, width:25, tintColor:'white', }}/>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'#e9eaff', textAlign:'center',textDecorationLine:'underline', marginLeft:5}}>Back to Home</Text>
                      </TouchableOpacity>
                </LinearGradient>
        
        </TouchableOpacity>
        </Modal>


      </View>
    );
  }
}

ServiceDetail.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ServiceDetail.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiceDetail);
