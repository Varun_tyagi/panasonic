/* eslint-disable react/no-unused-prop-types */
/* eslint-disable import/first */
/* eslint-disable import/extensions */
/* eslint-disable no-unused-vars */
/* eslint-disable import/order */
/* eslint-disable import/no-unresolved */
/* eslint-disable react/jsx-indent */
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  AsyncStorage,Modal
  // eslint-disable-next-line import/first
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
// eslint-disable-next-line import/order
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import AmcList from '../AmcList';
import { drawer } from '../navigation/AppNavigator';
import Swiper from 'react-native-swiper';
import * as constants from '../../helpers/constants';
import * as authFun from '../../helpers/auth';
import Loader from '../common/Loader';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import PDFView from 'react-native-view-pdf';
import { createNavigator, createNavigationContainer, StackRouter, addNavigationHelpers, StackActions, } from 'react-navigation';
import NavigationService from '../navigation/NavigationService';

const widthFull = Dimensions.get('window').width;

class MyAmc extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      token: '',
      customerID: '',
      mainData: [],
      BannerdataSource: [],
      fileB64:'',
      showFile:false,
      activeSlide: 0,
      filterOptions: [{ label: 'MY AMC', image: require('../../assets/ic_home/warranty.png'), data: [], selected: true, press:'' },
      { label: 'ABOUT TO EXPIRE', image: require('../../assets/ic_home/aboutto_active.png'), data: [], selected: false, press:'' },
      { label: 'EXPIRED', image: require('../../assets/ic_home/expired_active.png'), data: [], selected: false, press:'' },
      // { label: 'MY ORDER STATUS', image: require('../../assets/ic_home/myorder_active.png'), data: [], selected: false, press:'navigate' }
    ]
    }
    this.capitalize = this.capitalize.bind(this);
    this.getDate = this.getDate.bind(this);
    this.productAdded = this.productAdded.bind(this);
    this.fetchAppliance = this.fetchAppliance.bind(this);
  }

  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  productAdded = async () => {
    const result = await AsyncStorage.getItem('productAdded');
    if (result == 1) {
      await AsyncStorage.removeItem('productAdded');
      return true;
    } else {
      return false;
    }
  }
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
      }, () => {
        // this.getData();
        
        this.fetchAppliance();
      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken = async () => {
    var token = await authFun.checkAuth();
    this.setState({ token: token })
    return token;
  }

 async fetchAppliance() {

    await this.setToken();
    let temp = this.state.filterOptions;
    temp = temp.map((item)=>{item.selected=false;return item;});
    temp[0].selected = true;
    this.setState({loading:true, filterOptions:temp});
        var location = 'getApplianceData?customer_id='+this.state.customerID
        var url = constants.base_url+location;
        // var query = id!=0?name=='state'?'&country_id='+'101':name=='city'?'&state_id='+id:'&city_id='+id:'';
        url=url+"&access_token="+this.state.token;
        // alert(url)
        fetch(url,{headers: {'Content-Type':'application/json'}})
            .then(response => response.json())
            .then((responseJson)=> {
                console.warn(responseJson.customer_products[0])
            if (responseJson.status) {
              let allowed_brands = ['panasonic', 'sanyo'];
                // alert(JSON.stringify(Object.values(JSON.parse(responseJson.data.plan))))
                
                
                this.setState({
                  BannerdataSource: responseJson.banners,
                  mainData:responseJson.customer_products.filter((item)=>(allowed_brands.indexOf(item.brand_name.toLowerCase())!=-1 && (item.WarrantyStartDate || item.WarrantyExpiryDate)))
                }
                  ,()=>{
                  var temp = this.state.filterOptions;  
                  temp[0].data = this.state.mainData;
                  temp[1].data = this.state.mainData.filter((item)=>item.amc_days<30 && item.amc_days!=0);
                  temp[2].data = this.state.mainData.filter((item)=>item.amc_days==0);
                  // temp[3].data = this.state.mainData;
                  this.setState({loading:false, filterOptions:temp});
                    }
                );
              } else {

                this.setState({ loading: false })
              }
            
            }).catch((error) =>{
              NetInfo.fetch().then(async (state)=>{
                if(!state.isConnected){
                  alert('Please check your internet connection.');
                }else{
                  alert(error);
                }
              })
              this.setState({loading:false})
            }) 
    // this.setState({ loading: true }, () => {
    //   var url = constants.base_url + 'getcustomer_order';
    //   url = url + "?access_token=" + this.state.token + '&customer_id=' + this.state.customerID + '&order_type=amc';
    //   fetch(url, { headers: { 'Content-Type': 'application/json' } })

    //     .then(response => response.json())
    //     .then((responseJson) => {
    //       // progressOff();a

    //       // alert(JSON.stringify(responseJson))
    //       if (responseJson.status == 1) {
    //         // alert(JSON.stringify(Object.values(JSON.parse(responseJson.data.plan))))
    //         this.setState({
    //           BannerdataSource: responseJson.banners,
    //            mainData:responseJson.data
    //         }
    //           ,()=>{
    //           var temp = this.state.filterOptions;  
    //           temp[0].data = this.state.mainData;
    //           temp[1].data = this.state.mainData.filter((item)=>item.amc_days<30);
    //           temp[2].data = this.state.mainData.filter((item)=>item.amc_days==0);
    //           temp[3].data = this.state.mainData;
    //           this.setState({loading:false, filterOptions:temp});
    //             }
    //         );
    //       } else {

    //         this.setState({ loading: false })
    //       }

    //     })
    //     .catch(error => console.log(error))
    // })

  }


  getData = () => {
    // https://www.ecarewiz.com/ewarrantyapi/getApplianceData?customer_id=1
    this.setState({ loading: true });
    var location = 'getApplianceData?customer_id=' + this.state.customerID
    var url = constants.base_url + location;
    // var query = id!=0?name=='state'?'&country_id='+'101':name=='city'?'&state_id='+id:'&city_id='+id:'';
    url = url + "&access_token=" + this.state.token;
    fetch(url, { headers: { 'Content-Type': 'application/json' } })
      .then(response => response.json())
      .then((responseJson) => {

        if (responseJson.status) {
          // alert(JSON.stringify(responseJson.customer_products));

          this.setState({ mainData: responseJson.customer_products }, () => {
            var temp = this.state.filterOptions;
            temp[0].data = this.state.mainData;
            temp[1].data = this.state.mainData;
            temp[2].data = this.state.mainData;
            // temp[3].data = this.state.mainData;
            this.setState({ loading: false, filterOptions: temp });
          });

        } else {
          // alert(JSON.stringify(responseJson))
        }
        this.setState({ loading: false })

      })
      .catch(error => {
        // alert(error);
        this.setState({ loading: false })
      })
  }


  async componentDidMount() {
    this.props.navigation.addListener('willFocus', async () => {
      var update = await this.productAdded();
        this.loadData();
    });
    // alert(JSON.stringify(this.props))
    var token = await this.setToken();
    if (token) {
      this.loadData();
    }

  }

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example * */
    this.forceUpdate();
  };
  // eslint-disable-next-line class-methods-use-this

  capitalize = (txt) => {
    if(txt)
    return txt.split(' ').map((word) => word.split('')[0].toUpperCase() + word.split('').splice(1).join('')).join(' ');
    else
    return '';
  }
  getDate = (dt) => {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')

    return dt[2] + ' ' + months[dt[1][0] == 0 ? dt[1][1] : dt[1]] + ' ' + dt[0];
  }

  selectedStyle = (selected) => {
    if (selected) {
      return {
        flex: 1, backgroundColor: 'white', height: 70, width: 70, alignSelf: 'center', borderRadius: 5, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,
        marginRight: 10, padding: 10, justifyContent: 'flex-start', opacity: 1
      }
    } else {
      return {
        flex: 1, backgroundColor: 'white', height: 70, width: 70, alignSelf: 'center', borderRadius: 5, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5,
        marginRight: 10, padding: 10, justifyContent: 'flex-start', opacity: 0.8
      }
    }
  }
  get pagination() {
    const {BannerdataSource, activeSlide} = this.state;
    return (<Pagination dotsLength={BannerdataSource.length} activeDotIndex={activeSlide} 
      dotStyle={{
        width: 8,
        height: 8,
        borderRadius: 5,
        borderWidth:1,
        borderColor:'#5960e5',
        backgroundColor: "#5960e5"
      }} inactiveDotStyle={{
        width: 10,
        height: 10,
        borderWidth: 1,
        backgroundColor: "transparent",
        borderColor: '#5960e5',
        // Define styles for inactive dots here
      }} inactiveDotOpacity={1} inactiveDotScale={1} />);
  }

  getWarrantyPeriod(product){
    let mainProduct = (Object.values(JSON.parse(product.plan))[0]);
    if(Object.prototype.toString.call(mainProduct).indexOf('Array')!=-1){
      mainProduct = mainProduct[0];
    }
    // console.warn('product : ',mainProduct, ':::::::', product)
    // return 'xyz';

      let warranty = mainProduct.WarrantyPeriod 
      if(warranty){
        warranty = parseInt((warranty.split(' ')[0])/12);
        let txt = warranty>1?' Years EWC':' Year EWC';
        return warranty+txt;
      }
  }


  getCertificate = async (ewcID)=>{
    console.warn('called')
    this.setState({loading:true});
    await this.setToken();
    var url = constants.base_url+'getCertificate';
    url=url+"?access_token="+this.state.token+'&EWCId='+ewcID;
    fetch(url,{headers: {'Content-Type':'application/json'}})
  
        .then(response => response.json())
        .then((responseJson)=> {
          if(responseJson.status){
            this.setState({fileB64:responseJson.data, showFile:true, });
          }else{
            alert('PDf could not be fetched.');
          }
        })
        .catch(error=>{alert(error);
            this.setState({loading:false})})
  }
  render() {
    // if(this.state.filterOptions.filter((item) => item.selected)[0].data.length)
    // (this.state.filterOptions.filter((item) => item.selected)[0].data).forEach((element)=>{
    //   console.warn(Object.prototype.toString.call(Object.values(JSON.parse(element.plan))[0]).indexOf('Array'))
    // })
    return (
      <View style={{ flex: 1, backgroundColor: "#e6e8e9", justifyContent: 'flex-start' }} >
        <StatusBar translucent={true} barStyle={"light-content"} backgroundColor={this.state.showFile?"black":"transparent"}  />
        {this.state.loading ? <Loader /> : null}
        <ScrollView contentContainerStyle={{ justifyContent: 'flex-start', backgroundColor: '#e6e8e9' }}>
          <View style={{ marginTop: StatusBar.currentHeight,paddingTop:30 }}>
            <LinearGradient colors={['#5960e5', '#b785f8', '#b785f8', '#b785f8']} locations={[0.1, 0.5, 0.8, 1]}
              angleCenter={{ x: 0, y: 0 }} useAngle={true} angle={45} style={{
                backgroundColor: 'purple',
                alignSelf: 'center',
                top: -620,
                height: 800,
                width: 800,
                borderRadius: 400,
                position: 'absolute'
              }}
            >
            </LinearGradient>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>
              <TouchableOpacity onPress={() => drawer.current.open()} style={{zIndex:1}}>
                <Image source={require('../../assets/ic_home/align-left.png')} style={{ height: 20, width: 20, }} />
              </TouchableOpacity>
              <Text style={{ flex: 1, color: 'white', fontFamily: 'RubikMedium', fontSize: 20, textAlign: 'center', marginLeft: -30 }}>MY AMC</Text>
              <TouchableOpacity onPress={() => drawer.current.open()}>
                <Image source={require('../../assets/ic_home/bell.png')} style={{ height: 20, width: 20, }} />
              </TouchableOpacity>
            </View>
            <View style={{ height: 170, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10 }}>
            {this.state.BannerdataSource.length?  <Swiper 
                    onIndexChanged={(index)=>this.setState({activeSlide:index})}
                    showsButtons={false} 
                    showsPagination={false}
                    loop={true} 
                    horizontal={true}
                    autoplayTimeout={2} 
                    autoplay={true}
                    >
                  {
                    this.state.BannerdataSource.map((item, i) => (
                      <View style={{height:'100%', flex:1, alignSelf:'stretch', margin:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:10,borderRadius:5}}>
                          <Image key={i} source={{uri:item.image}} style={{ resizeMode: 'cover',width:'100%', height: '100%',borderRadius:5 }}/>
                      </View>
                    
                  ))
                  }
                </Swiper>:null}
                <View style={{
                    position: 'absolute',
                    bottom: -10
                  }}>{this.pagination}</View>
            </View>
          </View>


          <View style={{
            flex: 1, flexDirection: 'row', alignSelf: 'center', justifyContent: 'flex-start', alignItems: 'flex-start',
            marginHorizontal: 10, marginBottom: 10
          }}>
            {
              this.state.filterOptions.map((item, index) =>
                <TouchableOpacity key={index}
                  onPress={() => {

                    if(item.press){
                      this.props.navigation.navigate('MyOrders');
                    }else{
                      var temp = this.state.filterOptions;

                      var temp = this.state.filterOptions;
                      temp = temp.map((val) => {
                        if (val.label == item.label) {
                          val.selected = true;
                        } else {
                          val.selected = false;
                        }
                        return val;
                      });
                      this.setState({ filterOptions: temp })
                    } 
                  }}
                  style={{
                    flex: 1, height: 70, width: 70, alignSelf: 'center',
                    marginRight: index != 3 ? 10 : 0, justifyContent: 'flex-start'
                  }}>
                  <View style={{ height: '100%', width: '100%', padding: 10, opacity: item.selected ? 1 : 0.5, backgroundColor: 'white', borderRadius: 5, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 5 }}>
                    <Image source={item.image} style={{ resizeMode: 'contain', alignSelf: 'center', height: 30, width: 30 }} />
                    <Text style={{ color: 'black', textAlign: 'center', fontFamily: 'RubikMedium', fontSize: 10, marginTop: 5 }}>{item.label}</Text>
                  </View>

                </TouchableOpacity>
              )}

          </View>

          {/*products and filter button area  end*/}
          {
            this.state.filterOptions.filter((item) => item.selected)[0].data.length ?
              this.state.filterOptions.filter((item) => item.selected)[0].data.map((customerProduct, ind) =>
                <View key={ind} style={{backgroundColor: 'white', margin: 10,marginHorizontal:20, alignSelf: 'stretch',alignItems:'stretch', borderRadius: 8, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation: 10, padding: 15, justifyContent: 'flex-start',}}>
                  <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center',  }}>
                    <View style={{ borderRadius: 5 }}>
                    <TouchableOpacity  onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})} style={{borderRadius:5,borderWidth:0.5, borderColor:'#b4b5b8', height:100, width: 100,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:1, }}>
                        <Image source={{ uri: customerProduct.image }} style={{ resizeMode: 'cover', height:'100%', width: '100%',}} />
                      </TouchableOpacity>
                      {customerProduct.plan?
                      <Text style={{fontFamily:'RubikRegular', fontSize:12, textAlign:'center',padding:3, marginTop:10, backgroundColor:'transparent', borderRadius:10,borderWidth:0.5, borderColor:'#b4b5b8'}}>
                      {this.getWarrantyPeriod(customerProduct)}</Text>:null}
                    </View>
                   
                    <TouchableOpacity  onPress={()=>this.props.navigation.navigate('ApplianceDetail',{id:customerProduct.id,customer_id:this.state.customerID, applianceData:customerProduct})} style={{ flex: 1, marginLeft: 15 }}>
                      {/* <Text style={{fontFamily:'RubikBold', fontSize:13, color:'#4f4f4f'}}>{customerProduct.amc_days>0?"EXPIRY IN ":"WARRANTY EXPIRED"}{customerProduct.amc_days>0?<Text style={{color:'#ff629e'}}>{customerProduct.amc_days+" DAYS"}</Text>:''}</Text> */}
                      <Text style={{ color: '#3875c9', fontFamily: 'RubikBold', fontSize: 12, marginTop: 5 }}>{customerProduct.product_name.toUpperCase()}</Text>
                      <Text style={{ color: '#4f4f4f', fontFamily: 'RubikMedium', fontSize: 15, marginTop: 5 }}>{this.capitalize(customerProduct.sub_product_name)}</Text>
                      <Text style={{ color: '#4f4f4f', fontFamily: 'RubikRegular', fontSize: 12 }}>Model No : {customerProduct.model_name}</Text>
                      <Text style={{ color: '#4f4f4f', fontFamily: 'RubikRegular', fontSize: 12 }}>Brand : {customerProduct.brand_name}</Text>
                      {customerProduct.WarrantyStartDate?<View style={{flexDirection:'row'}}>
                        <Text style={{ justifyContent:'center', fontFamily:'RubikBold', fontSize:12, color:'black'}}>AMC Start : </Text> 
                        <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'black'}}>{this.getDate(customerProduct.WarrantyStartDate)}</Text>
                      </View>:null}
                      {customerProduct.WarrantyExpiryDate? <View style={{flexDirection:'row'}}>
                        <Text style={{ justifyContent:'center', fontFamily:'RubikBold', fontSize:12, color:'black'}}>AMC End : </Text> 
                        <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'black'}}>{this.getDate(customerProduct.WarrantyExpiryDate)}</Text>
                      </View>:null}
                      <TouchableOpacity onPress={()=>this.getCertificate(customerProduct.EWCId)} style={{ flexDirection: 'row', flex: 1, alignItems: 'center',marginTop:5 }}>
                        <Image source={require('../../assets/ic_home/pdf.png')} style={{ resizeMode: 'contain', height: 13, width: 13 }} />
                        <Text style={{ color: '#4f4f4f', fontFamily: 'RubikMedium', fontSize: 12, marginLeft: 5, textDecorationLine:'underline' }}>EWC Certificate</Text>
                      </TouchableOpacity>
                    </TouchableOpacity>
                  </View>
                  <View style={{  flexDirection: 'row', justifyContent: 'center', alignItems: 'center',marginTop:10  }}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:customerProduct})}} style={{flex:1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
                      borderRadius: 5, borderColor: '#c0c0c2', borderWidth: 1, marginRight: 10, padding:10}}>
                      <Text style={{ paddingLeft: 10, color: '#2c2c2e', fontFamily: 'RubikRegular', fontSize: 12 }}>Request Service</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('AccessoriesNavigation',{product_group_id:customerProduct.product_group_id})} style={{flex:1, flexDirection: 'row', alignItems: 'center',
                      justifyContent: 'center', borderRadius: 5, borderColor: '#c0c0c2', borderWidth: 1, padding:10}}>

                      <Text style={{ paddingLeft: 10, color: '#2c2c2e', fontFamily: 'RubikRegular', fontSize: 12 }}>Buy Accessories</Text>
                    </TouchableOpacity>
                  </View>
                </View>) : <Text style={{fontFamily:'RubikRegular', color:'gray', fontSize:20, alignSelf:'center'}}>No data found.</Text>
          }

        </ScrollView>
        <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showFile}
        onShow={()=>this.setState({loading:false})}
        onRequestClose={() => {this.setState({showFile:false,  fileB64:''})}}>
        
          <View style={{flex:1}}>
           <PDFView
              // fadeInDuration={250.0}
              style={{ flex: 1 }}
              resource={this.state.fileB64}
              resourceType={'base64'}
              onLoad={() => console.warn(`PDF rendered from base64`)}
              onError={() => console.warn('Cannot render PDF', error)}
            />
            <TouchableOpacity activeOpacity={1} onPress={()=>this.setState({showFile:false,  fileB64:''})} style={{width:'100%', alignSelf:'stretch', height:50, bottom:0,  zIndex:20, backgroundColor:'black'}}>
            <Text style={{flex:1,fontFamily:'RubikMedium', fontSize:20, alignSelf:'center', textAlignVertical:'center', color:'white' }}>CLOSE</Text>  
          </TouchableOpacity>
         </View>
          
      </Modal>
      </View>

    );
  }
}

MyAmc.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

MyAmc.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyAmc);
