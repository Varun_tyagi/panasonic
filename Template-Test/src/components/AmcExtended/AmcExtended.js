
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
 
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import Icon from 'react-native-vector-icons/FontAwesome';

import Swiper from 'react-native-swiper';
import TNC from '../TNC';


const widthFull = Dimensions.get('window').width;

class AmcExtended extends Component {
  constructor(props) {
    super(props);
    const {state} = props.navigation;
    this.state = {
      amcData: state.params.user,
      warrantyIndex: state.params.index,
      termsOfUse: '',
      oneYearSelected: true,
      twoYearSelected: false,
      threeYearSelected: false,
    };

    this.warrantyIndex = state.params.index+1; 
    this.amcData =  state.params.user;
this.termsOfUse = 'Copyright Notice\n\nCopyright ©2008 E-warranty India Pvt. Ltd. E-warranty Home Appliances India Co. Ltd.. All rights reserved.\n\nE-warranty India Pvt. Ltd\n\nNo. 88, 6th Floor, “ SPIC building Annexe”, Mount Road, Guindy, Chennai – 600032\n\nE-warranty, Technics and National are registered trademarks of E-warranty Corporation. Quasar is a registered trademark of Matsushita Electric Corporation of America.';
  
console.log("\n\n\n Hello PROPS \n\n " + JSON.stringify(state.params.user) + JSON.stringify(state.params.user.productList));
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  

  onPress() {
    this.props.navigation.goBack();
  }
onCheckBoxPressState(warrantyIndex){
  console.log(warrantyIndex);
  if (warrantyIndex+1 == 1 ){
  this.setState({twoYearSelected:false, oneYearSelected:true, threeYearSelected: false});
  console.log('check 1 change');

  }

  if (warrantyIndex+1 == 2 ){
    this.setState({twoYearSelected:true, oneYearSelected:false, threeYearSelected: false});
    console.log('check 2 change');

  } 
     if (warrantyIndex+1 === 3 ){
      this.setState({twoYearSelected:false, oneYearSelected:false, threeYearSelected: true});
      console.log('check 3 change');

  } 
  }
  

  onCheckBoxPress(warrantyIndex){
  if (warrantyIndex+1 == 1 ){
  if  (this.state.oneYearSelected===false){
  return 'square';
  } else{
    return 'check-square';
  }
}

  if (warrantyIndex+1 == 2) {
  if (this.state.twoYearSelected===false){
    return 'square';
  } else{
    return 'check-square';
  } 
}

    if (warrantyIndex+1 === 3 ){
      if (this.state.threeYearSelected===false){
   return 'square';
  } else{
    return 'check-square';
  } 
}
  }
  

  render() {
    return (
 <ImageBackground source={require('../../assets/ic_home/amcExtendBg.png')} style={{ flex: 1, }} >
   <View style={{ flex: 1, backgroundColor: 'transparent' }} >
      <Header
        containerStyle={{ backgroundColor: 'transparent', height: '10%' }}
        backgroundColor={'transparent'}
        leftContainerStyle={{ marginTop: -20 }}
        rightContainerStyle={{ marginTop: -20 }}
        centerContainerStyle={{ marginTop: -20 }}
        centerComponent={{ text: 'MY AMC', style: { color: '#fff', fontSize: 20 } }}
        leftComponent={
            {
              icon: 'arrow-back',
              color: '#FFFF',
              type: 'material',
              size: 30,
            onPress: () => { this.props.navigation.goBack() },

          }}
      />

      <ScrollView >  
           <View style={styles.productImageContainer}>
           <Image 
             source={require('../../assets/ic_home/amcDetailTopIcon.png')} 
             style={styles.productImageStyle} />
           </View>

           <Text  numberOfLines={4} style={{ ...styles.title,}}>
                        {'REFRIEGRATOR'}
                    </Text>
                    <Text  numberOfLines={4} style={{ ...styles.title,}}>
                        {'FROST FREE REFRIEGRATOR'}
                    </Text>
                    <Text  numberOfLines={4} style={{ ...styles.title,}}>
                        {'E-warranty'}
                    </Text>
          
          
          <View style={styles.containerWarranty} >
           <Image 
             source={require('../../assets/ic_home/warranty.png')} 
             style={styles.warrantyImageStyle} />
             <Text  style={{ ...styles.title,color:'#4e56c2',} }>
                        {'EXTENDED WARRANTY'}
                </Text>
           </View>

{/* //Start OF Warranty buttons */}

{
       this.amcData.productList.map((warrantyDict, indexWarranty) => (
           <View style={styles.containerWarrantyButton} key={indexWarranty} >   
           <TouchableOpacity onPress={() => { 
             this.onCheckBoxPressState(indexWarranty)
              }}>
           <Icon 
           name={
             this.onCheckBoxPress(indexWarranty)
               }
             size={30} color='white' />
             
           </TouchableOpacity>
           <Text  style={{ ...styles.title,color:'white',} }>
                        { (indexWarranty+1) +' YEAR WARRANTY RS '+ warrantyDict.yearPrice}
           </Text>
               <TouchableOpacity onPress={() => { 
                 this.props.navigation.navigate('TNC', { user: this.amcData, index:this.warrantyIndex });

                   }}>
          
                 <Image 
                 source={require('../../assets/ic_home/information.png')} 
                  style={styles.warrantyImageStyle} />
               </TouchableOpacity>
           
           </View>

           ))
}
               
        {/* <View style={styles.containerWarrantyButton} >  
           <TouchableOpacity onPress={() => { 
           this.setState({twoYearSelected:!this.state.twoYearSelected, oneYearSelected:!this.state.oneYearSelected})

              }}>
           <Icon 
           name={this.state.twoYearSelected==false ? 'square':'check-square'}
             size={30} color='white' />
             
           </TouchableOpacity>
           <Text  style={{ ...styles.title,color:'white',} }>
                        {'2 YEAR WARRANTY RS 2100'}
           </Text>
               <TouchableOpacity onPress={() => { 
                 this.props.navigation.navigate('TNC', { user: this.amcData, index:this.warrantyIndex });

                   }}>
          
                 <Image 
                 source={require('../../assets/ic_home/information.png')} 
                  style={styles.warrantyImageStyle} />
               </TouchableOpacity>
           
          </View> */}
    
          <TouchableOpacity onPress={() => { 
     this.props.navigation.navigate('TNC', { user: this.amcData, index:this.warrantyIndex });

              }}>
           <Text  style={{ ...styles.title,color:'grey', marginBottom:20,marginTop:20,} }>
                        {'Terms And Conditions'}
           </Text>  
             
           </TouchableOpacity>
          
           <TouchableOpacity 
    //        onPress={() => { 
    //  this.props.navigation.navigate('TNC', { user: this.amcData, index:this.warrantyIndex });
    //           }}
              >
              <ImageBackground source={require('../../assets/ic_home/pinkGradient.png')} style={{ flex: 1,marginBottom:20,marginTop:20,width:200,borderRadius:10,marginLeft :(widthFull/2)-100, }} >
           <Text  style={{ ...styles.title,color:'white', marginBottom:15,marginTop:15,} }>
                        {'CONFIRM AND PAY'}
           </Text>  
           </ImageBackground>
             
           </TouchableOpacity>



      </ScrollView>
     
     

   </View>
</ImageBackground>
  );
  }
}

AmcExtended.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

AmcExtended.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AmcExtended);
