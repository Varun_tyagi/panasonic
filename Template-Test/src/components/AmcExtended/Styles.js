

// eslint-disable-next-line no-unused-vars
import { StyleSheet, Dimensions } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';


const widthFull = Dimensions.get('window').width;

const styles = StyleSheet.create({

  productImageContainer:{
    flex: 1,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    // backgroundColor: 'red',
    height: 160,
    paddingTop: 10,
    paddingBottom: 10,
  },

  productImageStyle:{
    flex: 1,
    width: 140,
    height: 140,
    // resizeMode: 'cover',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    resizeMode: 'contain',
  },

  containerWarranty: {
    flex: 1,
    flexDirection: 'row',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    height:120,
    paddingTop:50,
      },
      
  warrantyImageStyle:{
    flex: 0.2,
    width: 30,
    height: 30,
    alignSelf:'center',
    resizeMode: 'contain',

  },

  containerWarrantyButton: {
        flex: 0.8,
       flexDirection: 'row',
        paddingLeft: 5,
       paddingRight: 5,
       backgroundColor: '#9a7ced',
       justifyContent:'center',
       alignItems:'center',
       alignSelf:'center',
       borderRadius: 10,
       shadowRadius: 1,
       shadowOffset: { width: 1, height: 1 },
       shadowColor: 'black',
       height: 80,
       width: widthFull-20,
       marginBottom:10,
      },

       
 

  title: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    padding: 5,
  },
  
  description: {
    fontSize: 14,
    color: '#fff',
    fontWeight: 'normal',
    alignSelf:'center',
    justifyContent:'center',
    alignItems:'center',
    textAlignVertical: 'center',
  },


  buttonClose: {flex:0.2,
      justifyContent:'center',
      alignItems:'center',
      alignSelf:'center',
      marginBottom:20, 
      width:70
    },

  
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
});

export default styles;

