
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import TNC from '../TNC';
import Swiper from 'react-native-swiper';
import AmcExtended from '../AmcExtended/AmcExtended';

const widthFull = Dimensions.get('window').width;

class AmcList extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.onPress = this.onPress.bind(this);

    this.amcCategoryArray = [{
      catrgoryURl:'',
      categoryNAme:'Home Appliances',
    },{
      catrgoryURl:'',
      categoryNAme:'Air Conditioners',
    },{
      catrgoryURl:'',
      categoryNAme:'Home Entertainment',
    },{
      catrgoryURl:'',
      categoryNAme:'Mobile Phones',
    },{
      catrgoryURl:'',
      categoryNAme:'Beauty Care',
    },];

    this.amcProductListArray = [{
      sectionName:'DIRECT COOL',
      productList:[{
        yearPrice:1025,
          yearDesc:'',
          year:1,
      },{
        yearPrice:1641,
          yearDesc:'',
          year:2,

      },{
        yearPrice:2667,
          yearDesc:'',
          year:3,

         }
      ],
    },
    {
      sectionName:'500 LITRE & ABOVE',
      productList:[{
        yearPrice:3590,
          yearDesc:'',
          year:1,

      },{
        yearPrice:5745,
          yearDesc:'', 
                   year:2,

      },{
        yearPrice:9234,
          yearDesc:'',
          year:3,

         }
      ],
    },
    {
      sectionName:'BELOW 500 LITRE',
      productList:[{
          yearPrice:3077,
        yearDesc:'',
        yearImage:'oneYear',
        year:1,

      },{
        yearPrice:5129,
        yearDesc:'',
        yearImage:'twoYear',
        year:2,

      },{
        yearPrice:7900,
        yearDesc:'',
        yearImage:'threeYear',
        year:3,

         }
      ],
    },
    ];
    
  }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  componentDidUpdate() {
    if (this.props.user === null) {
      this.props.navigation.navigate('Auth');
    }
    return null;
  }

  onPress(index, dataDict) {
    Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
  }
  onPressTnc(index,prodCatDict){
    this.props.navigation.navigate('TNC', { user: prodCatDict, index:index });

  }
  onPressCell(index,prodCatDict){
    this.props.navigation.navigate('AmcExtended', { user: prodCatDict, index:index });

  }

  render() {
    return (
      <ImageBackground source={require('../../assets/ic_home/pinkBG.png')} style={{ flex: 1, backgroundColor: '#EFEFF4' }} >


    <View style={{ flex: 1, backgroundColor: 'transparent' }} >
      <Header
        containerStyle={{ backgroundColor: 'transparent', height: '10%' }}
        backgroundColor={Colors.primary}
        leftContainerStyle={{ marginTop: -20 }}
        rightContainerStyle={{ marginTop: -20 }}
        centerContainerStyle={{ marginTop: -20 }}
        centerComponent={{ text: 'BUY AMC', style: { color: '#fff', fontSize: 20 } }}
        leftComponent={
            {
              icon: 'arrow-back',
              color: '#FFFF',
              type: 'material',
              size: 30,
            onPress: () => { this.props.navigation.goBack() },

          }}

      />


      <ScrollView>

      <ScrollView  bounces={false} showsHorizontalScrollIndicator={false} style={{ flex: 1, flexDirection: 'row', paddingLeft: 15, paddingRight: 15,height:110,marginTop:30,}}> 
       <View style={{ flex: 1, flexDirection: 'row', paddingLeft: 0, paddingRight: 0,height:110,}}>
       
          {
          this.amcCategoryArray.map((prod, ind) => (
                <View style={{  flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection:'column', width:120,borderRightWidth:10, borderRightColor:'transparent', height:100, }} key={ind}>
                <Image source={require('../../assets/ic_home/ac.png')} 
            //source={{ uri: prod.image_url }}  // to get from url
             style={styles.photo} />
                <Text  numberOfLines={2} style={styles.AMCCategoryTitle}>
                    {prod.categoryNAme}
                     </Text>
                </View>
              ))
              }

        </View>
      </ScrollView>


  <View style={{  flex: 1, flexDirection: 'column', padding: 20,marginTop:40, }} >
          <Text style={{  flex: 1, color: '#4a7ec3',height: 30,fontWeight: 'bold',justifyContent: 'flex-start',fontSize:16,}}>
    {'AIR CONDITIONERS'}
    </Text>
    <Text style={{  flex: 1, color: '#AAA',justifyContent: 'flex-start',fontSize:14,}}>
    {'We at E-warranty appreciate that you expect a little extra from us each time.Reason why.We bring you \'E-warranty Warranty +\'. a unique offering that allows your E-warranty product  to enjoy an extended warranty over longer periods.\nEnjoy peace of mind like never before.' }
        </Text>
            <Text style={{ flex: 1, color: '#000' ,fontWeight:'bold',justifyContent: 'flex-start',fontSize:14,}}>
                 {'\nEnjoy peace of mind like never before.' }
             </Text>
          </View>

        {/* // Botom Warranty List   */}
        <View style={{ flex: 1,flexDirection:'column', paddingLeft: 0, paddingRight: 0, justifyContent: 'center', alignItems: 'center',}}>
       {
       this.amcProductListArray.map((prodCatDict, ind) => (
             <View style={{  flex: 1,borderRightWidth:10, borderRightColor:'transparent', }} key={ind}>
           
             <Text  numberOfLines={2} style={{ ...styles.AMCCategoryTitleHeader,alignItems: 'flex-start', justifyContent: 'flex-start', textAlign: 'left',paddingLeft:15,}}>
                 {prodCatDict.sectionName}
             </Text>

             <View style={{  flex: 1 ,paddingTop:5,paddingBottom:5, }} >
             {
       prodCatDict.productList.map((prodCatItem, indInternal) => (
        <View style={{  height:80, flex: 1, borderBottomWidth:5,borderTopWidth:5,borderTopColor:'transparent',borderBottomColor:'transparent',width:widthFull,paddingLeft:15,paddingRight:15, }} key={indInternal}>
       
       {/* //start section row */}
           <View style={{  backgroundColor:'white', height:80, flex: 1,flexDirection:'row',justifyContent: 'center', alignItems: 'center'}} >
           
           <TouchableOpacity
               onPress={() => {  this.onPressCell(ind, prodCatDict); }}
              style={styles.photo}
                  >
             <Image 
             source={require('../../assets/ic_home/oneYear.png')} 
             style={styles.photo} />
              </TouchableOpacity>

                <View style={{flex:1,justifyContent: 'center',alignSelf:'center',alignItems:'center',alignContent:'center',paddingTop:10,}}>
                 
                  <View style={{flex:1,flexDirection:'row',alignItems:'flex-end',}}>
                     <Text  numberOfLines={4} style={{ ...styles.AMCCategoryDescHeader,textAlign:'left',color:'grey',}}>
                        {'Year Warranty'}
                    </Text>
                       {/* <View style={{flex:1,paddingLeft:2,paddingRight:5,}}> */}
                   <TouchableOpacity 
               onPress={() => {  this.onPressTnc(indInternal, prodCatItem); }}
              style={{ flex:1,width:12, height:12,}}
                  >
              <Image
                source={require('../../assets/ic_home/information.png')}
                style={{ resizeMode: 'contain', height: 12,width:12, }}
              />
                    </TouchableOpacity> 
                    {/* </View> */}
             </View>
             <Text  numberOfLines={4} style={{ ...styles.AMCCategoryDescHeader,textAlign:'left',alignSelf:'flex-start',color:'#f4aac3',}}>
                 {'GST/Taxes applicable extra'}
             </Text>
          </View>  

          {/* // End of section row */}
          
          <TouchableOpacity
               onPress={() => {  this.onPressCell(ind, prodCatDict); }}
              style={{ ...styles.AMCCategoryDescHeader,flex:0.4,alignSelf:'center',paddingRight:5,fontSize:16,color:'grey',textAlign:'right',justifyContent:'center',alignItems: 'center',}}
                  >
             <Text  numberOfLines={3} style={{ ...styles.AMCCategoryDescHeader,flex:0.4,alignSelf:'center',paddingRight:5,fontSize:16,color:'grey',textAlign:'right',justifyContent:'center',alignItems: 'center',}}>
                 {'RS. '+prodCatItem.yearPrice}
             </Text>
             </TouchableOpacity>
               </View>
          </View>
           ))
           }
           </View>


             </View>
           ))
     }
           {/* Terms & Condition View */}
    
      <View style={{ flex: 1, height:80,width:widthFull,paddingLeft:15,paddingRight:15,justifyContent: 'center', alignItems: 'center',}} >

<View style={{ flex: 1, flexDirection:'column', backgroundColor:'white',width:widthFull-30,paddingTop:10,paddingBottom:10,}} >
 
 <Text  numberOfLines={2} style={{ ...styles.AMCCategoryTitleHeader,justifyContent: 'center', alignItems: 'center' ,textAlign: 'center' ,color:'grey', fontSize: 22,height:30,}}>
      {'TERMS & CONDITIONS'}
  </Text>
  <Text  numberOfLines={2} style={{ ...styles.AMCCategoryTitleHeader,justifyContent: 'center', alignItems: 'center', textAlign: 'center', fontSize: 15,height:30,}}>
      {'You can read all terms and conditions'}
  </Text>
 
    </View>
    </View>

           {/* TNC END HERE */}
   </View>

   {/* //Bottom waranty list end here */}


        

      </ScrollView>


    </View>
      </ImageBackground>
  );
  }
}

AmcList.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

AmcList.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AmcList);
