import React, { Component, } from 'react';
import {
  View,
  Text,
  StatusBar,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  KeyboardAvoidingView,
  Alert,
  Dimensions, BackHandler,TouchableOpacity
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
// import Button from '../common/Button';
import { Button, Image, Input, } from 'react-native-elements';
import TextField from '../common/TextField';
import ErrorView from '../common/ErrorView';
import ShadowStyles from '../../helpers/ShadowStyles';
import TextStyles from '../../helpers/TextStyles';
import strings from '../../localization';
import { login, actionTypes } from '../../actions/UserActions';
import Swiper from 'react-native-swiper'
import Loader from '../common/Loader';
// import { relative } from 'path';
import LinearGradient from 'react-native-linear-gradient';
import * as constants from '../../helpers/constants';
import * as authFun from '../../helpers/auth';
class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.otpChanged = this.otpChanged.bind(this);
    this.getBanners = this.getBanners.bind(this);
      // this.props.navigation.navigate('App');
    this.onBackSpace = this.onBackSpace.bind(this);
    this.handleBackPress = this.handleBackPress.bind(this);
  this.state = {
    mobile: '',
    password: '',
    showOtp:false,
    otp:['','','',''],
    otpFull:null,
    dataLoading:false,
    std:'+91',
    token:'',banners:[]
  };

  }

  checkLogin(){
    AsyncStorage.getItem('loggedIn',(val)=>{
      if(val!=this.state.mobile){
        this.loginAPi();
        console.log(
        'value = ' + val
        )
      }else{
        this.props.navigation.navigate('App');
        console.log(
          'ERROR ERROR ERROR ERROR'
          )
      }
    });
  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }
  async getBanners(){
    await this.setToken();

    let access_token = this.state.token;
    console.warn(constants.base_url+'login_banner?access_token='+access_token);
    fetch(constants.base_url+'login_banner?access_token='+access_token,{headers: {'Content-Type':'application/json'}}).then(res => res.json())
      .then((result) => {
        if(result.status){
            this.setState({banners:result.banners, dataLoading:false})
        }else{
          console.warn(result)
          this.setState({dataLoading:false})
        }
      },
        (error) => {
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              console.warn('error: ',error);
            }
          });
        console.warn(error);
        this.setState({dataLoading:false})}
      );
  }

  handleBackPress(){
    if(this.state.showOtp){
      this.setState({showOtp:false});
    }else{
      BackHandler.exitApp();
    }
    return true;
  }
  
 async componentDidMount(){
  
  await this.setToken();
    this.getBanners();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }
 
  
  loginAPi=async  ()=>{
    await this.setToken();
    var access_token = this.state.token;
    console.warn(access_token);
    // var token_time = await AsyncStorage.getItem('token_time');
// alert(access_token+'');
    if (this.state.mobile.trim() === '') {
      Alert.alert('Please enter  mobile number');
      return false;
    } else if (this.state.mobile.length < 7) {
      Alert.alert('Please enter valid mobile number');
      return false;
    }
    this.setState({dataLoading:true})
    data = new FormData();
      fetch(constants.base_url+'login?access_token='+access_token,{  
      method: "POST",
        headers: {'Content-Type':'application/json'},
        body:JSON.stringify({"mobile":this.state.mobile})
      }).then(res => res.json())
      .then((result) => {
        console.warn(result)
        if(result.status){
          this.setState({showOtp:true},()=>{
            this.setState({dataLoading:false})
          });
        }else{
          alert(result.message);
          this.setState({dataLoading:false})
        }
        
      },
        (error) => { 
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          });
          this.setState({dataLoading:false})}
      );
    
  }

  verifyOtp=async ()=>{
    await this.setToken();
    var access_token = this.state.token;
 
    if(this.state.otp[0]&&this.state.otp[1]&&this.state.otp[2]&&this.state.otp[3]){
      this.setState({dataLoading:true});
      fetch(constants.base_url+'verifyOtp?access_token='+access_token, {
          method: 'POST',
          headers: {  'Content-Type':'application/json'},
          body:JSON.stringify({"mobile":this.state.mobile.toString(),"otp":parseInt(this.state.otp.join(""))})
        }).then(res => res.json())
        .then((result) => {
          if(result.status){
            this.setState({dataLoading:false},()=>{
              AsyncStorage.setItem('loggedIn', this.state.mobile+'');
              global.profile_image = result.data.profile_image;
              AsyncStorage.setItem('userData', JSON.stringify(result.data),()=>{
              this.props.navigation.navigate('App');
            });
            });
            
          }else{
            this.setState({dataLoading:false, otp:['','','','']},()=>{
              Alert.alert(result.message);
              this.otp1.focus();
            })
          }
        },
        (error) => { 
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          });
          this.setState({dataLoading:false})}
      );
    }else{
      Alert.alert('Please enter valid OTP.');
    }
  }
  passwordChanged = value => this.setState({ password: value });

  mobileChanged = value => /^[0-9]*$/.test(value)||value==''?this.setState({ mobile: value }):null;
  otpChanged = (value,id,callBack=undefined) => {
    var temp = this.state.otp;
    const testExp = /^[0-9]*$/;
    if(testExp.test(value)){
      temp[id]=value
    this.setState({ otp: temp },()=>{
      if(callBack){
        callBack();
      }
    });
    }
    
}
componentWillUnmount(){
  this.backHandler.remove();
  // this.props.navigation.navigate('App');
}
onBackSpace=(focusOn)=>{
  if(focusOn){
    focusOn.focus();
  }
  
}
  // login = () => this.props.login(this.state.email, this.state.password);

  render() {
    // const { isLoading, errors } = this.props;
    
    return (
      <LinearGradient colors={['#5960e5','#b785f8']} 
           angleCenter={{x:0,y:0}}  useAngle={true} angle={60} 
           style={{
             flex:1,
           backgroundColor:'purple', 
         }}
           >
      <StatusBar hidden />
      {this.state.dataLoading?<Loader/>:null}
        <View style={{height:260, marginBottom:35}}>
         
           {this.state.banners.length?
            <Swiper  showsButtons={false} loop={true} 
            dotStyle={{borderColor:'white', borderWidth:1,height:8, width:8, marginBottom:-70 }}
            activeDotStyle={{borderColor:'white', borderWidth:1,height:8, width:8,marginBottom:-70 }} 
            dotColor="transparent" activeDotColor="white"
            autoplayDirection={true} autoplayTimeout={2} autoplay={true} 
            containerStyle={{width:Math.round(Dimensions.get('window').width), height:250 ,  }}>

              {
                this.state.banners.map((item, ind)=>
                item.image?<Image key={ind}  source={{uri:item.image}}  style={{flex:1, resizeMode:'stretch', height:'100%'}}  containerStyle={{flex:1, flexDirection:'row',justifyContent:'center', }}/>:<View></View>)  
              }
                
            </Swiper>:null}
            
         </View>
      
      {/* <ScrollView> */}
      <KeyboardAvoidingView behavior="position" enabled >
        <Text style={{fontSize:27.5,marginBottom:10,textAlign:'center',color: 'white',fontFamily:'RubikBold'}}>
              {!this.state.showOtp?"USER LOGIN":"OTP VERIFICATION"}
        </Text>
        {
          !this.state.showOtp?
           <View style={{backgroundColor:'white',marginBottom:18.5,marginTop:14,marginHorizontal:29.5, paddingHorizontal:22.5, borderRadius:30,  fontSize:20,shadowOffset: {
                    width: 3,
                    height: 3,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:4, flexDirection:'row', justifyContent:'flex-start'}}>
            <TextInput
            style={{textAlign:'center',fontFamily:'RubikMedium', fontSize:16, flex:0.2, }} 
            keyboardType="phone-pad"
            editable={false}
            placeholderTextColor="black"
            contextMenuHidden={true}
            placeholder={this.state.std}
            />
            <View style={{height:20, width:1, backgroundColor:'#2b26242a', alignSelf:'center'}}></View>
            <TextInput
            style={{textAlign:'center',fontFamily:'RubikMedium', fontSize:16, flex:1 , height : 40}} 
            keyboardType="phone-pad"
            returnKeyType='done' 
            maxLength={10}
            secureTextEntry={false}
            contextMenuHidden={true}
            placeholder="Mobile number"
            onEndEditing={(val)=>{
              if(this.state.mobile.length!=10){
                // this.setState({error:true},()=>{
                  alert('Please enter a valid number.');
                  this.mobile.focus();
                // });
              }
              else{
                
                this.loginAPi();
              }
            }}
            onChangeText={(value)=>this.mobileChanged(value)}
            value={this.state.mobile}
            ref={(input)=>this.mobile=input}
            />
           </View>

           
           :null}
            <Text style={{fontSize: 16,textAlign:'center',marginHorizontal:47.5,color: '#e9eaff', marginBottom:30,fontFamily:'RubikRegular'}}>
            {!this.state.showOtp?
            <Text>
            By logging in you agree to <Text style={{fontFamily:'RubikBold'}}>Terms & Conditions</Text> and that you have read our 
            <Text style={{fontFamily:'RubikBold'}}> Privacy Policy</Text>
            </Text>
            :
            <Text>Please enter the verification code sent to <Text style={{fontFamily:'RubikBold'}}>{"+91 "+this.state.mobile}</Text></Text>}
           </Text>
            {/*!this.state.showOtp &&
                <TouchableOpacity onPress={this.state.showOtp?()=>this.verifyOtp():()=>this.loginAPi()} style={{ height:60, width:60, alignSelf:'center'}} >
                <Image  source={require('../../assets/ic_home/next.png')}  
                style={{ resizeMode:'contain',height:70 }}

                containerStyle={{flex:1,alignSelf:'center'}}
                />
              </TouchableOpacity>
            */}
            
           </KeyboardAvoidingView>
           {this.state.showOtp?
           <View style={{flexDirection:'row',alignSelf:'center', marginBottom:19.5, height:61.5}}>
            <TextInput

            style={{backgroundColor:'white',marginEnd:15, textAlign:'center', fontSize:22.5, fontFamily:'RubikBold',width:40, borderRadius:5}}
            keyboardType="phone-pad"
            returnKeyType='done' 

            onFocus={()=>this.otpChanged('',0)}
            maxLength={1}
            secureTextEntry={true}
            input={"[1-9]{1}[0-9]{9}"}
            pattern={"[1-9]{1}[0-9]{9}"}
            onChangeText={(value)=>this.otpChanged(value,0,()=>this.otp2.focus())}
            value={this.state.otp[0]}
            ref={(input)=>this.otp1=input}
            />
            <TextInput
            style={{backgroundColor:'white',marginEnd:15, width:40, textAlign:'center', fontSize:22.5, fontFamily:'RubikBold', borderRadius:5}}
            keyboardType="phone-pad"
            returnKeyType='done' 

            onFocus={()=>this.otpChanged('',1)}
            maxLength={1}
            secureTextEntry={true}
            // onKeyPress={()=>{
            //   this.onBackSpace(this.otp1)
            // }}
            onChangeText={(value)=>this.otpChanged(value,1,()=>this.otp3.focus())}
            value={this.state.otp[1]}
            ref={(input)=>this.otp2=input}
            />
            <TextInput
            style={{backgroundColor:'white',marginEnd:15,width:40, textAlign:'center', fontSize:22.5, fontFamily:'RubikBold', borderRadius:5}}
            keyboardType="phone-pad"
            returnKeyType='done' 

            onFocus={()=>this.otpChanged('',2)}
            maxLength={1}
            secureTextEntry={true}
            // onKeyPress={()=>{
            //   this.onBackSpace(this.otp2)
            // }}
            onChangeText={(value)=>this.otpChanged(value,2,()=>this.otp4.focus())}
            value={this.state.otp[2]}
            ref={(input)=>this.otp3=input}
            />
            <TextInput
                        returnKeyType='done' 

            style={{backgroundColor:'white', width:40, textAlign:'center', fontSize:22.5, fontFamily:'RubikBold', borderRadius:5}}
            keyboardType="phone-pad"
            onFocus={()=>this.otpChanged('',3)}
            maxLength={1}
            // onKeyPress={()=>{
            //   this.onBackSpace(this.otp3)
            // }}
           // onEndEditing={this.state.showOtp?()=>this.verifyOtp():()=>this.loginAPi()}
            secureTextEntry={true}
            onChangeText={(value)=>this.otpChanged(value,3) }
            value={this.state.otp[3]}
            ref={(input)=>this.otp4=input}
            />
             
          </View>:null
          }
          {this.state.showOtp?
          
            <TouchableOpacity onPress={this.loginAPi} style={{marginBottom:10}}>
              <Text style={{textAlign:'center', color:'white', fontSize:14,fontFamily:'RubikBold', textDecorationLine:'underline'}}>Resend the code</Text>
            </TouchableOpacity>
            
          :null}
           {this.state.showOtp?
            <TouchableOpacity onPress={()=>this.setState({showOtp:false})} style={{marginBottom:42}}>
                <Text style={{textAlign:'center', color:'white', fontSize:14,fontFamily:'RubikBold', textDecorationLine:'underline'}}>Login with a different mobile number</Text>
            </TouchableOpacity>
           :null}
           {/* <ErrorView errors={errors} /> */}
           <TouchableOpacity onPress={this.state.showOtp?()=>this.verifyOtp():()=>this.loginAPi()} style={{ height:60, width:60, alignSelf:'center', marginBottom:47.5}} >
                <Image  source={require('../../assets/ic_home/next.png')}  
                style={{ resizeMode:'contain',height:70 }}

                containerStyle={{flex:1,alignSelf:'center'}}
                />
              </TouchableOpacity>
              {!this.state.showOtp?<TouchableOpacity onPress={()=>Alert.alert('Coming soon...')} style={{flex:1, flexDirection:'row', justifyContent:'center',alignSelf:'center', alignSelf:'center', }} >
                <Text style={{fontFamily:'RubikMedium', fontSize:22.5, color:'white'}}>Corporate Login</Text>
              </TouchableOpacity>:null}
      {/* </KeyboardAvoidingView> */}
      {/* </ScrollView> */}
    </LinearGradient>);
    //place your now nested component JSX code here
   
  }
}

// Login.propTypes = {
//   login: PropTypes.func.isRequired,
//   user: PropTypes.object,
//   isLoading: PropTypes.bool.isRequired,
//   errors: PropTypes.array,
//   navigation: PropTypes.object.isRequired,
// };

// Login.defaultProps = {
//   user: null,
//   errors: [],
// };

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
  login: (email, password) => dispatch(login(email, password)),
});






export default connect(mapStateToProps)(Login);
