import { StyleSheet } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import Colors from '../../helpers/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.primary,
  },
  formContainer: {
    flex: 1,
    alignSelf: 'stretch',
    // marginBottom:20,
    backgroundColor:'red',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginHorizontal: 10,
    padding: 20,
  },
  formContainerOtp: {
    flex: 1,
    alignSelf: 'stretch',
    // // marginBottom:20,
    padding:10,
    justifyContent: 'flex-start',
    alignItems: 'center',
    // marginHorizontal: 10,
    // padding: 20,
  },

});

export default styles;
