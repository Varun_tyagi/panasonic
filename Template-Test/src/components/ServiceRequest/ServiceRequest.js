import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  FlatList,
  Alert,
  Button,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  ScrollView,
  StatusBar,
  AsyncStorage,
  Platform,
// eslint-disable-next-line import/first
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors } from 'react-native-elements';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import * as constants from '../../helpers/constants';
import styles from './Styles';
import Swiper from 'react-native-swiper';
import ApplianceDetail from '../ApplianceDetail';  
import ServiceDetail from '../ServiceDetail';
import LinearGradient from 'react-native-linear-gradient';
import * as authFun from '../../helpers/auth';
import Loader from '../common/Loader';
 import {createNavigator,createNavigationContainer,StackRouter,addNavigationHelpers, StackActions,} from 'react-navigation';




// import CustomListview from './CustomListview';
// import CustomRow from './CustomRow';
const widthFull = Dimensions.get('window').width;
const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};

class ServiceRequest extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    this.state={
      loading:true,
      token:'',
      customerID:'',
      customerProducts:[],
      customerProductsAmcExpired:[]
    }
    this.capitalize = this.capitalize.bind(this);
    this.getDate = this.getDate.bind(this);
    this.productAdded = this.productAdded.bind(this);
   }


  static navigationOptions = {
    headerVisible: false,
    header: null,
  };

  productAdded = async ()=>{
    const result = await AsyncStorage.getItem('productAdded');
    if(result==1){
      await AsyncStorage.removeItem('productAdded');
      return true;
    }else{
    return false;
    }
  }
  loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      this.setState({
        customerID: data.customer_id ? data.customer_id : '',
        mNum: data.mobile ? data.mobile : this.state.mNum,
      },()=>{
        this.getData();
      });
    } else {
      this.setState({ loading: false });
    }

  }
  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }


  getData = ()=>{
    // https://www.ecarewiz.com/ewarrantyapi/getApplianceData?customer_id=1
    this.setState({loading:true});
        var location = 'getApplianceData?customer_id='+this.state.customerID
        var url = constants.base_url+location;
        // var query = id!=0?name=='state'?'&country_id='+'101':name=='city'?'&state_id='+id:'&city_id='+id:'';
        url=url+"&access_token="+this.state.token;
        fetch(url,{headers: {'Content-Type':'application/json'}})
            .then(response => response.json())
            .then((responseJson)=> {
                
                if(responseJson.status){
                    this.setState({loading:false, customerProducts:responseJson.customer_products,
                    customerProductsAmcExpired:responseJson.customer_products.filter((item)=>parseInt(item.amc_days)<=30)});
                }else{
                  // alert(JSON.stringify(responseJson))
                }
                this.setState({loading:false})
            
            })
            .catch(error=>{
              // alert(error);
                this.setState({loading:false})})   
  }
  
  async componentDidMount(){
    this.props.navigation.addListener('willFocus', async ()=>{
      var update = await this.productAdded();
      // if(update){
        this.getData();
      // }
    });
    // alert(JSON.stringify(this.props))
    var token = await  this.setToken();
     if(token){
       this.loadData(); 
     }
     
   }

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe * */
    if (nextProps.navigation.state.index === 0) { this._drawer.blockSwipeAbleDrawer(false); }
    if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
      // eslint-disable-next-line no-underscore-dangle
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe * */
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example * */
    this.forceUpdate();
  };
  // eslint-disable-next-line class-methods-use-this
  
  capitalize=(txt)=>{
    return txt.split(' ').map((word)=>word.split('')[0].toUpperCase()+word.split('').splice(1,).join('')).join(' ');
  }
  getDate = (dt)=>{
    const months =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')
    
    return dt[2]+' '+months[dt[1][0]==0?dt[1][1]:dt[1]]+' '+dt[0];
  }

  render() {
    // console.warn(JSON.stringify(this.state.customerProducts[0]));
    return (
      <View style={{flex:1, backgroundColor:"#edf4f6",justifyContent:'flex-start'}} >
       <StatusBar translucent={true} barStyle="light-content" backgroundColor="transparent" />
       {this.state.loading?<Loader/>:null}
       
       <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#edf4f6'}}>
         <View style={{marginTop:StatusBar.currentHeight, flex:1,height:150,marginBottom:10,
           justifyContent:'flex-start', alignItems:'center',paddingBottom:15}}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:-850,
            height:1000, 
            width:1000,
            borderRadius:500,
            position:'absolute'
          }}
            >
          </LinearGradient>
             <View style={{flexDirection:'row', justifyContent:'flex-start',
              alignItems:'center', padding:5, marginTop:15 , paddingTop: Platform.OS == 'ios'? 30:0}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1 }} >
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'white',fontFamily:'RubikMedium', fontSize:20, textAlign:'center', marginLeft:-30}}></Text>
            
              </View>
              <Text style={{fontFamily:'RubikRegular', fontSize:20, color:'#FFFFFF', flex:1,textAlign:'center', padding:10, marginHorizontal:20}}>
              You may raise any service request for your products
            </Text>
         </View>
         {/* products list */}
          {this.state.customerProducts.length?
            <FlatList
              data={this.state.customerProducts} 
              numColumns={2}
              columnWrapperStyle={{marginHorizontal:10, marginVertical:7.5}}
              renderItem={({item, index}) => {
                  return (
                  <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:item})}  }   style={{flex:1,marginHorizontal:5,minWidth:120, maxWidth:200,  shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3, borderRadius:5, padding:10, backgroundColor:'white'}}>
                    <View style={{flex:1,borderRadius:5, borderWidth:2, borderColor:'#e6e8e9',margin:5,height:120}}>
                      {item.image?<Image source={{uri:item.image}} style={{height:'100%',width:'100%', resizeMode:'contain', }} />:null}
                    </View>
                    <View style={{flex:1, margin:5}}>
                      {item.product_name?<Text style={{color:'#3875c9', fontFamily:'RubikBold', fontSize:15}}>{item.product_name.toUpperCase()}</Text>:null}
                      {item.sub_product_name?<Text style={{color:'#000', fontFamily:'RubikMedium', fontSize:15}}>{item.sub_product_name}</Text>:null}
                      {item.model_name?<Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Model Name: {item.model_name}</Text>:null}
                      {item.location?<Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Location: {item.location}</Text>:null}
                      {item.purchase_date?<Text style={{color:'#4f4f4f', fontFamily:'RubikRegular', fontSize:12}}>Purchase Date: {authFun.getDate(item.purchase_date)}</Text>:null}

                    </View>
                    
                  </TouchableOpacity>
                  )
                }
              }
              keyExtractor={(item, index)=>index+''}
              />
          :null
        }
        <Text style={{flex:1, fontFamily:'RubikRegular',marginTop:20, textAlign:'center', fontSize:15, color:'black'}}>DID NOT FIND YOUR PRODUCT?</Text>
        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('ServiceDetail', {productDetails:undefined})}  }  style={{ flex:1,borderRadius:5, alignSelf:'center', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5,marginVertical:10}}>
            <LinearGradient style={{  borderRadius: 5, flexDirection:'row', alignItems: 'center', justifyContent:'center' , padding:15}}
                        colors={["#ff619e", "#ff9fa6"]}>
                      <Text style={{color: 'white',fontSize: 15,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                        CLICK HERE
                      </Text>
              </LinearGradient>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

ServiceRequest.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ServiceRequest.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ServiceRequest);
