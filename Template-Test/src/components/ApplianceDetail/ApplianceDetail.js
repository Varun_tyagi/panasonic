import React, { Component } from 'react';
import {  View,Text,Image,FlatList,Platform,BackHandler,TouchableOpacity,Modal,Dimensions,StatusBar} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { Header, colors,Card } from 'react-native-elements';
// eslint-disable-next-line import/order
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
// import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../localization';
import TextStyles from '../../helpers/TextStyles';
import { logout } from '../../actions/UserActions';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import styles from './Styles';
import AmcList from '../AmcList';
import MyOrders from '../MyOrders';
import LinearGradient from 'react-native-linear-gradient';
import Loader from '../common/Loader';
import Swiper from 'react-native-swiper';
import NavigationService from '../navigation/NavigationService';
import * as constants from '../../helpers/constants';
import * as authFun from '../../helpers/auth';
import ImageViewer from 'react-native-image-zoom-viewer';
import PDFView from 'react-native-view-pdf';
import axios from 'react-native-axios';
const widthFull = Dimensions.get('window').width;

class ApplianceDetail extends Component {
  // eslint-disable-next-line react/sort-comp
  constructor(props) {
    super(props);
    const {state} = props.navigation;
     this.getAppliance = this.getAppliance.bind(this);
     this.setToken = this.setToken.bind(this);
     this.getService = this.getService.bind(this);
     this.getDate = this.getDate.bind(this);
     //this.deleteProducts = this.deleteProducts.bind(this);
    // this.onPress = this.onPress.bind(this);
    // console.log("\n\n\n Hello PROPS \n\n " + state.params.user );
    this.state ={
      status:false,
      loading:true,
      token:'',
      data: '',
      services:[],
      warranties:[],
      coupons:[],
      related_accessories:[],
      collapseService:true,
      collapseDealer:true,
      collapseWarranty:true,
      collapseDocument:true,
      collapseOffer:true,
      collapseAccess:true,
      confirmDelete:false,
      fileType:'',
      pdfType:'url',
      filePath:'',
      showFile:false,
      fileB64:'',
      loading:false,
    }
     this.handleBackPress = this.handleBackPress.bind(this);
  }

  setToken=async ()=>{
    var token = await authFun.checkAuth();
    this.setState({token:token})
    return token;
  }

  getDate = (dt) => {
    // alert(dt);
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    dt = dt.split(' ')[0].split('-')

    return dt[2] + ' ' + months[dt[1][0] == 0 ? dt[1][1] : dt[1]] + ' ' + dt[0];
  }

  static navigationOptions = {
    headerVisible: false,
    header: null,
  };
  handleBackPress(){
    if(this.state.showFile){
      this.setState({showFile:false});
    }else{
      this.props.navigation.goBack(null);
    }
    
    return true;
  }
  async componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    var token = await  this.setToken();
    if(token){
      this.getAppliance(); 
      //this.deleteProducts();
    }
    
  }

  deleteProducts=async  ()=>{
    await this.setToken();
    var access_token = this.state.token;
    this.setState({loading:true});
    data = new FormData();
    data.append("customer_product_id",this.props.navigation.state.params.id)
    data.append("customer_id",this.props.navigation.state.params.customer_id)
      fetch(constants.base_url+'deleteProductOfCustomer?access_token='+access_token,{  
        method: "POST",
        headers: {'Content-Type': 'multipart/form-data'},
        body:data})
        .then(res => res.json())
        .then((result) => {
        if(result.status){
          this.props.navigation.goBack(null);
        }else{
          alert(result.message)
        }
      },
        (error) => { 
          NetInfo.fetch().then(async (state)=>{
            if(!state.isConnected){
              alert('Please check your internet connection.');
            }else{
              alert(error);
            }
          });
          this.setState({loading:false})}
      );
    
  }
 
 

  getAppliance = (id)=>{

    this.setState({loading:true});
    var url = constants.base_url+'productDetail';
    url=url+"?access_token="+this.state.token+'&product_id='+this.props.navigation.state.params.id+'&customer_id='+this.props.navigation.state.params.customer_id;
    
    fetch(url,{headers: {'Content-Type':'application/json'}})

        .then(response => response.json())
        .then((responseJson)=> {
          if(responseJson.status){
            this.setState({
              data: responseJson.data,
              services:responseJson.data.services?responseJson.data.services:[],
              coupons:responseJson.data.coupons?responseJson.data.coupons:[],
              warranties:responseJson.data.warranties?responseJson.data.warranties:[],
              related_accessories:responseJson.data.related_accessories.length?responseJson.data.related_accessories:[],
              loading:false
          })
          }else{
            alert(responseJson.message);
          }
          
        
        })
        .catch(error=>{alert(error);
            this.setState({loading:false})
          })
}

getCertificate = async (ewcID)=>{
  console.warn('called')
  this.setState({loading:true});
  await this.setToken();
  var url = constants.base_url+'getCertificate';
  url=url+"?access_token="+this.state.token+'&EWCId='+ewcID;
  fetch(url,{headers: {'Content-Type':'application/json'}})

      .then(response => response.json())
      .then((responseJson)=> {
        if(responseJson.status){
          this.setState({fileB64:responseJson.data, showFile:true, pdfType:'base64'});
        }else{
          alert('PDf could not be fetched.');
        }
      })
      .catch(error=>{alert(error);
          this.setState({loading:false})})
}

getService(ind){
    i=ind+1;
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st Service";
    }
    if (j == 2 && k != 12) {
        return i + "nd Service";
    }
    if (j == 3 && k != 13) {
        return i + "rd Service";
    }
    return i + "th Service";
}
  // componentDidUpdate() {
  //   if (this.props.user === null) {
  //     this.props.navigation.navigate('Auth');
  //   }
  //   return null;
  // }

  // eslint-disable-next-line class-methods-use-this
  // onPress(index, dataDict) {
  //  // Alert.alert(`on Press! = ${index} \n UserName = ${dataDict.title}`);
  //   this.props.navigation.navigate('AMCThankyou');
  // }
  openDetail(item) {
    this.props.navigation.navigate('Detail', {selectedItem: item});
  }

  render() {
    console.warn((this.state.data))
    return (
    <View style={{flex:1, backgroundColor:"#e6e8e9",justifyContent:'flex-start',}} >
      {this.state.loading ? <Loader /> : null}
      <StatusBar translucent={true} barStyle={"light-content"} backgroundColor={this.state.showFile?"black":"transparent"}  />
    
      <ScrollView contentContainerStyle={{justifyContent:'flex-start', backgroundColor:'#e6e8e9'}}>
       <View style={{marginTop:StatusBar.currentHeight,marginBottom:10 , flex:1,
           justifyContent:'flex-start', paddingBottom:15}} >
       <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
            angleCenter={{x:0,y:0}}  useAngle={true} angle={45} style={{
            backgroundColor:'purple', 
            alignSelf:'center',
            top:-570,
            height:800, 
            width:800,
            borderRadius:400,
            position:'absolute'
          }}
            >
          </LinearGradient>

          <View style={{flexDirection:'row', justifyContent:'flex-start',
              alignItems:'center', padding:5, paddingTop:Platform.OS=='ios'?30:0, marginTop:15}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack(null)} style={{zIndex:1}}>
                    <Image source={require('../../assets/ic_home/back.png')} style={{height:30,width:30,}}/>
                </TouchableOpacity>
                <Text style={{flex:1, color:'white',fontFamily:'RubikMedium', fontSize:20, textAlign:'center', marginLeft:-30}}> MY APPLIANCE</Text>
              </View>
         
         <View style={{flex:1, alignSelf:'center',justifyContent:'center', alignItems:'center',  height:100, width:100, marginTop:10 }}>
           <Image source={require('../../assets/ic_home/modal_back.png')} style={{position:'absolute', height:'100%', width:'100%',alignSelf:'center', flex:1}} />
          <Image source={{uri:this.state.data.icon}} style={{ resizeMode: 'center', justifyContent:"center", alignItems: "center",alignSelf:'center',height: 50, width: 50 ,marginRight:10}}/>
          
         </View>
         
       <Text
        style={{alignItems: "center", marginTop: Platform.OS=='ios'?5:10, paddingLeft: 10, paddingRight: 10,color:'white',fontFamily:'RubikMedium', fontSize:15,textAlign:'center'}}>
          {this.state.data.status!=3?this.state.data.amc_days?this.state.data.amc_days_name:'':null}{this.state.data.status!=3?this.state.data.amc_days==0? ' Expired':' Expires In '+this.state.data.amc_days+(this.state.data.amc_days!=1?' Days':' Day'):null}
          </Text>
          <View style={{flex:1,marginTop:80, paddingLeft:20,alignItems:"flex-start",alignSelf:"flex-start"}}>
            <Text style={{color:"#3875c9", fontSize:14,textAlign:"left",fontFamily:'RubikBold'}}>
            {this.state.data.product_name}</Text>
          </View>

          <View style={{flex:1,marginTop:2, paddingLeft:20,alignSelf:"flex-start",}}>
            <Text style={{color:"#423e3e", fontSize:16,fontFamily:'RubikBold',alignSelf:"flex-start",}}>
            {this.state.data.sub_product_name}</Text>
          </View>

          <View style={{flexDirection:"row",marginTop:5, paddingLeft:20,alignSelf:"flex-start",}}>
            <Text style={{color:"#4f4f4f", fontSize:13,fontFamily:'RubikRegular'}}>
            {'Brand:'}</Text>
            <Text style={{color:"#4f4f4f", fontSize:13,fontFamily:'RubikBold',marginLeft:3}}>
            {this.state.data.brand_name}</Text>
          </View>

          <View style={{flexDirection:"row",marginTop:10, justifyContent:"center",marginTop:25}}>

          <View style={{flexDirection:"column",flex:1,alignContent:"center",alignItems:"center",justifyContent:"center",backgroundColor:"#FFFFFF",padding:5,borderRadius:5,marginLeft:15,height:100}}>
          <View style={{backgroundColor: '#3875c9', height: 3, width: 15,margin:5}} />
        
          <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikRegular',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {'Model No.'}</Text>
            <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikBold',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {this.state.data.model_name}</Text>
          </View>

          <View style={{flexDirection:"column",flex:1,alignContent:"center",alignItems:"center",justifyContent:"center",backgroundColor:"#FFFFFF",padding:5,borderRadius:5,marginLeft:15,height:100}}>
          <View style={{backgroundColor: '#3875c9', height: 3, width: 15,margin:5}} />
          <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikRegular',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {'Serial No.'}</Text>
            <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikBold',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {this.state.data.serial_no}</Text>
          </View>

          <View style={{flexDirection:"column",flex:1,alignContent:"center",alignItems:"center",justifyContent:"center",backgroundColor:"#FFFFFF",padding:5,borderRadius:5,marginLeft:15,marginRight:15,height:100}}>
          <View style={{backgroundColor: '#3875c9', height: 3, width: 15,margin:5}} />
          <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikRegular',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {'Date of Purchase'}</Text>
            <Text style={{color:"#262020", fontSize:11,fontFamily:'RubikBold',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
            {this.state.data?authFun.getDate(this.state.data.purchase_date):''}</Text>
          </View>
          </View>
          {this.state.data.full_address?<View style={{backgroundColor:'white',flexDirection:'row',alignSelf:'stretch',borderRadius:5,margin:15,padding:10,justifyContent:'flex-start',}}>
            <Image style={{resizeMode:'stretch', height:15, width:15,}} source={require('../../assets/ic_home/address_icon.png')} /> 
            <View style={{marginLeft:5}}>
              <Text style={{color:"#262020", fontSize:12,fontFamily:'RubikRegular',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
                {this.state.data.address_type  +" ADDRESS"}</Text>
                <Text style={{color:"#262020", fontSize:14,fontFamily:'RubikBold',justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",}}>
                
                {this.state.data.full_address.split(',').map((item)=>item+'\n')}</Text>
            </View>
          </View>:null}

          {this.state.data.product_location?<View style={{
                    flex:1, 
                    alignSelf:'stretch',
                    marginHorizontal:15,
                    height:80, 
                    backgroundColor:'transparent',
                    borderRadius:5,
                    margin:5,
                    marginTop:10,
                    justifyContent:'center',alignItems:'center'
                    }}>
                      <Image source={require('../../assets/ic_home/location_background.png')} 
                      style={{width:'100%', height:'100%', resizeMode:'stretch', position:'absolute', top:0, left:0}} />
                    <View style={{  flexDirection:'row', justifyContent:'center', alignItems:'center',flex:1,}}>
                        {
                          this.state.data.product_location?
                        <Image source={this.state.data.product_location.toLowerCase()=='kitchen'?
                        require('../../assets/ic_home/kitchen.png'):this.state.data.product_location.toLowerCase()=='bedroom'?
                        require('../../assets/ic_home/bedroom.png'):this.state.data.product_location.toLowerCase()=='bathroom'?
                        require('../../assets/ic_home/bathroom.png'):this.state.data.product_location.toLowerCase()=='studyroom'?
                        require('../../assets/ic_home/studyroom.png'):require('../../assets/ic_home/waiting_room.png')} style={{height:50, width:50, resizeMode:'contain'}}/>
                        :null}
                        <View style={{height:'100%', marginLeft:15, alignSelf:'center',  justifyContent:'center'}}>
                          <Text style={{color:'#262020', fontFamily:'RubikMedium', fontSize:12,}}>{"Product Installed in"}</Text>
                          <Text style={{color:'black', fontFamily:'RubikBold', fontSize:17,textAlign:'left'}}>{this.state.data.product_location}</Text>
                        
                        </View>
                    </View> 
                  </View>:null}

                    
       <View style={{flexDirection:"column",marginBottom:20, }}>

      
    
        <Card containerStyle={{marginHorizontal:0,padding:0, backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseService?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>SERVICE INFORMATION</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseService:!this.state.collapseService})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
            <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>SERVICE INFORMATION</Text>
            <TouchableOpacity onPress={()=>this.setState({collapseService:!this.state.collapseService})}>
              <Image 
                source={require('../../assets/ic_home/collapse.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}/>
            </TouchableOpacity>

          </View>
          {
            this.state.data.services && this.state.data.services.length ? this.state.services.map((val, index)=>{
              console.warn(val);
              if(!(index+1 == this.state.data.services.length))
              return (
                <View style={{ marginLeft:15,borderLeftColor:'#3875c9',borderLeftWidth:2, }}>
                <View style={{position:'absolute', alignSelf:'flex-start', backgroundColor:'#3875c9', top:0,borderRadius:10, left:val.status==1?-10:-6}}>
                  {val.status == 1?<Image 
                  source={require('../../assets/ic_home/tick.png')}
                  style={{height: 20, width: 20, resizeMode:'cover'}}/>:<View style={{height:10, width:10, borderRadius:5}}></View>}
                </View>
              
              <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:14,alignSelf:"flex-start", marginLeft:20,}}>
              {this.getService(index)}
              </Text>
                <Text style={{color: '#4f4f4f', fontFamily:'RubikBold',fontSize:14,justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",marginLeft:20}}>
                {authFun.getDate(val.JobCreationDate)}</Text>
                <Text style={{color: '#262020',fontFamily:'RubikRegular',fontSize:14,textAlign:"left",marginLeft:20,paddingBottom:20}}>
                {val.JobClassification}</Text>
              </View>
              )
              else
              return (
                <View style={{ marginLeft:15,}}>
                <View style={{position:'absolute', alignSelf:'flex-start', backgroundColor:'#3875c9', top:0,borderRadius:10, left:val.status==1?-10:-6}}>
                  {val.status == 1?<Image 
                  source={require('../../assets/ic_home/tick.png')}
                  style={{height: 20, width: 20, resizeMode:'cover'}}/>:<View style={{height:10, width:10, borderRadius:5}}></View>}
                </View>
              
              <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:14,alignSelf:"flex-start", marginLeft:20,}}>
              {this.getService(index)}
              </Text>
                <Text style={{color: '#4f4f4f', fontFamily:'RubikBold',fontSize:14,justifyContent:"flex-start",alignContent:"flex-start",alignItems:"flex-start",textAlign:"left",marginLeft:20}}>
                {authFun.getDate(val.JobCreationDate)}</Text>
                <Text style={{color: '#262020',fontFamily:'RubikRegular',fontSize:14,textAlign:"left",marginLeft:20, }}>
                {val.JobClassification}</Text>
              </View>
              )
            }):null

          }
          
          
          <TouchableOpacity  onPress={()=>{this.props.navigation.navigate('ServiceDetail',{productDetails:this.props.navigation.state.params.applianceData})}} style={{alignSelf:"flex-start",flexDirection:"row",justifyContent:"center",alignItems: "center",overflow:'hidden',
          borderWidth:1,height:40,padding:5,borderRadius:6,borderColor:"#abaaaa",backgroundColor:"#FFFFFF",marginLeft:15,marginRight:10,marginTop:10, marginBottom:10,}} >
            <Text
              style={{alignItems: "center", paddingLeft: 10,color:"#6a6666", paddingRight: 10,fontSize:15,fontFamily:'RubikMedium'}}>
               Request Service
            </Text>
          </TouchableOpacity>

          </View>
          }
        </Card>

      
      {/* Dealer information */}
    {this.state.data.dealer_name || this.state.data.dealer_address || this.state.data.invoice_number?
      <Card containerStyle={{marginHorizontal:0,padding:0, backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseDealer?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>{'Dealer information'.toUpperCase()}</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseDealer:!this.state.collapseDealer})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
            <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
              <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>DEALER INFORMATION</Text>
              <TouchableOpacity onPress={()=>this.setState({collapseDealer:!this.state.collapseDealer})}>
                <Image 
                  source={require('../../assets/ic_home/collapse.png')}
                  style={{height: 20, width: 20, resizeMode:"contain" }}/>
              </TouchableOpacity>

            </View>
            {this.state.data.dealer_name?<View style={{backgroundColor:'dodgeblue', flexDirection:'row',padding:10 }}>
              <Image source={require('../../assets/ic_home/dealer.png')} style={{ width:15, resizeMode:'contain'}} />
              <View>
                <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:10}}>DEALER NAME</Text>
                <Text style={{fontFamily:'RubikBold', fontSize:12, marginLeft:10}}>{this.state.data.dealer_name}</Text>
              </View>
            </View>:null}
            {this.state.data.dealer_address?<View style={{backgroundColor:'dodgeblue', flexDirection:'row',padding:10 }}>
              <Image source={require('../../assets/ic_home/dealer_address.png')} style={{ width:15, resizeMode:'contain'}} />
              <View>
                <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:10}}>DEALER ADDRESS</Text>
                <Text style={{fontFamily:'RubikBold', fontSize:12, marginLeft:10}}>{this.state.data.dealer_address}</Text>
              </View>            
            </View>:null } 
            {this.state.data.invoice_number?<View style={{backgroundColor:'dodgeblue', flexDirection:'row',padding:10 }}>
              <Image source={require('../../assets/ic_home/invoice.png')} style={{ width:15, resizeMode:'contain'}} />
              <View>
                <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:10}}>INVOICE NO.</Text>
                <Text style={{fontFamily:'RubikBold', fontSize:12, marginLeft:10}}>{this.state.data.invoice_number}</Text>
              </View>
            </View>:null}
          </View>
          
          }
        </Card>

          :null}
      {/* Warranty information */}

        <Card containerStyle={{padding: 0,marginHorizontal:0,marginTop:10,backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseWarranty?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>{'Warranty information'.toUpperCase()}</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseWarranty:!this.state.collapseWarranty})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
          <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>WARRANTY INFORMATION</Text>
              <TouchableOpacity  onPress={()=>this.setState({collapseWarranty:!this.state.collapseWarranty})}>
                <Image 
                  source={require('../../assets/ic_home/collapse.png')}
                  style={{height: 20, width: 20, resizeMode:"contain", }}/>
              </TouchableOpacity>

            </View>

            <View style={{flexDirection:'row', marginBottom:15, justifyContent:'center', paddingRight:10}}>
            {this.state.warranties.length?
            <FlatList
              data={this.state.warranties} 
              numColumns={2}
              columnWrapperStyle={{marginHorizontal:5, marginVertical:7.5}}
              renderItem={({item, index}) => {
                  return (
                    <View  style={{flex:1, height:120,marginHorizontal:5, backgroundColor:'rgba(103, 200, 195, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                      <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>{item.TypeOfWarranty=='In-Warranty'?'STANDARD':item.TypeOfWarranty}</Text>
                      {item.WarrantyStartDate?<Text style={{ justifyContent:'center', fontFamily:'RubikBold', fontSize:12, color:'black'}}>
                         From: {authFun.getDate(item.WarrantyStartDate)}
                        </Text>:null}
                        
                        {item.WarrantyExpiryDate?<Text style={{ justifyContent:'center', fontFamily:'RubikBold', fontSize:12, color:'black'}}>
                         To: {authFun.getDate(item.WarrantyExpiryDate)}
                        </Text>:null}
                      {
                    item.TypeOfWarranty=='EWC'?
                    <TouchableOpacity onPress={
                      ()=>{
                        this.getCertificate(item.EWCId)
                      }
                    } style={{flexDirection:'row', justifyContent:'center',alignSelf:'center', marginTop:5, alignItems:'center'}}>
                  
                        <Text style={{fontFamily:'RubikMedium', fontSize:12, textDecorationLine:'underline'}}>VIEW PDF</Text>
                        <Image source={require('../../assets/ic_home/detail_pdf.png')} style={{height:20,width:20,marginLeft:5, resizeMode:'contain'}} /> 
                   </TouchableOpacity>:null}
                    </View>
                  )
                }
              }
              keyExtractor={(item, index)=>index+''}
              />
          :null
        }
              {/* {
               this.state.warranties.map((warranties, ind)=>
               <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(231, 119, 157, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
               <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>{warranties.id}</Text>
               <TouchableOpacity style={{flexDirection:'row', justifyContent:'center',alignSelf:'center', marginTop:5, alignItems:'center'}}>
                 <Text style={{fontFamily:'RubikMedium', fontSize:12, textDecorationLine:'underline'}}>VIEW PDF</Text>
                 <Image source={require('../../assets/ic_home/detail_pdf.png')} style={{height:20,width:20,marginLeft:5, resizeMode:'contain'}} />
               </TouchableOpacity>
             </View>)} */}
            
            
              {/* <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(103, 200, 195, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>PROMOTION</Text>
                  <TouchableOpacity style={{flexDirection:'row', justifyContent:'center', marginTop:5, alignItems:'center'}}>
                    <Text style={{fontFamily:'RubikMedium', fontSize:12, textDecorationLine:'underline'}}>VIEW PDF</Text>
                    <Image source={require('../../assets/ic_home/detail_pdf.png')} style={{height:20,width:20,marginLeft:5, resizeMode:'contain'}} />
                  </TouchableOpacity>
              </View> */}
            </View>

            {/* <View style={{flexDirection:'row', marginBottom:15, justifyContent:'center', paddingRight:10}}>
              <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(242, 201, 84, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>EXTENDED</Text>
                <TouchableOpacity style={{flexDirection:'row', justifyContent:'center', marginTop:5, alignItems:'center'}}>
                  <Text style={{fontFamily:'RubikMedium', fontSize:12, textDecorationLine:'underline'}}>VIEW PDF</Text>
                  <Image source={require('../../assets/ic_home/detail_pdf.png')} style={{height:20,width:20,marginLeft:5, resizeMode:'contain'}} />
                </TouchableOpacity>
              </View>
              <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(107, 236, 165, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>CERTIFICATE</Text>
                  <TouchableOpacity style={{flexDirection:'row', justifyContent:'center', marginTop:5, alignItems:'center'}}>
                    <Text style={{fontFamily:'RubikMedium', fontSize:12, textDecorationLine:'underline'}}>VIEW PDF</Text>
                    <Image source={require('../../assets/ic_home/detail_pdf.png')} style={{height:20,width:20,marginLeft:5, resizeMode:'contain'}} />
                  </TouchableOpacity>
              </View>
            </View> */}

            {/* <View style={{flexDirection:'row', marginBottom:15, justifyContent:'center', paddingRight:10}}>
              <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(70, 214, 250, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>WARRANTY TYPE</Text>
                <View style={{flexDirection:'row', justifyContent:'center', marginTop:5, alignItems:'center'}}>
                  <Text style={{fontFamily:'RubikBold', fontSize:15, }}>{this.state.data.warranties[0].TypeOfWarranty}</Text>
                </View>
              </View>
              <View style={{flex:1, height:100,marginHorizontal:5, backgroundColor:'rgba(255, 109, 160, 0.5)', borderRadius:5, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontFamily:'RubikBold', fontSize:15, color:'black', textAlign:'center'}}>WARRANTY START{'\nDATE'}</Text>
                  <TouchableOpacity style={{flexDirection:'row', justifyContent:'center', marginTop:5, alignItems:'center'}}>
                    <Text style={{fontFamily:'RubikBold', fontSize:15,}}>{authFun.getDate(this.state.data.warranties[0].WarrantyStartDate)}</Text>
                  </TouchableOpacity>
              </View>
            </View> */}
            {this.state.data.warranties && this.state.data.warranties.length?<Text style={{flexDirection:'row', marginBottom:15, justifyContent:'center', paddingHorizontal:10, fontFamily:'RubikBold', fontSize:15, color:'#ea1f1f'}}>
              Expire Date: {authFun.getDate(this.state.data.warranties[0].WarrantyExpiryDate)}
            </Text>:null}
      
          </View>
          }
        </Card>


        {/* Document Information */}

        <Card containerStyle={{padding: 0,marginHorizontal:0,marginTop:10,backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseDocument?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>{'Document information'.toUpperCase()}</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseDocument:!this.state.collapseDocument})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
          <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>DOCUMENT INFORMATION</Text>
              <TouchableOpacity onPress={()=>this.setState({collapseDocument:!this.state.collapseDocument})}>
                <Image 
                  source={require('../../assets/ic_home/collapse.png')}
                  style={{height: 20, width: 20, resizeMode:"contain" }}/>
              </TouchableOpacity>
            </View>

            {this.state.data.invoice_copy || this.state.data.product_sticker || this.state.data.warranty_card || this.state.data.other_document?
              <View>
                {this.state.data.invoice_copy?<View>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'black', paddingHorizontal:10, }}>Invoice Document</Text>
                  
                  <TouchableOpacity onPress={()=>{this.setState({filePath:this.state.data.invoice_copy, fileType:this.state.data.invoice_copy.indexOf('.pdf')!=-1?'pdf':'image', showFile:true})}}  style={{flexDirection:'row', height:110, marginLeft:10, marginRight:15, justifyContent:'flex-start', alignItems:'center'}}>
                    {
                      this.state.data.invoice_copy.indexOf('.pdf')!=-1?
                      <Image source={require('../../assets/ic_home/pdf.png')} style={{height:90, width:90,  resizeMode:'contain', marginRight:10, alignSelf:'center'}} />:
                      <Image source={this.state.data.invoice_copy?{uri:this.state.data.invoice_copy}:require('../../assets/ic_home/extende_warranty.png')} style={{height:90, width:90, borderRadius:5, resizeMode:'center', marginRight:10, alignSelf:'center'}} />
                    }
                  </TouchableOpacity>
                </View>:null}

              
                {this.state.data.product_sticker?
                  <View>
                    <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'black', paddingHorizontal:10, }}>Product Sticker Image</Text>
                    <TouchableOpacity  onPress={()=>{this.setState({filePath:this.state.data.product_sticker, fileType:this.state.data.product_sticker.indexOf('.pdf')!=-1?'pdf':'image', showFile:true})}}   style={{flexDirection:'row', height:110, marginLeft:10, marginRight:15, justifyContent:'flex-start', alignItems:'center', }}>
                      {
                        this.state.data.product_sticker.indexOf('.pdf')!=-1?
                        <Image source={require('../../assets/ic_home/pdf.png')} style={{height:90, width:90, resizeMode:'contain', marginRight:10, alignSelf:'center'}} />:
                        <Image source={this.state.data.product_sticker?{uri:this.state.data.product_sticker}:require('../../assets/ic_home/extende_warranty.png')} style={{height:90, width:90, borderRadius:5, resizeMode:'contain', marginRight:10, alignSelf:'center'}} />
                      }
                    </TouchableOpacity> 
                  </View>:null}

                {this.state.data.warranty_card?
                  <View>
                    <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'black', paddingHorizontal:10, }}>Warranty Card</Text>
                    
                    <View  style={{flexDirection:'row', height:110, marginLeft:10, marginRight:15, justifyContent:'flex-start', alignItems:'center'}}>
                      {
                        this.state.data.warranty_card.indexOf('.pdf')!=-1?
                        <Image source={require('../../assets/ic_home/pdf.png')} style={{height:90, width:90,  resizeMode:'contain', marginRight:10, alignSelf:'center'}} />:
                        <Image source={this.state.data.warranty_card?{uri:this.state.data.warranty_card}:require('../../assets/ic_home/extende_warranty.png')} style={{height:90, width:90, borderRadius:5, resizeMode:'contain', marginRight:10, alignSelf:'center'}} />
                      }
                    </View>
                  </View>
                  :null}

                {this.state.data.other_document?
                <View>
                  <Text style={{fontFamily:'RubikMedium', fontSize:15, color:'black', paddingHorizontal:10, }}>Other Files</Text>
                  <View  style={{flexDirection:'row', height:110, marginLeft:10, marginRight:15, justifyContent:'flex-start', alignItems:'center'}}>
                    {
                      this.state.data.other_document.indexOf('.pdf')!=-1?
                      <Image source={require('../../assets/ic_home/pdf.png')} style={{height:90, width:90, resizeMode:'contain', marginRight:10, alignSelf:'center'}} />:
                      <Image source={this.state.data.other_document?{uri:this.state.data.other_document}:require('../../assets/ic_home/extende_warranty.png')} style={{height:90, width:90, borderRadius:5, resizeMode:'contain', marginRight:10, alignSelf:'center'}} />
                    }
                  </View>
                </View>
                  :null}
              </View>:<Text style={{flex:1, alignSelf:'stretch',fontFamily:'RubikRegular',color:'gray', fontSize:15,textAlign:'center'}}>No Documents Were Found</Text>
            }
            
          </View>
        }
        </Card>

        {/* Accessories Data */}

        <Card containerStyle={{padding: 0,marginHorizontal:0,marginTop:10,backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseAccess?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>{'Related Accessories'.toUpperCase()}</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseAccess:!this.state.collapseAccess})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
            <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>RELATED ACCESSORIES</Text>
            <TouchableOpacity onPress={()=>this.setState({collapseAccess:!this.state.collapseAccess})}>
              <Image 
                source={require('../../assets/ic_home/collapse.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}/>
            </TouchableOpacity>

          </View>
          <View style={{flex:1, marginLeft:10, marginRight:15, justifyContent:'space-evenly', alignItems:'stretch'}}>
          {
            this.state.related_accessories.length ? 
            <FlatList
                    data = {this.state.related_accessories}
                    // extraData={this.state}
                    
                    numColumns={3}
                    renderItem = {({item}, index) =>
                    <Card
                    title={null}
                    height={120}
                    width={90}
                    containerStyle={{ padding: 0, margin: 0, marginBottom: 10, marginRight: 5, alignItems: 'center', alignContent: 'center', alignSelf: 'center', borderRadius:5, marginRight:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:6 }}>
                      <TouchableOpacity onPress={()=>this.openDetail(item)} style={{flex:1,justifyContent:'center'}}>
                        {item.images[0]?<Image source={{ uri: item.images[0] }} style={{ height: 80, width: 80, resizeMode:"center" }} />:null}
                        {/* <Text numberOfLines={1} style={{ paddingLeft: 10, marginTop: 10, color: '#000000', fontFamily: 'RubikMedium' }}>{rowData.accessories_name}</Text> */}
                      </TouchableOpacity >
                    </Card>
                      
                  }
                />:null
                 }
                 <TouchableOpacity style={{marginVertical:15, alignSelf:'center'}} onPress={()=>{NavigationService.navigate('AccessoriesNavigation')}}>
                   <Text style={{fontFamily:'RubikMedium', fontSize:15, textDecorationLine:'underline'}}>VIEW ALL</Text>
                 </TouchableOpacity>
          </View>
          

          </View>
          }
        </Card>

        {/* Offer View */}

        <Card containerStyle={{padding: 0,marginHorizontal:0,marginTop:10,backgroundColor:'transparent', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:0}}>
          {
            this.state.collapseOffer?
            <View style={{flexDirection:"row", padding: 10,backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginHorizontal:15, borderRadius:5}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>{'Offers'.toUpperCase()}</Text>
            <TouchableOpacity  onPress={()=>this.setState({collapseOffer:!this.state.collapseOffer})}>
              <Image 
                source={require('../../assets/ic_home/expand.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}>
              </Image>
            </TouchableOpacity>
          </View>:
          <View style={{paddingHorizontal:10, backgroundColor:'white'}}>
            <View style={{backgroundColor: 'white',width:"100%",flexDirection:"row", padding: 10,}}>
            <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',fontSize:15,width:"90%"}}>Offers</Text>
            <TouchableOpacity onPress={()=>this.setState({collapseOffer:!this.state.collapseOffer})}>
              <Image 
                source={require('../../assets/ic_home/collapse.png')}
                style={{height: 20, width: 20, resizeMode:"contain" }}/>
            </TouchableOpacity>

          </View>
          {
            this.state.data.coupons && this.state.data.coupons.length ? this.state.coupons.map((val)=>{
              return (
              <View style={{flex:0.5, alignSelf:'center', borderRadius:10,borderTopLeftRadius:30,borderBottomLeftRadius:30, margin:10, shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:2, padding:5,backgroundColor:'#ff9fa6', flexDirection:'row', alignItems:'center'}}>
                <View style={{height:15, width:15, borderRadius:7.5, backgroundColor:'white', shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:3,marginRight:10, marginLeft:5}}></View>
                <Image source={require('../../assets/ic_home/ic_home_cat_offers.png')} style={{height:30, width:30,resizeMode:'contain'}}/>
                <View>
                  <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',margin:5,fontSize:14,alignSelf:"flex-start", marginLeft:10,}}>{val.coupon_type}</Text>
                  <Text style={{color: '#4f4f4f',fontFamily:'RubikMedium',margin:5,fontSize:14,alignSelf:"flex-start", marginLeft:10,}}>{val.coupon_code}</Text>
                </View>
               
              </View>
              )
            }):null
          }
        
          </View>
          }
        </Card>
            </View>
            
            <TouchableOpacity onPress={()=>this.setState({confirmDelete:true})  }  style={{ flex:1, alignSelf:'center', borderRadius:25,height:50, width:50}}>
            <LinearGradient style={{  borderRadius: 25, flexDirection:'row', alignItems: 'center', justifyContent:'center' , height:50, width:50,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5 }}
                        colors={["#ff619e", "#ff9fa6"]}>
                      <Image source={require('../../assets/ic_home/line_trash.png')} style={{height:25, width:25, resizeMode:'center'}}/>
              </LinearGradient>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.setState({confirmDelete:true}) } style={{padding:5}}>
            <Text style={{color: 'black',fontSize: 12,flexWrap:'wrap',fontFamily: "RubikMedium",textAlign:'center',alignSelf:'center',}}>
                    DELETE PRODUCT
            </Text>
          </TouchableOpacity>
         
            </View>
              
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.confirmDelete}
        onRequestClose={() => {this.setState({choice:''})}}>
        <TouchableOpacity onPress = {()=> this.setState({confirmDelete:false})} style={{flex:1, backgroundColor:'rgba(0,0,0,0.5)', justifyContent:'flex-end', alignItems:'stretch'}}>
            <View style={{ height:100, flexDirection:'row', backgroundColor:'white', justifyContent:'space-around', alignItems:'center', }} >
                <TouchableOpacity onPress = {() => this.setState({confirmDelete:false},()=>this.deleteProducts())} style={{ margin:10, justifyContent:'center', alignItems:'center'}}>
                    <LinearGradient style={{  borderRadius: 25, flexDirection:'row', alignItems: 'center', justifyContent:'center' , height:50, width:50,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5 }}
                      colors={["#ff619e", "#ff9fa6"]}>
                      <Image source={require('../../assets/ic_home/line_trash.png')} style={{height:25, width:25, resizeMode:'center'}}/>
                    </LinearGradient>
                    <Text>Yes</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress = {() => this.setState({confirmDelete:false})} style={{ margin:10, justifyContent:'center', alignItems:'center'}}>
                    <LinearGradient style={{  borderRadius: 25, flexDirection:'row', alignItems: 'center', justifyContent:'center' , height:50, width:50,shadowOffset: {
                    width: 2,
                    height: 2,
                  },
                  shadowOpacity: 0.23,
                  shadowRadius: 4.0,elevation:5 }}
                      colors={["#ff619e", "#ff9fa6"]}>
                      <Image source={require('../../assets/ic_home/cross_line.png')} style={{height:'100%', width:'100%', resizeMode:'cover'}}/>
                    </LinearGradient>
                    <Text>No</Text>
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
          
      </Modal>
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.showFile}
        onShow={()=>this.setState({loading:false})}
        onRequestClose={() => {this.setState({showFile:false, filePath:'', fileType:'', pdfType:'url', fileB64:''})}}>
        {
          this.state.fileType=='image'?
          <View style={{flex:1}}>
              {/* <TouchableOpacity onPress={()=>this.setState({showFile:false})} style={{height:30, width:30, position:'absolute', top:50, left:15, zIndex:20}}><Image source={require('../../assets/ic_home/back.png')} style={{height:'100%', width:'100%',resizeMode:'cover'}}/></TouchableOpacity> */}
              <ImageViewer imageUrls={[{url: this.state.filePath, props:{}}]} backgroundColor="black" />
              <TouchableOpacity activeOpacity={1} onPress={()=>this.setState({showFile:false, filePath:'', fileType:'', pdfType:'url'})} style={{width:'100%', alignSelf:'stretch', height:50, bottom:0,  zIndex:20, backgroundColor:'white'}}>
                <Text style={{flex:1,fontFamily:'RubikMedium', fontSize:20, alignSelf:'center', textAlignVertical:'center' }}>CLOSE</Text>  
              </TouchableOpacity>
          </View>
            

          :
          <View style={{flex:1}}>
           <PDFView
              // fadeInDuration={250.0}
              style={{ flex: 1 }}
              resource={this.state.pdfType=='base64'?this.state.fileB64:this.state.filePath}
              resourceType={this.state.pdfType}
              onLoad={() => console.warn(`PDF rendered from ${this.state.pdfType}`)}
              onError={() => console.warn('Cannot render PDF', error)}
            />
            <TouchableOpacity activeOpacity={1} onPress={()=>this.setState({showFile:false,  filePath:'', fileType:'', pdfType:'url', fileB64:''})} style={{width:'100%', alignSelf:'stretch', height:50, bottom:0,  zIndex:20, backgroundColor:'black'}}>
            <Text style={{flex:1,fontFamily:'RubikMedium', fontSize:20, alignSelf:'center', textAlignVertical:'center', color:'white' }}>CLOSE</Text>  
          </TouchableOpacity>
         </View>
        }
          
      </Modal>
    </View>
  );
  }
}

ApplianceDetail.propTypes = {
  user: PropTypes.object,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.object.isRequired,
};

ApplianceDetail.defaultProps = {
  user: null,
};

const mapStateToProps = state => ({
  user: getUser('state'),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplianceDetail);
