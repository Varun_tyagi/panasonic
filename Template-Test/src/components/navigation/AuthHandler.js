/* eslint-disable react/jsx-no-undef */
/* eslint-disable no-unused-vars */
import React, { Component, createRef } from 'react';
import {
  View,
  StyleSheet,
  AsyncStorage
} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ScalingDrawer from 'react-native-scaling-drawer';
import getUser from '../../selectors/UserSelectors';
import Colors from '../../helpers/Colors';
import ProductList from '../ProductList';

// import SideMenu from '../SideMenu';
//import { AppStack } from '../navigation/AppNavigator';

export const drawer = createRef();

const defaultScalingDrawerConfig = {
  scalingFactor: 0.6,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
});

class AuthHandler extends Component {
  constructor(props) {
    super(props);
    this.navigateWithAuth();
  }

  navigateWithAuth = async () => {

    var loggedIn =await AsyncStorage.getItem('loggedIn');
    if(loggedIn){
      // alert('hello'+JSON.stringify(loggedIn))
      this.props.navigation.navigate('App');
    }else{
      this.props.navigation.navigate('Auth');
    }
    // if (this.props.user !== null) {
    //   this.props.navigation.navigate('App');
    // } else {
    //   this.props.navigation.navigate('Auth');
    // }
  }

  render() {
    return (
     <View style={styles.container} />

      // <ScalingDrawer
      //   ref={drawer}
      //   content={<ProductList drawer={drawer} />}
      //   {...defaultScalingDrawerConfig}
      //   onClose={() => console.log('close')}
      //   onOpen={() => console.log('open')}
      // >
      //   <AppStack
      //     ref={(navigatorRef) => {
      //  // NavigationService.setTopLevelNavigator(navigatorRef);
      //     // <View style={styles.container} />;
      // }}
      //   />
      // </ScalingDrawer>
    );
  }
}

// AuthHandler.propTypes = {
//   user: PropTypes.object,
//   navigation: PropTypes.object.isRequired,
// };

// AuthHandler.defaultProps = {
// };

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(AuthHandler);
