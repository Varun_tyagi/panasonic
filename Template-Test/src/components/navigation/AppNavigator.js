/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React,{ createRef} from 'react';
import { Image } from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import {
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  createAppContainer,
} from 'react-navigation';
import homeIcon from '../../assets/ic_settings/ic_home.png';
import profileIcon from '../../assets/ic_settings/ic_profile.png';

import applianceIcon from '../../assets/ic_settings/ic_appliance.png';

import settingsIcon from '../../assets/ic_settings/ic_settings.png';
import myAmcIcon from '../../assets/ic_settings/archive.png';
import myOrderIcon from '../../assets/ic_settings/product.png';
import Colors from '../../helpers/Colors';
import Profile from '../Profile';
import Home from '../Home';
import ProductList from '../ProductList';
import MyAppliance from '../MyAppliance';
import MyAmc from '../MyAmc';
import MyOrders from '../MyOrders';
import AmcList from '../AmcList';
import TNC from '../TNC';
import ApplianceDetail from '../ApplianceDetail';
import AMCThankyou from '../AMCThankyou';
import AmcExtended from '../AmcExtended'
import AccessoriesData from '../AccessoriesData';
import NavigationService from './NavigationService';
import ScalingDrawer from 'react-native-scaling-drawer';
import LeftMenu from './MainDrawer';
import ServiceDetail from '../ServiceDetail';
import ServiceRequest from '../ServiceRequest';
import ServiceTrack from '../ServiceTrack';
import ServiceTrackDetail from '../ServiceTrackDetail';
// NEW CODE SCREENS IMPORT

// import Home from '/src/containers/Home';
import AccessoriesHome from '../Transaction/containers/Home';
import Detail from '../Transaction/containers/Detail';
import MyCart from '../Transaction/containers/MyCart';
import ReviewOrder from '../Transaction/containers/ReviewOrder';
import AmcPlans from '../Transaction/containers/AmcPlans';
import AllEwcPlans from '../Transaction/containers/AllEwcPlans';
import AMCCart from '../Transaction/containers/AMCCart';
import MyAddress from '../Transaction/containers/My Address';
import UserQuery from '../Transaction/containers/UserQuery';
import ReferalCode from '../Transaction/containers/ReferalCode';
import NewAddress from '../Transaction/containers/NewAddress';
import AddAppliance from '../Home/addAppliance';
// END NEW CODE SCREENS IMPORT

const iconForTab = ({ state }) => {
  switch (state.routeName) {
    case 'Home':
      return homeIcon;
    case 'Profile':
      return profileIcon;
    case 'My Appliance':
      return applianceIcon;
    case 'My Orders':
      return myOrderIcon;
    case 'My AMC':
      return myAmcIcon;
    default:
      return null;
  }
};

const TabIcon = ({ icon, tintColor }) => (
          <Image source={icon} style={{ tintColor,height:20,width:20, resizeMode:'contain'}} />
);

const ProfileStack = createStackNavigator({ Profile });
const HomeStack = createStackNavigator({
  Main:{screen: Home},
  ProductList: { screen: ProductList },
  AccessoriesData,
});

const ServiceRequestStack = createStackNavigator({
  ServiceRequest,
  ServiceDetail,
});
const ServiceTrackStack = createStackNavigator({
  ServiceTrack, 
  ServiceTrackDetail,
});
const MyApplianceStack = createStackNavigator({
  MyAppliance,
  ProductList: { screen: ProductList },
  AMCThankyou,
});
const MyAmcStack = createStackNavigator({
  MyAmc,
  AmcList,
  TNC,
  AmcExtended,
});
const AddApplianceStack = createStackNavigator({
  AddAppliance
})
const ApplianceDetailStack = createStackNavigator({
  ApplianceDetail
})
const MyOrdersStack = createStackNavigator({
  MyOrders,
});
const DetailStack = createStackNavigator({
  Detail
})
// NEW CODE START
const AccessoriesNavigationStack=createStackNavigator({

  AccessoriesHomeScreen: {
    screen: AccessoriesHome,
  },
  MyCart: {
    screen: MyCart,
  },
  MyAddress: {
    screen: MyAddress,
  },
  NewAddress: {
    screen: NewAddress,
  },
  ReviewOrder: {
    screen: ReviewOrder,
  },

},{
  headerMode:'none'
}
);
const AMCNavigation=createStackNavigator({

  AMCPlans: {
    screen: AmcPlans,
  },
  AllEwcPlans: {
    screen: AllEwcPlans,
  },
  AMCCart: {
    screen: AMCCart,
  },
  MyCart: {
    screen: MyCart,
  },
  UserQuery:{
    screen:UserQuery,
  },

},{
  headerMode:'none'
}
);
// END NEW CODE 

const AppStack = createBottomTabNavigator(
  {
    Home: HomeStack,
    Profile: ProfileStack,
    'My Appliance': {screen:MyApplianceStack},
    'My AMC': MyAmcStack,
    'My Orders': MyOrdersStack,
  },
  {
    tabBarPosition: 'bottom',
    tabBarOptions: {
      activeTintColor: Colors.primary,
      inactiveTintColor: "#929292",
      style: {
        backgroundColor: '#f7f7f8',
        paddingHorizontal:10
      },

    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => (// eslint-disable-line
        <TabIcon
          icon={iconForTab(navigation)}
          tintColor={tintColor}
        />
      ),
    }),
  },
);

const defaultScalingDrawerConfig = {
  scalingFactor: 0.7,
  minimizeFactor: 0.6,
  swipeOffset: 20,
};
const DrawerStack = createStackNavigator({
  Home:AppStack,
  'AccessoriesNavigation':AccessoriesNavigationStack,
  'AMCNavigation':AMCNavigation,
  ServiceRequestStack,
  ServiceTrackStack,
  AddApplianceStack,
  ApplianceDetailStack,
  DetailStack
},{
  headerMode:'none'
});

export const drawer = createRef();
const MainStack = createAppContainer(DrawerStack);
class CustomDrawerView extends React.Component {
  constructor(props) {
    super(props);
  }
 

  componentWillReceiveProps(nextProps) {
    /** Active Drawer Swipe **/
    if (nextProps.navigation.state.index === 0)
      this._drawer.blockSwipeAbleDrawer(false);

    if (
      nextProps.navigation.state.index === 0 &&
      this.props.navigation.state.index === 0
    ) {
      this._drawer.blockSwipeAbleDrawer(false);
      this._drawer.close();
    }

    /** Block Drawer Swipe **/
    if (nextProps.navigation.state.index > 0) {
      this._drawer.blockSwipeAbleDrawer(true);
    }
  }

  setDynamicDrawerValue = (type, value) => {
    defaultScalingDrawerConfig[type] = value;
    /** forceUpdate show drawer dynamic scaling example **/
    this.forceUpdate();
  };

  render() {
    // const { routes, index } = this.props.navigation.state;
    // const store = require('./src/redux/store').default;
    
    return (
      <ScalingDrawer
        ref={drawer}
        content={<LeftMenu drawer={drawer} navigation={this.props.navigation} />}
        {...defaultScalingDrawerConfig}
        onClose={() => console.log('close')}
        onOpen={() => console.log('open')}
      >
        <MainStack
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </ScalingDrawer>
    );
  }
}




export default CustomDrawerView;
