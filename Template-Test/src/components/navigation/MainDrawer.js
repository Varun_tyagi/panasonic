import React, { Component } from 'react';
import {View,StatusBar,Text,Platform, Image,TouchableOpacity, AsyncStorage} from 'react-native';
import NetInfo from '@react-native-community/netinfo';
import LinearGradient from 'react-native-linear-gradient';
import {drawer} from './AppNavigator';
import { Avatar } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import NavigationService from './NavigationService';
import axios from 'axios';
import * as authFun from '../../helpers/auth';

export default class DrawerContent extends Component {
  constructor(props) {
  super(props)
  this.state = {
        showActions: false,
        showMore: false,
        menu:{Home:false,Profile:false,'My Appliance':false,'My AMC':false,'My Orders':false, 'MyCart':false},
        name:'',
        profile_image:'',
        myApplianceCount:0,
        customerID:'',
        showCount:false,
        
   };
   this.menuActions = this.menuActions.bind(this);
   this.loadData = this.loadData.bind(this);
}
componentDidMount(){

    this.props.navigation.addListener('willFocus', async () => {     
       console.warn('focused')
      });

    this.loadData();
}
async getCartItemCount() {
    
    const accessToken =  await authFun.checkAuth();;
    const customer_id = this.state.customerID;
    axios.get("https://www.ecarewiz.com/ewarrantyapi/getCartCount?access_token=" + accessToken + "&customer_id=" + customer_id).then((response) => {
      global.cartItemsCount = response.data.data;
      this.setState({showCount:true});
    }).catch((error) => {
      NetInfo.fetch().then(async (state)=>{
      if(!state.isConnected){ 
        alert('Please check your internet connection.');
      }else{
        alert(error);
      }
    });
    });
  }
loadData = async () => {
    const userData = await AsyncStorage.getItem('userData');
    if (userData) {
      const data = JSON.parse(userData);
      global.profile_image = data.profile_image;
    //   alert('usere data : '+userData);
      this.setState({
        name: data.name ? data.name : '',
        profile_image:data.profile_image?data.profile_image:null,
        customerID: data.customer_id ? data.customer_id : '',
      });
      this.getCartItemCount();
    } 
  }

menuActions = (navigate2)=>
{   var params='';
    var temp = this.state.menu;
    temp = {Main:false,Profile:false,'My Appliance':false,'My AMC':false,'My Orders':false};
    if(navigate2 == 'Main')
    {   
        // params = (count)=>{alert('params called');this.setState({myApplianceCount:count});}
        temp['Main'] = true;
    }
    else if(navigate2 == 'Profile')
    temp['Profile'] = !temp['Profile']
    else if(navigate2 == 'My Appliance')
    temp['My Appliance'] = !temp['My Appliance']
    else if(navigate2 == 'My AMC')
    temp['My AMC'] = !temp['My AMC']
    else if(navigate2 == 'My Orders')
    temp['My Orders'] = !temp['My Orders']
    this.setState({menu:temp,showMore:false, showActions:false});
    this.props.drawer.current.close();
    NavigationService.navigate(navigate2);
    
}

  render(){
      
    return(
       <View style={{backgroundColor:'white', flex:1}}>
           <View style={{marginTop:StatusBar.currentHeight,height:120 }}>
            <LinearGradient colors={['#5960e5','#b785f8','#b785f8','#b785f8']} locations={[0.1,0.5,0.8,1]} 
                angleCenter={{x:0,y:0}}  
                useAngle={true} angle={45} style={{
                backgroundColor:'purple', 
                alignSelf:'center',
                top:-900,
                height:1000, 
                width:1000,
                borderRadius:500,
                position:'absolute'
            }}
                >
            </LinearGradient>
            <View style={{flexDirection:'row', justifyContent:'flex-start', alignItems:'center', padding:10,paddingTop:Platform.OS=='ios'?30:0}}>
                <TouchableOpacity 
                onPress={()=> drawer.current.close()}>
                    <Image source={require('../../assets/ic_home/align-left.png')} style={{height:25,width:25,}}/>
                    {/* <Text style={{color:'white', fontFamily:'RubikBold', fontSize:25}}>X</Text> */}
                </TouchableOpacity>
                <Avatar
                containerStyle={{ marginLeft:15 }}
                size={40}
                rounded
                activeOpacity={0.8}
                source={{uri:global.profile_image?global.profile_image:'https://img.icons8.com/clouds/100/000000/user.png'}}
                />
                <View style={{flex:1, alignItems:'flex-start', marginLeft:15}}>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, color:'white'}}>{this.state.name}</Text>
                    {/* <Text style={{fontFamily:'RubikRegular', fontSize:12, color:'white', fontStyle:'italic'}}>New Delhi</Text> */}
                </View>
                </View>
            </View>
            <ScrollView style={{flex:1, }}>
                <TouchableOpacity
                onPress={()=>this.menuActions('Main')}
                activeOpacity={1}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'red', width:'100%', height:40, alignItems:'center', }}>
                     <LinearGradient 
                        colors={[this.state.menu.Home?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        >  
                    <Image source={require('../../assets/sidemenu/home_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15}}>Home</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={()=>{this.menuActions('Profile')}}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}>
                   <LinearGradient 
                        colors={[this.state.menu.Profile?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        >  
                    <Image source={require('../../assets/sidemenu/profile_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15}}>My Profile</Text>
                    {/* <LinearGradient colors={['#ff619e','#ff9fa6','#ff9fa6','#ff9fa6']} locations={[0.1,0.5,0.8,1]} 
                        angleCenter={{x:0,y:0}}  
                        style={{height:20, width:30,borderRadius:10,marginLeft:15}}
                        >
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, textAlign:'center', color:'white'}}>1</Text>
                    </LinearGradient> */}
                    </LinearGradient>
                    
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={()=>{this.menuActions('My Appliance');}}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}>
                    <LinearGradient 
                        colors={[this.state.menu['My Appliance']?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        > 
                    <Image source={require('../../assets/sidemenu/app_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My Appliances</Text>
                    {global.myApplianceCount?<LinearGradient colors={['#ff619e','#ff9fa6','#ff9fa6','#ff9fa6']} locations={[0.1,0.5,0.8,1]} angleCenter={{x:0,y:0}}  style={{height:20, width:30,borderRadius:10,marginLeft:15}}>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, textAlign:'center', color:'white'}}>{global.myApplianceCount}</Text>
                    </LinearGradient>:null}
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity 
                // disabled={!global.cartItemsCount?true:false}
                onPress={()=>{this.menuActions('MyCart');}}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}>
                    <LinearGradient 
                        colors={[this.state.menu['MyCart']?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        > 
                    <Image source={require('../../assets/sidemenu/purchase_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My Cart</Text>
                   {/* <LinearGradient colors={['#ff619e','#ff9fa6','#ff9fa6','#ff9fa6']} locations={[0.1,0.5,0.8,1]} angleCenter={{x:0,y:0}}  style={{height:20, width:30,borderRadius:10,marginLeft:15}}>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, textAlign:'center', color:'white'}}>{global.cartItemsCount?global.cartItemsCount:0}</Text>
                    </LinearGradient> */}
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={()=>{this.menuActions('My AMC');}}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}>
                   <LinearGradient 
                        colors={[this.state.menu['My AMC']?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        > 
                    <Image source={require('../../assets/sidemenu/amc_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My AMC</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <TouchableOpacity 
                onPress={()=>{this.menuActions('My Orders');}}
                style={{flexDirection:'row',marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}>
                    <LinearGradient 
                        colors={[this.state.menu['My Orders']?'#DCDCDC':'#fff','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center', height:'100%'}}
                        > 
                    <Image source={require('../../assets/sidemenu/order_dark.png')} style={{resizeMode:'contain', height:25, width:25,marginLeft:15}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My Orders</Text>
                    </LinearGradient>
                </TouchableOpacity>
                {this.state.showActions?
                    // <View style={{backgroundColor:'#DCDCDC',width:'100%'}}>
                        <LinearGradient 
                        colors={['#DCDCDC','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',}}
                        >       
                            <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}
                            onPress={()=>this.setState({showActions:!this.state.showActions, showMore:false})}>
                                <Image source={require('../../assets/sidemenu/action_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                                <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My Actions</Text>
                            </TouchableOpacity>
                        <TouchableOpacity onPress={() => {NavigationService.navigate('ServiceRequest');this.props.drawer.current.close();}}  style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/service_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Request Service</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/track_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Track Service</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}
                        onPress={()=>{NavigationService.navigate('AMCNavigation');this.props.drawer.current.close();}}
                        >
                            <Image source={require('../../assets/sidemenu/purchase_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Purchase AMC</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent',
                         width:'100%', height:40, alignItems:'center', }}
                         onPress={()=>{NavigationService.navigate('AccessoriesNavigation');this.props.drawer.current.close();}}
                         >
                            <Image source={require('../../assets/sidemenu/acc_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Purchase Accessories</Text>
                        </TouchableOpacity>   
                        </LinearGradient>
                    // {/* </View> */}
                    :<TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}
                    onPress={()=>this.setState({showActions:!this.state.showActions, showMore:false})}>
                        <Image source={require('../../assets/sidemenu/action_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>My Actions</Text>
                    </TouchableOpacity>
                }
                {this.state.showMore?
                    // <View style={{backgroundColor:'#DCDCDC',width:'100%'}}>
                        <LinearGradient 
                        colors={['#DCDCDC','#fff','#fff',]} 
                        start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                        style={{ width:'100%',}}
                        >       
                            <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}
                            onPress={()=>this.setState({showMore:!this.state.showMore, showActions:false})}>
                                <Image source={require('../../assets/sidemenu/more_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                                <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>More</Text>
                            </TouchableOpacity>
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/quick_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Quick Links</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/web_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>eWarranty Website</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/offer_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Offers</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/feedback_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Feedback</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/faq_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>FAQ</Text>
                        </TouchableOpacity> 
                        <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:5,backgroundColor:'transparent', width:'100%', height:40, alignItems:'center', }}>
                            <Image source={require('../../assets/sidemenu/help_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                            <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Help</Text>
                        </TouchableOpacity>  
                        </LinearGradient>
                    // {/* </View> */}
                    :<TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'white', width:'100%', height:40, alignItems:'center', }}
                    onPress={()=>this.setState({showMore:!this.state.showMore, showActions:false})}>
                        <Image source={require('../../assets/sidemenu/more_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                        <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>More</Text>
                    </TouchableOpacity>
                }
                <TouchableOpacity style={{flexDirection:'row',marginLeft:15,marginBottom:10,backgroundColor:'white', 
                width:'100%', height:40, alignItems:'center', }}
                onPress={async ()=>{
                    await AsyncStorage.multiRemove(['loggedIn','userData'],async ()=>{
                        this.props.navigation.navigate('Auth');
                    });
                   
                }}>
                    <Image source={require('../../assets/sidemenu/logout_dark.png')} style={{resizeMode:'contain', height:25, width:25}}/>
                    <Text style={{fontFamily:'RubikRegular', fontSize:15, marginLeft:15, color:'#4f4f4f'}}>Logout</Text>
                </TouchableOpacity>
            </ScrollView>
          </View>
    )
  }
}


