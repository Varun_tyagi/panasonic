import {AsyncStorage} from 'react-native';


export default async (state) => {
    const userData = await AsyncStorage.getItem('userData');
    // alert('data from user : '+userData);
    return JSON.parse (userData);
};
