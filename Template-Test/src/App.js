import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet, Image, Dimensions, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import Navigation from './components/navigation';
import Colors from './helpers/Colors';
import {API_ROOT} from './env';
import { store, persist } from './reducers';
import {setConfiguration} from './components/Transaction/utils/configuration';
import * as constants from './helpers/constants';
import * as authFun from './helpers/auth';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
});

class App extends Component {
  state = {
    ready: false,
  };

  setToken=async ()=>{
    var token = await authFun.checkAuth();
    setConfiguration('API_ROOT', API_ROOT);
    persist(() => {
      this.setState({ ready: true });
    });
    // return token;
  }
  componentDidMount() {
    this.setToken();
  }

  renderEmpty = () => (
    <View style={styles.container}>
      <StatusBar hidden={true} />
      <Image source={require('./assets/splash/splash_background.png')} style={{height:'100%', width:'100%', resizeMode:'stretch',position:'absolute', top:0, }}/>
      <Image source={require('./assets/splash/splash_logo.png')} style={{height:100, width:'100%', marginHorizontal:'10%', resizeMode:'center', }}/>
      <ActivityIndicator />
    </View>
  );

  render() {
    const store = require('./components/Transaction/redux/store').default;

    const { ready } = this.state;
    if (!ready) return this.renderEmpty();
    return (
      <Provider store={store} >
        <Navigation />
      </Provider>
    );
  }
}

export default App;

