package com.rntemplate;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.rumax.reactnative.pdfviewer.PDFViewPackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import com.dylanvann.fastimage.FastImageViewPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import org.reactnative.camera.RNCameraPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import io.github.elyx0.reactnativedocumentpicker.DocumentPickerPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.RNFetchBlob.RNFetchBlobPackage;
import com.imagepicker.ImagePickerPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.razorpay.rn.RazorpayPackage;
// import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
// import com.oblador.vectoricons.VectorIconsPackage;
// import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
// import com.swmansion.gesturehandler.react.RN GestureHandlerPackage;
// import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.babisoft.ReactNativeLocalization.ReactNativeLocalizationPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return  false;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new PDFViewPackage(),
            new NetInfoPackage(),
            new FastImageViewPackage(),
            new AsyncStoragePackage(),
            new RNCameraPackage(),
            new RNCWebViewPackage(),
            new DocumentPickerPackage(),
            new LinearGradientPackage(),
            new RNFetchBlobPackage(),
            new ImagePickerPackage(),
           // new VectorIconsPackage(),
            new ReactNativeLocalizationPackage(),
            new RNGestureHandlerPackage(),
            new VectorIconsPackage(),
            new RazorpayPackage()
          //  new ReactNativeLocalizationPackage(),
           // new RNGestureHandlerPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
